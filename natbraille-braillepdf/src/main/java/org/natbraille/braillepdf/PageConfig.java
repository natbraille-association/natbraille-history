/*
 * NatBraille Music 
 * Copyright (C) 2014 Vivien Guillet, Bruno Mascret
 * Contact: vivien.guillet@univ-lyon1.fr
 * 
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */
package org.natbraille.braillepdf;

/**
 * Configuration Object for {@link PdfFormatter}
 * <p>
 * @author vivien
 */
public class PageConfig {

    public Float pageHeight = null;
    public Float pageWidth = null;
    public Float fontSize = 20f;
    public String fontFileName = null;
    public float marginLeft = 0;
    public float marginTop = 0;

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("format:").append(pageWidth).append("x").append(pageHeight)
                .append(" font:").append((fontFileName != null) ? (fontFileName) : ("LouisLouis"))
                .append(" margin:").append(marginLeft).append("/").append(marginTop);
        return sb.toString();
    }

}
