/*
 * NatBraille Music 
 * Copyright (C) 2014 Vivien Guillet, Bruno Mascret
 * Contact: vivien.guillet@univ-lyon1.fr
 * 
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */
package org.natbraille.braillepdf;

import java.io.IOException;
import org.apache.pdfbox.exceptions.COSVisitorException;
import org.apache.pdfbox.pdmodel.PDDocument;
import org.apache.pdfbox.pdmodel.PDPage;
import org.apache.pdfbox.pdmodel.edit.PDPageContentStream;
import org.apache.pdfbox.pdmodel.font.PDFont;
import org.apache.pdfbox.pdmodel.font.PDTrueTypeFont;

/**
 *
 */
/**
 * Produces a pdf file using a page configuraiton @{see PageConfig}.
 * <p>
 * input Braille format
 * <ul>
 * <li>\n for line separation</li>
 * <li>\f for page separation</li>
 * </ul>
 * <p>
 * @author vivien
 */
public class PdfFormatter {

    private final PageConfig pageConfig;

    public PdfFormatter(PageConfig pageConfig) {
        this.pageConfig = pageConfig;
    }

    /**
     * Writes a pdf file from given Braille CodeUS String.
     * @param brlString the brailleString in CodeUS Braille Table
     * @param pdfName
     * @throws IOException
     * @throws COSVisitorException
     */
    public void brailleToPdf(String brlString, String pdfName) throws IOException, COSVisitorException {
        
        //System.err.println(pageConfig.toString());
        PDDocument document = new PDDocument();
        if (pdfName != null) {
            document.getDocumentInformation().setTitle(pdfName);
        }
        PDFont font;
        if (pageConfig.fontFileName == null) {
            font = PDTrueTypeFont.loadTTF(document, getClass().getResourceAsStream("LouisLouis.ttf"));
            //font = PDTrueTypeFont.loadTTF(document, getClass().getResourceAsStream("DejaVuSans.ttf"));
        } else {
            font = PDTrueTypeFont.loadTTF(document, pageConfig.fontFileName);
        }
        /**
         * foreach page
         */
        for (String brlPage : brlString.split("\f")) {
            PDPage page = new PDPage();
            if (pageConfig.pageWidth != null) {
                page.getMediaBox().setUpperRightX(pageConfig.pageWidth);
            }

            if (pageConfig.pageHeight != null) {
                page.getMediaBox().setUpperRightY(pageConfig.pageHeight);
            }

            PDPageContentStream contentStream = new PDPageContentStream(document, page);
            contentStream.beginText();
            contentStream.setFont(font, pageConfig.fontSize);
            contentStream.moveTextPositionByAmount(pageConfig.marginLeft,
                    page.getMediaBox().getHeight()  - pageConfig.marginTop -pageConfig.fontSize);
            /**
             * foreach line
             */
            for (String line : brlPage.split("\n")) {
                contentStream.drawString(line);
                contentStream.moveTextPositionByAmount(0, -pageConfig.fontSize);
            }
            contentStream.endText();
            contentStream.close();
            document.addPage(page);

        }
        document.save(pdfName);
        document.close();

    }

}
