/*
 * NatBraille - BrailleTable - An universal Translator
 * Copyright (C) 2014 Vivien Guillet, Bruno Mascret
 * Contact: bmascret@free.fr
 * 
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */
package org.natbraille.brailletable;

import java.util.List;

/**
 * Representation of a Braille character table.
 * <p>
 * A BrailleTable associates Characters to Braille cells represented by
 * unicode Characters.
 * <p>
 * A BrailleTable have an (optional) name.
 * <p>
 * Braille tables should be created using convenience methods from
 * {@link BrailleTables}
 * <p>
 * @author vivien
 */
public interface BrailleTable {

    /**
     * get the name of the Braille table, if set
     * <p>
     * @return the name ; null if not set
     */
    public String getName();

    /**
     * set the name of the Braille table.
     * <p>
     * @param name
     * @throws org.natbraille.brailletable.BrailleTableException
     */
    public void setName(String name) throws BrailleTableException;

    /**
     * Get the Unicode Braille symbol Character of a given Character in this
     * Braille Table
     * <p>
     * @param substitution a Character in this Braille Table
     * @return the Braille glyph Unicode representation and null if the
     * Character
     * does not exist
     */
    public abstract Character toUnicodeBraille(Character substitution);

    /**
     * Get the Character used in this Braille table for a given Unicode Braille
     * symbol.
     * <p>
     * @param brailleUnicode the Braille symbol as unicode Character
     * @return
     */
    public abstract Character getChar(Character brailleUnicode);

    /**
     * Set the Character used in this Braille table for a given Unicode Braille
     * symbol.
     * <p>
     * @param brailleUnicode the Braille symbol as unicode Character
     * @param substitution the corresponding Character in this Braille table
     * @throws BrailleTableException
     */
    public abstract void setChar(Character brailleUnicode, Character substitution) throws BrailleTableException;

    /**
     * Check compatibiliy of this Braille with other Braille Tables.
     * Braille tables are incompatibles when a least one given symbol represents
     * different Braille cells.
     * @param otherBrailleTables
     * @return
     */
    public boolean isCompatible(List<BrailleTable> otherBrailleTables);
}