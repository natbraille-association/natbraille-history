/*
 * NatBraille - BrailleTable - An universal Translator
 * Copyright (C) 2014 Vivien Guillet, Bruno Mascret
 * Contact: bmascret@free.fr
 * 
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */
package org.natbraille.brailletable;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.UnsupportedEncodingException;

/**
 * Stream converter from one {@link BrailleTable} + encoding pair to another
 * <p>TODO important : javadoc
 * @author vivien
 */
public class StreamConverter  {

    private final String sourceEncoding;
    private final String destEncoding;
    private final Converter converter;

    public StreamConverter(String sourceEncoding, BrailleTable sourceTable, String destEncoding, BrailleTable destTable) {
        this.converter = new Converter(sourceTable, destTable);
        this.sourceEncoding = sourceEncoding;
        this.destEncoding = destEncoding;
    }

    public StreamConverter(Converter c, String sourceEncoding, String destEncoding) {
        this.converter = c;
        this.sourceEncoding = sourceEncoding;
        this.destEncoding = destEncoding;
    }

    public void convert(InputStream is, OutputStream os) throws UnsupportedEncodingException, IOException, BrailleTableException {
        InputStreamReader isr = new InputStreamReader(new BufferedInputStream(is), sourceEncoding);
        try (OutputStreamWriter osw = new OutputStreamWriter(new BufferedOutputStream(os), destEncoding)) {
            boolean over = false;
            do {
                int read = isr.read();
                if (read == -1) {
                    over = true;
                } else {
                    Character cc = converter.convert((char) read);
                    osw.write(cc);
                }
            } while (!over);
        }
    }

    public Converter getConverter(){
        return converter;                
    }
}
