/*
 * NatBraille - BrailleTable - An universal Translator
 * Copyright (C) 2014 Vivien Guillet, Bruno Mascret
 * Contact: bmascret@free.fr
 * 
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */
package org.natbraille.brailletable.auto;

import org.natbraille.brailletable.BrailleTable;
import org.natbraille.brailletable.BrailleTableException;
import org.natbraille.brailletable.BrailleTables;
import org.natbraille.brailletable.UnicodeBraille;
import org.natbraille.brailletable.system.SystemBrailleTables;

/**
 *
 * @author vivien
 */
public class BrailleTableGuesser {

    private static Integer score(String unknown, BrailleTable bt) {
        Integer score = 0;
        for (int i = 0; i < unknown.length(); i++) {
            if (bt.toUnicodeBraille((char) unknown.codePointAt(i)) != null) {
                score++;
            }
        }
        return score;
    }

    /**
     * Guess the {@link BrailleTable} used in given Braille String amongst given
     * {@link BrailleTables}.
     *
     * @param unknown the Braille String
     * @param checkedBrailleTables the Braille tables to be checked against.
     * @return the best matching {@link BrailleTable}, or null if none.
     */
    public static BrailleTable guess(String unknown, BrailleTable checkedBrailleTables[]) {

        BrailleTable bestBrailleTable = null;
        Integer bestScore = 0;
        for (BrailleTable bt : checkedBrailleTables) {
            Integer score = score(unknown, bt);
            if (score > bestScore) {
                bestBrailleTable = bt;
                bestScore = score;
            }
        }

        return bestBrailleTable;

    }

    /**
     * Guess the System {@link BrailleTable} used in given Braille String.
     *
     * @param unknown the Braille String
     * @return the best matching {@link BrailleTable}, or null if none.
     * @throws BrailleTableException
     */
    public static BrailleTable guess(String unknown) throws BrailleTableException {
        String systemBrailleTableNames[] = SystemBrailleTables.getNames();
        BrailleTable systemBrailleTables[] = new BrailleTable[systemBrailleTableNames.length];
        for (int i = 0; i < systemBrailleTableNames.length; i++) {
            systemBrailleTables[i] = BrailleTables.forName(systemBrailleTableNames[i]);
        }
        return BrailleTableGuesser.guess(unknown, systemBrailleTables);

    }

    /**
     * Test TableGuesser on all known {@link SystemBrailleTables}.
     *
     * @param args : unused
     * @throws BrailleTableException
     */
    public static void main(String[] args) throws BrailleTableException {
        for (String mysteryTableName : SystemBrailleTables.getNames()) {

            BrailleTable mysteryTable = BrailleTables.forName(mysteryTableName);
            StringBuilder sb = new StringBuilder();
            for (Character c : UnicodeBraille.allSixDots()) {
                sb.append(mysteryTable.getChar(c));
            }
            String mysteryString = sb.toString();
            System.err.println("Test table " + mysteryTableName + " : \"" + mysteryString + "\"");
            BrailleTable bestBrailleTable = guess(mysteryString);
            if (bestBrailleTable == null) {
                System.err.println("[KO] no guess");
            } else if (!mysteryTableName.equals(bestBrailleTable.getName())) {
                System.err.println("[KO] bad guess : " + bestBrailleTable.getName());
            } else {
                System.err.println("[OK] good guess : " + bestBrailleTable.getName());
            }
        }

    }
}
