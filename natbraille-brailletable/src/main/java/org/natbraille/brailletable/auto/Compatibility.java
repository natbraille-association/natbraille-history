/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.natbraille.brailletable.auto;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.natbraille.brailletable.BrailleTable;
import org.natbraille.brailletable.BrailleTableException;
import org.natbraille.brailletable.BrailleTables;
import org.natbraille.brailletable.Converter;
import org.natbraille.brailletable.UnicodeBraille;
import org.natbraille.brailletable.system.SystemBrailleTables;

/**
 *
 * @author vivien
 */
public class Compatibility {

    /**
     * Check compatibiliy of given Braille tables.
     * <p>
     * Braille Tables are incompatible when a least one given symbol represents
     * different Braille cells.
     * <p>
     * @param brailleTables the Mapping of incompatible symbols with all braille
     * cell meaning.
     * @return
     * @throws BrailleTableException
     */
    public static Map<Character, String> checkIncompatibilities(List<BrailleTable> brailleTables) throws BrailleTableException {
        BrailleTable bt_unicode = BrailleTables.forName(SystemBrailleTables.BrailleUTF8);
        Map<Character, String> m = new HashMap<>();

        // foreach braille table
        for (BrailleTable brailleTable : brailleTables) {
            Converter converter = new Converter(bt_unicode, brailleTable);
            // foreach 6dot braille character
            for (Character c : UnicodeBraille.allSixDots()) {
                Character symb = converter.convert(c);
                String usedfor = m.get(symb);
                // has the symbol already been used
                if (usedfor != null) {
                    if (usedfor.contains("" + c)) {
                    } else {
                        m.put(symb, usedfor + "" + c);
                    }
                } else {
                    m.put(symb, "" + c);
                }
            }
        }

        Map<Character, String> resu = new HashMap<>();
        for (Character k : m.keySet()) {
            String meanings = m.get(k);
            // different meanings : report
            if (meanings.length() > 1) {
                resu.put(k, meanings);
            }
        }
        return resu;
    }

}
