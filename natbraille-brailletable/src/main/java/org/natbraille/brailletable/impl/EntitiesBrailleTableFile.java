/*
 * NatBraille - BrailleTable - An universal Translator
 * Copyright (C) 2014 Vivien Guillet, Bruno Mascret
 * Contact: bmascret@free.fr
 * 
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */
package org.natbraille.brailletable.impl;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.UnsupportedEncodingException;
import org.natbraille.brailletable.BrailleTable;
import org.natbraille.brailletable.BrailleTableException;
import org.natbraille.brailletable.BrailleTables;
import org.natbraille.brailletable.UnicodeBraille;

/**
 * Import/export from/to .ent file of {@link BrailleTable}.
 * This format was used by natbraille 2.0 for storing Braille tables.
 * see org.natbraille.brailletable.system.unused in resources for details.
 * <p>
 * @author vivien
 */
public class EntitiesBrailleTableFile {

    /**
     * Write a brailleTable as an .ent .
     * <p>
     * @param brailleTable the Braille table to be written
     * @param entitiesOs the destination stream where entities will be writen
     * @throws Exception
     */
    public static void toOutputStream(BrailleTable brailleTable, OutputStream entitiesOs) throws Exception {
        OutputStreamWriter osr = new OutputStreamWriter(entitiesOs, "UTF-8");
        for (char c = UnicodeBraille.FIRST; c <= UnicodeBraille.LAST_6DOTS; c++) {
            String braillePoints = UnicodeBraille.toPoints(c);
            Character destCode = brailleTable.getChar(c);
            if (destCode != null) {
                String s = String.format("<!ENTITY pt%s \"&#%d;\">\n", braillePoints, (int) destCode);
                osr.write(s);
            }
        }
        osr.flush();
        osr.close();
    }

    /**
     * Read a brailleTable from an .ent stream
     * <p>
     * @param entitiesIs the stream containing the entities
     * @return
     * @throws BrailleTableException
     */
    public static BrailleTable fromInputStream(InputStream entitiesIs) throws BrailleTableException {

        BrailleTable brailleTable = BrailleTables.empty();
        try {
            InputStreamReader isr = new InputStreamReader(entitiesIs, "UTF-8");
            try (BufferedReader raf = new BufferedReader(isr)) {
                String ligne;

                ligne = raf.readLine();

                /**
                 * parse to entities start
                 */
                while (ligne != null && !ligne.startsWith("<!ENTITY")) {
                    ligne = raf.readLine();
                }
                if (ligne == null) {
                    throw new BrailleTableException("unable to parse entity file (braille table)");
                }

                /**
                 * parse entities
                 */
                do {
                    Character pt = null;
                    Character code = null;

                    String recs[] = ligne.split(" ");

                    if (recs[1].startsWith("pt")) {
                        String pts = recs[1].substring(2, recs[1].length());
                        pt = UnicodeBraille.forPoints(pts);
                    }

                    if (recs[2].startsWith("\"&#")) {
                        String nums = recs[2].substring(3, recs[2].length());
                        int end = nums.indexOf(';');
                        String intString = nums.substring(0, end);
                        code = (char) (Integer.parseInt(intString));
                    } else if (recs[2].startsWith("\"&apos;")) {
                        code = '\'';
                    } else if (recs[2].startsWith("\"&quot;")) {
                        code = '"';
                    } else if (recs[2].startsWith("\"&lt;")) {
                        code = '<';
                    } else if (recs[2].startsWith("\"&gt;")) {
                        code = '>';
                    } else if (recs[2].startsWith("\"&amp;")) {
                        code = '&';
                    }

                    if ((code != null) && (pt != null)) {
                        brailleTable.setChar(pt, code);
                    }

                } while ((ligne = raf.readLine()) != null);
            }
        } catch (UnsupportedEncodingException e) {
            throw new BrailleTableException(e.getLocalizedMessage());

        } catch (IOException e) {
            throw new BrailleTableException(e.getLocalizedMessage());

        }
        return brailleTable;
    }

}
