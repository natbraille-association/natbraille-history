/*
 * NatBraille - BrailleTable - An universal Translator
 * Copyright (C) 2014 Vivien Guillet, Bruno Mascret
 * Contact: bmascret@free.fr
 * 
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */
package org.natbraille.brailletable.ui;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.apache.commons.cli.CommandLine;
import org.apache.commons.cli.HelpFormatter;
import org.apache.commons.cli.Options;
import org.apache.commons.cli.ParseException;
import org.apache.commons.cli.PosixParser;
import org.apache.commons.cli.UnrecognizedOptionException;
import org.natbraille.brailletable.BrailleTable;
import org.natbraille.brailletable.BrailleTables;
import org.natbraille.brailletable.Converter;
import org.natbraille.brailletable.StreamConverter;
import org.natbraille.brailletable.system.SystemBrailleTables;
import org.xnap.commons.i18n.I18n;
import org.xnap.commons.i18n.I18nFactory;

/**
 * Command line interface for braille table conversion.
 * <p>
 * java -jar natbraille-brailletable.jar --help for help
 * <p>
 * @author vivien
 */
public class BrailleTableCli {

    private static final Logger l = Logger.getLogger(BrailleTableCli.class.getName());
    private static final I18n i18n = I18nFactory.getI18n(BrailleTable.class);

    private static final String O_FROM_CODE = "c";
    private static final String O_TO_CODE = "C";
    private static final String O_FROM_TABLE = "t";
    private static final String O_COPY_MISSING = "m";
    private static final String O_FORCE_MISSING = "f";
    private static final String O_TO_TABLE = "T";
    private static final String O_OUTPUT = "o";
    private static final String O_LIST_TABLES = "l";
    private static final String O_HELP = "h";

    private static final Options options = new Options()
            .addOption(O_FROM_CODE, "from-code", true,
                    i18n.tr("set input encoding to <arg>"))
            .addOption(O_TO_CODE, "to-code", true,
                    i18n.tr("set output encoding to <arg>"))
            .addOption(O_FROM_TABLE, "from-table", true,
                    i18n.tr("set source braille table to <arg>"))
            .addOption(O_TO_TABLE, "to-table", true,
                    i18n.tr("set destination braille table to <arg>"))
            .addOption(O_COPY_MISSING, "copy-missing", true,
                    i18n.tr("copy source chars missing in source braille table"))
            .addOption(O_FORCE_MISSING, "force", false,
                    i18n.tr("copy source chars missing in source or destination braille table"))
            .addOption(O_LIST_TABLES, "list", false,
                    i18n.tr("list all known braille table names"))
            .addOption(O_OUTPUT, "output", true,
                    i18n.tr("name output filenames by appending <arg>"))
            .addOption(O_HELP, "help", false,
                    i18n.tr("display help"));

    public static void print(String s) {
        System.out.println(s);
    }

    public static void printerr(String s) {
        System.err.println(s);
    }

    public static void convertFiles(StreamConverter sc, String filenames[], String extension) throws Exception {
        if (filenames.length == 0) {
            sc.convert(System.in, System.out);
            System.out.flush();
        } else {
            for (String filename : filenames) {

                printerr(i18n.tr("file : ''{0}''", filename));
                try {

                    File inputFile = new File(filename);
                    File outputFile = new File(filename + "." + extension);
                    try (FileOutputStream fos = new FileOutputStream(outputFile)) {
                        sc.convert(
                                new FileInputStream(inputFile),
                                fos
                        );
                        fos.flush();
                        fos.close();
                    }
                    printerr(i18n.tr("wrote file : ''{0}''", outputFile));
                } catch (IOException ex) {
                    l.log(Level.SEVERE, ex.getLocalizedMessage());
                } catch (Exception ex) {
                    l.log(Level.SEVERE, null, ex);
                }
            }
        }
    }

    private static void printHelp(Options options) {
        HelpFormatter formatter = new HelpFormatter();
        formatter.printHelp(BrailleTableCli.class.getCanonicalName() + " [options] file1 file2 ... filen", options);
    }

    public static void main(String[] args) throws Exception {

        l.setLevel(Level.CONFIG);
        TheNatbrailleBannerDrawIt();
        try {

            CommandLine cl = new PosixParser().parse(options, args);
            if (cl.hasOption(O_HELP)) {
                printHelp(options);
            } else if (cl.hasOption(O_LIST_TABLES)) {
                for (String s : SystemBrailleTables.getNames()) {
                    print(s);
                }
            } else {
                String sourceEncoding = null;
                String destEncoding = null;
                BrailleTable sourceTable = null;
                BrailleTable destTable = null;

                if (cl.hasOption(O_FROM_CODE)) {
                    sourceEncoding = cl.getOptionValue(O_FROM_CODE);
                } else {
                    printerr(i18n.tr("no input encoding provided"));
                    printHelp(options);
                    System.exit(1);
                }
                if (cl.hasOption(O_TO_CODE)) {
                    destEncoding = cl.getOptionValue(O_TO_CODE);
                } else {
                    printerr(i18n.tr("no output encoding provided"));
                    printHelp(options);
                    System.exit(1);
                }

                if (cl.hasOption(O_FROM_TABLE)) {
                    sourceTable = BrailleTables.forName(cl.getOptionValue(O_FROM_TABLE));
                }
                if (sourceTable == null) {
                    printerr(i18n.tr("not a braille table name : ''{0}''", cl.getOptionValue(O_FROM_TABLE)));
                    System.exit(1);
                }

                if (cl.hasOption(O_TO_TABLE)) {
                    destTable = BrailleTables.forName(cl.getOptionValue(O_TO_TABLE));
                }
                if (destTable == null) {
                    printerr(i18n.tr("not a braille table name : ''{0}''", cl.getOptionValue(O_TO_TABLE)));
                    System.exit(1);
                }
                {
                    Converter converter = new Converter(sourceTable, destTable);

                    if (cl.hasOption(O_COPY_MISSING)) {
                        converter.getConfig().setCopyMissingSource(Boolean.getBoolean(O_COPY_MISSING));
                    }
                    if (cl.hasOption(O_FORCE_MISSING)) {
                        converter.getConfig().setCopyMissingSource(true);
                        converter.getConfig().setCopyMissingDest(true);
                    }

                    StreamConverter streamConverter = new StreamConverter(converter, sourceEncoding, destEncoding);

                    String extension;
                    {
                        if (cl.hasOption(O_OUTPUT)) {
                            extension = cl.getOptionValue(O_OUTPUT);
                        } else {
                            extension = destEncoding + "_" + destTable.getName();
                        }
                    }

                    String fileNames[] = cl.getArgs();
                    convertFiles(streamConverter, fileNames, extension);

                }
            }
        } catch (UnrecognizedOptionException e) {
            printHelp(options);
            l.log(Level.SEVERE, e.getMessage());
        } catch (ParseException e) {
            printHelp(options);
            l.log(Level.SEVERE, e.getMessage());
        } catch (Exception e) {
            l.log(Level.SEVERE, e.getMessage());
        }

    }

    public static void TheNatbrailleBannerDrawIt() {
        System.err.println("Natbraille - brailletable");
    }
}
