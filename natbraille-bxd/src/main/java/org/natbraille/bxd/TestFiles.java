/*
 * NatBraille - An universal Translator
 * Copyright (C) 2005 Bruno Mascret
 * Contact: bmascret@free.fr
 * 
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */
package org.natbraille.bxd;


import java.io.File;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 *
 * @author vivien
 */
public class TestFiles {

    static final String baseLocation = "dbtfiles/";

    public static File[] tmpl111 = new File[]{
        new File(baseLocation + "tmpl111/./Nguni (Xhosa or Zulu) - basic.dxt"),
        new File(baseLocation + "tmpl111/./English (American Textbook DE) - BANA Nemeth.dxt"),
        new File(baseLocation + "tmpl111/./Indonesian - basic.dxt"),
        new File(baseLocation + "tmpl111/./Norse.dxt"),
        new File(baseLocation + "tmpl111/./Xhosa - basic.dxt"),
        new File(baseLocation + "tmpl111/./English (American Textbook DE) - Textbook Format.dxt"),
        new File(baseLocation + "tmpl111/./Portuguese Uncontracted - basic.dxt"),
        new File(baseLocation + "tmpl111/./Francais 2006 - integral braille de base.dxt"),
        new File(baseLocation + "tmpl111/./Swedish Uncontracted - basic.dxt"),
        new File(baseLocation + "tmpl111/./Telugu - basic.dxt"),
        new File(baseLocation + "tmpl111/./Chinese Yue (Cantonese) - basic.dxt"),
        new File(baseLocation + "tmpl111/./Kannada - basic.dxt"),
        new File(baseLocation + "tmpl111/./Kirghiz - basic.dxt"),
        new File(baseLocation + "tmpl111/./English (American) - Standard Literary Format.dxt"),
        new File(baseLocation + "tmpl111/./Hausa - basic.dxt"),
        new File(baseLocation + "tmpl111/./Meitei (Manipuri) - basic.dxt"),
        new File(baseLocation + "tmpl111/./Estonian - basic.dxt"),
        new File(baseLocation + "tmpl111/./Korean - basic.dxt"),
        new File(baseLocation + "tmpl111/./Cymraeg (Welsh) - basic.dxt"),
        new File(baseLocation + "tmpl111/./Ewe - basic.dxt"),
        new File(baseLocation + "tmpl111/./Afrikaans - basic.dxt"),
        new File(baseLocation + "tmpl111/./Dari - basic.dxt"),
        new File(baseLocation + "tmpl111/./Yoruba - basic.dxt"),
        new File(baseLocation + "tmpl111/./Bosnian - basic.dxt"),
        new File(baseLocation + "tmpl111/./English (British) - with capitals.dxt"),
        new File(baseLocation + "tmpl111/./Nepali - basic.dxt"),
        new File(baseLocation + "tmpl111/./Belarusian - basic.dxt"),
        new File(baseLocation + "tmpl111/./Sinhala - basic.dxt"),
        new File(baseLocation + "tmpl111/./Tibetan - basic.dxt"),
        new File(baseLocation + "tmpl111/./Latvian - basic.dxt"),
        new File(baseLocation + "tmpl111/./Assamese - basic.dxt"),
        new File(baseLocation + "tmpl111/./Armenian (Eastern) - basic.dxt"),
        new File(baseLocation + "tmpl111/./Chittagonian - basic.dxt"),
        new File(baseLocation + "tmpl111/./Irish.dxt"),
        new File(baseLocation + "tmpl111/./Greek (Modern) - basic.dxt"),
        new File(baseLocation + "tmpl111/./Venda - basic.dxt"),
        new File(baseLocation + "tmpl111/./Italiano.dxt"),
        new File(baseLocation + "tmpl111/./English (American Textbook DE) - BANA.dxt"),
        new File(baseLocation + "tmpl111/./English (Unified) - basic.dxt"),
        new File(baseLocation + "tmpl111/./Tsonga - basic.dxt"),
        new File(baseLocation + "tmpl111/./Thai - basic.dxt"),
        new File(baseLocation + "tmpl111/./Hebrew (American) - basic.dxt"),
        new File(baseLocation + "tmpl111/./Armenian (Western) - basic.dxt"),
        new File(baseLocation + "tmpl111/./Espanol Reducida.dxt"),
        new File(baseLocation + "tmpl111/./EBU Pharmaceutical.dxt"),
        new File(baseLocation + "tmpl111/./German - basic.dxt"),
        new File(baseLocation + "tmpl111/./Azerbaijani - basic.dxt"),
        new File(baseLocation + "tmpl111/./Ukrainian - basic.dxt"),
        new File(baseLocation + "tmpl111/./Bhojpuri - basic.dxt"),
        new File(baseLocation + "tmpl111/./Malayalam - basic.dxt"),
        new File(baseLocation + "tmpl111/./Sylheti - basic.dxt"),
        new File(baseLocation + "tmpl111/./Tigrinya - basic.dxt"),
        new File(baseLocation + "tmpl111/./Turkish - basic.dxt"),
        new File(baseLocation + "tmpl111/./Sanskrit - basic.dxt"),
        new File(baseLocation + "tmpl111/./Panjabi - basic.dxt"),
        new File(baseLocation + "tmpl111/./Greek (Classical-American) - basic.dxt"),
        new File(baseLocation + "tmpl111/./Russian - no capitals.dxt"),
        new File(baseLocation + "tmpl111/./Marathi - basic.dxt"),
        new File(baseLocation + "tmpl111/./Slovenian - basic.dxt"),
        new File(baseLocation + "tmpl111/./Lao - basic.dxt"),
        new File(baseLocation + "tmpl111/./Sotho (Southern and Northern (Pedi)) - basic.dxt"),
        new File(baseLocation + "tmpl111/./Lithuanian - basic.dxt"),
        new File(baseLocation + "tmpl111/./Catalan - basic.dxt"),
        new File(baseLocation + "tmpl111/./Malay - basic.dxt"),
        new File(baseLocation + "tmpl111/./Latin - basic.dxt"),
        new File(baseLocation + "tmpl111/./Kazakh - basic.dxt"),
        new File(baseLocation + "tmpl111/./Filipino - basic.dxt"),
        new File(baseLocation + "tmpl111/./Irish no capitals.dxt"),
        new File(baseLocation + "tmpl111/./Iloko - basic.dxt"),
        new File(baseLocation + "tmpl111/./Swahili - basic.dxt"),
        new File(baseLocation + "tmpl111/./Icelandic - basic.dxt"),
        new File(baseLocation + "tmpl111/./German Vollschrift - basic.dxt"),
        new File(baseLocation + "tmpl111/./Danish - basic.dxt"),
        new File(baseLocation + "tmpl111/./Khmer - basic.dxt"),
        new File(baseLocation + "tmpl111/./Pushto - basic.dxt"),
        new File(baseLocation + "tmpl111/./Esperanto - basic.dxt"),
        new File(baseLocation + "tmpl111/./Urdu (Pakistani) - basic.dxt"),
        new File(baseLocation + "tmpl111/./Swedish Uncontracted - basic no capitals.dxt"),
        new File(baseLocation + "tmpl111/./Francais 2006 - abrege.dxt"),
        new File(baseLocation + "tmpl111/./Kurukh - basic.dxt"),
        new File(baseLocation + "tmpl111/./Swati - basic.dxt"),
        new File(baseLocation + "tmpl111/./Chinese Mandarin with tones - basic.dxt"),
        new File(baseLocation + "tmpl111/./Farsi (Persian) - basic.dxt"),
        new File(baseLocation + "tmpl111/./Macedonian - basic.dxt"),
        new File(baseLocation + "tmpl111/./Polish - basic.dxt"),
        new File(baseLocation + "tmpl111/./Uzbek - basic.dxt"),
        new File(baseLocation + "tmpl111/./English Moon - basic.dxt"),
        new File(baseLocation + "tmpl111/./Hiligaynon - basic.dxt"),
        new File(baseLocation + "tmpl111/./Hungarian - basic.dxt"),
        new File(baseLocation + "tmpl111/./Hindi - basic.dxt"),
        new File(baseLocation + "tmpl111/./Somali - basic.dxt"),
        new File(baseLocation + "tmpl111/./Kurdish - basic.dxt"),
        new File(baseLocation + "tmpl111/./Espanol con Contracciones.dxt"),
        new File(baseLocation + "tmpl111/./English (British) - no capitals in literary.dxt"),
        new File(baseLocation + "tmpl111/./Amharic - basic.dxt"),
        new File(baseLocation + "tmpl111/./Cymraeg (Welsh) - basic no capitals.dxt"),
        new File(baseLocation + "tmpl111/./Ndebele - basic.dxt"),
        new File(baseLocation + "tmpl111/./Slovak - basic.dxt"),
        new File(baseLocation + "tmpl111/./Maltese - basic.dxt"),
        new File(baseLocation + "tmpl111/./Santali - basic.dxt"),
        new File(baseLocation + "tmpl111/./Zulu - basic.dxt"),
        new File(baseLocation + "tmpl111/./Burmese - basic.dxt"),
        new File(baseLocation + "tmpl111/./Tajik - basic.dxt"),
        new File(baseLocation + "tmpl111/./English Moon - basic no capitals.dxt"),
        new File(baseLocation + "tmpl111/./German Uncontracted - basic.dxt"),
        new File(baseLocation + "tmpl111/./Japanese (Kana) - basic.dxt"),
        new File(baseLocation + "tmpl111/./Oriya - basic.dxt"),
        new File(baseLocation + "tmpl111/./Francais 2006 - integral.dxt"),
        new File(baseLocation + "tmpl111/./Serbian - basic.dxt"),
        new File(baseLocation + "tmpl111/./IPA - basic.dxt"),
        new File(baseLocation + "tmpl111/./Urdu (Indian) - basic.dxt"),
        new File(baseLocation + "tmpl111/./Espanol sin Contracciones.dxt"),
        new File(baseLocation + "tmpl111/./Haryanvi - basic.dxt"),
        new File(baseLocation + "tmpl111/./Czech - basic.dxt"),
        new File(baseLocation + "tmpl111/./Bulgarian - basic.dxt"),
        new File(baseLocation + "tmpl111/./Tagalog - basic.dxt"),
        new File(baseLocation + "tmpl111/./Chinese Mandarin - basic.dxt"),
        new File(baseLocation + "tmpl111/./Dutch - basic.dxt"),
        new File(baseLocation + "tmpl111/./Maithili - basic.dxt"),
        new File(baseLocation + "tmpl111/./Tswana - basic.dxt"),
        new File(baseLocation + "tmpl111/./Gujarati - basic.dxt"),
        new File(baseLocation + "tmpl111/./Dutch Uncontracted - basic.dxt"),
        new File(baseLocation + "tmpl111/./Turkmen - basic.dxt"),
        new File(baseLocation + "tmpl111/./Romanian - basic.dxt"),
        new File(baseLocation + "tmpl111/./English (Unified) - Australian formatting.dxt"),
        new File(baseLocation + "tmpl111/./Croatian - basic.dxt"),
        new File(baseLocation + "tmpl111/./Igbo - basic.dxt"),
        new File(baseLocation + "tmpl111/./English (American) - XML (Daisy-NISO-NIMAS).dxt"),
        new File(baseLocation + "tmpl111/./Finnish - basic.dxt"),
        new File(baseLocation + "tmpl111/./Bengali (Bangla) - basic.dxt"),
        new File(baseLocation + "tmpl111/./Dzongkha - basic.dxt"),
        new File(baseLocation + "tmpl111/./Mongolian - basic.dxt"),
        new File(baseLocation + "tmpl111/./Georgian - basic.dxt"),
        new File(baseLocation + "tmpl111/./Tamil - basic.dxt"),
        new File(baseLocation + "tmpl111/./Vietnamese - basic.dxt"),
        new File(baseLocation + "tmpl111/./Magahi - basic.dxt"),
        new File(baseLocation + "tmpl111/./Awadhi - basic.dxt"),
        new File(baseLocation + "tmpl111/./Cebuano - basic.dxt"),
        new File(baseLocation + "tmpl111/./Luxembourgish - basic.dxt"),
        new File(baseLocation + "tmpl111/./Sindhi - basic.dxt"),
        new File(baseLocation + "tmpl111/./Arabic - basic.dxt"),
        new File(baseLocation + "tmpl111/./Hebrew (Israeli) - basic.dxt"),
        new File(baseLocation + "tmpl111/./Francais 2006 - abrege braille de base.dxt"),
        new File(baseLocation + "tmpl111/./Albanian - basic.dxt"),
        new File(baseLocation + "tmpl111/./Sadri - basic.dxt")
    };

    public static File[] hyphenTest = new File[]{
        new File(baseLocation + "hyph/hyphentable_none.dxp"),
        new File(baseLocation + "hyph/hyphentable_dash.dxp"),
        new File(baseLocation + "hyph/hyphentable_dashesandhyphen.dxp"),
        new File(baseLocation + "hyph/hyphentable_french.dxp"),
        new File(baseLocation + "translation/translation_table_afrikaans.dxp"),
        new File(baseLocation + "translation/translation_table_bulgarian_uncontracted.dxp"),
        new File(baseLocation + "translation/translation_table_cymarwelsh_with_capitals.dxp"),
        new File(baseLocation + "translation/translation_table_cymarwelsh_without_capitals.dxp"),
        new File(baseLocation + "translation/translation_table_french.dxp"),
        new File(baseLocation + "translation/translation_table_french_uncontracted.dxp"),};

    public static File[] printandbraillefiles = new File[]{
        new File(baseLocation + "brailleandprint/tmpl0004.dxt"),
        new File(baseLocation + "brailleandprint/print_print.dxp"),
        new File(baseLocation + "brailleandprint/print_print.dxp"),};

    public static File[] dxbpfiles = new File[]{
        // 10.4

        // 1.4
        new File(baseLocation + "perso/test1.dxb"),
        new File(baseLocation + "perso/p1.dxb"),
        new File(baseLocation + "perso/p2.dxb"),
        new File(baseLocation + "perso/p3.dxb"),
        new File(baseLocation + "perso/p4.dxb"),
        new File(baseLocation + "perso/p1.dxp"),
        new File(baseLocation + "perso/p2.dxp"),
        new File(baseLocation + "perso/p3.dxp"),
        new File(baseLocation + "perso/p4.dxp"),
        new File(baseLocation + "perso/p4.dxp"),
        new File(baseLocation + "perso/nostyle.dxb"),
        new File(baseLocation + "perso/st1.dxb"),
        new File(baseLocation + "perso/st2.dxb"),
        new File(baseLocation + "perso/styles.dxb"),
        new File(baseLocation + "perso/styles.dxp"),
        // 1.?

        new File(baseLocation + "perso/Manuel_abrege2013_Vol1.dxb"),
        // 1.?

        new File(baseLocation + "ctrdv/cfg.dxp"),
        new File(baseLocation + "ctrdv/cfg.dxb"),
        // 1.7
        new File(baseLocation + "ctrdv/monstres2.dxb"),
        new File(baseLocation + "ctrdv/monstres2.dxp"),
        new File(baseLocation + "ctrdv/ce1.dxb"),
        new File(baseLocation + "ctrdv/ce1.dxp"),
        new File(baseLocation + "ctrdv/lecon2.dxb"),
        new File(baseLocation + "ctrdv/lecon2.dxp"),
        new File(baseLocation + "ctrdv/quebien.dxb"),
        new File(baseLocation + "ctrdv/quebien.dxp"),
        new File(baseLocation + "ctrdv/svt.dxb"),
        new File(baseLocation + "ctrdv/svt.dxp"),
        // 1.4 

        new File(baseLocation + "sys/pn/lr3.dxp"),
        new File(baseLocation + "sys/pn/ll2.dxp"),
        new File(baseLocation + "sys/pn/none2.dxp"),
        new File(baseLocation + "sys/pn/lr2.dxp"),
        new File(baseLocation + "sys/pn/ul2.dxp"),
        new File(baseLocation + "sys/pn/ur2.dxp"),
        new File(baseLocation + "sys/hy/french.dxp"),
        new File(baseLocation + "sys/hy/dashes.dxp"),
        new File(baseLocation + "sys/hy/none.dxp"),
        new File(baseLocation + "sys/hy/dashesandhyphen.dxp"),
        new File(baseLocation + "sys/pr/d26.dxp"),
        new File(baseLocation + "sys/pr/d27.dxp"),
        new File(baseLocation + "sys/pr/44.dxp"),
        new File(baseLocation + "sys/pr/43.dxp"),
        new File(baseLocation + "sys/pr/42.dxp"),
        new File(baseLocation + "sys/tt/tt_bulga.dxp"),
        new File(baseLocation + "sys/tt/tt_afri.dxp"),
        new File(baseLocation + "sys/tt/tt_welsh.dxp"),
        new File(baseLocation + "sys/pn2/PNO_4.dxp"),
        new File(baseLocation + "sys/pn2/PNOR_3.dxp"),
        new File(baseLocation + "sys/pn2/PNO_1.dxp"),
        new File(baseLocation + "sys/pn2/PNOR_2244.dxp"),
        new File(baseLocation + "sys/pn2/PNO_3.dxp"),
        new File(baseLocation + "sys/pn2/PNOR_1.dxp"),
        new File(baseLocation + "sys/pn2/PNO_2.dxp"),
        new File(baseLocation + "sys/pn2/PNOR_41.dxp"),
        new File(baseLocation + "sys/pn2/PNOR_4.dxp"),
        new File(baseLocation + "sys/pn2/PNER_5.dxp"),
        new File(baseLocation + "sys/pn2/PNOR_1234.dxp"),
        new File(baseLocation + "sys/pn2/PNOR_5.dxp"),
        new File(baseLocation + "sys/pn2/PNO_none.dxp"),
        new File(baseLocation + "sys/pn2/PNOR_2.dxp"),
        //11.1 ?
        new File(baseLocation + "docdbt111/en/docdoll.dxp"),
        new File(baseLocation + "docdbt111/en/tutoria.dxp"),
        new File(baseLocation + "docdbt111/en/duxguidw.dxp"),
        new File(baseLocation + "docdbt111/en/dochow.dxp"),
        new File(baseLocation + "docdbt111/en/nemex.dxp"),
        new File(baseLocation + "docdbt111/en/trans.dxp"),
        new File(baseLocation + "docdbt111/en/sgml.dxp"),
        new File(baseLocation + "docdbt111/fr/docdollf.dxp"),
        new File(baseLocation + "docdbt111/fr/docchrf.dxp"),
        new File(baseLocation + "docdbt111/fr/duxguidf.dxp"),
        new File(baseLocation + "docdbt111/fr/Importation de fichiers BRF.dxp"),
        new File(baseLocation + "docdbt111/fr/docdolqf.dxp"),
        new File(baseLocation + "docdbt111/fr/DocTab (Francais).dxp"),
        new File(baseLocation + "docdbt111/fr/Informations complementaires de transcription.dxp")

    };

    public static List<File> all() {
        List<File> allFiles = new ArrayList<>();

        allFiles.addAll(Arrays.asList(printandbraillefiles));        
        allFiles.addAll(Arrays.asList(hyphenTest));
        allFiles.addAll(Arrays.asList(dxbpfiles));
        allFiles.addAll(Arrays.asList(tmpl111));

        return allFiles;
    }
}
