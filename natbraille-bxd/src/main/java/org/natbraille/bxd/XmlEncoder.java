/*
 * NatBraille - An universal Translator
 * Copyright (C) 2005 Bruno Mascret
 * Contact: bmascret@free.fr
 * 
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */
package org.natbraille.bxd;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import nu.xom.Attribute;
import nu.xom.Document;
import nu.xom.Element;
import nu.xom.Node;
import nu.xom.Nodes;
import org.apache.commons.io.IOUtils;

/**
 * Xml import/export to/from a {@link BinaryDeEncoder.BxdDocument}.
 * <p>
 * Exports a {@link BinaryDeEncoder.BxdDocument} to a xml representation for use
 * with xml manipulation tools. Transformed or generated xml can be imported
 * back to a {@link BinaryDeEncoder.BxdDocument} and finally be written as DBT
 * binary flat file.
 * <p>
 * By exporting to the xml representation, some information is lost that have to
 * be rebuild later on while importing xml after transformations. :
 * <ul>
 * <p>
 * Not problematic at all :
 * <ul>
 * <li>some header structures containing only recomputable informations</li>
 * <li>some null string endings (restaured while writing bytes)  </li>
 * </ul>
 * <p>
 * A little more problematic :
 * <ul>
 * <li>some block that never seem to vary amongst the DBT file corpus (might be
 * data structure informations whose lenth never varies)</li>
 * <li>probable zero padding data before 0xea (constant document contents start
 * offset)</li>
 * </ul>
 * <p>
 * Maybe problematic
 * <ul>
 * <li>the printing informations</li>
 * <li>some block that rarely vary amongst the DBT file corpus</li>
 * <li>some varying blocks that we don't fully understand yet, filled at import
 * with default values DBT seems to be happy with </li>
 * </ul>
 * </ul>
 * <p>
 * The rationale for import is to use model, default or computed values for
 * every unknown (problematic) elements not found in the xml document.
 *
 * @author vivien
 */
public class XmlEncoder {

    private static String byteArrayToInternalString(byte[] bytes, String charset) throws IOException {
        
        return IOUtils.toString(new ByteArrayInputStream(bytes), charset);      
        
        
    }

    private static byte[] internalStringToByteArray(String internalString, String charset) throws IOException {
        return internalString.getBytes(charset);
    }

    private static short integerToShort(int integerInteger) throws Exception {
        short shortInteger = (short) (0xffff & integerInteger);
        if (integerInteger != shortInteger) {
            throw new Exception("cannot encode integer " + integerInteger + "in a short");
        }
        return shortInteger;
    }

    private static short integerToShort(String s) throws Exception {
        return integerToShort(Integer.parseInt(s));
    }

    ///// Encode    
    private static List<Element> splitDocumentContent(byte[] contentBytes, String charset) throws Exception {
        List<Element> documentItems = new ArrayList<>();

        Element curElement = null;
        ByteArrayOutputStream sb = new ByteArrayOutputStream();
        for (byte b : contentBytes) {
            switch (b) {
                case (byte) 0x1c:      // open command
                    if (curElement != null) {
                        String internalString = byteArrayToInternalString(sb.toByteArray(), charset);
                        internalString = internalString.replaceAll(Character.toString((char)0x1d), "__BAD_CHAR_0x1d__");
                        internalString = internalString.replaceAll(Character.toString((char)0x1e), "__BAD_CHAR_0x1e__");
                        curElement.appendChild(internalString);
                        documentItems.add(curElement);
                    }
                    curElement = new Element("command");
                    sb.reset();
                    break;
                case (byte) 0x1f:     // close command
                    if (curElement != null) {
                        boolean doneAppend = false;
                        if (curElement.getLocalName().equals("command")) {
                            String internalString = byteArrayToInternalString(sb.toByteArray(), charset);
                            if (internalString.startsWith("es~")) {
                                curElement = new Element("style-start");
                                curElement.addAttribute(new Attribute("name", internalString.substring(3)));
                                doneAppend = true;
                            } else if (internalString.startsWith("ee~")) {
                                curElement = new Element("style-end");
                                curElement.addAttribute(new Attribute("name", internalString.substring(3)));
                                doneAppend = true;
                            }
                        }
                        if (!doneAppend) {
                            curElement.appendChild(byteArrayToInternalString(sb.toByteArray(), charset));
                        }
                        documentItems.add(curElement);

                    }
                    curElement = null;
                    sb.reset();

                    break;

                default:
                    if (curElement == null) {
                        curElement = new Element("text");
                    }
                    sb.write(b);
            }
        }
        if (curElement != null) {
            curElement.appendChild(sb.toString());
            documentItems.add(curElement);
        }
        return documentItems;
    }

    private static List<Element> splitInsertion(byte[] insertionBytes, String charset) throws Exception {
        ByteArrayOutputStream sb = new ByteArrayOutputStream();
        List<Element> insertions = new ArrayList<>();
        Element curElement = null;
        for (byte b : insertionBytes) {
            switch (b) {
                case (byte) 0x1c:
                    if (curElement != null) {
                        throw new Exception("starting insertion definition before closing");
                    }
                    sb.reset();
                    curElement = new Element("insert");
                    break;
                case (byte) 0x1f:
                    if (curElement != null) {
                        curElement.appendChild(byteArrayToInternalString(sb.toByteArray(), charset));
                        insertions.add(curElement);
                    }
                    curElement = null;
                    sb.reset();
                    break;

                default:
                    sb.write(b);
            }
        }
        return insertions;
    }

    private static List<Element> splitDictionnaryRefs(byte[][] dictionaries) {
        List<Element> references = new ArrayList<>();

        List<String> names = new ArrayList<>();
        List<String> cs = new ArrayList<>();

        for (byte[] bs : dictionaries) {
            StringBuilder name = new StringBuilder();
            for (int i = 1; i < bs.length; i++) {
                if (bs[i] != (byte) 0x00) {
                    name.append((char) bs[i]);
                }
            }
            names.add(name.toString());
            cs.add(String.format("0x%02X", bs[0]));
        }
        {
            Element print = new Element("print");
            {
                Element btb = new Element("transcription-table");
                btb.addAttribute(new Attribute("type", cs.get(0)));
                btb.addAttribute(new Attribute("name", names.get(0)));
                print.appendChild(btb);
            }
            {
                Element sct = new Element("scan-control-table");
                sct.addAttribute(new Attribute("type", cs.get(1)));
                sct.addAttribute(new Attribute("name", names.get(1)));
                print.appendChild(sct);
            }
            references.add(print);
        }

        {
            Element braille = new Element("Braille");
            {
                Element btb = new Element("transcription-table");
                btb.addAttribute(new Attribute("type", cs.get(2)));
                btb.addAttribute(new Attribute("name", names.get(2)));
                braille.appendChild(btb);
            }
            {
                Element sct = new Element("scan-control-table");
                sct.addAttribute(new Attribute("type", cs.get(3)));
                sct.addAttribute(new Attribute("name", names.get(3)));
                braille.appendChild(sct);
            }
            references.add(braille);
        }

        return references;
    }

    private static String bytesToString(byte[] bytes) {
        StringBuilder sb = new StringBuilder();

        for (byte b : bytes) {
            switch (b) {
                case (byte) 0x00:
                case (byte) 0x1c:
                case (byte) 0x1d:
                case (byte) 0x1e:
                case (byte) 0x1f:
                    sb.append(String.format("!!unknown_char_0x%02X!!", b));
                    break;
                default:
                    sb.append((char) b);
            }
        }
        return sb.toString();
    }
    /*
     if (b == 0x1c) { // file sep
     System.err.print("{\\fs}");
     } else if (b == 0x1f) { // unit sep
     System.err.print("{\\us}");
     } else if (b == 0x1d) { // group separator
     System.err.print("{\\GS}");
     } else if (b == 0x1e) { // record separator
     System.err.print("{\\RS}");
     } else {
     System.err.print((char) b);
     }
     */

    private static String decodeNumbering(short val) throws Exception {

        switch (val) {
            case BinaryDeEncoder.HeaderSection.BXD_NUMBERING_NONE:
                return ("none");
            case BinaryDeEncoder.HeaderSection.BXD_NUMBERING_UPPER_LEFT:
                return ("upper-left");
            case BinaryDeEncoder.HeaderSection.BXD_NUMBERING_UPPER_RIGHT:
                return ("upper-right");
            case BinaryDeEncoder.HeaderSection.BXD_NUMBERING_LOWER_LEFT:
                return ("lower-left");
            case BinaryDeEncoder.HeaderSection.BXD_NUMBERING_LOWER_RIGHT:
                return ("lower-right");
            default:
                throw new Exception("unknown value for numbering position" + val);
        }
    }

    private static String decodeDocumentType(short val) throws Exception {

        switch (val) {
            case BinaryDeEncoder.HeaderSection.BXD_DOCUMENT_TYPE_BRAILLE:
                return ("Braille");
            case BinaryDeEncoder.HeaderSection.BXD_DOCUMENT_TYPE_PRINT:
                return ("print");
            default:
                throw new Exception("unknown value for document type" + val);
        }
    }

    private static String decodeHyphenTable(short val) throws Exception {

        switch (val) {
            case BinaryDeEncoder.HeaderSection.BXD_HYPHEN_TABLE_NONE:
                return ("none");
            case BinaryDeEncoder.HeaderSection.BXD_HYPHEN_TABLE_DASHES:
                return ("dashes");
            case BinaryDeEncoder.HeaderSection.BXD_HYPHEN_TABLE_DASHES_AND_HYPHENS:
                return ("dashes-and-hyphens");
            case BinaryDeEncoder.HeaderSection.BXD_HYPHEN_TABLE_FRENCH:
                return ("french");
            default:
                throw new Exception("unknown value for numbering position" + val);
        }
    }

    /**
     * Export a in-memory representation of a DBT document to XML.
     *
     * @param bxdDocument the in-memory representation. See class header for
     * details.
     * @param charset the document charset
     * @return the XML element
     * @throws Exception
     */
    public static Element toXML(BinaryDeEncoder.BxdDocument bxdDocument, String charset) throws Exception {

        BinaryDeEncoder.HeaderSection headerSection = bxdDocument.headerSection;
        BinaryDeEncoder.ContentsSection contentsSection = bxdDocument.contentsSection;
        BinaryDeEncoder.StyleSection styleSection = bxdDocument.styleSection;

        Element el = new Element("bxd");
        {
            Element version = new Element("version");
            Attribute majorVersion = new Attribute("version-major", Integer.toString(headerSection.documentMajorVersion));
            version.addAttribute(majorVersion);

            Attribute type = new Attribute("type", decodeDocumentType(headerSection.documentType));
            version.addAttribute(type);

            Attribute minorVersion = new Attribute("version-minor", Integer.toString(headerSection.documentMinorVersion));
            version.addAttribute(minorVersion);

            el.appendChild(version);
        }
        //////////////// options
        {
            Element options = new Element("options");
            {
                Element hyphenTable = new Element("hyphen-table");
                Attribute hyphenTableName = new Attribute("name", decodeHyphenTable(headerSection.hyphenTable));
                hyphenTable.addAttribute(hyphenTableName);
                options.appendChild(hyphenTable);
            }
            {
                Element dictionaryRefs = new Element("transcriptor");
                List<Element> dictionnaryRefsList = splitDictionnaryRefs(headerSection.dictionaryRefs);
                for (Element dictionaryRef : dictionnaryRefsList) {
                    dictionaryRefs.appendChild(dictionaryRef);

                }
                options.appendChild(dictionaryRefs);
            }
            {
                Element numbering = new Element("numbering");
                {
                    Element position = new Element("position");
                    position.addAttribute(new Attribute("even", decodeNumbering(headerSection.numberingEven)));
                    position.addAttribute(new Attribute("odd", decodeNumbering(headerSection.numberingOdd)));
                    position.addAttribute(new Attribute("reference-even", decodeNumbering(headerSection.numberingRefEven)));
                    position.addAttribute(new Attribute("reference-odd", decodeNumbering(headerSection.numberingRefOdd)));
                    numbering.appendChild(position);
                }
                {
                    Element startPage = new Element("start-page");

                    startPage.addAttribute(new Attribute("number", Integer.toString(headerSection.numberingFirstPage)));
                    numbering.appendChild(startPage);
                }

                options.appendChild(numbering);
            }

            el.appendChild(options);
        }

        //////////////// contents
        {
            Element contents = new Element("contents");
            for (Element e : splitDocumentContent(contentsSection.text, charset)) {
                contents.appendChild(e);
            }
            el.appendChild(contents);

        }

        //////////////// style
        {
            Element styles = new Element("styles");

            int styleCount = styleSection.styleStructureHeader.number;
            byte[][] styleNames = styleSection.names;
            byte[][] styleDefinitions = styleSection.definitions;
            short[] styleOrders = styleSection.order;

            for (int i = 0; i < styleCount; i++) {
                Element style = new Element("style");

                String styleName = bytesToString(styleNames[i]);
                style.addAttribute(new Attribute("name", styleName));
                int definitionNum = styleOrders[i];

                style.addAttribute(new Attribute("order", Integer.toString(definitionNum)));

                Element before = new Element("before");
                byte insertBefore[] = styleDefinitions[definitionNum * 2];
                for (Element e : splitInsertion(insertBefore, charset)) {
                    before.appendChild(e);
                }
                style.appendChild(before);

                Element after = new Element("after");
                byte insertAfter[] = styleDefinitions[definitionNum * 2 + 1];
                for (Element e : splitInsertion(insertAfter, charset)) {
                    after.appendChild(e);
                }
                style.appendChild(after);

                styles.appendChild(style);
            }
            el.appendChild(styles);
        }
        return el;
    }

    /**
     * NOT USABLE. Import a XML representation of a DBT document to the in-memory
     * representation. 
     *
     * @param xmlDocument the XML document
     * @return the in-memory representation. See class header for details.
     * @throws Exception
     */
    public static BinaryDeEncoder.BxdDocument fromXml(Document xmlDocument, String charset) throws Exception {

        BinaryDeEncoder.BxdDocument bxdDocument = new BinaryDeEncoder.BxdDocument();
        Element root = xmlDocument.getRootElement();
        {
            /**
             * Header
             */
//            bxdDocument.headerSection.documentMajorVersion;
//            bxdDocument.headerSection.documentMinorVersion;
//            bxdDocument.headerSection.documentType;
//            bxdDocument.headerSection.hyphenTable;
//            bxdDocument.headerSection.numberingEven;
//            bxdDocument.headerSection.numberingOdd;
//            bxdDocument.headerSection.numberingRefEven;
//            bxdDocument.headerSection.numberingRefOdd;
//            bxdDocument.headerSection.numberingFirstPage;
//            bxdDocument.headerSection.
        }
        {
            /**
             * Contents
             */
            Nodes contentsNodes = root.query("//contents/*");
            int count = contentsNodes.size();
            if (count > 0) {
                try (ByteArrayOutputStream baos = new ByteArrayOutputStream()) {
                    for (int contentIdx = 0; contentIdx < count; contentIdx++) {
                        Node contentNode = contentsNodes.get(contentIdx);
                        if (contentNode instanceof Element) {
                            Element contentEl = (Element) contentNode;
                            String textString = contentEl.getValue();
                            switch (contentEl.getLocalName()) {
                                case "text":
                                    if (textString != null) { // pass silently
                                        byte[] byteString = internalStringToByteArray(textString, charset);
                                        baos.write(byteString, 0, byteString.length);
                                    }
                                    break;
                                case "command":
                                    if (textString == null) {
                                        throw new Exception("command content cannot be empty");
                                    }
                                    byte[] byteCommand = internalStringToByteArray(textString, charset);
                                    baos.write((byte) 0x1c);
                                    baos.write(byteCommand, 0, byteCommand.length);
                                    baos.write((byte) 0x1f);
                                    break;
                                case "style-start":
                                    String startStyleName = contentEl.getAttributeValue("name");
                                    if (startStyleName == null) {
                                        throw new Exception("style-start name attribute must be defined");
                                    }
                                    byte[] byteStyleStart = internalStringToByteArray("es~" + startStyleName, charset);
                                    baos.write((byte) 0x1c);
                                    baos.write(byteStyleStart, 0, byteStyleStart.length);
                                    baos.write((byte) 0x1f);
                                    break;
                                case "style-end":
                                    String endStyleName = contentEl.getAttributeValue("name");
                                    if (endStyleName == null) {
                                        throw new Exception("style-end name attribute must be defined");
                                    }
                                    byte[] byteStyleEnd = internalStringToByteArray("ee~" + endStyleName, charset);
                                    baos.write((byte) 0x1c);
                                    baos.write(byteStyleEnd, 0, byteStyleEnd.length);
                                    baos.write((byte) 0x1f);
                                    break;
                                default:
                                    throw new Exception("unknown content element named " + contentEl.getLocalName());

                            }

                        }
                    }

                    bxdDocument.contentsSection.text = baos.toByteArray();
                }
            }
        }

        {
            /**
             * Style
             */
            Nodes qr = root.query("//style");
            int styleCount = qr.size();
            if (styleCount > 0) {

                short orderArray[] = new short[styleCount];
                byte namesArray[][] = new byte[styleCount][];
                byte definitionsArray[][] = new byte[styleCount * 2][];

                // for each style
                for (int styleIdx = 0; styleIdx < styleCount; styleIdx++) {
                    Node xmlStyleNode = qr.get(styleIdx);
                    if (xmlStyleNode instanceof Element) {
                        Element xmlStyleElement = (Element) xmlStyleNode;

                        // style order
                        String styleOrderString = xmlStyleElement.getAttribute("order").getValue();
                        int styleOrder = Integer.parseInt(styleOrderString);
                        orderArray[styleIdx] = integerToShort(styleOrderString);

                        // style name
                        String styleName = xmlStyleElement.getAttribute("name").getValue();
                        namesArray[styleIdx] = internalStringToByteArray(styleName, charset);

                        // style definitions
                        for (int beforeafter = 0; beforeafter < 2; beforeafter++) {
                            Nodes beforeDefinitionNodes;
                            if (beforeafter == 0) {
                                beforeDefinitionNodes = xmlStyleElement.query("before/*");
                            } else {
                                beforeDefinitionNodes = xmlStyleElement.query("after/*");
                            }
                            List<Byte> beforeBytes = new ArrayList<>();
                            for (int j = 0; j < beforeDefinitionNodes.size(); j++) {
                                Node bn = beforeDefinitionNodes.get(j);
                                if (bn instanceof Element) {
                                    String commandString = ((Element) bn).getValue();
                                    beforeBytes.add((byte) 0x1c);
                                    for (byte bbb : internalStringToByteArray(commandString, charset)) {
                                        beforeBytes.add(bbb);
                                    }
                                    beforeBytes.add((byte) 0x1f);
                                }
                            }
                            byte definitionArrayBefore[] = new byte[beforeBytes.size()];
                            for (int j = 0; j < beforeBytes.size(); j++) {
                                definitionArrayBefore[j] = beforeBytes.get(j);
                            }
                            if (beforeafter == 0) {
                                definitionsArray[2 * styleOrder] = definitionArrayBefore;
                            } else {
                                definitionsArray[2 * styleOrder + 1] = definitionArrayBefore;
                            }
                        }
                    }
                }
                bxdDocument.styleSection.names = namesArray;
                bxdDocument.styleSection.order = orderArray;
                bxdDocument.styleSection.definitions = definitionsArray;
            }
        }

        {
            /**
             * Printer
             */
            bxdDocument.printerSection.data = new byte[]{};
        }
        return bxdDocument;
    }

}
