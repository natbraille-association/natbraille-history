/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.natbraille.cli;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.Properties;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import org.apache.commons.cli.CommandLine;
import static org.natbraille.cli.NatCommandLine.CL_FILTER_OPTIONS_FILENAME;
import static org.natbraille.cli.NatCommandLine.CL_SET;
import org.natbraille.core.filter.NatFilterException;
import org.natbraille.core.filter.options.NatFilterOption;
import org.natbraille.core.filter.options.NatFilterOptions;
import org.natbraille.core.gestionnaires.GestionnaireErreur;
import org.natbraille.core.gestionnaires.LogLevel;
import org.natbraille.core.gestionnaires.MessageContents;
import org.natbraille.core.gestionnaires.MessageKind;

/**
 *
 * @author vivien
 */
public class CliFilterOptionTools {
    
    /**
     * Get fuzzy option name match. Currently, fuzzy = equalsIgnoreCase.
     * @param approximateName
     * @return 
     */
    protected static String getNatFilterOptionNameFuzzy(String approximateName) {
        for (NatFilterOption o : NatFilterOption.values()) {
            if (o.name().equalsIgnoreCase(approximateName)) {
                return o.name();
            }
        }
        return null;
    }

    /**
     * get filter options in command line, ie --set name=value.
     *
     * @param ge
     * @param cmd
     * @return
     * @throws NatFilterException
     */
    protected static NatFilterOptions getDirectFilterOptions(GestionnaireErreur ge, CommandLine cmd) throws NatFilterException {
        Properties p = new Properties();

        if (cmd.hasOption(CL_SET)) {
            for (String nameEqualsValue : cmd.getOptionValues(CL_SET)) {

                Matcher matcher = Pattern.compile("(.*)=(.*)").matcher(nameEqualsValue);
                while (matcher.find()) {
                    String nfoApproximateName = matcher.group(1);
                    String nfoName = getNatFilterOptionNameFuzzy(nfoApproximateName);
                    if (nfoName == null) {
                        ge.afficheMessage(MessageKind.ERROR, MessageContents.Tr("{0} is not a valid option name").setValue(nfoApproximateName), LogLevel.SILENT);
                        ge.afficheMessage(MessageKind.ERROR, MessageContents.Tr("did you mean.. {0} ?").setValue(NearestFilterOptionName.find(nfoApproximateName, 10,10)), LogLevel.SILENT);
                        
                        System.exit(1);
                    } else {
                        String nfoValue = matcher.group(2);
                        p.setProperty(nfoName, nfoValue);
                        ge.afficheMessage(MessageKind.INFO, MessageContents.Tr("set filter option {0} to value : {1}").setValues(nfoName, nfoValue), LogLevel.VERBOSE);
                    }
                }
            }
        }

        NatFilterOptions nfo = new NatFilterOptions(p);

        return nfo;

    }

    /**
     * get the filter filenames and set nat filter options accordingly. If
     * multiple values are founds, filter files are loaded in that order
     *
     * @param ge
     * @param cmd
     * @return
     * @throws Exception
     */
    protected static NatFilterOptions getFileFilterOptions(GestionnaireErreur ge, CommandLine cmd) throws Exception {
        NatFilterOptions natFilterOptions = new NatFilterOptions();

        if (cmd.hasOption(CL_FILTER_OPTIONS_FILENAME)) {
            for (String s : cmd.getOptionValues(CL_FILTER_OPTIONS_FILENAME)) {
                try {
                    natFilterOptions.setOptions(new FileInputStream(new File(s)));
                    MessageContents mc = MessageContents.Tr("override with options in file ''{0}''").setValue(s);
                    ge.afficheMessage(MessageKind.INFO, mc, LogLevel.VERBOSE);
                } catch (IOException | NatFilterException e) {
                    MessageContents mc = MessageContents.Tr("error reading options from file : ''{0}''").setValue(s);
                    ge.afficheMessage(MessageKind.ERROR, mc, LogLevel.NORMAL);
                }
            }
        }
        return natFilterOptions;
    }


}
