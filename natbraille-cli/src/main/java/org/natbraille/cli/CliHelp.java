package org.natbraille.cli;

import java.util.List;
import org.apache.commons.cli.HelpFormatter;
import org.natbraille.core.filter.options.NatFilterOption;
import org.natbraille.core.filter.options.NatFilterOptionDetail;

public class CliHelp extends HelpFormatter {
    /*
     public CliHelp() {
     this.setDescPadding(1);
     this.setLeftPadding(3);
     this.setWidth(120);
     }

     */

    /**
     * One String NatFilterOption help
     *
     * @param nfo
     * @return
     */
    public static String helpOption(NatFilterOption nfo) {
        StringBuilder sb = new StringBuilder();
        NatFilterOptionDetail detail = nfo.getDetail();
        sb.append(nfo.name())
                .append(" [")
                .append(detail.type())
                .append("] (")
                .append(detail.defaultValue())
                .append(") : ")
                .append(detail.comment());

        return sb.toString();
    }

    /**
     * Display possible match for misspelled nat filter option names.
     *
     * @param mysteryName
     * @return
     */
    public static String helpMysteryOption(String mysteryName) {
        StringBuilder sb = new StringBuilder();
        List<NatFilterOption> possibleMatches = NearestFilterOptionName.find(mysteryName, 10,10);
        for (NatFilterOption possibleMatch : possibleMatches) {
            sb.append(helpOption(possibleMatch));
            sb.append("\n");
        }

        return sb.toString();
    }
    
    static void helpOptions() {
        
        for (NatFilterOption natFilterOption : NatFilterOption.values()) {            
            System.out.println(helpOption(natFilterOption));
        }
    }
    static void helpOptions(String option) {
        List<NatFilterOption> natFilterOptions = NearestFilterOptionName.find(option, null, null);
        for (NatFilterOption natFilterOption : natFilterOptions) {            
            System.out.println(helpOption(natFilterOption));
        }
    }

    
}
