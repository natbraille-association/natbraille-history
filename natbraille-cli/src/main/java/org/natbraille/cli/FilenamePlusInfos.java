package org.natbraille.cli;

import java.nio.charset.Charset;
import java.nio.file.Path;
import java.nio.file.Paths;
import org.natbraille.brailletable.BrailleTable;
import org.natbraille.brailletable.BrailleTables;

/**
 * handles file stings in the command line.
 * see constructor for filename and data format content
 * @author vivien
 * 
 */
public class FilenamePlusInfos {

	/**
	 * the non decoded file description
	 */
	private String constructionCliFile;
	
	/**
	 * decoded filePath
	 */
	private Path filePath;
	/**
	 * decoded charset
	 */
	private Charset charset;
	/**
	 * decoded braille table
	 */
	private BrailleTable brailleTable;


	private String getConstructionCliFile() {
		return constructionCliFile;
	}

	private void setConstructionCliFile(String constructionCliFile) {
		this.constructionCliFile = constructionCliFile;
	}

	/**
	 * get the decoded path of the file 
	 * @return
	 */
	public Path getFilePath() {
		return filePath;
	}

	private void setFilePath(Path filePath) {
		this.filePath = filePath;
	}

	/**
	 * get the decoded charset of the file
	 * @return
	 */
	public Charset getCharset() {
		return charset;
	}

	private void setCharset(Charset charset) {
		this.charset = charset;
	}

	/**
	 * get the decoded braille table of the file
	 * @return
	 */
	public BrailleTable getBrailleTable() {
		return brailleTable;
	}

	public void setBrailleTable(BrailleTable brailleTable) {
		this.brailleTable = brailleTable;
	}

	/**
	 * cliFile has format : ( '-' | path ) [,info]* where 'info' is (currently)
	 * the encoding used in file at 'path' (could be used to attach any metadata
	 * to the path, doctype brailletable...)
	 * 
	 * @param cliFile
	 * @throws Exception 
	 */
	public FilenamePlusInfos(String cliFile) throws Exception{
		setConstructionCliFile(cliFile);
		try  {
			decode(cliFile);
		} catch (Exception e){
			throw new Exception("cannot decode file infos for '"+cliFile+"'",e);
		}
	}

	/**
	 * the entry is a charset name string
	 * @param entry
	 * @return the matching charset
	 */
	private Charset matchCharset(String entry) {
		return Charset.forName(entry);			
	}
	
	/**
	 * decode and set object with the given entry. 
	 * @param entry
	 * @return true if the entry has been successfully decoded
	 * false if the entry could not be matched with an info.
	 */
	private boolean decodeEntry(String entry){

		boolean foundSomething = false;
	
		// could be a charset encoding ?	
		try {
			Charset c = matchCharset(entry);
			setCharset(c);
			foundSomething = true;
//			System.err.println("decode charset"+c);
		} catch (java.nio.charset.UnsupportedCharsetException e){
			//e.printStackTrace();
			// not a charset
		}

		if (!foundSomething){
		try {
			BrailleTable b = BrailleTables.forName(entry);
			setBrailleTable(b);
			foundSomething = true;
	//		System.err.println("decode braille table"+b);
		} catch(Exception e){
		//	e.printStackTrace();
			// not a standard braille table
		}
		
			
		}
		return foundSomething;
		
	}
	
	/**
	 * extract filename and infos from clifile and set the object
	 * @param cliFile
	 * @throws Exception
	 */
	private void decode(String cliFile) throws Exception {
		
		
		String entries[] = cliFile.split(",");
		
		try {
			setFilePath(Paths.get(entries[0]));
		} catch (Exception e){
			throw new Exception("cannot get a filename in '"+cliFile+"'",e);
		}
		
		// decode each entry
		for (int i=1;i<entries.length;i++){			
			boolean decoded = decodeEntry(entries[i]);
			if (!decoded){
				throw new Exception("cannot decode parameter '"+entries[i]+"' : does not seem to be a text encoding nor a braille table");
			}
		}		
	}

	public String toString(){
		return getConstructionCliFile();
	}


	public static void main(String args[]) throws Exception{
		FilenamePlusInfos aa = new FilenamePlusInfos("/tlo/ororor,UTF-8");
		System.out.println(aa);
		System.out.println(aa.getFilePath());
		System.out.println(aa.getCharset());
	}
}
