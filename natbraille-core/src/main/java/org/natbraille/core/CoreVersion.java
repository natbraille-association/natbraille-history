package org.natbraille.core;

import com.jcabi.manifests.Manifests;
import java.util.jar.Manifest;

/**
 * uses /revision/core resources files to determine svn version and status
 *
 * @author vivien
 *
 */
public class CoreVersion {

    String compilationDate;
    String compilationHostname;

    private String get(String whatKey) throws Exception {
        Manifest man = new Manifest();
                
        String what = null;
        try {
            what = Manifests.read(whatKey);
        } catch (Exception e) {
            throw new Exception("cannot read key '" + whatKey + "' in manifest ", e);
        }
        return what;
    }

    private String getCompilationDate() throws Exception {
        return get("Implementation-Build");
    }

    private String getCompilationHostname() throws Exception {
        return get("Implementation-Build");
    }

    public String getDescription() throws Exception {
        StringBuilder sb = new StringBuilder();
        sb.append(" built ");
        sb.append(getCompilationDate());
        sb.append(" on ");
        sb.append(getCompilationHostname());
        sb.append("\n");
        for (String nn : new String[]{"AdobeIP", "Ant-Version", "Archiver-Version", "Bnd-LastModified", "Build-Jdk", "BuildDate", "Built-By", "Built-On", "Bundle-ActivationPolicy", "Bundle-Activator", "Bundle-Description", "Bundle-DocURL", "Bundle-License", "Bundle-ManifestVersion", "Bundle-Name", "Bundle-RequiredExecutionEnvironment", "Bundle-SymbolicName", "Bundle-Vendor", "Bundle-Version", "Class-Path", "Created-By", "Embed-Dependency", "Export-Package", "Extension-Name", "Fragment-Host", "Implementation-Build", "Implementation-Debug", "Implementation-EngBuild", "Implementation-Major", "Implementation-Micro", "Implementation-Minor", "Implementation-Title", "Implementation-URL", "Implementation-Vendor", "Implementation-Vendor-Id", "Implementation-Version", "Import-Package", "Include-Resource", "JCabi-Build", "JCabi-Date", "JCabi-Version", "Main-Class", "Manifest-Version", "Package", "Private-Package", "Project-Name", "Require-Bundle", "Sealed", "Specification-Title", "Specification-Vendor", "Specification-Version", "Tool", "Version", "X-Compile-Source-JDK", "X-Compile-Target-JDK", "url"}){
            
            sb.append(nn);
            sb.append(" : ");
            sb.append(Manifests.read(nn));
            
            sb.append("\n");
            
       }
        return sb.toString();

    }

    @Override
    public String toString() {
        try {
            return getDescription();
        } catch (Exception ex) {
            return ex.getMessage();
        }

    }
}