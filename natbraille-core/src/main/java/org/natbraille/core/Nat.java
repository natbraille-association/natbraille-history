/*
 * NatBraille - An universal Translator
 * Copyright (C) 2005 Bruno Mascret
 * Contact: bmascret@free.fr
 * 
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */

package org.natbraille.core;

//import org.natbraille.core.commmandLine.NatCommandLine;
import org.natbraille.core.tools.LibreOfficeTools;

import org.natbraille.core.gestionnaires.AfficheurAnsiConsole;
import org.natbraille.core.gestionnaires.GestionnaireErreur;
import org.natbraille.core.gestionnaires.LogLevel;
import org.natbraille.core.gestionnaires.MessageContents;
import org.natbraille.core.gestionnaires.MessageKind;


import org.natbraille.core.document.NatDocument;
import org.natbraille.core.filter.NatFilterConfigurator;
import org.natbraille.core.filter.NatTransformerFilterResolver;
import org.natbraille.core.filter.options.NatFilterOption;
import org.natbraille.core.filter.options.NatFilterOptions;
import org.natbraille.core.filter.transcriptor.Transcriptor;

//TODO : moteur+web    :    histoire de djidjo avec mise en page et modif nb lignes
//TODO : moteur(+web?)         :   txt+mathml produit différement par mathtype + tdl
//TODO : moteur         :    inverse
//TODO : moteur            :   passage des tests de non reg

//TODO : web             :    https

//TODO : java gui 		

public class Nat {

    private static final CoreVersion coreVersion = new CoreVersion();



    public static CoreVersion getCoreVersionDesc() {
	return coreVersion;
    }

    /**
     * @param args
     * @throws Exception
     */
    public static void main(String[] args) throws Exception {
	// saxon 9 he du trunk :  9.2.02
	//System.exit(0);
	// regle le probleme du parser par défaut merdique.
	System.setProperty("javax.xml.parsers.DocumentBuilderFactory",
			   "org.apache.xerces.jaxp.DocumentBuilderFactoryImpl");


	System.err.println("natbraille "+coreVersion.toString());

	GestionnaireErreur ge = new GestionnaireErreur();
		
	AfficheurAnsiConsole afficheurConsole = new AfficheurAnsiConsole(LogLevel.NORMAL);
	int s = 5;
	afficheurConsole.setLogLevel(s);
	ge.addAfficheur(afficheurConsole);

	MessageContents mc = MessageContents.Tr("I am working").setValue(s);

	ge.afficheMessage(MessageKind.INFO,mc,LogLevel.DEBUG);
	MessageContents mc2 = MessageContents.Tr("filter {0} starting").setValue("TEXTE");				
	ge.afficheMessage(MessageKind.INFO,mc2,LogLevel.DEBUG);


	NatDocument indoc = new NatDocument();
	indoc.setString("voila la traduc");

	NatFilterOptions nfo = new NatFilterOptions();
	nfo.setOption(NatFilterOption.debug_document_write_temp_file,"true");
	nfo.setOption(NatFilterOption.debug_dyn_xsl_write_temp_file,"true");
	nfo.setOption(NatFilterOption.debug_document_show,"true");
	NatTransformerFilterResolver ntfr = new NatTransformerFilterResolver(nfo, ge);
	NatFilterConfigurator factory = new NatFilterConfigurator().set(nfo).set(ge).set(ntfr);
	Transcriptor transcriptor = new Transcriptor(factory);
    
	NatDocument outDoc = transcriptor.run(indoc);
	System.out.println(outDoc.getString());

	if (!NatFilterOption.testOptionDetails(false)){			
	    System.exit(0);
	}
	//		NatCommandLine.main(args);
	LibreOfficeTools.stopServer();
		
    }

    public static String getLicence(String string, String string2) {
	// TODO Auto-generated method stub
	return "GPL v2";
    }

}
