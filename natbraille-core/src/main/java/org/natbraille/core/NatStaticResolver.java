/*
 * NatBraille - An universal Translator
 * Copyright (C) 2005 Bruno Mascret
 * Contact: bmascret@free.fr
 * 
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */
package org.natbraille.core;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.URI;
import java.net.URISyntaxException;
import java.nio.file.Files;

import org.natbraille.core.document.Utils;
import org.xml.sax.EntityResolver;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;

/**
 * The static resolver retrieves web cached files, xsl and options files stored
 * as resources.
 *
 * <p>
 * REMINDER from :
 * http://stackoverflow.com/questions/6298400/class-getresource-and-classloader-getsystemresource-is-there-a-reason-to-prefer
 * <p>
 * There are several ways to load resources, each with a slightly different
 * meaning -
 *
 * <p>
 * ClassLoader.getSystemResource() uses the system classloader. This uses the
 * classpath that was used to start the program. If you are in a web container
 * such as tomcat, this will NOT pick up resources from your WAR file.
 * <p>
 * Class.getResource() prepends the package name of the class to the resource
 * name, and then delegates to its classloader. If your resources are stored in
 * a package hierarchy that mirrors your classes, use this method.
 * <p>
 * Class.getClassLoader().getResource() delegates to its parent classloader.
 * This will eventually search for the resource all the way upto the system
 * classloader.
 *
 * <p>
 * http://stackoverflow.com/questions/2653322/getresourceasstream-not-loading-resource-in-webapp
 *
 *
 * @author vivien
 */
public class NatStaticResolver implements EntityResolver {

    private static void dbg(String string) {
//         System.err.println("RSLVDBG =" + (new Date().toString()) + "= " + string);
    }
    // TODO unifier, remplacer toutes les chaines
    public static final String XSL_NAT_TMP_PARAMS_ALL = "nat://temp/xsl/paramsAll.xsl";
    public static final String XSL_NAT_TMP_PARAMS_MEP = "nat://temp/xsl/paramsMEP.xsl";
    public static final String XSL_NAT_TMP_XSL_MEP = "nat://temp/xsl/xsl-mep.xsl";
    public static final String XSL_NAT_TMP_HYPHENS = "nat://temp/xsl/hyphens.xsl";

    /**
     * XSL Filter for page Layout
     */
    // private static final String LAYOUT_FILTER =
    // ConfigNat.getInstallFolder()+"xsl/miseEnPage.xsl";
    public static final String NO_LAYOUT_FILTER = "nat://system/xsl/no-mep.xsl";
    public static final String LAYOUT_FILTER = "nat://system/xsl/miseEnPage.xsl";
    /**
     * XSL Filter for no page layout and tag preservation
     */
    public static final String NO_LAYOUT_PRESERVE_TAG_FILTER = "nat://system/xsl/no-mep-preserve-tag.xsl";
    public static final String HTTP_CACHE_DIR = "webcache/";

    // scheme
    public static final String NAT_SCHEME = "nat";

    // authority
    public static final String NAT_SYSTEM_AUTHORITY = "system";
    public static final String NAT_TMP_AUTHORITY = "temp";
    public static final String NAT_DOCUMENT_AUTHORITY = "document";
    public static final String SYSTEM_BASE_URL = NAT_SCHEME + "://" + NAT_SYSTEM_AUTHORITY + "/";
    public static final String TMP_BASE_URL = NAT_SCHEME + "://" + NAT_TMP_AUTHORITY + "/";
    public static final String DOCUMENT_BASE_URL = NAT_SCHEME + "://" + NAT_DOCUMENT_AUTHORITY + "/";
    public static final String WINDOB_DTD_PUBLIC_ID = "-//NATBRAILLE//DTD WINDOB 1.0";
    public static final String WINDOB_DTD_SYSTEM_ID = SYSTEM_BASE_URL + "xsl/mmlents/windob.dtd";
    public static final String SYSTEM_BRAILLETABLES_URI = "nat://system/xsl/tablesEmbosseuse/";

    /// ---------------------------
    /**
     * get the resolver as a {@link  org.sax.EntityResolver} instance.
     *
     * @return an instance
     */
    public static EntityResolver getEntityResolverInstance() {
        return new NatStaticResolver();
    }

    @Override
    public InputSource resolveEntity(String publicId, String systemId) throws SAXException, IOException {
        // [scheme:][//[user-info@]host[:port]][path][?query][#fragment]
        // authority : [user-info@]host[:port]

        String contentsg = this.getClass() + " :: entity" + " PUBLIC " + publicId + " SYSTEM " + systemId;

        dbg("== resolveEntity == " + contentsg);

        InputSource entityInputSource = null; // and input source the localURI

        try {
            URI systemIdURI = new URI(systemId);
            switch (systemIdURI.getScheme()) {
                case "nat":
                    switch (systemIdURI.getAuthority()) {
                        case "system":
                            InputStream is = getNatResourceAsInputStream(systemIdURI);
                            if (is != null) {
                                entityInputSource = new InputSource(is);
                            }
                            break;
                        case "temp":
                            dbg("Cannot resolve dynamic entity " + systemId);
                            break;
                    }
                    break;
                case "http":
                    InputStream is = getHttpResourceAsInputStream(systemIdURI);
                    if (is != null) {
                        entityInputSource = new InputSource(is);
                    }
                    break;
            }
        } catch (URISyntaxException | IOException e) {
            dbg("Exception while trying to resolve an entity");
            e.printStackTrace();
        }

        if (entityInputSource == null) {
            System.err.println("failed to resolve : " + contentsg);
        }

        return entityInputSource;
    }

    /**
     * get a cached version of http files.
     *
     * @param systemIdURI uri of the form http://authori.ty/path
     * @return an InputStream to the cache, null if none
     * @throws IOException
     */
    private InputStream getHttpResourceAsInputStream(URI systemIdURI) throws IOException {

        /**
         * try to retrieve cached http from resources
         */
        // determine the resource name
        String resourceName = "webcache/" + systemIdURI.getAuthority() +  systemIdURI.getPath();

        // get the stream;
        InputStream is = NatStaticResolver.class.getResourceAsStream(resourceName);

        if (is != null) {
            return is;
        } else {
            System.err.println("We have no cache for "+resourceName);
        }

        /**
         * try to store and retrieve cached http from temp file
         */
        // create the cache tmp dir if not exists
        File savePathRoot = new File(System.getProperty("java.io.tmpdir"), getClass().getCanonicalName());
        if (!savePathRoot.exists()) {
            Files.createDirectory(savePathRoot.toPath());
        }

        // determine the cache tmp file name
        File f = new File(savePathRoot, systemIdURI.getAuthority());
        for (String s : systemIdURI.getPath().split("/")) {
            f = new File(f, s);
        }

        // download and write if it does not exist
        if (!f.exists()) {
            Utils.URItoFile(systemIdURI, f.toURI());
        }

        // return if exists
        if (f.exists()) {
            return new FileInputStream(f);
        }

        /**
         * failed, return null
         */
        return null;
    }

    /**
     * Get the resource corresponding to a natbraille uri.
     *
     * @param systemIdURI uri of the form nat://system//PATH.
     * @return an InputStream to the resource, null if none
     */
    public static InputStream getNatResourceAsInputStream(URI systemIdURI) {

        // determine the resource name wich is the path withouth leading "/"
        String resourceName = systemIdURI.getPath().substring(1);

        dbg("resource Name is '" + resourceName + "'");
        // get the stream;
        InputStream is = NatStaticResolver.class.getResourceAsStream(resourceName);

        if (is != null) {
            return is;
        }

        /**
         * failed, return null
         */
        return null;
    }
}
