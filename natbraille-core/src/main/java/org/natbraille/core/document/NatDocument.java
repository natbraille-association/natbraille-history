/*
 * NatBraille - An universal Translator
 * Copyright (C) 2005 Bruno Mascret
 * Contact: bmascret@free.fr
 * 
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */
package org.natbraille.core.document;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.nio.ByteBuffer;
import java.nio.channels.Channels;
import java.nio.channels.ReadableByteChannel;
import java.nio.charset.Charset;
import java.nio.file.Path;
import javax.xml.transform.TransformerException;
import org.apache.tika.config.TikaConfig;
import org.apache.tika.detect.Detector;
import org.apache.tika.io.TikaInputStream;
import org.apache.tika.metadata.Metadata;
import org.apache.tika.mime.MediaType;
import org.apache.tika.mime.MimeTypes;
import org.apache.tika.parser.txt.CharsetDetector;
import org.apache.tika.parser.txt.CharsetMatch;
import org.natbraille.brailletable.BrailleTable;
import org.natbraille.core.tools.NatTempFiles;
import org.w3c.dom.Document;
import org.xml.sax.XMLReader;

/**
 * <p>
 * NatDocument encapsulate documents used by nat. NatDocuments data can be set
 * from Path, InputStream, Document and ByteBuffer ; its data can be retrieved
 * as Temporary File, InputStream, Document, ByteBuffer (and SAXSource).
 *
 * <p>
 * Only one of the data setter can be called on a given NatDocument. Setting
 * data multiple times will result in a NatDocumentException.
 *
 * <p>
 * text encoding (charset) must be consistent.
 *
 * @author vivien
 *
 */
public class NatDocument {

    /**
     * braille tabled used when document is in braille
     */
    private BrailleTable brailletable = null;

    /**
     * get braille tabled in usage in this document, null if not set
     *
     * @return
     */
    public BrailleTable getBrailletable() {
        return brailletable;
    }

    /**
     * set braille tabled in usage in this document
     *
     * @param brailletable
     */
    public void setBrailletable(BrailleTable brailletable) {
        this.brailletable = brailletable;
    }

    /**
     * data storage as a ByteBuffer
     */
    private ByteBuffer bytebuffer = null;

    /**
     * data storage as a String
     */
    private String string = null;
    /**
     * data storage as Document
     */
    private Document document = null;

    /**
     * data storage as a Path
     */
    private Path path = null;

    /**
     * user provided content type
     */
    private String contentType = null;
    private Boolean forceContentType = false;

    /**
     * user provided encoding in use within the NatDocument
     */
    private Charset charset = null;
    /**
     * default encoding when unspecified and unable to guess
     */
    private static Charset defaultCharset = Charset.forName("UTF-8");
    /**
     * when true, the charset is prefered over the guessed charset ; when false,
     * the charset is used only for fallback; if both are null, the default
     * charsetis used
     */
    private Boolean forceCharset = false;
    private String originalFileName = null;

    public String getOriginalFileName() {
        return originalFileName;
    }

    public void setOriginalFileName(String originalFileName) {
        this.originalFileName = originalFileName;
    }

    /**
     * constructor
     */
    public NatDocument() {

    }

    private Charset getCharset() {
        return charset;
    }

    private void setCharset(Charset charset) {
        this.charset = charset;
    }

    private Boolean getForceCharset() {
        return forceCharset;
    }

    private void setForceCharset(Boolean forceCharset) {
        this.forceCharset = forceCharset;
    }

    /**
     * always use the provided charset : never try to guess
     *
     * @param charset
     */
    public void forceCharset(Charset charset) {
        setCharset(charset);
        setForceCharset(true);
    }

    /**
     * the provided charset will be used whenever automatic detection fails
     *
     * @param charset
     */
    public void suggestCharset(Charset charset) {
        setCharset(charset);
        setForceCharset(false);
    }

    private String getContentType() {
        return contentType;
    }

    private void setContentType(String contentType) {
        this.contentType = contentType;
    }

    private boolean getForceContentType() {
        return forceContentType;
    }

    private void setForceContentType(Boolean force) {
        this.forceContentType = force;
    }

    /**
     * always use the provided charset : never try to guess
     *
     * @param charset
     */
    public void forceContentType(String contentType) {
        setContentType(contentType);
        setForceContentType(true);
    }

    /**
     * the provided charset will be used whenever automatic detection fails
     *
     * @param charset
     */
    public void suggestContentType(String contentType) {
        setContentType(contentType);
        setForceContentType(false);
    }

    /*
     AutoDetectReader 
     An input stream reader that automatically detects the character encoding to be used for converting bytes to characters.
     CompositeDetector 	
     Content type detector that combines multiple different detection mechanisms.
     DefaultDetector 	
     A composite detector based on all the Detector implementations available through the service provider mechanism.
     EmptyDetector 	
     Dummy detector that returns application/octet-stream for all documents.
     MagicDetector 	
     Content type detection based on magic bytes, i.e.
     NameDetector 	
     Content type detection based on the resource name.
     TextDetector 	
     Content type detection of plain text documents.
     TextStatistics 	
     Utility class for computing a histogram of the bytes seen in a stream.
     TypeDetector 	
     Content type detection based on a content type hint.
     XmlRootExtractor 	
     Utility class that uses a SAXParser to determine the namespace URI and local name of the root element of an XML file.
     */
    /**
     * try to guess charset used for encoding using the bytebuffer , document
     * properties, and url
     *
     * @return
     */
    public Charset guessCharset() {

        Charset foundCharset = null;

        try {
            if ((bytebuffer != null) || (path != null)) {
                // use tika to detect charset
                CharsetDetector cdetector = new CharsetDetector();
                byte[] b = null;
                if (bytebuffer != null) {
                    b = bytebuffer.array();
                } else {
                    b = Utils.toByteBuffer(path).array();
                }
                cdetector.setText(b);
                CharsetMatch match = cdetector.detect();
                foundCharset = Charset.forName(match.getName());
            } else if (string != null) {
				// too early or too late
                // must be told explicity by user
            } else if (document != null) {
				// trust the document content (in doctype or 
                // kept while reading)
                foundCharset = Utils.guessDocumentCharset(document);
            }

        } catch (Exception e) {

        }

        return foundCharset;
    }

    /**
     *
     */
    public Charset getOrGuessCharset() {
        Charset resu = getCharset();
        if (!getForceCharset()) {
            try {
                resu = guessCharset();
            } catch (Exception e) {
            }
        }
        if (resu == null) {
            return defaultCharset;
        }
        return resu;
    }

    /**
     * @throws NatDocumentException
     *
     */
    public String getOrGuessContentType() throws NatDocumentException {
        String resu = getContentType();
        if (!getForceContentType()) {
            //			try {
            resu = guessContentType();
			//			} catch (Exception e){
            //			}
        }
        if (resu == null) {
            return MimeTypes.OCTET_STREAM;
        }
        return resu;
    }

    /**
     * ensure the object data has not been set
     *
     * @throws NatDocumentException if the data is already set
     */
    private void checkSet() throws NatDocumentException {

        boolean allNull = ((bytebuffer == null) && (document == null) && (string == null) && (path == null));
        if (!allNull) {
            throw new NatDocumentException(NatDocumentErrorCode.DOCUMENT_DATA_ALREADY_SET, "");
        }
    }

    /*
     * 
     */
    /**
     * sets object data as a bytebuffer if not already set
     *
     * @param bytebuffer
     * @throws NatDocumentException
     */
    public void setByteBuffer(ByteBuffer bytebuffer) throws NatDocumentException {
        checkSet();
        this.bytebuffer = bytebuffer;
    }

    public void setByteBuffer(byte[] bytes) throws NatDocumentException {
        setByteBuffer(ByteBuffer.wrap(bytes));
    }
    public void setByteArrayOutputStream(ByteArrayOutputStream baos) throws NatDocumentException{        
        setByteBuffer(baos.toByteArray());
    }
    /**
     * sets object data as a String if not already set
     *
     * @param string
     * @throws NatDocumentException
     */
    public void setString(String string) throws NatDocumentException {
        checkSet();
        this.string = string;
    }

    /**
     * sets object data as a Document if not already set
     *
     * @param document
     * @throws NatDocumentException
     */
    public void setDocument(Document document) throws NatDocumentException {
        checkSet();
        this.document = document;
    }

    /**
     * sets object data as a ByteBuffer from an Input Stream
     *
     * @param inputstream
     * @throws NatDocumentException
     */
    public void setInputstream(InputStream inputstream) throws NatDocumentException {
        checkSet();
		// inputstream is immediately stored in a bytebuffer in order to be
        // available
        // multiple times
        try {
            setByteBuffer(Utils.toByteBuffer(inputstream));
        } catch (IOException e) {
            throw new NatDocumentException(NatDocumentErrorCode.DOCUMENT_CANNOT_SET_DATA, "cannot use inputstream", e);
        }
    }

    /**
     * sets object data as a ByteBuffer from an Path
     *
     * @param path
     * @throws NatDocumentException
     */
    public void setPath(Path path) throws NatDocumentException {
        checkSet();
        /*		try {
         setInputstream(new FileInputStream(file));
         } catch (FileNotFoundException e) {
         throw new NatDocumentException(NatDocumentErrorCode.CANNOT_SET_DATA, "cannot use file " + file.getAbsolutePath(), e);
         }
         */
        this.path = path;
        setOriginalFileName(path.toString());
    }

    /**
     * get data as a ByteBuffer
     *
     * @return
     * @throws NatDocumentException
     */
    public ByteBuffer getBytebuffer() throws NatDocumentException {
        try {
            if (bytebuffer != null) {
                return bytebuffer;
            } else if (document != null) {
                return Utils.toByteBuffer(document, getOrGuessCharset());
            } else if (string != null) {
                return Utils.toByteBuffer(string, getOrGuessCharset());
            } else if (path != null) {
                return Utils.toByteBuffer(path);
            }
        } catch (Exception e) {
            throw new NatDocumentException(NatDocumentErrorCode.DOCUMENT_CANNOT_GET_DATA, "cannot get bytebuffer", e);
        }
        return null;
    }

    /**
     * get data as a String
     *
     * @return
     * @throws NatDocumentException
     * @throws IOException
     * @throws TransformerException
     */
    public String getString() throws NatDocumentException, IOException, TransformerException {

        try {
            if (string != null) {
                return string;
            } else if (bytebuffer != null) {
                return Utils.toString(bytebuffer, getOrGuessCharset());
            } else if (document != null) {
                return Utils.toString(document);
            } else if (path != null) {
                return Utils.toString(path, getOrGuessCharset());
            }
        } catch (IOException | TransformerException e) {
            throw new NatDocumentException(NatDocumentErrorCode.DOCUMENT_CANNOT_GET_DATA, "cannot get string", e);

        }
        return null;
    }

    /**
     * get data as a Document
     *
     * @param xmlreader
     * @return
     * @throws NatDocumentException
     */
    public Document getDocument(XMLReader xmlreader) throws NatDocumentException {
        try {
            if (document != null) {
                return document;
            } else if (bytebuffer != null) {
                return Utils.toDocument(bytebuffer, xmlreader);
            } else if (string != null) {
                return Utils.toDocument(string, getOrGuessCharset(), xmlreader);
            } else if (path != null) {
                return Utils.toDocument(path, xmlreader);
            }
        } catch (Exception e) {
            throw new NatDocumentException(NatDocumentErrorCode.DOCUMENT_CANNOT_GET_DATA, "cannot get document", e);

        }
        return null;
    }

    /**
     * get an InputStream on data
     *
     * @return
     * @throws NatDocumentException
     */
    public InputStream getInputstream() throws NatDocumentException {
        try {
            if (document != null) {
                return Utils.toInputStream(document, getOrGuessCharset());
            } else if (bytebuffer != null) {
                return Utils.toInputStream(bytebuffer);
            } else if (string != null) {
                return Utils.toInputStream(string, getOrGuessCharset());
            } else if (path != null) {
                return new FileInputStream(path.toFile());
            }
        } catch (TransformerException | IOException e) {
            throw new NatDocumentException(NatDocumentErrorCode.DOCUMENT_CANNOT_GET_DATA, "cannot get an inputstream to data", e);
        }
        return null;
    }

    /**
     * gets a temporary File object with the given suffix
     *
     * @param dir
     * @param prefix
     * @param suffix
     * @return
     * @throws NatDocumentException
     */
    public File reserveTmpFile(Path dir, String prefix, String suffix) throws NatDocumentException {
        File tmpFile;
        try {
            tmpFile = NatTempFiles.newFile(dir, prefix, suffix);
        } catch (IOException e) {
            throw new NatDocumentException(NatDocumentErrorCode.DOCUMENT_TMP_FILE_ERROR, "", e);
        }
        return tmpFile;
    }

    /**
     * write data to a temporary File object having the given suffix
     *
     * @param suffix
     * @return
     * @throws NatDocumentException
     */
    public File writeTmpFile(Path dir, String prefix, String suffix) throws NatDocumentException {
        File tmpFile = null;
        tmpFile = reserveTmpFile(dir, prefix, suffix);
        FileOutputStream fos;
        try {
            fos = new FileOutputStream(tmpFile);
        } catch (FileNotFoundException e) {
            throw new NatDocumentException(NatDocumentErrorCode.DOCUMENT_TMP_FILE_ERROR, "cannot open ofile", e);
        }
        ReadableByteChannel rbc = Channels.newChannel(getInputstream());
        try {
            fos.getChannel().transferFrom(rbc, 0, 1 << 24);
            fos.close();
        } catch (IOException e) {
            throw new NatDocumentException(NatDocumentErrorCode.DOCUMENT_TMP_FILE_ERROR, "cannot copy data to temporay file" + tmpFile.getAbsolutePath(), e);
        }
        return tmpFile;
    }

    /**
     * write data to a temporary file having the default (".tmp") suffix
     *
     * @param dir
     * @return
     * @throws NatDocumentException
     */
    public File writeTmpFile(Path dir) throws NatDocumentException {
        return writeTmpFile(dir, "document", ".tmp");
    }

    public void writeFile(File file) throws NatDocumentException {

        FileOutputStream fos;
        try {
            fos = new FileOutputStream(file);
        } catch (FileNotFoundException e) {
            throw new NatDocumentException(NatDocumentErrorCode.DOCUMENT_TMP_FILE_ERROR, "cannot open o file" + file.getAbsolutePath(), e);
        }
        ReadableByteChannel rbc = Channels.newChannel(getInputstream());
        try {
            fos.getChannel().transferFrom(rbc, 0, 1 << 24);
            fos.close();
        } catch (IOException e) {
            throw new NatDocumentException(NatDocumentErrorCode.DOCUMENT_TMP_FILE_ERROR, "cannot copy data to file" + file.getAbsolutePath(), e);
        }
    }

    /**
     * writefile
     */
    /**
     * guess the content type of the data using URLConnection
     *
     * @return
     * @throws NatDocumentException
     */
    public String guessContentType() throws NatDocumentException {

        String mimeType = null;

		//nd.setInputstream(is);
        //	System.out.println(nd.getString());
        String fn = getOriginalFileName();
        //System.out.println("fn :"+fn);
        MediaType mt = null;
        /*
         try {
         DefaultDetector dd = new DefaultDetector();
         Metadata md = new Metadata();
         // !!buffered : "detect" uses only markable input streams 			
         mt = dd.detect(new BufferedInputStream(getInputstream()),md);
         } catch (Exception e) {
         // TODO Auto-generated catch block
         //e.printStackTrace();
         throw new NatDocumentException(NatDocumentErrorCode.DOCUMENT_CANNOT_GUESS_CONTENT_TYPE,null,e);
         }

         */
        // 	 http://stackoverflow.com/questions/7137634/getting-mimetype-subtype-with-apache-tika

        try {

            TikaInputStream stream = TikaInputStream.get(getInputstream());
            Metadata metadata = new Metadata();
            if (getOriginalFileName() != null) {
                metadata.add(Metadata.RESOURCE_NAME_KEY, getOriginalFileName());
            }
            TikaConfig config = TikaConfig.getDefaultConfig();
            Detector detector = config.getDetector();
            mt = detector.detect(stream, metadata);
            //	System.out.println("media type"+mt);
        } catch (IOException e) {
            throw new NatDocumentException(NatDocumentErrorCode.DOCUMENT_CANNOT_GUESS_CONTENT_TYPE, null, e);
			// TODO Auto-generated catch block
            //			e.printStackTrace();
        }

        /*

         try {
         mimeType = URLConnection.guessContentTypeFromStream(getInputstream());	
         } catch (Exception e) {
         // null mimeType
         //throw new NatDocumentException(NatDocumentErrorCode.UNKNOWN_MIME_TYPE, "unable to guess mime type");
         }
         */
        //return mimeType;
        return mt.toString();
    }

}
