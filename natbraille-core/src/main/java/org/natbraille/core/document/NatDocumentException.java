/*
 * NatBraille - An universal Translator
 * Copyright (C) 2005 Bruno Mascret
 * Contact: bmascret@free.fr
 * 
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */
package org.natbraille.core.document;


/**
 * exceptions throw for {@link NatDocument} operations.
 * 
 * See {@link NatDocumentErrorCode} for error codes
 * @author vivien
 *
 */
public class NatDocumentException extends Exception {

	private NatDocumentErrorCode code = null;
	
	/**
	 * @return the code
	 */
	public NatDocumentErrorCode getCode() {
		return code;
	}

	/**
	 * @param code the code to set
	 */
	private void setCode(NatDocumentErrorCode code) {
		this.code = code;
	}
	@Override
	public String getMessage() {
		return "["+getCode()+"] "+super.getMessage();
	}

	private static final long serialVersionUID = 1L;

	public NatDocumentException(NatDocumentErrorCode code, String message) {
		super(message);
		setCode(code);
	}
	public NatDocumentException(NatDocumentErrorCode code, String message,Throwable cause) {
		super(message);
		setCode(code);
		initCause(cause);
	}
}
