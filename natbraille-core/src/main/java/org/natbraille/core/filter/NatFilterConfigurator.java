package org.natbraille.core.filter;

import java.lang.reflect.Constructor;
import java.util.HashMap;
import org.natbraille.core.filter.options.NatFilterOptions;
import org.natbraille.core.gestionnaires.GestionnaireErreur;

/**
 * build {@link NatFilter} from filter class name. Uses previously set class-object
 * assocation to to get the set of correct actual filter constructor parameters. 
 *  
 * @author vivien
 *
 */
public class NatFilterConfigurator {

	/**
	 * association of natfilter constructor classes and objects.
	 */
	private final HashMap<Class<?>,Object> objectForClass = new HashMap<>();

	
	/**
	 * @return the objectForClass
	 */
	private HashMap<Class<?>, Object> getObjectForClass() {
		return objectForClass;
	}
	
	/**
	 * associate a class with an object
	 * @param c 
	 * @param o 
	 * @return
	 */
	public NatFilterConfigurator set(Class<?> c, Object o){
		getObjectForClass().put(c,o);
		return this;
	}
	/**
	 * associate the class of an object with the object
	 * @param o 
	 * @return
	 */
	public NatFilterConfigurator set(Object o){
		return set(o.getClass(),o);
	}
	
	/**
	 * get the object associated with the class
	 * @param c 
	 * @return
	 */
	public Object get(Class<?> c) {
		return getObjectForClass().get(c);
	}

	/**
	 * builds an array of objects matching each constructor parameters class
	 * @param constructor
	 * @return
	 * @throws NatFilterException
	 */
	public Object[] getConstructorParams(Constructor<?> constructor) throws NatFilterException{

		Class<?>[] consParamsClasses = constructor.getParameterTypes();
		Object consParamsObjects[] = new Object[consParamsClasses.length];

		int idx = 0;

		for (Class<?> c :consParamsClasses){
			if (getObjectForClass().containsKey(c)){
				Object o = getObjectForClass().get(c);
				consParamsObjects[idx] = o;	
			} else {
				throw new NatFilterException(NatFilterErrorCode.FACTORY_CANNOT_BUILD_FILTER_PARAM,c.getName());					
			}	
			idx++;
		}


		return consParamsObjects;
	}


	/**
	 * builds a new nat filter
	 * @param filterClass
	 * @return
	 * @throws NatFilterException
	 */
	//public NatFilter newFilter(Class<? extends NatFilter> filterClass) throws NatFilterException {
        public NatFilter newFilter(Class<? extends NatFilter> filterClass) throws NatFilterException {
		NatFilter newFilter = null;

		try  {

			// get filterClass constructor list
			Constructor<?>[] filterConstructors = filterClass.getDeclaredConstructors();

			// for each possible constructor
			for (Constructor<?> c : filterConstructors){
				try {
					Object[] cp = getConstructorParams(c);
					return (NatFilter) c.newInstance(cp);
				} catch (Exception e){
				}
			}

			return newFilter;		
		} catch (Exception e){
			throw new NatFilterException(NatFilterErrorCode.FACTORY_CANNOT_BUILD_FILTER,filterClass.toString(),e);
		}
	}
        
        public NatFilterChain newFilterChain(){
            NatFilterChain nfo = new NatFilterChain(this);
            return nfo;            
        }
        
	public GestionnaireErreur getGestionnaireErreur() {
		return (GestionnaireErreur) get(GestionnaireErreur.class);
	}
	
	public NatFilterOptions getNatFilterOptions() {
		return (NatFilterOptions) get(NatFilterOptions.class);
	}
	
	public NatTransformerFilterResolver getNatTransformerFilterResolver(){
		return (NatTransformerFilterResolver ) get(NatTransformerFilterResolver.class);
		
	}

	public NatFilterConfigurator(){
		set(NatFilterConfigurator.class,this);
	}
}
