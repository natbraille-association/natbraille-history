/*
 * NatBraille - An universal Translator
 * Copyright (C) 2005 Bruno Mascret
 * Contact: bmascret@free.fr
 * 
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */
package org.natbraille.core.filter;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.net.URI;
import java.nio.charset.Charset;
import java.nio.file.Path;
import javax.xml.transform.ErrorListener;
import javax.xml.transform.Source;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerConfigurationException;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.URIResolver;
import javax.xml.transform.sax.SAXSource;
import net.sf.saxon.Controller;
import net.sf.saxon.TransformerFactoryImpl;
import net.sf.saxon.serialize.MessageEmitter;
import net.sf.saxon.trans.XPathException;
import org.natbraille.core.NatStaticResolver;
import org.natbraille.core.document.NatDocument;
import org.natbraille.core.document.NatDocumentException;
import org.natbraille.core.document.Utils;
import org.natbraille.core.filter.dynxsl.AbstractNatDynamicXslDocument;
import org.natbraille.core.filter.dynxsl.FrG2RulesXsl;
import org.natbraille.core.filter.dynxsl.HyphensXsl;
import org.natbraille.core.filter.dynxsl.ParamsAllXsl;
import org.natbraille.core.filter.dynxsl.ParamsMepXsl;
import org.natbraille.core.filter.dynxsl.ParamsTransXsl;
import org.natbraille.core.filter.dynxsl.XslMepXsl;
import org.natbraille.core.filter.dynxsl.XslXsl;
import org.natbraille.core.filter.options.NatFilterOption;
import org.natbraille.core.filter.options.NatFilterOptions;
import org.natbraille.core.gestionnaires.GestionnaireErreur;
import org.natbraille.core.gestionnaires.LogLevel;
import org.natbraille.core.gestionnaires.MessageContents;
import org.natbraille.core.gestionnaires.MessageKind;
import org.natbraille.core.gestionnaires.MessageWriter;
import org.w3c.dom.Document;
import org.xml.sax.DTDHandler;
import org.xml.sax.EntityResolver;
import org.xml.sax.ErrorHandler;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;
import org.xml.sax.SAXParseException;
import org.xml.sax.XMLReader;

/**
 * dynamic url and Entity resolver.
 *
 * @author vivien
 *
 */
public class NatTransformerFilterResolver implements URIResolver, EntityResolver, DTDHandler, ErrorHandler, ErrorListener {

    private NatFilterOptions options = null;
    protected GestionnaireErreur gestionnaireErreur = null;
    private Path tmpDir = null;

    /**
     * @return the tempDir
     */
    private Path getTmpDir() {
        return tmpDir;
    }

    /**
     * @param tempDir the tempDir to set
     */
    public void setTmpDir(Path tempDir) {
        this.tmpDir = tempDir;
    }

    /*
     * cached xmlReader
     */
    private XMLReader xmlReader = null;

    /**
     * get a new XMLReader at first call and the same one on subsequent calls
     *
     * @return
     */
    public final XMLReader getXMLReader() {
        if (xmlReader == null) {
            xmlReader = Utils.newXMLReader(this, this, this);
        }
        return xmlReader;
    }

    /**
     * @return the options
     */
    public NatFilterOptions getOptions() {
        return options;
    }

    /**
     * @return the gestionnaireErreur
     */
    public GestionnaireErreur getGestionnaireErreur() {
        return gestionnaireErreur;
    }

    /**
     * constructor
     * @param options
     * @param gestionnaireErreur
     */
    public NatTransformerFilterResolver(NatFilterOptions options, GestionnaireErreur gestionnaireErreur) {
        this.options = options;
        this.gestionnaireErreur = gestionnaireErreur;
    }

    /*
     * 
     */
    protected SAXSource newSAXSource(InputSource xslInputSource) {
        XMLReader xslReader = getXMLReader();
        SAXSource xslSaxSource = new SAXSource(xslReader, xslInputSource);
        return xslSaxSource;

    }

    protected SAXSource newSAXSource(Document doc) throws IOException, TransformerException {

        Charset charset = Utils.guessDocumentCharset(doc);
        if (charset == null) {
            charset = Charset.forName("UTF-8");
        }

        InputStream xslistream = Utils.toInputStream(doc, charset);
        InputSource xslisource = new InputSource(xslistream);

        return newSAXSource(xslisource);

    }

    public Transformer newTransformer(SAXSource saxSource) throws TransformerConfigurationException, XPathException {
        Transformer tr = null;

        TransformerFactoryImpl tranFactory = new TransformerFactoryImpl();

        tranFactory.getConfiguration().setCompileWithTracing(true);

        TransformerFactory transformerFactory = tranFactory;// TransformerFactory.newInstance();
        transformerFactory.setURIResolver(this);
        transformerFactory.setErrorListener(this);
        // transformerFactory.setCompileWithTracing(true);
        tr = transformerFactory.newTransformer(saxSource);
        tr.setURIResolver(this);
        tr.setErrorListener(this);
        try {
            Controller c = (net.sf.saxon.Controller) tr;
            MessageEmitter me = new MessageEmitter();
            MessageWriter mw = new MessageWriter(getGestionnaireErreur(), MessageKind.INFO, LogLevel.NORMAL);
            me.setWriter(mw);
            c.setMessageEmitter(me);

            //		Receiver mr = new Receiver();
        } catch (Exception e) {
            getGestionnaireErreur().afficheMessage(MessageKind.WARNING, new Exception("<xsl:message> will not be shown", e));
        }
        return tr;
    }

    /**
     * method for source debugging (writes xsl temp files to temp dirs if
     * NatFilterOption.debug_dyn_xsl_write_temp_files is true sends the xsl temp
     * files to the {@link GestionnaireErreur} if debug_dyn_xsl_show is true
     *
     * @param xslDocument
     * @param u
     * @throws NatFilterException
     * @throws NatDocumentException
     * @throws IOException
     * @throws TransformerException
     */
    private void debugSource(Document xslDocument, URI u) throws NatFilterException, NatDocumentException, IOException, TransformerException {

        if ((getOptions().getValueAsBoolean(NatFilterOption.debug_dyn_xsl_write_temp_file))
                || (getOptions().getValueAsBoolean(NatFilterOption.debug_dyn_xsl_show))) {
            NatDocument xslNatDoc = new NatDocument();
            xslNatDoc.setDocument(xslDocument);
            if (getOptions().getValueAsBoolean(NatFilterOption.debug_dyn_xsl_write_temp_file)) {
                try {
                    String name = u.getPath();
                    int lastslash = name.lastIndexOf("/");
                    if (lastslash > -1) {
                        name = name.substring(lastslash + 1);
                    } else {
                        name = "xxx";
                    }
                    File xslNatDocFile = xslNatDoc.writeTmpFile(getTmpDir(), name, ".xsl");
                    gestionnaireErreur.afficheMessage(MessageKind.INFO, "copied to " + xslNatDocFile.getAbsolutePath(), LogLevel.NONE);
                } catch (Exception e) {
                    gestionnaireErreur.afficheMessage(MessageKind.WARNING, "unable to write temp file", LogLevel.NONE);

                }
            }
            if (getOptions().getValueAsBoolean(NatFilterOption.debug_dyn_xsl_show)) {
                gestionnaireErreur.afficheMessage(MessageKind.INFO, xslNatDoc.getString(), LogLevel.NONE);
            }
        }

    }

    /*
     * javax.xml.transform.URIResolver
     * =================================
     */
    @Override
    public Source resolve(String href, String base) throws TransformerException {

        EntityResolver er = NatStaticResolver.getEntityResolverInstance();

        Source source = null;
        try {
            // debug
            if (getOptions().getValueAsBoolean(NatFilterOption.debug_dyn_res_show)) {
                String contentsg = this.getClass() + " :: resolve  href:'" + href + "' base :'" + base + "'";
                getGestionnaireErreur().afficheMessage(MessageKind.INFO, contentsg, LogLevel.NONE);
            }

            URI u = new URI(href);
            switch (u.getScheme()) {

                case "nat": 							
                    switch (u.getAuthority()) {
                        case "system": {
                            getGestionnaireErreur().afficheMessage(MessageKind.INFO, "using resource: " + href + " refered by " + base, LogLevel.DEBUG);
                            InputSource is = er.resolveEntity(null, u.toString());
                            source = newSAXSource(is);
                        }
                        break;
                        case "temp": {		
                            getGestionnaireErreur().afficheMessage(MessageKind.INFO, "generating : " + href + " refered by " + base, LogLevel.DEBUG);

                            AbstractNatDynamicXslDocument xslDynDocument = null;

                            switch (u.getPath()) {
                                case "/xsl/xsl.xsl":
                                    xslDynDocument = new XslXsl(getGestionnaireErreur(), getOptions());
                                    break;
                                case "/xsl/hyphens.xsl":
                                    xslDynDocument = new HyphensXsl(getGestionnaireErreur(), getOptions(), getXMLReader());
                                    break;
                                case "/xsl/fr-g2-rules.xsl":
                                    xslDynDocument = new FrG2RulesXsl(this);
                                    break;
                                case "/xsl/paramsAll.xsl":
                                    xslDynDocument = new ParamsAllXsl(getGestionnaireErreur(), getOptions());
                                    break;
                                case "/xsl/paramsTrans.xsl":
                                    xslDynDocument = new ParamsTransXsl(getGestionnaireErreur(), getOptions());
                                    break;
                                case "/xsl/paramsMEP.xsl":
                                    xslDynDocument = new ParamsMepXsl(getGestionnaireErreur(), getOptions());
                                    break;
                                case "/xsl/xsl-mep.xsl":
                                    xslDynDocument = new XslMepXsl(getGestionnaireErreur(), getOptions());
                                    break;
                            }

                            // make xslt document
                            Document xslDocument = xslDynDocument.getStyleSheet();
                            // make transformer
                            source = newSAXSource(xslDocument);
                            // display or print to file debug options set						
                            debugSource(xslDocument, u);
                        }
                    }

            }
        } catch (Exception e) {
            String msg = "while asking for source : " + href + " " + base;
            throw new TransformerException(msg, e);
        }
        source.setSystemId(href);
        return source;
    }

    /*
     *  org.xml.sax.entityResolver
     * =================================
     */
    @Override
    public InputSource resolveEntity(String publicId, String systemId) throws SAXException, IOException {

        InputSource inputSource = null;
        EntityResolver er = NatStaticResolver.getEntityResolverInstance();

        try {

            if (getOptions().getValueAsBoolean(NatFilterOption.debug_dyn_ent_res_show)) {

                MessageContents contents = MessageContents.Tr("{0} resolve entity : PUBLIC : {1} SYSTEM : {2}").setValues(this.getClass(), publicId, systemId);
                getGestionnaireErreur().afficheMessage(MessageKind.WARNING, contents, LogLevel.DEBUG);
            }

            URI u = new URI(systemId);

            switch (u.getScheme()) {
                case "nat":
                    // System.err.println("scheme is nat");
                    switch (u.getAuthority()) {

                        case "system":
                            switch (u.toString()) {
                                case NatStaticResolver.WINDOB_DTD_SYSTEM_ID:
                                    inputSource = er.resolveEntity(NatStaticResolver.WINDOB_DTD_PUBLIC_ID, systemId);
                                    break;
                                default:
                                    inputSource = er.resolveEntity(publicId, systemId);
                            }
                            break;

                    }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        if (inputSource == null) {
            inputSource = er.resolveEntity(publicId, systemId);
        }
        if (systemId == null) {
            MessageContents contents = MessageContents.Tr("null systemId asking for entity : PUBLIC : {0} SYSTEM : {1}").setValues(publicId, systemId);
            getGestionnaireErreur().afficheMessage(MessageKind.WARNING, contents, LogLevel.DEBUG);
        } else {
            inputSource.setSystemId(systemId);
        }
        if (inputSource == null) {
            MessageContents contents = MessageContents.Tr("null inputSource asking for entity : PUBLIC : {0} SYSTEM : {1}").setValues(publicId, systemId);
            getGestionnaireErreur().afficheMessage(MessageKind.WARNING, contents, LogLevel.DEBUG);

        }

        return inputSource;
    }


    /*
     * 
     * org.xml.sax.ErrorHandler
     * ===============================
     */
    /**
     * {
     *
     * @see org.xml.sax.ErrorHandler}
     */
    @Override
    public void warning(SAXParseException exception) throws SAXException {
        String contents = "PASSING SAXPARSE WARNING "
                + exception.getPublicId() + " "
                + exception.getSystemId()
                + ":" + exception.getLineNumber()
                + " " + exception.getMessage();
        getGestionnaireErreur().afficheMessage(MessageKind.WARNING, new Exception(contents, exception));
    }

    /**
     * @see org.xml.sax.ErrorHandler}
     */
    @Override
    public void error(SAXParseException exception) throws SAXException {
        boolean display = true;
        // pass wearying musicxml dtd errors
        if ((exception.getSystemId() != null) && (exception.getSystemId().contains("www.musicxml.org"))) {
            display = false;
        }
        if (display) {
            String contents = "PASSING SAXPARSE ERROR "
                    + exception.getPublicId() + " "
                    + exception.getSystemId()
                    + ":" + exception.getLineNumber()
                    + " " + exception.getMessage();
            getGestionnaireErreur().afficheMessage(MessageKind.ERROR, new Exception(contents, exception), LogLevel.DEBUG);
        }
    }

    /**
     * @see org.xml.sax.ErrorHandler}
     */
    @Override
    public void fatalError(SAXParseException exception) throws SAXException {
        String contents = "PASSING??? SAXPARSE FATAL ERROR"
                + exception.getPublicId() + " "
                + exception.getSystemId()
                + ":" + exception.getLineNumber()
                + " " + exception.getMessage();
        getGestionnaireErreur().afficheMessage(MessageKind.ERROR, new Exception(contents, exception), LogLevel.DEBUG);
        // TODO 
        //throw new SAXException(contents,exception);
    }


    /* 
     * 
     * javax.xml.transform.ErrorListener
     * =================================
     */
    /**
     * {
     *
     * @see javax.xml.transform.ErrorListener}
     */
    @Override
    public void fatalError(TransformerException exception) throws TransformerException {
        StringBuilder sb = new StringBuilder();

        /**
         * all the exception handling (i.e. afficheMessage) must be done here
         * since the exception thrown here will be discarded by transformer...
         */
        String contents = "FATAL TRANSFORMER ERROR";
        sb.append(contents);

        /**
         * so we display the message and throw nothing
         */
        getGestionnaireErreur().afficheMessage(MessageKind.ERROR, new Exception(contents, exception));

    }

    /**
     * {
     *
     * @see javax.xml.transform.ErrorListener}
     */
    @Override
    public void warning(TransformerException exception) throws TransformerException {
        String contents = "TRANSFORMER WARNING";
        getGestionnaireErreur().afficheMessage(MessageKind.WARNING, new Exception(contents, exception));
    }

    /**
     * {
     *
     * @see javax.xml.transform.ErrorListener}
     */
    @Override
    public void error(TransformerException exception) throws TransformerException {
        String contents = "TRANSFORMER ERROR";
        getGestionnaireErreur().afficheMessage(MessageKind.WARNING, new Exception(contents, exception));
    }


    /*
     *  org.xml.sax.dtdHandler
     * =================================
     */
    /**
     * {
     *
     * @see org.xml.sax.dtdHandler}
     */
    @Override
    public void notationDecl(String name, String publicId, String systemId) throws SAXException {

        MessageContents contents = MessageContents.Tr("dtd handler : notationDecl NAME {0} : PUBLIC : {1} SYSTEM : {2}").setValues(name, publicId, systemId);
        getGestionnaireErreur().afficheMessage(MessageKind.WARNING, contents, LogLevel.DEBUG);

    }

    /**
     * {
     *
     * @see org.xml.sax.dtdHandler}
     */
    @Override
    public void unparsedEntityDecl(String name, String publicId, String systemId, String notationName) {
        MessageContents contents = MessageContents.Tr("dtd handler : unparsedEntityDecl NAME {0} : PUBLIC : {1} SYSTEM : {2}").setValues(name, publicId, systemId);
        getGestionnaireErreur().afficheMessage(MessageKind.INFO, contents, LogLevel.ULTRA);

    }

}

//@Override
//public void fatalError(TransformerException exception) throws TransformerException {
//
//	String contents = "FATAL TRANSFORMER ERROR " + exception.getMessage();
//	getGestionnaireErreur().afficheMessage(MessageKind.WARNING, contents, LogLevel.DEBUG);
////	
//
//		if (exception.getClass().getName().equals("net.sf.saxon.trans.XPathException")) {
//
//			net.sf.saxon.trans.XPathException xpe = (net.sf.saxon.trans.XPathException) exception;
//
//			System.err.println(exception.getLocalizedMessage());
//			System.err.println(exception.getLocationAsString());
//			System.err.println(exception.getLocator().getLineNumber());
//			System.err.println(exception.getLocator().getColumnNumber());
//
//			System.err.println();
//
//			net.sf.saxon.expr.XPathContext xpc = xpe.getXPathContext();
//			while (xpc != null) {
//				// TODO : how to get the origin in last saxon he ?
//
//				InstructionInfo info = null; // xpc.getOrigin();
//
//	
//					switch (info.getConstructType()) {
//					case StandardNames.XSL_TEMPLATE:
//						System.err.print("XSL_TEMPLATE");
//						break;
//					case Location.FUNCTION_CALL:
//						System.err.print("FUNCTION_CALL");
//						break;
//					case StandardNames.XSL_CALL_TEMPLATE:
//						System.err.print("XSL_CALL_TEMPLATE");
//						break;
//					case StandardNames.XSL_APPLY_TEMPLATES:
//						System.err.print("XSL_APPLY_TEMPLATES");
//						break;
//					case StandardNames.XSL_INCLUDE:
//						System.err.print("XSL_INCLUDE");
//						break;
//					default:
//						System.err.print("'type " + info.getConstructType() + "'");
//
//					}
//					System.err.print(" '" + info.getObjectName() + "'");
//
//					System.err.print(" P: " + info.getPublicId());
//					System.err.print(" S: " + info.getSystemId());
//					System.err.print(" at line : " + info.getLineNumber());
//					System.err.print(" col : " + info.getColumnNumber());
//
//					System.err.println();
//				}
//				StackFrame sf = xpc.getStackFrame();
//
//				net.sf.saxon.expr.instruct.SlotManager zuffe = sf.getStackFrameMap();
//				List<StructuredQName> alzkja = zuffe.getVariableMap();
//				ValueRepresentation[] vals = sf.getStackFrameValues();
//
//				Integer counter = 0;
//				for (StructuredQName sqn : alzkja) {
//					String name = sqn.getDisplayName();
//					String value = "";
//					if (vals.length > counter) {
//						ValueRepresentation vr = vals[counter];
//						if (vr != null) {
//							value = vals[counter].toString();
//						}
//					}
//					System.err.println("\t- " + name + " = " + value);
//					counter++;
//				}
//
//				xpc = xpc.getCaller();
//
//		}
//}
