/*
 * NAT - An universal Translator
 * Copyright (C) 2005 Bruno Mascret
 * Contact: bmascret@free.fr
 * 
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */
package org.natbraille.core.filter.convertisseur;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.ByteArrayOutputStream;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.nio.charset.Charset;
import java.util.ArrayList;
import org.natbraille.core.NatStaticResolver;
import org.natbraille.core.document.NatDocument;
import org.natbraille.core.filter.options.NatFilterOption;
import org.natbraille.core.filter.options.NatFilterOptions;
import org.natbraille.core.gestionnaires.GestionnaireErreur;

/**
 * Convertisseur Texte pour TAN Supprime les coupures et les sauts de pages du
 * document braille d'origine
 * 
 * @author bruno
 * 
 */
public class ConvertisseurTan extends ConvertisseurTexte {
	public ConvertisseurTan(NatFilterOptions options, GestionnaireErreur gestionnaireErreurs) {
		super(options, gestionnaireErreurs);
	}

	@Override
	public NatDocument process(NatDocument indoc) throws Exception {
		// conv
		//			FileToolKit.convertBrailleFile(source, fTempTan, 
		//					ConfigNat.getUserBrailleTableFolder()+"Brltab.ent", ConfigNat.getInstallFolder()+"xsl/tablesEmbosseuse/brailleUTF8.ent",
		//					ConfigNat.getCurrentConfig().getBrailleEncoding(), "UTF-8", gest);


		ByteArrayOutputStream baos = new ByteArrayOutputStream();


		InputStream is = indoc.getInputstream();
		BufferedReader raf = new BufferedReader(new InputStreamReader(is, "UTF-8"));
		BufferedWriter fcible = new BufferedWriter(new OutputStreamWriter(baos, "UTF8"));


		//on met les entêtes au fichier xml
		fcible.write("<?xml version=\"1.1\" encoding=\"UTF-8\"?>");
		fcible.write("\n<!DOCTYPE doc:doc SYSTEM \""+NatStaticResolver.WINDOB_DTD_SYSTEM_ID+"\">");
		fcible.write("\n<doc:doc xmlns:doc=\"espaceDoc\">");

		String l;
		ArrayList<String>lignes = new ArrayList<String>();
		l =  raf.readLine();
		while (l != null)
		{
			if (!getOptionAsBoolean(NatFilterOption.LAYOUT_PAGE_BREAKS_KEEP_ORIGINAL)){

				lignes.add(l.replaceAll(""+(char)12, ""));
			}
			else
			{
				if(!l.equals(""+(char)12))
				{
					String [] splitPage = l.split(""+(char)12);
					if(splitPage.length==1){lignes.add(l);}
					else
					{
						lignes.add(splitPage[0]);
						lignes.add(""+(char)12);
						lignes.add(splitPage[1]);
					}
				}
			}
			l =  raf.readLine();
		}
		raf.close();
		for(int i = 0; i< lignes.size()-1;i++)
		{
			String ligne = lignes.get(i);
			if (!getOptionAsBoolean(NatFilterOption.MODE_DETRANS_KEEP_HYPHEN)
					&& (ligne.endsWith(""+'\u2810') 
							|| 	(ligne.endsWith(""+'\u2824')&&!ligne.endsWith(""+'\u2824'+'\u2824')))){
				String s = ligne.substring(0,ligne.length()-1)+lignes.get(i+1);
				lignes.set(i+1, s);
			}
			else
			{
				if(ligne.equals(""+(char)12)){fcible.write("\n\t<page-break/>\n");}
				else
				{
					fcible.write("\n\t<phrase>\n");
					ligneLit(ligne, fcible);
					fcible.write("\n\t</phrase>\n");
				}
			}
		}
		//dernière phrase:
		fcible.write("\n\t<phrase>\n");
		ligneLit(lignes.get(lignes.size()-1), fcible);
		fcible.write("\n\t</phrase>\n");

		fcible.write("\n</doc:doc>");
		fcible.close();

		NatDocument outDoc = new NatDocument();
		outDoc.setByteBuffer(baos.toByteArray());
		outDoc.forceCharset(Charset.forName("UTF-8"));
		return outDoc;
	}

	@Override
	protected String getPrettyName() {
		return "BRAILLE to FORMAT INTERNE";
	}

}
