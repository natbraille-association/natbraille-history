/*
 * NatBraille - An universal Translator
 * Copyright (C) 2005 Bruno Mascret
 * Contact: bmascret@free.fr
 * 
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */
package org.natbraille.core.filter.convertisseur;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.ByteArrayOutputStream;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import org.natbraille.core.NatStaticResolver;
import org.natbraille.core.document.NatDocument;
import org.natbraille.core.filter.NatFilterErrorCode;
import org.natbraille.core.filter.NatFilterException;
import org.natbraille.core.filter.options.NatFilterOptions;
import org.natbraille.core.gestionnaires.GestionnaireErreur;
import org.natbraille.core.gestionnaires.LogLevel;
import org.natbraille.core.gestionnaires.MessageContents;
import org.natbraille.core.gestionnaires.MessageKind;

/**
 * Text+MathMl to natbraille internal format conversion
 * @author bruno
 *
 */
public class ConvertisseurTexteMixte extends ConvertisseurTexte
{
	public ConvertisseurTexteMixte(NatFilterOptions options, GestionnaireErreur gestionnaireErreurs) {
		super(options, gestionnaireErreurs);
	}
	//String src, String tgt,String sEncoding ;
	
	@Override
	public NatDocument process(NatDocument inDoc) throws NatFilterException {

		NatDocument outDoc = null;

		int nbCars = 0;
		int nbMots = 0;
		int nbPhrases = 0;
		
		GestionnaireErreur gest = getGestionnaireErreur();
		try
		{
			//RandomAccessFile raf = new RandomAccessFile(source, "r");
//			BufferedReader raf = new BufferedReader(new InputStreamReader(new FileInputStream(source),sourceEncoding));

			BufferedReader raf = new BufferedReader(new InputStreamReader(inDoc.getInputstream()));
			
			
			gest.afficheMessage(MessageKind.INFO,MessageContents.Tr("Conversion du fichier source ..."),LogLevel.NORMAL);
			//BufferedWriter fcible = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(cible),"UTF8"));
		
			ByteArrayOutputStream baos = new ByteArrayOutputStream();
			BufferedWriter fcible = new BufferedWriter(new OutputStreamWriter(baos,"UTF8"));
			
			
			//on met les entêtes au fichier xml
			fcible.write("<?xml version=\"1.1\" encoding=\"UTF-8\"?>");
			fcible.write("\n<!DOCTYPE doc:doc SYSTEM \"" + NatStaticResolver.WINDOB_DTD_SYSTEM_ID +"\">");
			fcible.write("\n<doc:doc xmlns:doc=\"espaceDoc\">");
	
			String ligne;

			boolean recopie = false;
			ligne =  raf.readLine();
			while ( ligne != null )
			{
				//Ajout du namespace xmlns="http://www.w3.org/1998/Math/MathML"si il n' en a pas pour les maths
				ligne = ligne.replaceAll("<semantics>", "");
				ligne = ligne.replaceAll("</semantics>", "");
				if (ligne.indexOf("<math ")!=-1 && ligne.indexOf("xmlns")==-1)
				{
					ligne=ligne.replace("<math ","\n<math xmlns=\"http://www.w3.org/1998/Math/MathML\" ");
				}
				else if (ligne.indexOf("<math>")!=-1)
				{
					ligne=ligne.replace("<math>","\n<math xmlns=\"http://www.w3.org/1998/Math/MathML\">");
				}
				if (recopie==true)
				{
					if (ligne.indexOf("</math>")!=-1 || ligne.indexOf("</m:math>")!=-1)
					{
						fcible.write("\n\t\t" + ligne.substring(0,ligne.indexOf("</math>")+7));
						//System.out.println(ligne.substring(0,ligne.indexOf("</math>")+7));
						ligne = ligne.substring(ligne.indexOf("</math>")+7);
						recopie = false;
						if (ligne.length() ==0)
						{
							fcible.write("\n\t</phrase>\n");
							ligne = raf.readLine();
						}
						else
						{
							if (ligne.indexOf("<math")!=-1 || ligne.indexOf("<m:math")!=-1)
							{
								ligneLit(ligne.substring(0,ligne.indexOf("<math")), fcible);
								ligne=ligne.substring(ligne.indexOf("<math"));
								recopie = true;
							}
							else
							{
								ligneLit(ligne, fcible);
								fcible.write("\n\t</phrase>\n");
								ligne =  raf.readLine();
							}
						}
					}
					else
					{
						fcible.write("\t\t" + ligne);
						ligne =  raf.readLine();
					}
				}
				else //(recopie==false)
				{
					if (ligne.indexOf("<math")!=-1 || ligne.indexOf("<m:math")!=-1)
					{
						//fcible.print(ligne.substring(0,ligne.indexOf("<math")+5) + "\n");
						fcible.write("\n\t<phrase>");
						ligneLit(ligne.substring(0,ligne.indexOf("<math")), fcible);
						ligne=ligne.substring(ligne.indexOf("<math"));
						recopie = true;
					}
					if (recopie==false)
					{
						nbPhrases++;
						fcible.write("\n\t<phrase>\n");
						ligneLit(ligne, fcible);
						fcible.write("\n\t</phrase>\n");
						ligne =  raf.readLine();
					}
				}
			}
			fcible.write("\n</doc:doc>");
			fcible.close();
			raf.close();
			
			outDoc = new NatDocument();
			outDoc.setByteBuffer(baos.toByteArray());
			
			gest.afficheMessage("Le document contient " + nbPhrases +" paragraphes, " + nbMots + " mots et " + nbCars +" caractères.",LogLevel.VERBOSE);
		} catch (Exception e)  {
			throw new NatFilterException(NatFilterErrorCode.FILTER_RUNTIME_ERROR, "unable to convert mixed text",e);
		}		
		return outDoc;
	}
	
	@Override
	protected String getPrettyName() {
		return "TEXT + MATHML to FORMAT INTERNE";
	}
}
