package org.natbraille.core.filter.dynxsl;

import org.natbraille.core.filter.NatFilterErrorCode;
import org.natbraille.core.filter.NatFilterException;
import org.natbraille.core.gestionnaires.GestionnaireErreur;
import org.w3c.dom.Document;
import org.w3c.dom.Element;

/**
 * simple abstract class and utils for building xsl documents
 * @author vivien
 *
 */
public abstract class AbstractNatDynamicXslDocument  {

	private GestionnaireErreur gestionnaireErreur;
		
	protected GestionnaireErreur getGestionnaireErreur() {
		return gestionnaireErreur;
	}

	/**
	 * gets the generated style sheet as a {@link Document} 
	 * @return
	 * @throws NatFilterException whenever an exception occurs
	 */
	public final Document getStyleSheet() throws NatFilterException {
		Document doc = null;
		try {
			doc = makeStyleSheet();
		} catch (Exception e){
			throw new NatFilterException(NatFilterErrorCode.FACTORY_CANNOT_BUILD_XSL_DOCUMENT,"",e);
		}
		return doc;
	}
	
	/**
	 * class implementation of generated style sheet as a {@link Document}
	 * @throws Exception
	 */
	protected abstract Document makeStyleSheet() throws Exception;
	
	protected AbstractNatDynamicXslDocument(GestionnaireErreur ge){
		this.gestionnaireErreur = ge;
	}

//////////////////////////////////:
	/**
	 * Fabrique une variable de nom nom et de type type pour le document doc
	 * 
	 * @param doc
	 *            le document xml
	 * @param nom
	 *            le nom de la variable
	 * @param type
	 *            le type de la variable
	 * @return l'element variable
	 */
	protected static Element fabriqueVariable(Document doc, String nom, String type) {
		Element e = doc.createElement("xsl:variable");
		e.setAttribute("name", nom);
		e.setAttribute("as", type);
		return e;
	}

	/**
	 * Fabrique une variable de nom nom et de type type pour le document doc
	 * contenant la valeur valeur
	 * 
	 * @param doc
	 *            le document xml
	 * @param nom
	 *            le nom de la variable
	 * @param valeur
	 *            valeur de la variable
	 * @param type
	 *            le type de la variable
	 * @return l'element variable
	 */
	protected static Element fabriqueVariable(Document doc, String nom, String valeur, String type) {
		Element e = doc.createElement("xsl:variable");
		e.setAttribute("name", nom);
		e.setAttribute("as", type);
		e.setAttribute("select", valeur);
		return e;
	}

	/**
	 * Fabrique un paramètre de nom nom et de type type pour le document doc
	 * contenant la valeur valeur
	 * 
	 * @param doc
	 *            le document xml
	 * @param nom
	 *            le nom de la paramètre
	 * @param valeur
	 *            valeur de la paramètre
	 * @param type
	 *            le type de la paramètre
	 * @return l'element paramètre
	 */
	protected static Element fabriqueParam(Document doc, String nom, String valeur, String type) {
		Element e = doc.createElement("xsl:param");
		e.setAttribute("name", nom);
		e.setAttribute("as", type);
		e.setAttribute("select", valeur);
		return e;
	}

	
	
	
}
