package org.natbraille.core.filter.dynxsl;

import java.util.ArrayList;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.TransformerException;
import org.natbraille.core.filter.NatFilterException;
import org.natbraille.core.filter.options.NatFilterOption;
import org.natbraille.core.filter.options.NatFilterOptions;
import org.natbraille.core.gestionnaires.GestionnaireErreur;
import org.w3c.dom.Document;
import org.w3c.dom.Element;

public class ParamsMepXsl extends AbstractNatDynamicXslDocument {
	NatFilterOptions options;
	
	private NatFilterOptions getOptions() {
		return options;
	}

	public ParamsMepXsl(GestionnaireErreur ge, NatFilterOptions nfo) {
		super(ge);
		this.options = nfo;
	}

	@Override
	protected Document makeStyleSheet() throws Exception {
		return createParamMEP(getOptions(), getGestionnaireErreur());
	}
	
	/**
	 * Fabrique le fichier paramsMEP.xsl contenant les valeurs des éléments de
	 * configuration et de paramétrage utilisés uniquement pour la mise en page
	 * 
	 * @param gestErreur
	 *            Une instance de GestionnaireErreur
	 * @throws ParserConfigurationException
	 *             erreur de parsage
	 * @throws TransformerException
	 *             erreur lors de la transformation
	 * @throws NatFilterException
	 */
	public static Document createParamMEP(NatFilterOptions options, GestionnaireErreur ge) throws ParserConfigurationException, TransformerException, NatFilterException {

		// GestionnaireErreur gestErreur = getErrorHandler();

		DocumentBuilderFactory fabrique = DocumentBuilderFactory.newInstance();
		DocumentBuilder constructeur = fabrique.newDocumentBuilder();
		Document docParams = constructeur.newDocument();
		// Propriétés de docParam
		docParams.setXmlVersion("1.1");
		docParams.setXmlStandalone(true);

		// racine
		Element racine = docParams.createElement("xsl:stylesheet");
		racine.setAttribute("version", "2.0");
		racine.setAttribute("xmlns:xsl", "http://www.w3.org/1999/XSL/Transform");
		racine.setAttribute("xmlns:xs", "http://www.w3.org/2001/XMLSchema");
		racine.appendChild(docParams.createComment("Auto-generated file; see Transcodeur.java"));
		// racine.appendChild(docParams.createComment(nat.getLicence("", "")));
		// paramètres
		ArrayList<Element> params = new ArrayList<Element>();

		/*
	     
	 	
	     
	     
	     */
		params.add(fabriqueParam(docParams, "numerosDePage", options.getValue(NatFilterOption.LAYOUT_NUMBERING_MODE), "xs:string"));
		params.add(fabriqueParam(docParams, "startNumberingPage", options.getValue(NatFilterOption.LAYOUT_NUMBERING_START_PAGE) + "", "xs:integer"));
		params.add(fabriqueParam(docParams, "couplit", options.getValue(NatFilterOption.LAYOUT_LIT_HYPHENATION) + "()", "xs:boolean"));
		params.add(fabriqueParam(docParams, "sagouin", options.getValue(NatFilterOption.LAYOUT_DIRTY_HYPHENATION) + "()", "xs:boolean"));
		params.add(fabriqueParam(docParams, "modeLigneVide", options.getValue(NatFilterOption.LAYOUT_PAGE_EMPTY_LINES_MODE) + "", "xs:integer"));
		params.add(fabriqueParam(docParams, "min1ligne", options.getValue(NatFilterOption.LAYOUT_PAGE_EMPTY_LINES_CUSTOM_1) + "", "xs:integer"));
		params.add(fabriqueParam(docParams, "min2ligne", options.getValue(NatFilterOption.LAYOUT_PAGE_EMPTY_LINES_CUSTOM_2) + "", "xs:integer"));
		params.add(fabriqueParam(docParams, "min3ligne", options.getValue(NatFilterOption.LAYOUT_PAGE_EMPTY_LINES_CUSTOM_3) + "", "xs:integer"));
		params.add(fabriqueParam(docParams, "minPageBreak", options.getValue(NatFilterOption.LAYOUT_PAGE_BREAKS_MIN_LINES) + "", "xs:integer"));
		params.add(fabriqueParam(docParams, "lignesParPage", options.getValue(NatFilterOption.FORMAT_PAGE_LENGTH) + "", "xs:integer"));
		// params.add(fabriqueParam(docParams,"formFeedEnd",options.getValue(NatOption.SautPageFin()+"()","xs:boolean"));
		params.add(fabriqueParam(docParams, "generatePageBreak", options.getValue(NatFilterOption.LAYOUT_PAGE_BREAKS) + "()", "xs:boolean"));
		params.add(fabriqueParam(docParams, "keepPageBreak", options.getValue(NatFilterOption.LAYOUT_PAGE_BREAKS_KEEP_ORIGINAL) + "()", "xs:boolean"));
//		params.add(fabriqueParam(docParams, "styleNoteTr", "'" + options.getValue(NatFilterOption.StyleNoteTr) + "'", "xs:string"));
		params.add(fabriqueParam(docParams, "insertIndexBraille", options.getValue(NatFilterOption.LAYOUT_PAGE_TOC) + "()", "xs:boolean"));
		// ajout des parametres
		
		// TODO : remove this option
		params.add(fabriqueParam(docParams, "poetryStyle", "'" + "poesie" + "'", "xs:string"));

		for (int i = 0; i < params.size(); i++) {
			racine.appendChild(params.get(i));
		}
		docParams.appendChild(racine);
		/* Sauvegarde de document dans un fichier */
		/*
		try {
			System.out.println(Utils.toString(docParams));
		} catch (Exception exception) {
			// TODO Auto-generated catch block
			exception.printStackTrace();
		}
		*/
		return docParams;
		// Source source = new DOMSource(docParams);
		// return source;
	}

}
