package org.natbraille.core.filter.dynxsl;

import java.util.ArrayList;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.TransformerException;
import org.natbraille.core.Nat;
import org.natbraille.core.filter.NatFilterException;
import org.natbraille.core.filter.options.NatFilterOption;
import org.natbraille.core.filter.options.NatFilterOptions;
import org.natbraille.core.gestionnaires.GestionnaireErreur;
import org.w3c.dom.DOMException;
import org.w3c.dom.Document;
import org.w3c.dom.Element;

public class ParamsTransXsl extends AbstractNatDynamicXslDocument {

	NatFilterOptions options;
	
	private NatFilterOptions getOptions() {
		return options;
	}

	public ParamsTransXsl(GestionnaireErreur ge, NatFilterOptions nfo) {
		super(ge);
		this.options = nfo;
	}

	@Override
	protected Document makeStyleSheet() throws Exception {
		return createParamTrans(getOptions(), getGestionnaireErreur());
	}

	/**
	 * Fabrique le fichier paramsTrans.xsl contenant les valeurs des éléments de
	 * configuration et de paramétrage utilisés pour la première passe de la
	 * transcription
	 * 
	 * @param gestErreur
	 *            Une instance de GestionnaireErreur
	 * @throws ParserConfigurationException
	 *             erreur de parsage
	 * @throws TransformerException
	 *             erreur lors de la transformation
	 * @throws NatFilterException
	 * @throws DOMException
	 */
	public static Document createParamTrans(NatFilterOptions options, GestionnaireErreur ge) throws ParserConfigurationException, TransformerException, NatFilterException {

		// GestionnaireErreur gestErreur = getErrorHandler();

		DocumentBuilderFactory fabrique = DocumentBuilderFactory.newInstance();
		DocumentBuilder constructeur = fabrique.newDocumentBuilder();
		Document docParams = constructeur.newDocument();
		// Propriétés de docParam
		docParams.setXmlVersion("1.1");
		docParams.setXmlStandalone(true);

		// racine
		Element racine = docParams.createElement("xsl:stylesheet");
		racine.setAttribute("version", "2.0");
		racine.setAttribute("xmlns:xsl", "http://www.w3.org/1999/XSL/Transform");
		racine.setAttribute("xmlns:xs", "http://www.w3.org/2001/XMLSchema");
		racine.appendChild(docParams.createComment("Auto-generated file; see Transcodeur.java"));
		racine.appendChild(docParams.createComment(Nat.getLicence("", "")));
		// paramètres
		ArrayList<Element> params = new ArrayList<Element>();
		// params.add(fabriqueParam(docParams,"abrege",options.getValue(NatOption.Abreger()+"()","xs:boolean"));
		params.add(fabriqueParam(docParams, "trigoSpecif", options.getValue(NatFilterOption.MODE_MATH_SPECIFIC_NOTATION) + "()", "xs:boolean"));
		params.add(fabriqueParam(docParams, "cp_2", options.getValue(NatFilterOption.MODE_LIT_FR_MAJ_DOUBLE) + "()", "xs:boolean"));
		params.add(fabriqueParam(docParams, "cp_part", options.getValue(NatFilterOption.MODE_LIT_FR_MAJ_PASSAGE) + "()", "xs:boolean"));
		params.add(fabriqueParam(docParams, "cp_mix", options.getValue(NatFilterOption.MODE_LIT_FR_MAJ_MELANGE) + "()", "xs:boolean"));
		params.add(fabriqueParam(docParams, "emph_w", options.getValueAsBoolean(NatFilterOption.MODE_LIT_EMPH) + "()", "xs:boolean"));
		params.add(fabriqueParam(docParams, "emph_mix", options.getValue(NatFilterOption.MODE_LIT_FR_EMPH_IN_WORD) + "()", "xs:boolean"));
		params.add(fabriqueParam(docParams, "emph_part", options.getValue(NatFilterOption.MODE_LIT_FR_EMPH_IN_PASSAGE) + "()", "xs:boolean"));
		params.add(fabriqueParam(docParams, "min_cell_lin", options.getValue(NatFilterOption.MODE_ARRAY_2D_MIN_CELL_NUM) + "", "xs:integer"));
		params.add(fabriqueParam(docParams, "linearise_table", options.getValue(NatFilterOption.MODE_ARRAY_LINEARIZE_ALWAYS) + "()", "xs:boolean"));
		params.add(fabriqueParam(docParams, "forceMathPrefix", options.getValue(NatFilterOption.MODE_MATH_ALWAYS_PREFIX) + "()", "xs:boolean"));
		params.add(fabriqueParam(docParams, "minTitleAbr", "" + options.getValue(NatFilterOption.MODE_G2_HEADING_LEVEL), "xs:integer"));
		params.add(fabriqueParam(docParams, "ivbMajSeule", options.getValue(NatFilterOption.IvbMajSeule) + "()", "xs:boolean"));
		/*
		 * TODO on les mets où ces rajouts finalement?
		 */
		/*
		 * //les comptes des rajouts Element rajCpt =
		 * fabriqueVariable(docParams,"compteRajouts","xs:boolean*"); //la
		 * séquence pour les comptes des rajouts Element seqRajCpt =
		 * docParams.createElement("xsl:sequence"); String seqRajString =
		 * options.getValue(NatOption.RajoutCompte(); seqRajString =
		 * seqRajString.replaceAll(",", "(),")+"()";//pour avoir les booléens
		 * xsl seqRajCpt.setAttribute("select", "("+seqRajString+")");
		 * rajCpt.appendChild(seqRajCpt); params.add(rajCpt);
		 * 
		 * //les rajouts Element raj =
		 * fabriqueVariable(docParams,"rajouts","xs:string*"); //la séquence
		 * pour les rajouts Element seqRaj =
		 * docParams.createElement("xsl:sequence"); // TODO prévoir de formater
		 * la séquence (&apos et le reste) //On double ici les apostrophes et
		 * les & pour pas que ça plante la séquence xsl //Il n'est pas possible
		 * d'avoir une chaine ajout contenant des champs vides car les
		 * string.split ne fonctionnent pas sinon //On est donc obligé
		 * d'encadrer dès maintenant les champs par des '' //ex:
		 * ajouts="'','','','','','a','b','c','d'" et pas ajouts=",,,,,,a,b,c,d"
		 */

		/*
		 * // String [] tabSeqRaj = options.getValue(NatOption.Rajout().split(",");
		 * 
		 * 
		 * /// ---------> String[] tabSeqRaj =
		 * ConfigNat.intelliSplit(options.getValue(NatOption.Rajout(),",");
		 * 
		 * String strSeqRaj=""; for(int i=0;i<tabSeqRaj.length;i++) { strSeqRaj
		 * = strSeqRaj + "'" +tabSeqRaj[i].substring(1,tabSeqRaj[i].length()-1
		 * ).replaceAll("'", "''") +"',"; } seqRaj.setAttribute("select",
		 * "("+strSeqRaj.substring(0,strSeqRaj.length()-1)+")");
		 * raj.appendChild(seqRaj); params.add(raj);
		 */

		/*
		 * variables
		 */
		// les titres
		Element titre = fabriqueVariable(docParams, "listeTitres", "xs:integer*");
		// la séquence pour les titres
		Element sequence = docParams.createElement("xsl:sequence");
		sequence.setAttribute("select", "(" + options.getValue(NatFilterOption.LAYOUT_PAGE_HEADING_LEVEL) + ")");
		titre.appendChild(sequence);
		params.add(titre);
		
		Element integral = fabriqueVariable(docParams, "l_noms_propres", "xs:string*");
		integral.setAttribute("select", "(" + options.getValue(NatFilterOption.MODE_G2_G1_WORDS) + ")");
		params.add(integral);

		Element integralAmb = fabriqueVariable(docParams, "l_noms_propres_amb", "xs:string*");
		integralAmb.setAttribute("select", "(" + options.getValue(NatFilterOption.MODE_G2_G1_AMB_WORDS) + ")");
		params.add(integralAmb);

		// en abrégé: liste de mots à conserver en intégral et ivb
		if (options.getValueAsBoolean(NatFilterOption.MODE_G2)) {

			Element ivb = fabriqueVariable(docParams, "l_ivb", "xs:string*");
			ivb.setAttribute("select", "(" + options.getValue(NatFilterOption.MODE_G2_IVB_WORDS) + ")");
			params.add(ivb);

			Element ivbAmb = fabriqueVariable(docParams, "l_ivb_amb", "xs:string*");
			ivbAmb.setAttribute("select", "(" + options.getValue(NatFilterOption.MODE_G2_IVB_AMB_WORDS) + ")");
			params.add(ivbAmb);
		}

		// ajout des parametres
		for (int i = 0; i < params.size(); i++) {
			racine.appendChild(params.get(i));
		}
		docParams.appendChild(racine);
		/*
		try {
			System.out.println(Utils.toString(docParams));
		} catch (Exception exception) {
			// TODO Auto-generated catch block
			exception.printStackTrace();
		}
		*/
		return docParams;
		/* Sauvegarde de document dans un fichier */
		// Source source = new DOMSource(docParams);
		// return source;
	}
	/*******/

	
}
