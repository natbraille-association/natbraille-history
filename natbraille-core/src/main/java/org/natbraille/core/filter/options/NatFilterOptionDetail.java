/*
 * NatBraille - An universal Translator
 * Copyright (C) 2005 Bruno Mascret
 * Contact: bmascret@free.fr
 * 
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */
package org.natbraille.core.filter.options;
/**
 * 
 */
import java.lang.annotation.Documented;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * annotated informations for a {@link NatFilterOption}
 * @author vivien
 *
 */
@Documented
@Target({ElementType.FIELD})
@Retention(RetentionPolicy.RUNTIME)
public @interface NatFilterOptionDetail {
	/**
	 * the commentary (or description of the option)
	 * @return
	 */
	String comment() default "";
	/**
	 * the default value of the option (as a String)
	 * @return
	 */
	String defaultValue() ;
	/**
	 * whether the option must be "hidden" to the user
	 * (presentation choice)
	 * @return
	 */
	String hidden() default "false";
	/**
	 * the possible values of the option if applies
	 * @return
	 */
	String[] possibleValues() default {};
	
	/**
	 * the options categories {@link NatFilterOptionCategory}
	 * the option belongs to
	 * @return
	 */
	NatFilterOptionCategory[] categories() default {};
	
	/**
	 * the option type {@link NatFilterOptionType}
	 * of the option
	 * @return
	 */
	NatFilterOptionType type() default NatFilterOptionType.String;
	
}

