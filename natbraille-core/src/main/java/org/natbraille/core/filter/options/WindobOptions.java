/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package org.natbraille.core.filter.options;

import org.natbraille.core.filter.NatFilterException;
import org.natbraille.windob.Configuration;
import org.natbraille.windob.Option;
import org.natbraille.windob.Options;
import org.natbraille.windob.Rules;

/**
 *
 * @author vivien
 */
public class WindobOptions {
    public static void main(String argv[]) throws NatFilterException {
        
        NatFilterOptions nfos = new NatFilterOptions();
        
        Options options = new Options();        
        for (NatFilterOption nfo : NatFilterOption.values()){            
            Option option = new Option();
            option.setName(nfo.name());
            option.setValue(nfos.getValue(nfo));
            options.getOption().add(option);
        }     
        
        Configuration wc = new Configuration();
        wc.setOptions(options);
        
        Rules wr = new Rules();
        //List<Rule> ruleList = wr.getRule();
        //Rule r = new Rule();

        
        wc.setRules(wr);
    }
}
