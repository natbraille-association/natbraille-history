///*
// * NatBraille - An universal Translator
// * Copyright (C) 2005 Bruno Mascret
// * Contact: bmascret@free.fr
// * 
// * This program is free software; you can redistribute it and/or
// * modify it under the terms of the GNU General Public License
// * as published by the Free Software Foundation; either version 2
// * of the License.
// * 
// * This program is distributed in the hope that it will be useful,
// * but WITHOUT ANY WARRANTY; without even the implied warranty of
// * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// * GNU General Public License for more details.
// * 
// * You should have received a copy of the GNU General Public License
// * along with this program; if not, write to the Free Software
// * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
// */
//package org.natbraille.core.filter.presentateur;
//
//import java.io.BufferedWriter;
//import java.io.ByteArrayOutputStream;
//import java.io.OutputStreamWriter;
//import java.net.URI;
//
//import javax.xml.parsers.DocumentBuilder;
//import javax.xml.parsers.DocumentBuilderFactory;
//import javax.xml.transform.Transformer;
//import javax.xml.transform.dom.DOMSource;
//import javax.xml.transform.stream.StreamResult;
//
//import org.natbraille.core.document.NatDocument;
//import org.natbraille.core.filter.NatTransformerFilter;
//import org.natbraille.core.filter.NatTransformerFilterResolver;
//import org.natbraille.core.gestionnaires.LogLevel;
//import org.natbraille.core.gestionnaires.MessageKind;
//
//import org.w3c.dom.Document;
//import org.w3c.dom.Node;
//
//public class BrailleTableModifierXsl extends NatTransformerFilter {
//
//	@Override
//	protected void initialize() throws Exception {
//		setTransformer(new URI("nat://system/xsl/brailleTableConv.xsl"));
//	}
//
//	public BrailleTableModifierXsl(NatTransformerFilterResolver uriResolver) {
//		super(uriResolver);
//	}
//
//	@Override
//	protected NatDocument process(NatDocument indoc) throws Exception {
//
//		Transformer trans = getTransformer();
//
//		/*
//		 * input document building
//		 */
//		DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
//		dbf.setValidating(false);
//		DocumentBuilder bd = dbf.newDocumentBuilder();
//		Document doc = bd.newDocument();
//		Node node = doc.createElement("doc");
//		node.setTextContent(indoc.getString());
//		doc.appendChild(node);
//
//		/*
//		 * output buffer building
//		 */
//		// Création du buffer de sortie
//		ByteArrayOutputStream baos = new ByteArrayOutputStream();
//		BufferedWriter fcible = new BufferedWriter(new OutputStreamWriter(baos, "UTF8"));
//		StreamResult out = new StreamResult(fcible);
//
//		DOMSource in = new DOMSource(doc);
//
//		/*
//		 * transformation
//		 */
//		trans.transform(in, out);
//
//		/*
//		 * création du document de sortie
//		 */
//		NatDocument outDoc = new NatDocument();
//		getGestionnaireErreur().afficheMessage(MessageKind.INFO, "set output document buffer", LogLevel.VERBOSE);
//		outDoc.setByteBuffer(baos.toByteArray());
//
//		return outDoc;
//	}
//
//	@Override
//	protected String getPrettyName() {
//		return "BRAILLE TABLE CHANGE (xsl)";
//	}
//}
