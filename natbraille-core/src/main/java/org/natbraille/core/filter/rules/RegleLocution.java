/*
 * NatBraille - An universal Translator
 * Copyright (C) 2005 Bruno Mascret
 * Contact: bmascret@free.fr
 * 
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */


package org.natbraille.core.filter.rules;


/**
 * Classe premettant de représenter une règle de type "Locution";
 * Les règles de locution font correspondre plusieurs mots à un seul ensemble de signes braille.
 * Les locutions sont invariantes et ne se composent pas avec d'autres règles
 * @author bruno
 *
 */
public class RegleLocution extends RegleMot
{	
	/**
	 * Constructeur
	 * @param n la locution en noir
	 * @param b la transcription en braille
	 * @param a true si règle active
	 * @param p true si règle pédagogique
	 */
	public RegleLocution(String n, String b, boolean a, boolean p)
	{
		super("Locution", "IV",n,b,a,p);
	}
	
	/**
	 * Redéfinition de {@link outils.regles.Regle#toString()}
	 * @see outils.regles.Regle#toString()
	 */
	@Override
	public String toString()
	{
		return description + " ("+reference+"): "+ noir + " est transcrit par " + braille; 
	}

	/**
	 * Renvoie vrai si r est une RegleLocution et que les attributs noir sont égaux
	 */
	@Override
	public boolean equals(Object r)
	{
		return (r instanceof RegleLocution) && ((RegleLocution)r).noir.equals(noir);
	}

	/**
	 * @see outils.regles.Regle#getXML()
	 */
	@Override
	public String getXML()
	{
		String a = actif ? " actif=\"true\"" : "actif=\"false\"";
		String p = peda ? " peda=\"true\"" : "";
		return "\t<locution " + a +p+">\n" +
				"\t\t<noir>"+noir+"</noir>\n" +
				"\t\t<braille>" + braille + "</braille>\n" +
				"\t</locution>\n";
	}
}
