/*
 * NatBraille - An universal Translator
 * Copyright (C) 2005 Bruno Mascret
 * Contact: bmascret@free.fr
 * 
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */


package org.natbraille.core.filter.rules;



/**
 * Classe de regrouppement des règles portant sur l'abréviation de mot (Locution, symboles, signes)
 * Ces règles génèrent les listes de mots et leurs abréviations
 * @author bruno
 *
 */
public abstract class RegleMot extends Regle implements Comparable<RegleMot>
{
	/** Mot(s) en noir */
	protected String noir;
	/** transcription */
	protected String braille;
	
	/**
	 * @param d description
	 * @param r référence
	 * @param n mot(s) en noir
	 * @param b transcription en braille
	 * @param a true si règle active
	 * @param p true si règle pédagogique
	 */
	public RegleMot(String d, String r, String n, String b, boolean a, boolean p)
	{
		super(d,r,a,p);
		noir = n;
		braille = b;
	}
	
	/**
	 * Renvoie le(s) mot(s) en noir
	 * @return {@link #noir}
	 */
	public String getNoir(){return noir;}
	/**
	 * Renvoie la transcription de {@link #noir} en braille
	 * @return {@link #braille}
	 */
	public String getBraille(){return braille;}
	
	/**
	 * Tri des mots par longueur
	 * @see java.lang.Comparable#compareTo(java.lang.Object)
	 */
	@Override
	public int compareTo(RegleMot rm) {
		return rm.getNoir().length()-noir.length();
	}
}
