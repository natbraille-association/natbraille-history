/*
 * NatBraille - An universal Translator
 * Copyright (C) 2005 Bruno Mascret
 * Contact: bmascret@free.fr
 * 
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */

package org.natbraille.core.filter.rules;


/**
 * Classe premettant de représenter une règle de type "symbole";
 * Les règles de symbole font correspondre une chaine de caractère un ou plusieurs signes braille.
 * Les symboles peuvent être utilisé lorsqu'on les trouve dans des mots
 * @author bruno
 *
 */
public class RegleSymbole extends RegleMot
{
	/** indique si le symbole est invariant*/
	private boolean invariant = false;
	/** indique si le symbole est composable */
	private boolean composable = true;
	/**
	 * Constructeur
	 * @param n le symbole fondamental en noir
	 * @param b la transcription en braille
	 * @param inv vrai si le symbole est invariant
	 * @param comp vrai si le symbole est composable
	 * @param a true si règle active
	 * @param p true si règle pédagogique
	 */
	public RegleSymbole(String n, String b, boolean inv, boolean comp, boolean a,boolean p)
	{
		super("Symbole", "III",n,b,a,p);
		invariant = inv;
		composable = comp;
	}
	
	/** 
	 * @return true si le symbole est invariant
	 * @see #invariant
	 */
	public boolean isInvariant(){return invariant;}
	
	/**
	 * @return true si le symbole est composable
	 * @see #composable
	 */
	public boolean isComposable() {return composable;}
	
	/**
	 * Redéfinition de {@link outils.regles.Regle#toString()}
	 * @see outils.regles.Regle#toString()
	 */
	@Override
	public String toString()
	{
		String inv = "";
		if(invariant){inv=" (invariant)";}
		return description + " ("+reference+"): "+ noir + " est transcrit par " + braille + inv; 
	}
	
	/**
	 * Renvoie vrai si r est une RegleSymbole et que les attributs noir sont égaux
	 */
	@Override
	public boolean equals(Object r)
	{
		return (r instanceof RegleSymbole) && ((RegleSymbole)r).noir.equals(noir);
	}

	/**
	 * @see outils.regles.Regle#getXML()
	 */
	@Override
	public String getXML()
	{
		String p = invariant ? " invariant=\"true\"" : "";
		String a = actif ? " actif=\"true\"" : "actif=\"false\"";
		String comp = composable ? "" : " composable=\"false\""; 
		String ped = peda ? " peda=\"true\"" : "";
		return "\t<symbole" + p + " " + a + comp+ped+">\n" +
			"\t\t<noir>"+noir+"</noir>\n" +
			"\t\t<braille>" + braille + "</braille>\n" +
			"\t</symbole>\n";
	}

	/**
	 * Tri par longueur de mot en noir
	 * @see java.lang.Comparable#compareTo(java.lang.Object)
	 *
	@Override
	public int compareTo(RegleMot rs)
	{
		return rs.noir.length() - noir.length();
	}*/

	
}

