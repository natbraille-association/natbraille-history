/*
 * NatBraille - An universal Translator
 * Copyright (C) 2005 Bruno Mascret
 * Contact: bmascret@free.fr
 * 
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */

package org.natbraille.core.gestionnaires;

import java.io.BufferedWriter;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.io.UnsupportedEncodingException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

/**
 * Génère les fichiers de log
 * @author bruno
 *
 */
public class AfficheurLog extends Afficheur
{

	public AfficheurLog(int logLevel) {
		setLogLevel(logLevel);
	}
	/** l'adresse du fichier de log*/
	private String log_file = "/tmp/nat_log.1";
	/** l'encodage du fichier de log */
	private String encodage = "UTF-8";
	/** la date et l'heure du log à insérer */
	private Date date;
	
	/** 
	 * écrit le message <code>s</code> dans le fichier <code>log_file</code>
	 * @see gestionnaires.Afficheur#afficheMessage(java.lang.String)
	 */
//	@Override
    public void afficheMessage(MessageKind m, String s)
	{
		BufferedWriter bw;
		date = Calendar.getInstance().getTime();
		SimpleDateFormat formatter = new SimpleDateFormat("\n[dd/MM/yyyy-HH:mm:ss]");
		String formattedDate = formatter.format(date);
		try {
			bw = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(log_file,true),encodage));
			bw.write(formattedDate + s);
			bw.close();
		} catch (UnsupportedEncodingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}		
	}
	/**
	 * @param filename l'adresse du fichier de log
	 */
	public void setLogFile(String filename){log_file = filename;}
	@Override

	public void afficheMessage(LogMessage logMessage) {
		afficheMessage(logMessage.getKind(), logMessage.getContents().toString());
	}


}
