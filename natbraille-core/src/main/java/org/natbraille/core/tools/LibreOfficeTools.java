package org.natbraille.core.tools;

import java.io.IOException;
import org.natbraille.soffice.AutoConfigurator;
import org.natbraille.soffice.SofficeConfig;
import org.natbraille.soffice.SofficeConverter;
import org.natbraille.soffice.SofficeDisposableConverter;

/**
 * autoconfigured @{org.natbraille.soffice.SofficeConverter} singleton.
 * @author vivien
 */
public class LibreOfficeTools {

    /*
     * singleton
     */
    public static volatile LibreOfficeTools instance = null;

    public static LibreOfficeTools getInstance() throws IOException {
        if (instance == null) {
            synchronized (LibreOfficeTools.class) {
                if (instance == null) {
                    instance = new LibreOfficeTools();
                }

            }

        }
        return instance;
    }

    
    /**
     * content
     */
    private final SofficeConverter sofficeConverter;
    
    private LibreOfficeTools() throws IOException {
        SofficeConfig sofficeConfig = new AutoConfigurator().getSofficeConfig();
        this.sofficeConverter = new SofficeConverter(sofficeConfig);
        
        
        
        
    }

    /**
     * get the configured @{link org.natbraille.soffice.SofficeConverter}.     
     * @return
     * @throws IOException 
     */
    public static SofficeConverter getSofficeConverter() throws IOException {
        return getInstance().sofficeConverter;
    }


    /**
     * cleanup : must be called before exiting. 
     */
    public static void stopServer() {
        // does nothing for now
    }

}
