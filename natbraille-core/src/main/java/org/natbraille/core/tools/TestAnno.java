/*
 * NatBraille - An universal Translator
 * Copyright (C) 2005 Bruno Mascret
 * Contact: bmascret@free.fr
 * 
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */

package org.natbraille.core.tools;

import java.lang.annotation.Annotation;
import java.lang.reflect.Field;
import java.util.ArrayList;
import org.natbraille.core.filter.options.NatFilterOption;
import org.natbraille.core.filter.options.NatFilterOptionDetail;

public class TestAnno {


	static ArrayList<Annotation> getAnnotations(Class<?> enumClass,String fieldName, Class<?> annotationClass) throws NoSuchFieldException, SecurityException{
		Annotation[] od  = enumClass.getField(fieldName).getAnnotations();
		ArrayList<Annotation> details = new ArrayList<>();
		for (Annotation a : od){
			if (a.annotationType().equals(annotationClass)){
				details.add(a);
			}			
		}
		return details;
	}


	static ArrayList<Annotation> getEnumAnnotations(Field field,Class<?> annotationClass) throws NoSuchFieldException, SecurityException{
		
		Annotation[] od  = field.getAnnotations();
		ArrayList<Annotation> details = new ArrayList<>();
		for (Annotation a : od){
			if (a.annotationType().equals(annotationClass)){
				details.add(a);
			}			
		}
		return details;
	}

	static ArrayList<Annotation> getEnumAnnotations(NatFilterOption opt,Class<?> annotationClass) throws NoSuchFieldException, SecurityException{		
		Field field = opt.getClass().getField(opt.name());
		Annotation[] od  = field.getAnnotations();
		ArrayList<Annotation> details = new ArrayList<>();
		for (Annotation a : od){
			if (a.annotationType().equals(annotationClass)){
				details.add(a);
			}			
		}
		return details;
	}

	/**
	 * @param args
	 * @throws SecurityException 
	 * @throws NoSuchFieldException 
	 */
	public static void main(String[] args) throws NoSuchFieldException, SecurityException {
		// TODO Auto-generated method stub
		
		
		NatFilterOption opt = NatFilterOption.CONFIG_VERSION;
		ArrayList<Annotation> an = getAnnotations(NatFilterOption.class,opt.name(),NatFilterOptionDetail.class);
		
	
		for (Annotation a : an){
			Class<? extends Annotation> type = a.annotationType();
			if (type.equals(NatFilterOptionDetail.class)){
				NatFilterOptionDetail optDet = (NatFilterOptionDetail) a;
				System.out.println(optDet.comment());
				System.out.println(optDet.defaultValue());				
			}			
		}
		
		ArrayList<Annotation> an1 = getEnumAnnotations(NatFilterOption.class.getField(opt.name()),
				NatFilterOptionDetail.class);

		for (Annotation a : an1){
			Class<? extends Annotation> type = a.annotationType();
			if (type.equals(NatFilterOptionDetail.class)){
				NatFilterOptionDetail optDet = (NatFilterOptionDetail) a;
				System.out.println(optDet.comment());
				System.out.println(optDet.defaultValue());				
			}			
		}

		ArrayList<Annotation> an2 = getEnumAnnotations(opt,
				NatFilterOptionDetail.class);

		for (Annotation a : an2){
			Class<? extends Annotation> type = a.annotationType();
			if (type.equals(NatFilterOptionDetail.class)){
				NatFilterOptionDetail optDet = (NatFilterOptionDetail) a;
				System.out.println(optDet.comment());
				System.out.println(optDet.defaultValue());				
			}			
		}
		
	}

}
