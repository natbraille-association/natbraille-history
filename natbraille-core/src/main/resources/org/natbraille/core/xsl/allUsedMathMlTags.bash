#!/bin/sh

# Find all mathml tags used in stylesheets.
# Find use of m:thisoneforexemple in all files 
# and list the something.

find . -type f -print0 | \
    xargs -0 grep 'm:' | \
    sed 's/m:/\n:::/g' | \
    grep ':::' | \
    sed 's/:::\([a-Z-]*\).*/\1/g' | \
    sort | \
    uniq
