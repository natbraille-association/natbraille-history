my $num=1;
my %ids;
print "digraph {\n";
print "overlap = scale;\n";
print "splines = true;\n";
print "labelfontsize = 30;\n";
foreach (<STDIN>){
    if ($_ =~ /(.*?)\:.*href=\"(.*)\"/){
	my ($id1,$id2) = ($ids{$1},$ids{$2});

	if (!exists $ids{$1}){
	    $id1 = $num++;
	    $ids{$1} = 'noeud'.$id1;
	    print $ids{$1}.' [label="'.$1.'"]'."\n";
	}
	if (!exists $ids{$2}){
	    $id2 = $num++;
	    $ids{$2} = 'noeud'.$id2;
	    print $ids{$2}.' [label="'.$2.'"]'."\n"
	}
	print "$ids{$1} -> $ids{$2}\n";

    }
}
print "}\n";
