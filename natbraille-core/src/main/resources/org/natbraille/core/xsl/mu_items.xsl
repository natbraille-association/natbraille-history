<!DOCTYPE xsl:stylesheet SYSTEM "nat://system/xsl/mmlents/windob.dtd">

<xsl:stylesheet version="2.0"
		xmlns:xs='http://www.w3.org/2001/XMLSchema'
		xmlns:xsl='http://www.w3.org/1999/XSL/Transform'
		xmlns:fn='http://www.w3.org/2005/xpath-functions'
		xmlns:functx='http://www.functx.com' 
		xmlns:nat='http://natbraille.free.fr/xsl' >
  <xsl:output method="xml" indent="yes" />

  <!-- 
       debug mode
  -->
  <xsl:function name="nat:mdbg" as="xs:string?" >
    <xsl:param name="txt" as="xs:string" />

      <xsl:if test="$mdbg">
	  <xsl:value-of select=" $txt " />    
      </xsl:if>

  </xsl:function>

  <xsl:function name="nat:framelem" as="node()" >

    <xsl:param name="elname" as="xs:string" />
    <xsl:param name="txt" as="xs:string" />
    
    <xsl:element name="{$elname}">
      <xsl:value-of select="$txt"/>
    </xsl:element>
    
  </xsl:function>

  <!-- create a "lit" tree from words -->
  <xsl:function name="nat:plaintxt2lit" as="node()">
    <xsl:param name="txt" as="xs:string*" />
    <xsl:variable name="txtjoint" as="xs:string" select="string-join($txt,' ')" />
    <xsl:element name="lit">      
      <xsl:for-each select="tokenize($txtjoint,' ')">
	<xsl:element name="mot">
	  <xsl:value-of select="."/>
	</xsl:element>
      </xsl:for-each>
    </xsl:element>
  </xsl:function>
  
  <!-- 
       nat:get_armure_alters
       - prend +1 pour # , +2 pour ##, -3 pour bbb
       - renvoie une liste des notes avec leur altération de l'armure
       <note>  <step>C</step>  <alter>0</alter>   </note>
       <note>  <step>D</step>  <alter>0</alter>   </note>
       <note>  <step>E</step>  <alter>0</alter>   </note>
       <note>  <step>F</step>  <alter>1</alter>   </note>
       ...
  -->
  <xsl:function name="nat:get_armure_alters" as="node()*" >
    <xsl:param name="fifths"  as="xs:integer"/>  <!-- prend +1 pour # , +2 pour ##, -3 pour bbb -->
    <xsl:variable name='alterations'>
      <xsl:choose>
	<xsl:when test="$fifths &gt; 0">
	  <xsl:value-of select="fn:substring('FCGDAEB',1,$fifths)" />
	</xsl:when>
	<xsl:when test="$fifths &lt; 0">
	  <xsl:value-of select="fn:substring('BEADGCF',1,fn:abs($fifths))" />
	</xsl:when>
      </xsl:choose>
    </xsl:variable>
    <xsl:variable name="sequence_notes" as="xs:string*" select="('C','D','E','F','G','A','B')" />
    <xsl:for-each select="$sequence_notes">
      <xsl:element name="pitch">
	<xsl:element name="step">
	  <xsl:value-of select="." />
	</xsl:element>
	<xsl:element name="alter">
	  <xsl:choose>
	    <xsl:when test="contains($alterations,.)">
	      <xsl:value-of select="if ($fifths &gt; 0) then (+1) else (-1)" />
	    </xsl:when>
	    <xsl:otherwise>
	      <xsl:value-of select="0" />
	    </xsl:otherwise>
	  </xsl:choose>
	</xsl:element>
      </xsl:element>
    </xsl:for-each>
  </xsl:function>


  <!--
      nat:brl_keysig
      - prend un <fifths> 
      - renvoie l'armure en braille     &pt146; dieze ; &pt126; bémol ; &pt16; becarre
  -->
  <xsl:function name="nat:brl_keysig" as="xs:string" >
    <xsl:param name="fifths" as="xs:integer"/>
    
    <xsl:variable name="abs" select="fn:abs($fifths)" />
    <xsl:variable name="al_sign" select="if ($abs &lt; 0)  then ('&pt126;')  else (if ($abs &gt; 0) then ('&pt146;') else ('') )" />
    
    <xsl:value-of select="if ($abs = 1) then  ($al_sign) else (
			  if ($abs = 2) then (fn:concat($al_sign,$al_sign)) else (
			  if ($abs = 3) then (fn:concat($al_sign,$al_sign,$al_sign)) else (
			  if ($abs &gt; 3) then (fn:concat($abs,$al_sign)) else ())))
			  "/>
  </xsl:function>


  <xsl:function name="nat:brl_accidental" as="xs:string?" >
    <xsl:param name="alter" as="xs:string?"/>
    <xsl:choose>
      <xsl:when test="$alter">
	<xsl:variable name="alter_trans">
	  <h k="sharp">&pt146;</h>
	  <h k="flat">&pt126;</h>
	  <h k="natural">&pt16;</h>
	</xsl:variable>
	<xsl:value-of select="functx:id-untyped($alter_trans,$alter)" />
      </xsl:when>
      <xsl:otherwise>	
      </xsl:otherwise>
    </xsl:choose>
  </xsl:function>

  <!--
      nat:brl_octave_mark
      - takes a <pitch>  
      - returns octave mark 
  -->
  <xsl:function name="nat:brl_octave_mark" as="xs:string">
    <xsl:param name="pitch_t" as="node()"/>

    <xsl:variable name="brl_octave_marks">
      <h>&pt4;</h>
      <h>&pt45;</h>
      <h>&pt456;</h>
      <h>&pt5;</h>
      <h>&pt46;</h>
      <h>&pt56;</h>
      <h>&pt6;</h>
    </xsl:variable>

    <xsl:value-of select="$brl_octave_marks/h[position()=$pitch_t/octave]"/>
  </xsl:function>

  <!--
      nat:brl_octave_mark
      - takes an old <pitch> and a new <pitch>
      - returns octave mark if needed
  -->
  <xsl:function name="nat:brl_octave_mark" as="xs:string">

    <xsl:param name="pitch_f" as="node()"/>
    <xsl:param name="pitch_t" as="node()"/>

    <xsl:variable name="abs_interval_class" select="1 + abs(nat:pitches_to_interval_class($pitch_f,$pitch_t))" />

    <!--
	if the distance between the two notes 
	1. is an interval of <= 3rd     => no octave mark
	2. is an interval of >= 6th     => octave mark
	3. is an interval of 4th or 5th
	3a. and two notes in the same octave => no octave mark
	3b. and two notes in different octave => octave mark
	
	surprise ! il faut aussi une marque d'octave lorsque l'on est passé à la ligne !
    -->

    <xsl:variable name="om">
      <xsl:choose>
	<xsl:when test="$abs_interval_class &gt; 5">
	  <xsl:value-of select="$pitch_t/octave" />
	</xsl:when>
	<xsl:when test="$abs_interval_class &gt; 3">
	  <xsl:value-of select="if ($pitch_f/octave = $pitch_t/octave) then ('') else ($pitch_t/octave)" />
	</xsl:when>
	<xsl:otherwise>
	  <xsl:value-of select="''" />
	</xsl:otherwise>
      </xsl:choose>
    </xsl:variable>
    

    <xsl:choose>
      <xsl:when test="$om != ''">
	<xsl:value-of select="nat:brl_octave_mark($pitch_t)"/>
      </xsl:when>
      <xsl:otherwise>
	<xsl:value-of select="''" />
      </xsl:otherwise>
    </xsl:choose>
    
  </xsl:function>


  <!-- 
       nat:pitches_to_interval_class
       - takes an old <pitch> and a new <pitch>
       - return the interval class as <integer>
       !! unisson = 0
       !! seconde = 1
       !! ... 
  -->
  <xsl:function name="nat:pitches_to_interval_class" as="xs:integer">
    <xsl:param name="pitch_f" as="node()"/>
    <xsl:param name="pitch_t" as="node()"/>

    <xsl:variable name="note_equiv">
      <h k="C">0</h>
      <h k="D">1</h>
      <h k="E">2</h>
      <h k="F">3</h>
      <h k="G">4</h>
      <h k="A">5</h>
      <h k="B">6</h>
    </xsl:variable>
    <xsl:variable name="abs_deg_f" select="functx:id-untyped($note_equiv,$pitch_f/step) + (7 * $pitch_f/octave)" />
    <xsl:variable name="abs_deg_t" select="functx:id-untyped($note_equiv,$pitch_t/step) + (7 * $pitch_t/octave)" />
    <xsl:value-of select="($abs_deg_t - $abs_deg_f)" />

  </xsl:function>
  
  <!--
       nat:pitch_to_interval_class
       - takes a <pitch>
       - return the interval class from C0
       !! seconde = 1
       !! ..
  -->
  <xsl:function name="nat:pitches_to_interval_class" as="xs:integer">
    <xsl:param name="pitch" as="node()"/>
    <xsl:variable name="ref">
      <pitch>
	<note>C</note>
	<octave>0</octave>
      </pitch>
    </xsl:variable>
    <xsl:value-of select="nat:pitches_to_interval_class($ref,$pitch)"/>


  </xsl:function>
  <!-- 
       nat:brl_pitch_type
       - takes a <step> , a <type> and the number of dots
       - return the corresponding brl symbol
  -->
  <xsl:function name="nat:brl_pitch_type" as="xs:string">

    <xsl:param name="step" as="xs:string"/>  <!-- C,D,E,... -->
    <xsl:param name="type" as="xs:string"/>  <!-- 'whole' ou 'half' ou 'quarter' ou 'eight' .... -->
    <xsl:param name="dot"  as="xs:integer"/>  <!-- 'dot'  .... -->

    <xsl:variable name="type_equiv">
      <h k="whole" >whole</h>
      <h k="half">half</h>
      <h k="quarter">quarter</h>
      <h k="eighth">eighth</h>
      <h k="16th" >whole</h>
      <h k="32th">half</h>
      <h k="64th">quarter</h>
      <h k="128th">eighth</h>
    </xsl:variable>

    <xsl:variable name="pt">
      <h k="Cwhole">&pt13456;</h> <!--3 6 -->   <!-- 145 -->
      <h k="Dwhole">&pt135;</h>                <!-- 15  -->
      <h k="Ewhole">&pt12346;</h>              <!-- 124 -->
      <h k="Fwhole">&pt123456;</h>             <!-- 1245 -->
      <h k="Gwhole">&pt12356;</h>              <!-- 125 -->
      <h k="Awhole">&pt2346;</h>               <!-- 24  --> 
      <h k="Bwhole">&pt23456;</h>              <!-- 245 -->
      <h k="Chalf">&pt1345;</h> <!--3 -->
      <h k="Dhalf">&pt135;</h>
      <h k="Ehalf">&pt1234;</h>
      <h k="Fhalf">&pt12345;</h>
      <h k="Ghalf">&pt1235;</h>
      <h k="Ahalf">&pt234;</h>
      <h k="Bhalf">&pt2345;</h>
      <h k="Cquarter">&pt1456;</h> <!--6 -->
      <h k="Dquarter">&pt156;</h>
      <h k="Equarter">&pt1246;</h>
      <h k="Fquarter">&pt12456;</h>
      <h k="Gquarter">&pt1256;</h>
      <h k="Aquarter">&pt246;</h>
      <h k="Bquarter">&pt2456;</h>
      <h k="Ceighth">&pt145;</h>  <!-- -->
      <h k="Deighth">&pt15;</h>
      <h k="Eeighth">&pt124;</h>
      <h k="Feighth">&pt1245;</h>
      <h k="Geighth">&pt125;</h>
      <h k="Aeighth">&pt24;</h>
      <h k="Beighth">&pt245;</h>
    </xsl:variable>

    <xsl:variable name="r_type" select="functx:id-untyped($type_equiv,$type)" />
    <xsl:value-of>
      <xsl:value-of select="functx:id-untyped($pt,fn:concat($step,$r_type))" />
      <xsl:value-of select="functx:repeat-string('&pt3;', $dot)" />
    </xsl:value-of>

  </xsl:function>

  <!-- 
       nat:brl_rest_type
       correponds to nat:brl_pitch_type for rests
  -->
  <xsl:function name="nat:brl_rest_type" as="xs:string">
    <xsl:param name="type" as="xs:string"/>  <!-- 'whole' ou 'half' ou 'quarter' ou 'eight' .... -->
    <xsl:param name="dot"  as="xs:integer"/>  <!-- 'dot'  .... -->

    <xsl:variable name="type_equiv">
      <h k="whole" >whole</h>
      <h k="half">half</h>
      <h k="quarter">quarter</h>
      <h k="eighth">eighth</h>
      <h k="16th" >whole</h>
      <h k="32th">half</h>
      <h k="64th">quarter</h>
      <h k="128th">eighth</h>
    </xsl:variable>

    <xsl:variable name="pt">
      <h k="whole">&pt134;</h>
      <h k="half">&pt136;</h>
      <h k="quarter">&pt1236;</h>
      <h k="eighth">&pt1346;</h>
    </xsl:variable>

    <xsl:variable name="r_type" select="functx:id-untyped($type_equiv,$type)" />

    <xsl:value-of>
      <xsl:value-of select="functx:id-untyped($pt,$r_type)" />
      <xsl:value-of select="functx:repeat-string('&pt3;', $dot)" />
    </xsl:value-of>

  </xsl:function>


  <!-- 
       nat:brl_timesig
       - gets a <time> (of <attribute>)
       - returns the brl time signature
  -->
  <xsl:function name="nat:brl_timesig" as="xs:string"> <!-- La mesure se place en début de morceau entre deux espaces. -->
    <xsl:param name="time" as="node()" />
    
    <xsl:variable name="brl_beats">
      <h k="0">&pt245;</h>
      <h k="1">&pt1;</h>
      <h k="2">&pt12;</h>
      <h k="3">&pt14;</h>
      <h k="4">&pt145;</h>
      <h k="5">&pt15;</h>
      <h k="6">&pt124;</h>
      <h k="7">&pt1245;</h>
      <h k="8">&pt125;</h>
      <h k="9">&pt24;</h>
    </xsl:variable>

    <xsl:variable name="brl_beats_type">
      <h k="0">&pt356;</h>
      <h k="1">&pt2;</h>
      <h k="2">&pt23;</h>
      <h k="3">&pt25;</h>
      <h k="4">&pt256;</h>
      <h k="5">&pt26;</h>
      <h k="6">&pt235;</h>
      <h k="7">&pt2356;</h>
      <h k="8">&pt236;</h>
      <h k="9">&pt35;</h>
    </xsl:variable>

    <xsl:choose>
      <xsl:when test="($time/beats=4) and ($time/beat-type=4)" >
	<xsl:text>&pt46;&pt14;</xsl:text> <!-- C -->
      </xsl:when>
      <xsl:when test="($time/beats=2) and ($time/beat-type=2)" >
	<xsl:text>&pt456;&pt14;</xsl:text> <!-- C barre-->
      </xsl:when>
      <xsl:otherwise>
	<xsl:value-of >
	  <xsl:for-each select="functx:chars($time/beats)" >
	    <xsl:value-of select="functx:id-untyped($brl_beats,.)" />
	  </xsl:for-each>
	  <xsl:for-each select="functx:chars($time/beat-type)" >
	    <xsl:value-of select="functx:id-untyped($brl_beats_type,.)" />
	  </xsl:for-each>
	</xsl:value-of>
      </xsl:otherwise>
    </xsl:choose>
  </xsl:function>


  <!-- 
       returns the brl bar sign  from a @bar-style
  -->
  <xsl:function name="nat:brl_bar" as="xs:string?">
    <xsl:param name="bar-style" as="xs:string?"/>
    <xsl:variable name="bar-style-brl">
      <h k="">&pt;</h>
      <h k="regular">&pt;</h>
      <h k="dotted">dotted</h>
      <h k="dashed">dashed</h>
      <h k="heavy">heavy</h>
      <h k="light-light">&pt126;&pt13;&pt3;</h>
      <h k="light-heavy">&pt126;&pt13;</h>
      <h k="heavy-light">heavy-heavy</h>
      <h k="tick">tick</h>
      <h k="short">short</h>
      <h k="none">none</h>
    </xsl:variable>
    <xsl:value-of>
      <xsl:value-of select="nat:mdbg('mes.bar:')" />
      <xsl:value-of select="functx:id-untyped($bar-style-brl,$bar-style)" />
    </xsl:value-of>

  </xsl:function>

  <xsl:function name="nat:brl_dynamics" as="xs:string">
    <xsl:param name="dynamic" as="node()"/>
    <xsl:value-of select="fn:concat('&pt345;',local-name($dynamic/*))"/> <!--'&pt256;' -->
  </xsl:function>

  
  <!-- 
       nat:brl_accord_interv
       - get three pitches : 
       = base : chord lower or upper note
       = prev : previous note in chord 
       = cur  : note in the chord to be represented
       - returns the brl note of chord interval representation
  -->
  <xsl:function name="nat:brl_accord_interv" as="xs:string">
    <xsl:param name="pitch_base" as="node()"/>
    <xsl:param name="pitch_prev" as="node()"/>
    <xsl:param name="pitch_cur" as="node()"/>

    <xsl:variable name="i_bc" select="abs(nat:pitches_to_interval_class($pitch_base,$pitch_cur))+1"/>
    <xsl:variable name="i_pc" select="abs(nat:pitches_to_interval_class($pitch_prev,$pitch_cur))+1"/>

    <xsl:variable name="needsOctaveMark" select="$i_pc>=8" />

    <xsl:variable name="brl_interval">
      <h k="1">&pt36;</h>       <!-- octave -->
      <h k="2">&pt34;</h>       <!-- second -->
      <h k="3">&pt346;</h>      <!-- third -->
      <h k="4">&pt3456;</h>     <!-- fourth -->
      <h k="5">&pt35;</h>       <!-- fifth -->
      <h k="6">&pt356;</h>      <!-- sixth -->
      <h k="7">&pt25;</h>       <!-- seventh -->
      <h k="8">&pt36;</h>       <!-- real octave (not used <- mod 8) --> 

    </xsl:variable>

    <xsl:value-of select="concat(
			  (if ($needsOctaveMark) then (nat:mdbg('oct:')) else ('')),
			  (if ($needsOctaveMark) then (nat:brl_octave_mark($pitch_cur)) else ('')),
			  (nat:mdbg('interv:')),
			  ($brl_interval/h[@k=(( ($i_bc -1) mod 7)+1)])
			  )" />
  </xsl:function>

  <!-- 
       ELEMENT notations
       %editorial;, 
       (tied | slur | tuplet | glissando | slide | 
       ornaments | technical | articulations | dynamics |
       fermata | arpeggiate | non-arpeggiate | 
       accidental-mark | other-notation)*)
  -->

  <!--
      ELEMENT articulations
      (accent | strong-accent | staccato | tenuto |
      detached-legato | staccatissimo | spiccato |
      scoop | plop | doit | falloff | breath-mark | 
      caesura | stress | unstress | other-articulation)*)
  -->
  <!--
      ELEMENT technical
      (up-bow | down-bow | harmonic | open-string |
      thumb-position | fingering | pluck | double-tongue |
      triple-tongue | stopped | snap-pizzicato | fret |
      string | hammer-on | pull-off | bend | tap | heel |
      toe | fingernails | other-technical)*
  -->
  <!--
      ELEMENT ornaments
      (trill-mark|turn|delayed-turn|inverted-turn|
      shake|wavy-line|mordent|inverted-mordent|
      schleifer|tremolo)
  -->
  <!--
      marginal hand sign
      clef sign?
      forward-repeat sign
      first or second ending sign
      reminder tie?
      change of clef sign
      change of hand sign
      pedal depression (in piano music)
      simple word-sign expression
      line of continuation sign
      opening brack slur or overlapping slur
      music comma
      triplet or irregular-grouping sign
      larger or smaller value sign
      up-bow or down-bow
      accidental(s) for ornament (upper before lower)
      ornament or arpeggio
      signs of expression or execution that precede a note
      staccato or staccatissimo
      accent
      tenuto
      any other...
      accidental
      octave mark

NOTE

dot
finger mark
interval
finger mark for interval
tie for interval
fractioning (note repetition) or tremolo sign
fermata
single slur
slur between staves or opening double slur
closing bracket slur
tie chord tie or acculating arpeggio sign
termination sign for line of continuation of "hairpin"
breath mark
music comma (if needed)
pedal release (in piano music)
closing bar or backward-repeat sign
music hyphen
  -->
</xsl:stylesheet>


