<?xml version="1.1" encoding="utf-8"?>



<!DOCTYPE xsl:stylesheet SYSTEM "nat://system/xsl/mmlents/windob.dtd">

<xsl:stylesheet version="2.0"
		xmlns:xs='http://www.w3.org/2001/XMLSchema'
		xmlns:xsl='http://www.w3.org/1999/XSL/Transform'
		xmlns:fn='http://www.w3.org/2005/xpath-functions'
		xmlns:nat='http://natbraille.free.fr/xsl' >

  <xsl:output method="xml" indent="yes" />

<!--
  <xsl:template match="flat-score-partwise">
    <xsl:element name="titre">
      <xsl:attribute name="niveauBraille" select="1"/>
      <xsl:variable name="exprLit">
	<xsl:copy-of select="nat:plaintxt2lit(work/work-title)"/>
	<xsl:copy-of select="nat:plaintxt2lit(score-direction)"/>
      </xsl:variable>
      <xsl:apply-templates select="$exprLit"/>
    </xsl:element>
  </xsl:template>
-->
  

  <!-- 
       One recursive part iteration
  --> 
  <xsl:template name="trAduc">
    
    <xsl:param name="in_attributes" />
    <xsl:param name="in_num_child"  />
    <xsl:param name="in_max_num_child" />
    <xsl:param name="in_comparation_pitch" />
    <xsl:param name="in_past_is_word" />

    <xsl:variable name="c_is_name" select="fn:not(fn:compare(local-name(*[$in_num_child]),'name'))" />
    <xsl:variable name="c_is_attribute_key" select="*[$in_num_child]/key/fifths" />
    <xsl:variable name="c_is_note" select="*[$in_num_child]/pitch/step" />
    <xsl:variable name="c_is_rest" select="*[$in_num_child]/rest" />
    <xsl:variable name="c_is_section" select="*[$in_num_child]/section" />
    <xsl:variable name="c_is_copule" select="local-name(*[$in_num_child]) = 'copule'" />


    <!-- is? a chord -->
    
    <xsl:variable name="c_is_chorded" select="(*[$in_num_child]/chord)
					      and ( *[$in_num_child - 1]/@start-time = *[$in_num_child]/@start-time)
					      and ( *[$in_num_child - 1]/@end-time = *[$in_num_child]/@end-time)"/>

    <xsl:variable name="c_is_measure_bar" select="local-name(*[$in_num_child]) = 'barline'" />

    <!-- 
	 <xsl:message> 
	 <xsl:copy-of select="local-name(*[$in_num_child])"/> 
	 </xsl:message> 
    -->
    <xsl:variable name="c_is_attribute_time" select="*[$in_num_child]/time/beats" />  
    <xsl:variable name="c_is_tied" 
		  select="if ((*[$in_num_child]/notations/tied[@type='start'])) 
			  then (fn:true())
			  else (fn:false())
			  "/>
    <xsl:variable name="c_is_dynamics" select="*[$in_num_child]/direction-type/dynamics" />
    <xsl:variable name="c_is_wedge" select="*[$in_num_child]/direction-type/wedge" />
    

    <!-- get this element attributes (this one or last one) -->
    <xsl:variable name="c_attributes">
      <xsl:copy-of select="if ($c_is_attribute_key) then (*[$in_num_child]) else ($in_attributes/attributes)" />
    </xsl:variable>

    <xsl:variable name="c_armure"> 
      <xsl:copy-of select="nat:get_armure_alters($c_attributes/attributes/key/fifths)" />
    </xsl:variable>   

    <!-- display copule if it's a copule 
         and display copulepartielle if there is a section mark in 
	 measure -->
    <xsl:if test="$c_is_copule">
      <xsl:value-of select="nat:mdbg('copule:')" />
      <xsl:value-of select="if (./section) then ('&pt5;&pt2;') else ('&pt126;&pt345;')"/>
    </xsl:if>

    <!-- display section if it's a coupure -->
    <xsl:if test="$c_is_section">
      <xsl:value-of select="nat:mdbg('section:')" />
      <xsl:value-of select="'&pt46;&pt13;'"/>
    </xsl:if>
    
    <!-- display measure_bar if it's a measure_start -->
    <!--
	<xsl:if test="$c_is_measure_start" >      
	<xsl:value-of select="nat:mdbg('&#xA;')"/>     
	</xsl:if>
    -->

    <!-- display measure_bar if it's a measure_bar -->

    
    <xsl:if test="$c_is_measure_bar" >      
      <xsl:value-of select="nat:brl_bar(*[$in_num_child]/bar-style)"/>
    </xsl:if>


    <!-- display the time signature there's a time signature -->
    <xsl:if test="$c_is_attribute_time" >
      <xsl:value-of select="nat:mdbg('time:')"/>
      <xsl:value-of select="nat:brl_timesig(*[$in_num_child]/time)" />
    </xsl:if>

    <!-- display the key signature if there's a key signature-->
    <xsl:if test="$c_is_attribute_key" >
      <xsl:value-of select="nat:mdbg('key:')"/>
      <xsl:value-of select="nat:brl_keysig(*[$in_num_child]/key/fifths)" />
    </xsl:if>

    <!-- display dynamics p f mf ... -->
    <xsl:if test="$c_is_dynamics">
      <xsl:value-of select="nat:mdbg('dynamics:')" />
      <xsl:value-of select="nat:brl_dynamics(*[$in_num_child]/direction-type/dynamics)"/>
    </xsl:if>
    
    <!-- display rest if it's a note/rest -->
    <xsl:if test="$c_is_rest" >
      <xsl:value-of select="nat:mdbg('rest:')"/>
      <xsl:value-of select="nat:brl_rest_type(*[$in_num_child]/type,count(*[$in_num_child]/dot))"/>
    </xsl:if>

    <!-- crec  and diminuendo -->
    <xsl:if test="$c_is_wedge">
      <!-- crescendo -->
      <xsl:if test="*[$in_num_child]/direction-type/wedge[@type='crescendo']">
	<xsl:value-of select="nat:mdbg('cresc.:')"/>
	<xsl:value-of select="'&pt345;&pt14;'" />
      </xsl:if>
      <!-- diminuendo -->
      <xsl:if test="*[$in_num_child]/direction-type/wedge[@type='diminuendo']">
	<xsl:value-of select="nat:mdbg('decresc:')"/>
	<xsl:value-of select="'&pt345;&pt145;'" />
      </xsl:if>
      <!-- wedge end -->
      <xsl:if test="*[$in_num_child]/direction-type/wedge[@type='stop']">
	<!-- adds the wedge stop only if there is no rest or dynamics after -->
	<xsl:variable name="nextelems">
	  <xsl:copy-of select="*[$in_num_child]/following::*[self::step|self::dynamics|self::rest][position()=1]"/>
	</xsl:variable>
	<xsl:value-of select="nat:mdbg('finsoufflet?.')" />
	<xsl:if test="$nextelems/step"> 
	  <!-- ends a crescendo or a diminuendo ? -->
	  <xsl:variable name="typefinwedge" 
			select="*[$in_num_child]/preceding-sibling::direction/direction-type/wedge[@type=('crescendo','diminuendo')]/@type"/>
	  <xsl:value-of select="if ($typefinwedge[last()] = 'crescendo') then ('&pt345;&pt25;')	else ('&pt345;&pt256;')"/>
	</xsl:if>
      </xsl:if>
    </xsl:if>
    

    <xsl:if test="$c_is_note" >
      <xsl:variable name="n" select="*[$in_num_child]/notations"/>
      <!--
	  clef?
	  reminder tie?
	  simple word-sign expression
	  line of continuation sign
	  opening bracket slur or overlapping slur
	  up-bow or down-bow
      -->

      <!-- up-bow or down-bow -->
      <xsl:if test="$n/technical/up-bow"><xsl:value-of select="'&pt126;&pt3;'"/></xsl:if>
      <xsl:if test="$n/technical/down-bow"><xsl:value-of select="'&pt126;&pt12;'"/></xsl:if>
      
      <!-- expression/execution :  staccato or staccatissimo / accent /  tenuto -->
      <xsl:if test="$n/articulations/staccato"><xsl:value-of select="'&pt236;'"/></xsl:if>
      <xsl:if test="$n/articulations/staccatissimo"><xsl:value-of select="'&pt6;&pt236;'"/></xsl:if>
      <xsl:if test="$n/articulations/accent"><xsl:value-of select="'&pt46;&pt236;'"/></xsl:if>           
      <xsl:if test="$n/articulations/strong-accent[@type='up']"><xsl:value-of select="'strong-accent-up'"/></xsl:if>
      <xsl:if test="$n/articulations/strong-accent[@type='down']"><xsl:value-of select="'&pt132456;'"/></xsl:if>
      <xsl:if test="$n/articulations/tenuto"><xsl:value-of select="'&pt456;&pt236;'"/></xsl:if>
      <xsl:if test="$n/technical/stopped"><xsl:value-of select="'&pt123456;'"/></xsl:if>       
      <xsl:if test="$n/ornaments/inverted-turn"><xsl:value-of select="'&pt123456;'"/></xsl:if>
      <xsl:if test="$n/ornaments/turn"><xsl:value-of select="'&pt123456;'"/></xsl:if>
      <xsl:if test="$n/ornaments/trill-mark"><xsl:value-of select="'&pt123456;'"/></xsl:if>
      <xsl:if test="$n/ornaments/inverted-mordent"><xsl:value-of select="'&pt123456;'"/></xsl:if>
      <xsl:if test="$n/ornaments/mordent"><xsl:value-of select="'&pt123456;'"/></xsl:if>
      
      <!-- accidental -->
      <xsl:value-of select="nat:brl_accidental(*[$in_num_child]/accidental)"/>   

      <xsl:choose>
	<xsl:when test="$c_is_chorded">
	  <xsl:variable name="pitch_base" select="*[$in_num_child]/preceding-sibling::*[not(chord)][1]/pitch"/>
	  <xsl:variable name="pitch_prev" select="*[$in_num_child - 1][chord]/pitch"/>	  
	  <xsl:variable name="pitch_cur"  select="*[$in_num_child]/pitch"/>
	  <xsl:value-of select="nat:brl_accord_interv(
				$pitch_base,
				(if ($pitch_prev) then ($pitch_prev) else ($pitch_base)),
				$pitch_cur
				)"/>
	</xsl:when>
	<xsl:otherwise>

	  <!-- octave mark -->
	  <xsl:variable name="octave_mark"
			select="if ($in_comparation_pitch and not($in_past_is_word))
				then (nat:brl_octave_mark($in_comparation_pitch,*[$in_num_child]/pitch))
				else (nat:brl_octave_mark(*[$in_num_child]/pitch))  " />
	  
	  <xsl:if test="fn:compare('',$octave_mark)">
	    <xsl:value-of select="nat:mdbg('oct:')"/>
	    <xsl:value-of select="$octave_mark"/>
	  </xsl:if>
	  
	  <!-- pitch & length -->
	  <xsl:value-of select="nat:mdbg('not:')"/>
	  <xsl:value-of select="nat:brl_pitch_type(*[$in_num_child]/pitch/step,
				*[$in_num_child]/type,
				count(*[$in_num_child]/dot))" />     
	</xsl:otherwise>
      </xsl:choose>
      

      <!-- finger mark -->

      <!-- fermata -->
      <xsl:if test="$n/fermata[@type='upright']"><xsl:value-of select="'fermata'"/></xsl:if>
      <xsl:if test="$n/fermata[@type='inverted']"><xsl:value-of select="'fermata-inverted'"/></xsl:if>

      <!-- single slur or opening double slur -->
      <!-- closing bracket slur -->
      <!-- tie -->
      <xsl:if test="$c_is_tied">
	<xsl:value-of select="nat:mdbg('tie:')"/>
	<xsl:value-of select="'&pt4;&pt14;'" />
      </xsl:if>
      

      <xsl:if test="$n/articulations/breath-mark"><xsl:value-of select="'&pt123456;'"/></xsl:if>
      

    </xsl:if>

    
    <!-- tail recurse  -->
    <xsl:if test="($in_num_child &lt; $in_max_num_child)">     
      <xsl:call-template name="trAduc">
	<xsl:with-param name="in_attributes" select="$c_attributes" />
	<xsl:with-param name="in_num_child" select="($in_num_child+1)" />
	<xsl:with-param name="in_max_num_child"  select="$in_max_num_child" />
	<xsl:with-param name="in_comparation_pitch" 
			select="if ($c_is_note) 
				then (*[$in_num_child]/pitch)
				else ($in_comparation_pitch)" />
	<xsl:with-param name="in_past_is_word" select="$c_is_dynamics or $c_is_wedge"/>
      </xsl:call-template>
    </xsl:if>
  </xsl:template>

  <!-- 
       Default <Attributes> node
  --> 
  <xsl:template name="getAttributeDefautValues">
    <xsl:element name="attributes">
      <xsl:element name="time">
	<xsl:element name="beats" >
	  <xsl:number value="4" />
	</xsl:element>
	<xsl:element name="beat-type">
	  <xsl:number value="4" />
	</xsl:element>
      </xsl:element>
      <xsl:element name="key">
	<xsl:element name="fifths" >
	  <xsl:number value="0" />
	</xsl:element>
	<xsl:element name="mode">
	  <xsl:text>major</xsl:text>
	</xsl:element>
      </xsl:element>
    </xsl:element>
  </xsl:template>


  

</xsl:stylesheet>
