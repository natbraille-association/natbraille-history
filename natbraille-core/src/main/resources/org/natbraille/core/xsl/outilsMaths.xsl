<?xml version='1.1' encoding="UTF-8" ?>
<!--
 * NAT - An universal Translator
 * Copyright (C) 2005 Raphaël Mina
 * Contact: raphael.mina@gmail.com
 *          natbraille.Free.fr
 * 
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
-->

<!-- version: 1.3-b3 -->
<!DOCTYPE xsl:stylesheet SYSTEM "nat://system/xsl/mmlents/windob.dtd">
<!--
<!DOCTYPE xsl:stylesheet SYSTEM "nat://system/xsl/mmlents/windob.dtd"
[
  <!ENTITY % table_braille PUBLIC "table braille" "./tablesBraille/Brltab.ent">
  %table_braille;
  
]>-->

<!--Cette feuille a été créée pour alléger tanMaths.xsl, en y déclarant toutes les variables correspondant aux symboles braille mathématiques.-->
<xsl:stylesheet version="2.0"
xmlns:xsl='http://www.w3.org/1999/XSL/Transform' 
xmlns:saxon='http://icl.com/saxon'
xmlns:xs='http://www.w3.org/2001/XMLSchema'
xmlns:fn='http://www.w3.org/2005/xpath-functions'
xmlns:fo='http://www.w3.org/1999/XSL/Format'
xmlns:m='http://www.w3.org/1998/Math/MathML'
xmlns:lit='espacelit'
xmlns:functx='http://www.functx.com'
xmlns:doc='espaceDoc'>

<xsl:variable name="plus">
  <xsl:text>&pt235;</xsl:text>
</xsl:variable>

<xsl:variable name="grandPlus">
  <xsl:text>&pt5;&pt235;</xsl:text>
</xsl:variable>

<xsl:variable name="plusDansRond">
  <xsl:text>&pt46;&pt235;</xsl:text>
</xsl:variable>

<xsl:variable name="plusDansRondMML">
  <xsl:text>&oplus;</xsl:text>
</xsl:variable>

<xsl:variable name="grandPlusDansRond">
  <xsl:text>&pt46;&pt46;&pt235;</xsl:text>
</xsl:variable>

<xsl:variable name="grandPlusDansRondMML">
  <xsl:text>&xoplus;</xsl:text>
</xsl:variable>

<xsl:variable name="pourcent">
  <xsl:text>&pt5;&pt346;</xsl:text>
</xsl:variable>

<xsl:variable name="pourcentMML">
  <xsl:text>%</xsl:text>
</xsl:variable>

<xsl:variable name="pourmille">
  <xsl:text>&pt5;&pt346;&pt346;</xsl:text>
</xsl:variable>

<xsl:variable name="pourmilleMML">
  <xsl:text>&permil;</xsl:text>
</xsl:variable>

<xsl:variable name="produitTensoriel">
  <xsl:text>&pt46;&pt35;</xsl:text>
</xsl:variable>

<xsl:variable name="produitTensorielMML">
  <xsl:text>&otimes;</xsl:text>
</xsl:variable>

<xsl:variable name="grandProduitTensoriel">
  <xsl:text>&pt46;&pt46;&pt35;</xsl:text>
</xsl:variable>

<xsl:variable name="grandProduitTensorielMML">
  <xsl:text>&xotime;</xsl:text>
</xsl:variable>

<xsl:variable name="differentDe">
  <xsl:text>&pt46;&pt2356;</xsl:text>
</xsl:variable>

<xsl:variable name="differentDeMML">
  <xsl:text>&ne;</xsl:text>
</xsl:variable>

<xsl:variable name="nonCongru">
  <xsl:text>&pt46;&pt2356;&pt2356;</xsl:text>
</xsl:variable>

<xsl:variable name="nonCongruMML">
  <xsl:text>&nequiv;</xsl:text>
</xsl:variable>

<xsl:variable name="posterieur">
  <xsl:text>&pt46;&pt46;&pt345;</xsl:text>
</xsl:variable>

<xsl:variable name="posterieurMML">
  <xsl:text>&#x0227B;</xsl:text>
</xsl:variable>

<xsl:variable name="anterieur">
  <xsl:text>&pt46;&pt46;&pt126;</xsl:text>
</xsl:variable>

<xsl:variable name="anterieurMML">
  <xsl:text>&#x0227A;</xsl:text>
</xsl:variable>

<xsl:variable name="inclus">
  <xsl:text>&pt46;&pt16;</xsl:text>
</xsl:variable>

<xsl:variable name="inclusMML">
  <xsl:text>&sub;</xsl:text>
</xsl:variable>

<xsl:variable name="appartientSymetrique">
  <xsl:text>&pt46;&pt45;&pt16;</xsl:text>
</xsl:variable>

<xsl:variable name="appartientSymetriqueMML">
  <xsl:text>&niv;</xsl:text>
</xsl:variable>

<xsl:variable name="nonAppartientSymetrique">
  <xsl:text>&pt46;&pt45;&pt34;</xsl:text>
</xsl:variable>

<xsl:variable name="nonAppartientSymetriqueMML">
  <xsl:text>&notni;</xsl:text>
</xsl:variable>

<xsl:variable name="complementaire">
  <xsl:text>&pt46;&pt146;</xsl:text>
</xsl:variable>

<xsl:variable name="complementaireMML">
  <xsl:text>&comp;</xsl:text>
</xsl:variable>

<xsl:variable name="nabla">
  <xsl:text>&pt46;&pt1456;</xsl:text>
</xsl:variable>

<xsl:variable name="nablaMML">
  <xsl:text>&nabla;</xsl:text>
</xsl:variable>

<xsl:variable name="nonExiste">
  <xsl:text>&pt46;&pt456;&pt16;</xsl:text>
</xsl:variable>

<xsl:variable name="nonExisteMML">
  <xsl:text>&nexist;</xsl:text>
</xsl:variable>

<xsl:variable name="union">
  <xsl:text>&pt456;&pt235;</xsl:text>
</xsl:variable>

<xsl:variable name="unionMML">
  <xsl:text>&cup;</xsl:text>
</xsl:variable>

<xsl:variable name="grandUnion">
  <xsl:text>&pt456;&pt456;&pt235;</xsl:text>
</xsl:variable>

<xsl:variable name="grandUnionMML">
  <xsl:text>&Union;</xsl:text>
</xsl:variable>

<xsl:variable name="doubleMultiplication">
  <xsl:text>&pt456;&pt456;&pt35;</xsl:text>
</xsl:variable>

<xsl:variable name="doubleMultiplicationMML">
  <xsl:text>&#x02A33;</xsl:text>
</xsl:variable>

<xsl:variable name="parallele">
  <xsl:text>&pt456;&pt1256;</xsl:text>
</xsl:variable>

<xsl:variable name="paralleleMML">
  <xsl:text>&parallel;</xsl:text>
</xsl:variable>

<xsl:variable name="pointExclamation">
  <xsl:text>&pt456;&pt35;</xsl:text>
</xsl:variable>

<xsl:variable name="pointExclamationMML">
  <xsl:text>&excl;</xsl:text>
</xsl:variable>

<xsl:variable name="existe">
  <xsl:text>&pt456;&pt16;</xsl:text>
</xsl:variable>

<xsl:variable name="existeMML">
  <xsl:text>&exist;</xsl:text>
</xsl:variable>

<xsl:variable name="existeUnique">
  <xsl:text>&pt456;&pt16;&pt235;</xsl:text>
</xsl:variable>

<xsl:variable name="existeUniqueMML">
  <xsl:text>&exist;&excl;</xsl:text>
</xsl:variable>

<xsl:variable name="pourTous">
  <xsl:text>&pt456;&pt34;</xsl:text>
</xsl:variable>

<xsl:variable name="pourTousMML">
  <xsl:text>&forall;</xsl:text>
</xsl:variable>

<!-- Bruno -->
<xsl:variable name="pourTousNeg">
  <xsl:text>&pt46;&pt456;&pt34;</xsl:text>
</xsl:variable>
<!-- pas trouvé en mathml...) -->
<xsl:variable name="pourTousNegMML">
  <xsl:text>neg(&forall;)</xsl:text>
</xsl:variable>

<xsl:variable name="operateurRond">
  <xsl:text>&pt456;&pt3456;</xsl:text>
</xsl:variable>

<xsl:variable name="operateurRondMML">
  <xsl:text>&compfn;</xsl:text>
</xsl:variable>

<xsl:variable name="flecheEst">
  <xsl:text>&pt456;&pt156;</xsl:text>
</xsl:variable>

<xsl:variable name="flecheEstMML">
  <xsl:text>&rarr;</xsl:text>
</xsl:variable>

<xsl:variable name="flecheDoubleEstOuest">
  <xsl:text>&pt456;&pt12456;</xsl:text>
</xsl:variable>

<xsl:variable name="flecheDoubleEstOuestMML">
  <xsl:text>&lrarr;</xsl:text>
</xsl:variable>

<xsl:variable name="flecheOuest">
  <xsl:text>&pt456;&pt246;</xsl:text>
</xsl:variable>

<xsl:variable name="flecheOuestMML">
  <xsl:text>&larr;</xsl:text>
</xsl:variable>

<xsl:variable name="egalTilde">
  <xsl:text>&pt456;&pt2356;</xsl:text>
</xsl:variable>

<xsl:variable name="egalTildeMML">
  <xsl:text>&apE;</xsl:text>
</xsl:variable>

<xsl:variable name="carre">
  <xsl:text>&pt456;&pt1456;</xsl:text>
</xsl:variable>

<xsl:variable name="carreMML">
  <xsl:text>&squ;</xsl:text>
</xsl:variable>

<xsl:variable name="sousEnsemble">
  <xsl:text>&pt456;&pt46;&pt16;</xsl:text>
</xsl:variable>

<xsl:variable name="sousEnsembleMML">
  <xsl:text>⊆</xsl:text>
</xsl:variable>

<xsl:variable name="nonSousEnsemble">
  <xsl:text>&pt456;&pt46;&pt34;</xsl:text>
</xsl:variable>

<xsl:variable name="nonSousEnsembleMML">
  <xsl:text>&#x02288;</xsl:text>
</xsl:variable>

<xsl:variable name="doubleTildeSouligne">
  <xsl:text>&pt456;&pt5;&pt2356;</xsl:text>
</xsl:variable>

<xsl:variable name="doubleTildeSouligneMML">
  <xsl:text>&ape;</xsl:text>
</xsl:variable>

<xsl:variable name="nonInclus">
  <xsl:text>&pt46;&pt34;</xsl:text>
</xsl:variable>

<xsl:variable name="nonInclusMML">
  <xsl:text>&nsub;</xsl:text>
</xsl:variable>

<xsl:variable name="asterisque">
  <xsl:text>&pt5;&pt35;</xsl:text>
</xsl:variable>

<xsl:variable name="asterisqueMML">
  <xsl:text>&midast;</xsl:text>
</xsl:variable>

<xsl:variable name="grandMultiplie">
  <xsl:text>&pt5;&pt5;&pt35;</xsl:text>
</xsl:variable>

<xsl:variable name="moins">
  <xsl:text>&pt36;</xsl:text>
</xsl:variable>

<xsl:variable name="multiplie">
  <xsl:text>&pt35;</xsl:text>
</xsl:variable>

<xsl:variable name="multiplieMML">
  <xsl:text>&times;</xsl:text>
</xsl:variable>

<xsl:variable name="virgule">
  <xsl:text>&pt2;</xsl:text>
</xsl:variable>

<xsl:variable name="point">
  <xsl:text>&pt256;</xsl:text>
</xsl:variable>

<xsl:variable name="pointVirgule">
  <xsl:text>&pt23;</xsl:text>
</xsl:variable>

<xsl:variable name="plusMML">
  <xsl:text>&plus;</xsl:text>
</xsl:variable>

<xsl:variable name="minusMML">
  <xsl:text>&minus;</xsl:text>
</xsl:variable>

<xsl:variable name="seqOperatorSimple" as="xs:string*">
    <xsl:sequence select="($plus,$moins,$multiplie,$signeDivision,$egal)"/>
</xsl:variable>

<xsl:variable name="seqOperatorSimpleMono" as="xs:string*">
    <xsl:sequence select="($plus,$plus,$plus,$plus)"/>
</xsl:variable>


<xsl:variable name="seqOperatorSimpleMML" as="xs:string*">
    <xsl:sequence select="($plusMML,$minusMML,$multiplieMML,$signeDivisionMML,$egalMML)"/>
</xsl:variable>

<xsl:variable name="chaineChiffres">
  <xsl:text>&pt3456;&pt16;&pt126;&pt146;&pt1456;&pt156;&pt1246;&pt12456;&pt1256;&pt246;</xsl:text>
</xsl:variable>

<xsl:variable name="seqChiffres" as="xs:string*">
    <xsl:sequence select="($un,$deux,$trois,$quatre,$cinq,$six,$sept,$huit,$neuf,$zero)"/>
</xsl:variable>

<xsl:variable name="a">
  <xsl:text>&pt1;</xsl:text>
</xsl:variable>

<xsl:variable name="b">
  <xsl:text>&pt12;</xsl:text>
</xsl:variable>

<xsl:variable name="c">
  <xsl:text>&pt14;</xsl:text>
</xsl:variable>

<xsl:variable name="d">
  <xsl:text>&pt145;</xsl:text>
</xsl:variable>

<xsl:variable name="e">
  <xsl:text>&pt15;</xsl:text>
</xsl:variable>

<xsl:variable name="f">
  <xsl:text>&pt124;</xsl:text>
</xsl:variable>

<xsl:variable name="g">
  <xsl:text>&pt1245;</xsl:text>
</xsl:variable>

<xsl:variable name="h">
  <xsl:text>&pt125;</xsl:text>
</xsl:variable>

<xsl:variable name="i">
  <xsl:text>&pt24;</xsl:text>
</xsl:variable>

<xsl:variable name="j">
  <xsl:text>&pt245;</xsl:text>
</xsl:variable>

<xsl:variable name="k">
  <xsl:text>&pt13;</xsl:text>
</xsl:variable>

<xsl:variable name="l">
  <xsl:text>&pt123;</xsl:text>
</xsl:variable>

<xsl:variable name="m">
  <xsl:text>&pt134;</xsl:text>
</xsl:variable>

<xsl:variable name="n">
  <xsl:text>&pt1345;</xsl:text>
</xsl:variable>

<xsl:variable name="o">
  <xsl:text>&pt135;</xsl:text>
</xsl:variable>

<xsl:variable name="p">
  <xsl:text>&pt1234;</xsl:text>
</xsl:variable>

<xsl:variable name="q">
  <xsl:text>&pt12345;</xsl:text>
</xsl:variable>

<xsl:variable name="r">
  <xsl:text>&pt1235;</xsl:text>
</xsl:variable>

<xsl:variable name="s">
  <xsl:text>&pt234;</xsl:text>
</xsl:variable>

<xsl:variable name="t">
  <xsl:text>&pt2345;</xsl:text>
</xsl:variable>

<xsl:variable name="u">
  <xsl:text>&pt136;</xsl:text>
</xsl:variable>

<xsl:variable name="v">
  <xsl:text>&pt1236;</xsl:text>
</xsl:variable>

<xsl:variable name="w">
  <xsl:text>&pt2456;</xsl:text>
</xsl:variable>

<xsl:variable name="x">
  <xsl:text>&pt1346;</xsl:text>
</xsl:variable>

<xsl:variable name="y">
  <xsl:text>&pt13456;</xsl:text>
</xsl:variable>

<xsl:variable name="z">
  <xsl:text>&pt1356;</xsl:text>
</xsl:variable>

<xsl:variable name="integrale">
  <xsl:text>&pt12346;</xsl:text>
</xsl:variable>

<xsl:variable name="integraleCurviligne">
  <xsl:text>&pt46;&pt12346;</xsl:text>
</xsl:variable>

<xsl:variable name="integraleCurviligneMML">
  <xsl:text>&conint;</xsl:text>
</xsl:variable>

<xsl:variable name="integraleDouble">
  <xsl:text>&pt12346;&pt12346;</xsl:text>
</xsl:variable>

<xsl:variable name="integraleDoubleMML">
  <xsl:text>&Int;</xsl:text>
</xsl:variable>

<xsl:variable name="integraleSurface">
  <xsl:text>&pt46;&pt12346;&pt12346;</xsl:text>
</xsl:variable>

<xsl:variable name="integraleSurfaceMML">
  <xsl:text>&Conint;</xsl:text>
</xsl:variable>

<xsl:variable name="integraleTriple">
  <xsl:text>&pt12346;&pt12346;&pt12346;</xsl:text>
</xsl:variable>

<xsl:variable name="integraleTripleMML">
  <xsl:text>&tint;</xsl:text>
</xsl:variable>

<xsl:variable name="integraleVolume">
  <xsl:text>&pt46;&pt12346;&pt12346;&pt12346;</xsl:text>
</xsl:variable>

<xsl:variable name="integraleVolumeMML">
  <xsl:text>&Cconint;</xsl:text>
</xsl:variable>

<xsl:variable name="barre">
  <xsl:text>&pt123456;</xsl:text>
</xsl:variable>

<xsl:variable name="barreMultilignes">
  <xsl:text>&pt456;&pt123456;</xsl:text>
</xsl:variable>

<xsl:variable name="barreMML">
  <xsl:text>&mid;</xsl:text>
</xsl:variable>

<xsl:variable name="doubleBarre">
  <xsl:text>&pt45;&pt123456;</xsl:text>
</xsl:variable>

<xsl:variable name="doubleBarreMultilignes">
  <xsl:text>&pt46;&pt123456;</xsl:text>
</xsl:variable>

<xsl:variable name="doubleBarreMML">
  <xsl:text>&Verbar;</xsl:text>
</xsl:variable>

<!--<xsl:variable name="aAccentAigu">
  <xsl:text>&pt12356;</xsl:text>
</xsl:variable>

<xsl:variable name="eAccentAigu">
  <xsl:text>&pt2346;</xsl:text>
</xsl:variable>

<xsl:variable name="uAccentAigu">
  <xsl:text>&pt23456;</xsl:text>
</xsl:variable>-->

<xsl:variable name="zero">
  <xsl:text>&pt3456;</xsl:text>
</xsl:variable>

<xsl:variable name="un">
  <xsl:text>&pt16;</xsl:text>
</xsl:variable>

<xsl:variable name="deux">
  <xsl:text>&pt126;</xsl:text>
</xsl:variable>

<xsl:variable name="trois">
  <xsl:text>&pt146;</xsl:text>
</xsl:variable>

<xsl:variable name="quatre">
  <xsl:text>&pt1456;</xsl:text>
</xsl:variable>

<xsl:variable name="cinq">
  <xsl:text>&pt156;</xsl:text>
</xsl:variable>

<xsl:variable name="six">
  <xsl:text>&pt1246;</xsl:text>
</xsl:variable>

<xsl:variable name="sept">
  <xsl:text>&pt12456;</xsl:text>
</xsl:variable>

<xsl:variable name="huit">
  <xsl:text>&pt1256;</xsl:text>
</xsl:variable>

<xsl:variable name="neuf">
  <xsl:text>&pt246;</xsl:text>
</xsl:variable>



<xsl:variable name="seqAlphabet" as="xs:string*">
    <xsl:sequence select="($a,$b,$c,$d,$e,$f,$g,$h,$i,$j,$k,$l,$m,$n,$o,$p,$q,$r,$s,$t,$u,$v,$w,$x,$y,$z)"/>
</xsl:variable>

<xsl:variable name="alphabetBraille" as="xs:string">
    <xsl:value-of select="concat($a,$b,$c,$d,$e,$f,$g,$h,$i,$j,$k,$l,$m,$n,$o,$p,$q,$r,$s,$t,$u,$v,$w,$x,$y,$z,$integrale,$zero,$un,$deux,$trois,$quatre,$cinq,$six,$sept,$huit,$neuf,$parentheseOuvrante,$parentheseFermante,$crochetOuvrant,$crochetFermant,$virgule,$pointVirgule,$moins,$apostrophe,$point,$barre)"/>
</xsl:variable>

<xsl:variable name="alphabetNoir" as="xs:string">
     <xsl:value-of select="'abcdefghijklmnopqrstuvwxyz&int;0123456789&lpar;&rpar;&lsqb;&rsqb;,;-''.&mid;'"/>
</xsl:variable>

<xsl:variable name="point46">
  <xsl:text>&pt46;</xsl:text>
</xsl:variable>

<xsl:variable name="point5">
  <xsl:text>&pt5;</xsl:text>
</xsl:variable>

<xsl:variable name="blocOuvrant">
  <xsl:text>&pt56;</xsl:text>
</xsl:variable>

<!-- Le bdfgdfgdloc braille n'a pas de sens en noir, on ne le remplace donc par aucun caractère -->
<xsl:variable name="blocOuvrantMML" select="''" />

<xsl:variable name="blocFermant">
  <xsl:text>&pt23;</xsl:text>
</xsl:variable>

<!-- Le bloc braille n'a pas de sens en noir, on le remplace donc par aucun caractère -->
<xsl:variable name="blocFermantMML" select="''" />


<xsl:variable name="parentheseOuvrante">
  <xsl:text>&pt236;</xsl:text>
</xsl:variable>

<xsl:variable name="parentheseOuvranteMML">
  <xsl:text>&lpar;</xsl:text>
</xsl:variable>

<xsl:variable name="parentheseFermante">
  <xsl:text>&pt356;</xsl:text>
</xsl:variable>

<xsl:variable name="parentheseFermanteMML">
  <xsl:text>&rpar;</xsl:text>
</xsl:variable>

<xsl:variable name="grandeParentheseOuvrante">
  <xsl:text>&pt5;&pt236;</xsl:text>
</xsl:variable>

<xsl:variable name="grandeParentheseFermante">
  <xsl:text>&pt5;&pt356;</xsl:text>
</xsl:variable>

<xsl:variable name="parentheseOuvranteMultilignes">
  <xsl:text>&pt45;&pt236;</xsl:text>
</xsl:variable>

<xsl:variable name="parentheseOuvranteMultilignesMML">
  <xsl:text>&#x02985;</xsl:text>
</xsl:variable>

<xsl:variable name="parentheseFermanteMultilignes">
  <xsl:text>&pt45;&pt356;</xsl:text>
</xsl:variable>

<xsl:variable name="parentheseFermanteMultilignesMML">
  <xsl:text>&#x02986;</xsl:text>
</xsl:variable>

<xsl:variable name="accoladeOuvrante">
  <xsl:text>&pt46;&pt236;</xsl:text>
</xsl:variable>

<xsl:variable name="accoladeOuvranteMML">
  <xsl:text>&lcub;</xsl:text>
</xsl:variable>

<xsl:variable name="accoladeFermante">
  <xsl:text>&pt46;&pt356;</xsl:text>
</xsl:variable>

<xsl:variable name="accoladeFermanteMML">
  <xsl:text>&rcub;</xsl:text>
</xsl:variable>

<xsl:variable name="accoladeOuvranteMultilignes">
  <xsl:text>&pt456;&pt236;</xsl:text>
</xsl:variable>

<xsl:variable name="accoladeFermanteMultilignes">
  <xsl:text>&pt456;&pt356;</xsl:text>
</xsl:variable>

<xsl:variable name="crochetOuvrant">
  <xsl:text>&pt12356;</xsl:text>
</xsl:variable>

<xsl:variable name="crochetOuvrantMML">
  <xsl:text>[</xsl:text>
</xsl:variable>

<xsl:variable name="crochetFermant">
  <xsl:text>&pt23456;</xsl:text>
</xsl:variable>

<xsl:variable name="crochetFermantMML">
  <xsl:text>]</xsl:text>
</xsl:variable>

<xsl:variable name="grandCrochetOuvrant">
  <xsl:text>&pt5;&pt12356;</xsl:text>
</xsl:variable>

<xsl:variable name="grandCrochetFermant">
  <xsl:text>&pt5;&pt23456;</xsl:text>
</xsl:variable>

<xsl:variable name="crochetOuvrantMultilignes">
  <xsl:text>&pt45;&pt12356;</xsl:text>
</xsl:variable>

<xsl:variable name="crochetFermantMultilignes">
  <xsl:text>&pt45;&pt23456;</xsl:text>
</xsl:variable>

<xsl:variable name="crochetDoubleOuvrant">
  <xsl:text>&pt46;&pt12356;</xsl:text>
</xsl:variable>

<xsl:variable name="crochetDoubleOuvrantMML">
  <xsl:text>&lobrk;</xsl:text>
</xsl:variable>

<xsl:variable name="crochetDoubleFermant">
  <xsl:text>&pt46;&pt23456;</xsl:text>
</xsl:variable>

<xsl:variable name="crochetDoubleFermantMML">
  <xsl:text>&robrk;</xsl:text>
</xsl:variable>

<xsl:variable name="surEnsemble">
  <xsl:text>&pt5;&pt16;</xsl:text>
</xsl:variable>

<xsl:variable name="surEnsembleMML">
  <xsl:text>&sup;</xsl:text>
</xsl:variable>

<xsl:variable name="nonSurEnsemble">
  <xsl:text>&pt5;&pt34;</xsl:text>
</xsl:variable>

<xsl:variable name="nonSurEnsembleMML">
  <xsl:text>&nsup;</xsl:text>
</xsl:variable>

<xsl:variable name="flecheSudEst">
  <xsl:text>&pt46;&pt156;</xsl:text>
</xsl:variable>

<xsl:variable name="flecheSudEstMML">
  <xsl:text>&searr;</xsl:text>
</xsl:variable>

<xsl:variable name="flecheSud">
  <xsl:text>&pt46;&pt12456;</xsl:text>
</xsl:variable>

<xsl:variable name="flecheSudMML">
  <xsl:text>&darr;</xsl:text>
</xsl:variable>

<xsl:variable name="flecheSudOuest">
  <xsl:text>&pt46;&pt246;</xsl:text>
</xsl:variable>

<xsl:variable name="flecheSudOuestMML">
  <xsl:text>&swarr;</xsl:text>
</xsl:variable>

<xsl:variable name="intersection">
  <xsl:text>&pt45;&pt235;</xsl:text>
</xsl:variable>

<xsl:variable name="intersectionMML">
  <xsl:text>&cap;</xsl:text>
</xsl:variable>

<xsl:variable name="grandeIntersection">
  <xsl:text>&pt45;&pt45;&pt235;</xsl:text>
</xsl:variable>

<xsl:variable name="grandeIntersectionMML">
  <xsl:text>&xcap;</xsl:text>
</xsl:variable>

<xsl:variable name="produitVectoriel">
  <xsl:text>&pt45;&pt35;</xsl:text>
</xsl:variable>

<xsl:variable name="produitVectorielMML">
  <xsl:text>&and;</xsl:text>
</xsl:variable>

<xsl:variable name="grandProduitVectoriel">
  <xsl:text>&pt45;&pt45;&pt35;</xsl:text>
</xsl:variable>

<xsl:variable name="grandProduitVectorielMML">
  <xsl:text>&xwedge;</xsl:text>
</xsl:variable>

<xsl:variable name="precedeOuEgal">
  <xsl:text>&pt45;&pt45;&pt126;</xsl:text>
</xsl:variable>

<xsl:variable name="precedeOuEgalMML">
  <xsl:text>&prcue;</xsl:text>
</xsl:variable>

<xsl:variable name="suitOuEgal">
  <xsl:text>&pt45;&pt45;&pt345;</xsl:text>
</xsl:variable>

<xsl:variable name="suitOuEgalMML">
  <xsl:text>&sccue;</xsl:text>
</xsl:variable>

<xsl:variable name="ou">
  <xsl:text>&pt45;&pt26;</xsl:text>
</xsl:variable>

<xsl:variable name="ouMML">
  <xsl:text>&or;</xsl:text>
</xsl:variable>

<xsl:variable name="tilde">
  <xsl:text>&pt45;&pt2356;</xsl:text>
</xsl:variable>

<xsl:variable name="tildeMML">
  <xsl:text>&sim;</xsl:text>
</xsl:variable>

<xsl:variable name="plusGrandOuEgal">
  <xsl:text>&pt45;&pt345;</xsl:text>
</xsl:variable>

<xsl:variable name="plusGrandOuEgalMML">
  <xsl:text>&ge;</xsl:text>
</xsl:variable>

<xsl:variable name="plusPetitOuEgal">
  <xsl:text>&pt45;&pt126;</xsl:text>
</xsl:variable>

<xsl:variable name="plusPetitOuEgalMML">
  <xsl:text>&le;</xsl:text>
</xsl:variable>

<xsl:variable name="flecheNordEst">
  <xsl:text>&pt45;&pt156;</xsl:text>
</xsl:variable>

<xsl:variable name="flecheNordEstMML">
  <xsl:text>&nearr;</xsl:text>
</xsl:variable>

<xsl:variable name="flecheNord">
  <xsl:text>&pt45;&pt12456;</xsl:text>
</xsl:variable>

<xsl:variable name="flecheNordMML">
  <xsl:text>&uarr;</xsl:text>
</xsl:variable>

<xsl:variable name="flecheNordOuest">
  <xsl:text>&pt45;&pt246;</xsl:text>
</xsl:variable>

<xsl:variable name="flecheNordOuestMML">
  <xsl:text>&nwarr;</xsl:text>
</xsl:variable>








<xsl:variable name="perpendiculaire">
  <xsl:text>&pt45;&pt1256;</xsl:text>
</xsl:variable>

<xsl:variable name="perpendiculaireMML">
  <xsl:text>&perp;</xsl:text>
</xsl:variable>

<xsl:variable name="appartient">
  <xsl:text>&pt45;&pt16;</xsl:text>
</xsl:variable>

<xsl:variable name="appartientMML">
  <xsl:text>&isin;</xsl:text>
</xsl:variable>

<xsl:variable name="nonAppartient">
  <xsl:text>&pt45;&pt34;</xsl:text>
</xsl:variable>

<xsl:variable name="nonAppartientMML">
  <xsl:text>&#x02209;</xsl:text>
</xsl:variable>

<xsl:variable name="diametre">
  <xsl:text>&pt45;&pt3456;</xsl:text>
</xsl:variable>

<xsl:variable name="diametreMML">
  <xsl:text>&#x02205;</xsl:text>
</xsl:variable>

<xsl:variable name="aleph">
  <xsl:text>&pt45;&pt45;&pt1;</xsl:text>
</xsl:variable>

<xsl:variable name="alephMML">
  <xsl:text>&aleph;</xsl:text>
</xsl:variable>

<xsl:variable name="beth">
  <xsl:text>&pt45;&pt45;&pt12;</xsl:text>
</xsl:variable>

<xsl:variable name="bethMML">
  <xsl:text>&beth;</xsl:text>
</xsl:variable>

<xsl:variable name="daleth">
  <xsl:text>&pt45;&pt45;&pt145;</xsl:text>
</xsl:variable>

<xsl:variable name="dalethMML">
  <xsl:text>&daleth;</xsl:text>
</xsl:variable>

<xsl:variable name="gimel">
  <xsl:text>&pt45;&pt45;&pt1245;</xsl:text>
</xsl:variable>

<xsl:variable name="gimelMML">
  <xsl:text>&gimel;</xsl:text>
</xsl:variable>

<xsl:variable name="infini">
  <xsl:text>&pt45;&pt14;</xsl:text>
</xsl:variable>

<xsl:variable name="infiniMML">
  <xsl:text>&infin;</xsl:text>
</xsl:variable>

<xsl:variable name="flecheTaquetDroite">
  <xsl:text>&pt5;&pt156;</xsl:text>
</xsl:variable>

<xsl:variable name="flecheTaquetDroiteMML">
  <xsl:text>&map;</xsl:text>
</xsl:variable>

<xsl:variable name="flecheBilaterale">
  <xsl:text>&pt5;&pt12456;</xsl:text>
</xsl:variable>

<xsl:variable name="flecheBilateraleMML">
  <xsl:text>&harr;</xsl:text>
</xsl:variable>

<xsl:variable name="doubleFlecheGauche">
  <xsl:text>&pt5;&pt25;</xsl:text>
</xsl:variable>

<xsl:variable name="doubleFlecheGaucheMML">
  <xsl:text>&lArr;</xsl:text>
</xsl:variable>

<xsl:variable name="doubleFlecheDroite">
  <xsl:text>&pt25;&pt2;</xsl:text>
</xsl:variable>

<xsl:variable name="doubleFlecheDroiteMML">
  <xsl:text>&rArr;</xsl:text>
</xsl:variable>

<xsl:variable name="doubleFlecheGaucheBarree">
  <xsl:text>&pt46;&pt5;&pt25;</xsl:text>
</xsl:variable>

<xsl:variable name="doubleFlecheGaucheBarreeMML">
  <xsl:text>&nlArr;</xsl:text>
</xsl:variable>

<xsl:variable name="doubleFlecheDroiteBarree">
  <xsl:text>&pt46;&pt25;&pt2;</xsl:text>
</xsl:variable>

<xsl:variable name="doubleFlecheDroiteBarreeMML">
  <xsl:text>&nrArr;</xsl:text>
</xsl:variable>

<xsl:variable name="doubleFlecheBilaterale">
  <xsl:text>&pt5;&pt25;&pt2;</xsl:text>
</xsl:variable>

<xsl:variable name="doubleFlecheBilateraleMML">
  <xsl:text>&hArr;</xsl:text>
</xsl:variable>

<xsl:variable name="doubleFlecheBilateraleBarree">
  <xsl:text>&pt46;&pt5;&pt25;&pt2;</xsl:text>
</xsl:variable>

<xsl:variable name="doubleFlecheBilateraleBarreeMML">
  <xsl:text>&nhArr;</xsl:text>
</xsl:variable>

<xsl:variable name="inferieur">
  <xsl:text>&pt5;&pt126;</xsl:text>
</xsl:variable>

<xsl:variable name="inferieurMML">
  <xsl:text>&amp;lt;</xsl:text>
</xsl:variable>

<xsl:variable name="apostrophe">
  <xsl:text>&pt3;</xsl:text>
</xsl:variable>

<xsl:variable name="apostropheMML">
  <xsl:text>&apos;</xsl:text>
</xsl:variable>

<xsl:variable name="superieur">
  <xsl:text>&pt5;&pt345;</xsl:text>
</xsl:variable>

<xsl:variable name="superieurMML">
  <xsl:text>&gt;</xsl:text>
</xsl:variable>

<xsl:variable name="superieurInferieur">
  <xsl:text>&pt5;&pt345;&pt5;&pt126;</xsl:text>
</xsl:variable>

<xsl:variable name="superieurInferieurMML">
  <xsl:text>&#x02AA5;</xsl:text>
</xsl:variable>

<xsl:variable name="tresInferieur">
  <xsl:text>&pt5;&pt5;&pt126;</xsl:text>
</xsl:variable>

<xsl:variable name="tresInferieurMML">
  <xsl:text>&#x0226A;</xsl:text>
</xsl:variable>

<xsl:variable name="tresSuperieur">
  <xsl:text>&pt5;&pt5;&pt345;</xsl:text>
</xsl:variable>

<xsl:variable name="tresSuperieurMML">
  <xsl:text>&#x0226B;</xsl:text>
</xsl:variable>

<xsl:variable name="egal">
  <xsl:text>&pt2356;</xsl:text>
</xsl:variable>

<xsl:variable name="egalMML">
  <xsl:text>&equals;</xsl:text>
</xsl:variable>

<xsl:variable name="plusOuMoins">
  <xsl:text>&pt235;&pt36;</xsl:text>
</xsl:variable>

<xsl:variable name="plusOuMoinsMML">
  <xsl:text>&plusmn;</xsl:text>
</xsl:variable>

<xsl:variable name="moinsOuPlus">
  <xsl:text>&pt36;&pt235;</xsl:text>
</xsl:variable>

<xsl:variable name="moinsOuPlusMML">
  <xsl:text>&mnplus;</xsl:text>
</xsl:variable>

<xsl:variable name="produitScalaire">
  <xsl:text>&pt35;&pt35;</xsl:text>
</xsl:variable>

<xsl:variable name="produitScalaireMML">
  <xsl:text>&sdot;</xsl:text>
</xsl:variable>

<xsl:variable name="congru">
  <xsl:text>&pt2356;&pt2356;</xsl:text>
</xsl:variable>

<xsl:variable name="congruMML">
  <xsl:text>&equiv;</xsl:text>
</xsl:variable>

<xsl:variable name="egalDelta">
  <xsl:text>&pt25;&pt2356;</xsl:text>
</xsl:variable>

<xsl:variable name="egalDeltaMML">
  <xsl:text>&trie;</xsl:text>
</xsl:variable>


<xsl:variable name="environEgal">
  <xsl:text>&pt5;&pt2356;</xsl:text>
</xsl:variable>

<xsl:variable name="environEgalMML">
  <xsl:text>&ap;</xsl:text>
</xsl:variable>

<xsl:variable name="exposant">
  <xsl:text>&pt4;</xsl:text>
</xsl:variable>

<xsl:variable name="indice">
  <xsl:text>&pt26;</xsl:text>
</xsl:variable>

<xsl:variable name="racine">
  <xsl:text>&pt345;</xsl:text>
</xsl:variable>

<xsl:variable name="barreFraction">
  <xsl:text>&pt34;</xsl:text>
</xsl:variable>

<xsl:variable name="barreFractionMML">
  <xsl:text>/</xsl:text>
</xsl:variable>

<xsl:variable name="signeDivision">
  <xsl:text>&pt25;</xsl:text>
</xsl:variable>

<xsl:variable name="signeDivisionMML">
  <xsl:text>&#x000F7;</xsl:text>
</xsl:variable>

<xsl:variable name="majuscule" as="xs:string">
  <xsl:text>&pt46;</xsl:text>
</xsl:variable>

<xsl:variable name="point45" as="xs:string">
  <xsl:text>&pt45;</xsl:text>
</xsl:variable>

<xsl:variable name="point36" as="xs:string">
  <xsl:text>&pt36;</xsl:text>
</xsl:variable>

<xsl:variable name="point456" as="xs:string">
  <xsl:text>&pt456;</xsl:text>
</xsl:variable>

<xsl:variable name="arc" as="xs:string">
  <xsl:text>&pt4;&pt25;</xsl:text>
</xsl:variable>

<xsl:variable name="arcMML" as="xs:string">
  <xsl:text>&profline;</xsl:text>
</xsl:variable>

<xsl:variable name="angleSaillant" as="xs:string">
  <xsl:text>&pt45;&pt25;</xsl:text>
</xsl:variable>

<xsl:variable name="angleSaillantMML" as="xs:string">
  <xsl:value-of select="'&Hat;'"/>
</xsl:variable>

<xsl:variable name="vecteur" as="xs:string">
  <xsl:text>&pt46;&pt25;</xsl:text>
</xsl:variable>

<!--Il s'agit de la fleche est-->
<xsl:variable name="vecteurMML" as="xs:string">
  <xsl:text>&rarr;</xsl:text>
</xsl:variable>

<xsl:variable name="tenseur" as="xs:string">
  <xsl:text>&pt46;&pt25;&pt4;&pt1345;</xsl:text>
</xsl:variable>

<xsl:variable name="tenseurMML" as="xs:string">
  <xsl:text></xsl:text>
</xsl:variable>

<xsl:variable name="barreHorizontale" as="xs:string">
  <xsl:text>&pt456;&pt25;</xsl:text>
</xsl:variable>

<xsl:variable name="barreHorizontaleMML" as="xs:string">
  <xsl:text>&horbar;</xsl:text>
</xsl:variable>


<xsl:variable name="demiRondConvexeBas" as="xs:string">
  <xsl:text>&pt4;&pt4;&pt25;</xsl:text>
</xsl:variable>

<xsl:variable name="demiRondConvexeBasMML" as="xs:string">
  <xsl:text>&#x0FE36;</xsl:text>
</xsl:variable>

<xsl:variable name="angleRentrant" as="xs:string">
  <xsl:text>&pt456;&pt45;&pt25;</xsl:text>
</xsl:variable>

<xsl:variable name="angleRentrantMML" as="xs:string">
  <xsl:text>&or;</xsl:text>
</xsl:variable>

<xsl:variable name="vecteurAxial" as="xs:string">
  <xsl:text>&pt5;&pt46;&pt25;</xsl:text>
</xsl:variable>

<xsl:variable name="vecteurAxialMML" as="xs:string">
  <xsl:text>&olarr;</xsl:text>
</xsl:variable>

<xsl:variable name="tildeSurscrit" as="xs:string">
  <xsl:text>&pt5;&pt456;&pt25;</xsl:text>
</xsl:variable>

<xsl:variable name="tildeSurscritMML" as="xs:string">
  <xsl:text>&sim;</xsl:text>
</xsl:variable>

<xsl:variable name="flecheCirculaireNegatif" as="xs:string">
  <xsl:text>&pt45;&pt4;&pt25;</xsl:text>
</xsl:variable>

<xsl:variable name="flecheCirculaireNegatifMML" as="xs:string">
  <xsl:text>&orarr;</xsl:text>
</xsl:variable>

<xsl:variable name="tRenverse" as="xs:string">
  <xsl:text>&pt45;&pt45;&pt25;</xsl:text>
</xsl:variable>

<xsl:variable name="tRenverseMML" as="xs:string">
  <xsl:text>&perp;</xsl:text>
</xsl:variable>

<xsl:variable name="dague" as="xs:string">
  <xsl:text>&pt45;&pt46;&pt25;</xsl:text>
</xsl:variable>

<xsl:variable name="dagueMML" as="xs:string">
  <xsl:text>&#x0253C;</xsl:text>
</xsl:variable>

<xsl:variable name="doubleBarreHorizontale" as="xs:string">
  <xsl:text>&pt45;&pt456;&pt25;</xsl:text>
</xsl:variable>

<xsl:variable name="doubleBarreHorizontaleMML" as="xs:string">
  <xsl:text>&equals;</xsl:text>
</xsl:variable>

<xsl:variable name="flecheOuestSurscrit" as="xs:string">
  <xsl:text>&pt456;&pt46;&pt25;</xsl:text>
</xsl:variable>

<xsl:variable name="flecheOuestSurscritMML" as="xs:string">
  <xsl:text>&larr;</xsl:text>
</xsl:variable>

<!--Regroupe les signes surscrits-->
<xsl:variable name="seqSurscrit" as="xs:string*">
  <xsl:sequence select="($arc,$angleSaillant,$vecteur,$tenseur,$barreHorizontale,$demiRondConvexeBas,$angleRentrant,$vecteurAxial,$tildeSurscrit,$flecheCirculaireNegatif,$tRenverse,$doubleBarreHorizontale,$flecheOuestSurscrit,$dague)"/>
</xsl:variable>

<!--Équivalent MathML de l'entité ci-dessus-->
<xsl:variable name="seqSurscritMML" as="xs:string*">
  <xsl:sequence select="($arcMML,$angleSaillantMML,$vecteurMML,$tenseurMML,$barreHorizontaleMML,$demiRondConvexeBasMML,$angleRentrantMML,$vecteurAxialMML,$tildeSurscritMML,$flecheCirculaireNegatifMML,$tRenverseMML,$doubleBarreHorizontaleMML,$flecheOuestSurscritMML,$dagueMML)"/>
</xsl:variable>

<!--Bruno: Regroupe les signes non surscrits commençant comme un signe suscrit-->
<xsl:variable name="seqNonSurscrit" as="xs:string*">
  <xsl:sequence select="($doubleFlecheDroiteBarree)"/>
</xsl:variable>

<!--Regroupe les préfixes classiques qui débutent les opérateurs-->
<xsl:variable name="seqPrefixe" as="xs:string*">
  <xsl:sequence select="($point5,$point45,$point46,$point456)"/>
</xsl:variable>

<!--Regroupe les débuts possible des opérateurs et identifier qui ne commencent pas par un préfixe classique-->
<xsl:variable name="seqDebutSansPrefixe" as="xs:string*">
  <xsl:sequence select="($plus,$moins,$multiplie,$signeDivision,$barreFraction,$egal,$pointVirgule)"/>
</xsl:variable>

<!-- Sequence des symboles braille de deux caractères signifiant le début d'un bloc SUR PLUSIEURS LIGNES (utile pour tableau, matrices, systèmes...) -->
<xsl:variable name="seqOuvrantMultilignes" as="xs:string*">
    <xsl:sequence select="($parentheseOuvranteMultilignes,$accoladeOuvranteMultilignes,$crochetOuvrantMultilignes,$barreMultilignes,$doubleBarreMultilignes)"/>
</xsl:variable>

<!-- Équivalent MathML de la séquence précédente-->
<xsl:variable name="seqOuvrantMultilignesMML" as="xs:string*">
    <xsl:sequence select="($parentheseOuvranteMML,$accoladeOuvranteMML,$crochetOuvrantMML,$barreMML,$doubleBarreMML)"/>
</xsl:variable>


<!-- Sequence des symboles braille de deux caractères signifiant le début d'un bloc-->
<xsl:variable name="seqOuvrantDouble" as="xs:string*">
    <xsl:sequence select="($grandeParentheseOuvrante,$parentheseOuvranteMultilignes,$accoladeOuvrante,$accoladeOuvranteMultilignes,$grandCrochetOuvrant,$crochetOuvrantMultilignes,$crochetDoubleOuvrant)"/>
</xsl:variable>

<!-- Équivalent MathML de la séquence précédente-->
<xsl:variable name="seqOuvrantDoubleMML" as="xs:string*">
    <xsl:sequence select="($parentheseOuvranteMML,$parentheseOuvranteMultilignesMML,$accoladeOuvranteMML,$accoladeOuvranteMML,$crochetOuvrantMML,$crochetOuvrantMML,$crochetDoubleOuvrantMML)"/>
</xsl:variable>

<!-- Sequence des symboles braille de deux caractères signifiant la fin d'un bloc-->
<xsl:variable name="seqFermantDouble" as="xs:string*">
    <xsl:sequence select="($grandeParentheseFermante,$parentheseFermanteMultilignes,$accoladeFermante,$accoladeFermanteMultilignes,$grandCrochetFermant,$crochetFermantMultilignes,$crochetDoubleFermant)"/>
</xsl:variable>

<!-- Équivalent MathML de la séquence précédente-->
<xsl:variable name="seqFermantDoubleMML" as="xs:string*">
    <xsl:sequence select="($parentheseFermanteMML,$parentheseFermanteMultilignesMML,$accoladeFermanteMML,$accoladeFermanteMML,$crochetFermantMML,$crochetFermantMML,$crochetDoubleFermantMML)"/>
</xsl:variable>

<!-- Sequence des symboles braille de deux caractères signifiant la fin d'un bloc SUR PLUSIEURS LIGNES (utile pour tableau, matrices, systèmes...) -->
<xsl:variable name="seqFermantMultilignes" as="xs:string*">
    <xsl:sequence select="($parentheseFermanteMultilignes,$accoladeFermanteMultilignes,$crochetFermantMultilignes,$barreMultilignes,$doubleBarreMultilignes)"/>
</xsl:variable>

<!-- Équivalent MathML de la séquence précédente-->
<xsl:variable name="seqFermantMultilignesMML" as="xs:string*">
    <xsl:sequence select="($parentheseFermanteMML,$accoladeFermanteMML,$crochetFermantMML,$barreMML,$doubleBarreMML)"/>
</xsl:variable>

<!-- Sequence des symboles braille d'un caractère signifiant le début d'un bloc-->
<xsl:variable name="seqOuvrantSimple" as="xs:string*">
    <xsl:sequence select="($blocOuvrant,$parentheseOuvrante,$crochetOuvrant)"/>
</xsl:variable>

<!-- Équivalent MathML de la séquence précédente-->
<xsl:variable name="seqOuvrantSimpleMML" as="xs:string*">
    <xsl:sequence select="($blocOuvrantMML,$parentheseOuvranteMML,$crochetOuvrantMML)"/>
</xsl:variable>

<!-- Sequence des symboles braille d'un caractère signifiant la fin d'un bloc-->
<xsl:variable name="seqFermantSimple" as="xs:string*">
    <xsl:sequence select="($blocFermant,$parentheseFermante,$crochetFermant)"/>
</xsl:variable>

<!-- Équivalent MathML de la séquence précédente-->
<xsl:variable name="seqFermantSimpleMML" as="xs:string*">
    <xsl:sequence select="($blocFermantMML,$parentheseFermanteMML,$crochetFermantMML)"/>
</xsl:variable>


<!--Ensemble des caractères braille composés qui peuvent poser problème lors des détection, car mélangeant les caractères débutant les caractères spéciaux. -->
<xsl:variable name="seqOperatorSpeciaux" as="xs:string*">
    <xsl:sequence select="($doubleTildeSouligne,$doubleFlecheGaucheBarree,$doubleFlecheBilateraleBarree,$sousEnsemble,$nonSousEnsemble,$appartientSymetrique,$nonAppartientSymetrique)"/>
</xsl:variable>

<!-- Traduction MathMl de l'élément précédent-->
<xsl:variable name="seqOperatorSpeciauxMML" as="xs:string*">
    <xsl:sequence select="($doubleTildeSouligneMML,$doubleFlecheGaucheBarreeMML,$doubleFlecheBilateraleBarreeMML,$sousEnsembleMML,$nonSousEnsembleMML,$appartientSymetriqueMML,$nonAppartientSymetriqueMML)"/>
</xsl:variable>

<!--Sequence de même longueur que les deux d'en-dessus, utile pour effectuer des remplacements-->
<xsl:variable name="seqOperatorSpeciauxMono" as="xs:string*">
    <xsl:sequence select="($doubleTildeSouligne,$doubleTildeSouligne,$doubleTildeSouligne,$doubleTildeSouligne,$doubleTildeSouligne,$doubleTildeSouligne,$doubleTildeSouligne)"/>
</xsl:variable>







<!--Ensemble des caractères braille composés qui ne commencent pas par un préfixe classique et sont traduits par un operator MathML. -->
<xsl:variable name="seqOperatorSansPrefixe" as="xs:string*">
    <xsl:sequence select="($plusOuMoins,$moinsOuPlus,$produitScalaire,$integraleDouble,$integraleTriple,$congru,$egalDelta,$doubleFlecheDroite)"/><!--$barreFraction-->
</xsl:variable>

<!-- Traduction MathMl de l'élément précédent-->
<xsl:variable name="seqOperatorSansPrefixeMML" as="xs:string*">
    <xsl:sequence select="($plusOuMoinsMML,$moinsOuPlusMML,$produitScalaireMML,$integraleDoubleMML,$integraleTripleMML,$congruMML,$egalDeltaMML,$doubleFlecheDroiteMML,$egalMML)"/>
</xsl:variable>




<!--Ensemble des caractères hébreux disponibles en MathML-->
<xsl:variable name="seqHebraique" as="xs:string*">
    <xsl:sequence select="($aleph,$beth,$daleth,$gimel)"/>
</xsl:variable>

<!--Traduction MathML de seqHebraique".-->
<xsl:variable name="seqHebraiqueMML" as="xs:string*">
    <xsl:sequence select="($alephMML,$bethMML,$dalethMML,$gimelMML)"/>
</xsl:variable>

<!--Ensemble des caractères braille composés qui débutent par le point45 et qui sont traduits par des OPERATOR MathMl. Note : remplacement des grands crochet par des crochets simples-->
<xsl:variable name="seqOperatorPt45" as="xs:string*">
    <xsl:sequence select="($grandProduitVectoriel,$produitVectoriel,$ou,$intersection,$grandeIntersection,$precedeOuEgal,$suitOuEgal,$tilde,$plusGrandOuEgal,$plusPetitOuEgal,$flecheNordEst,$flecheNord,$flecheNordOuest,$perpendiculaire,$appartient,$nonAppartient,$diametre,$doubleBarre)"/>
</xsl:variable>

<!--Traduction MathML de seqIdentifierPt45. -->
<xsl:variable name="seqOperatorPt45MML" as="xs:string*">
    <xsl:sequence select="($grandProduitVectorielMML,$produitVectorielMML,$ouMML,$intersectionMML,$grandeIntersectionMML,$precedeOuEgalMML,$suitOuEgalMML,$tildeMML,$plusGrandOuEgalMML,$plusPetitOuEgalMML,$flecheNordEstMML,$flecheNordMML,$flecheNordOuestMML,$perpendiculaireMML,$appartientMML,$nonAppartientMML,$diametreMML,$doubleBarreMML)"/>
</xsl:variable>








<!--Ensemble des caractères braille composés qui débutent par le point456 et qui sont traduits par des OPERATOR MathMl-->
<xsl:variable name="seqOperatorPt456" as="xs:string*">
    <xsl:sequence select="($operateurRond,$carre,$doubleMultiplication,$union,$grandUnion,$parallele,$pointExclamation,$existe,$existeUnique,$pourTous,$flecheEst,$flecheDoubleEstOuest,$flecheOuest,$egalTilde,$carre,$seqBarreLetters)"/>
</xsl:variable>

<!--Traduction MathML de seqIdentifierPt456. -->
<xsl:variable name="seqOperatorPt456MML" as="xs:string*">
    <xsl:sequence select="($operateurRondMML,$carreMML,$doubleMultiplicationMML,$unionMML,$grandUnionMML,$paralleleMML,$pointExclamationMML,$existeMML,$existeUniqueMML,$pourTousMML,$flecheEstMML,$flecheDoubleEstOuestMML,$flecheOuestMML,$egalTildeMML,$carreMML,$seqBarreLettersMML)"/>
</xsl:variable>





<!--Ensemble des caractères braille composés qui débutent par le point5 et qui sont traduits par des OPERATOR MathMl-->
<xsl:variable name="seqOperatorPt5" as="xs:string*">
    <xsl:sequence select="($pourmille,$pourcent,$grandPlus,$grandMultiplie,$asterisque,$environEgal,$tresInferieur,$tresSuperieur,$superieurInferieur,$inferieur,$superieur,$surEnsemble,$nonSurEnsemble,$flecheTaquetDroite,$flecheBilaterale,$doubleFlecheGauche,$doubleFlecheBilaterale)"/>
</xsl:variable>

<!--Traduction MathML de seqIdentifierPT5. Note : il n'existe pas d'équivalent MathML au grandPlus et grandMultiplie, ils sont remplacés par des simple plus et multipliés."-->
<xsl:variable name="seqOperatorPt5MML" as="xs:string*">
    <xsl:sequence select="($pourmilleMML,$pourcentMML,$plusMML,$multiplieMML,$asterisqueMML,$environEgalMML,$tresInferieurMML,$tresSuperieurMML,$superieurInferieurMML,$inferieurMML,$superieurMML,$surEnsembleMML,$nonSurEnsembleMML,$flecheTaquetDroiteMML,$flecheBilateraleMML,$doubleFlecheGaucheMML,$doubleFlecheBilateraleMML)"/>
</xsl:variable>





<!--Ensemble des caractères braille composés qui débutent par le point46 et qui sont traduits par des OPERATOR MathMl-->
<!-- Bruno: j'ajoute aussi vecteur, nonexiste, n'implique pas -->
<xsl:variable name="seqOperatorPt46" as="xs:string*">
    <xsl:sequence select="($vecteur,$nonExiste,$pourTousNeg,$plusDansRond,$grandPlusDansRond,$produitTensoriel,$grandProduitTensoriel,$nabla,$integraleCurviligne,$integraleSurface,$integraleVolume,$anterieur,$posterieur,$differentDe,$nonCongru,$inclus,$nonInclus,$appartientSymetrique,$nonAppartientSymetrique,$complementaire,$doubleFlecheDroiteBarree,$doubleFlecheGaucheBarree,$doubleFlecheBilateraleBarree,$flecheSudEst,$flecheSud,$flecheSudOuest)"/>
</xsl:variable>

<!--Traduction MathML de seqIdentifierPT46.-->
<xsl:variable name="seqOperatorPt46MML" as="xs:string*">
    <xsl:sequence select="($vecteurMML,$nonExisteMML,$pourTousNegMML,$plusDansRondMML,$grandPlusDansRondMML,$produitTensorielMML,$grandProduitTensorielMML,$nablaMML,$integraleCurviligneMML,$integraleSurfaceMML,$integraleVolumeMML,$anterieurMML,$posterieurMML,$differentDeMML,$nonCongruMML,$inclusMML,$nonInclusMML,$appartientSymetriqueMML,$nonAppartientSymetriqueMML,$complementaireMML,$doubleFlecheDroiteBarreeMML,$doubleFlecheGaucheBarreeMML,$doubleFlecheBilateraleBarreeMML,$flecheSudEstMML,$flecheSudMML,$flecheSudOuestMML)"/>
</xsl:variable>


<!-- lettres "bar"-->
<xsl:variable name="seqBarreLetters" as="xs:string*">
    <xsl:sequence select="('&pt456;&pt36;&pt125;','&pt456;&pt36;&pt123;')"/>
</xsl:variable>

<!-- lettres "bar"-->
<xsl:variable name="seqBarreLettersMML" as="xs:string*">
    <xsl:sequence select="('','','','','','','','ℏ','','','','ƛ')"/>
</xsl:variable>

<!--Alphabet grecque minuscule (26 caractères, le V, inexistant normalement, est inséré, ainsi que l'infini qui correspond au c)-->
<xsl:variable name="seqGrecqueMinuscule" as="xs:string*">
    <xsl:sequence select="('&alpha;','&beta;',$infiniMML,'&delta;','&epsiv;','&phiv;','&gamma;','&eta;','&iota;','&theta;','&kappa;','&lambda;','&mu;','&nu;','o','&pi;','&chi;','&rho;','&sigma;','&tau;','&upsi;','v','&omega;','&xi;','&psi;','&zeta;')"/>
</xsl:variable>

<!--Alphabet grecque majuscule (26 caractères, le C et le V, inexistant normalement, sont insérés)-->
<xsl:variable name="seqGrecqueMajuscule" as="xs:string*">
    <xsl:sequence select="('A;','B','C','&Delta;','E','&Phi;','&Gamma;','H','I','&Theta;','K','&Lambda;','M','N','O','&Pi;','X','P','&Sigma;','T','Y','V','&Omega;','&Xi;','&Psi;','Z')"/>
</xsl:variable>

<!--Alphabet ronde minuscule ATTENTION : le d et le o sont respectivement transcrits par "dérivée partielle" et "degré"-->
<xsl:variable name="seqRondeMinuscule" as="xs:string*">
    <xsl:sequence select="('&#x1D4B6;','&#x1D4B7;','&#x1D4B8;','&part;','&#x0212F;','&#x1D4BB;','&#x0210A;','&#x1D4BD;','&#x1D4BE;','&#x1D4BF;','&#x1D4C0;','&#x1D4C1;','&#x1D4C2;','&#x1D4C3;','&deg;','&#x1D4C5;','&#x1D4C6;','&#x1D4C7;','&#x1D4C8;','&#x1D4C9;','&#x1D4CA;','&#x1D4CB;','&#x1D4CC;','&#x1D4CD;','&#x1D4CE;','&#x1D4CF;')"/>
</xsl:variable>

<!--Alphabet ronde majuscule -->
<xsl:variable name="seqRondeMajuscule" as="xs:string*">
    <xsl:sequence select="('&#x1D49C;','&#x0212C;','&#x1D49E;','&#x1D49F;','&#x02130;','&#x02131;','&#x1D4A2;','&#x0210B;','&#x02110;','&#x1D4A5;','&#x1D4A6;','&#x02112;','&#x02133;','&#x1D4A9;','&#x1D4AA;','&#x1D4AB;','&#x1D4AC;','&#x0211B;','&#x1D4AE;','&#x1D4AF;','&#x1D4B0;','&#x1D4B1;','&#x1D4B2;','&#x1D4B3;','&#x1D4B4;','&#x1D4B5;')"/>
</xsl:variable>

<!--Alphabet éclairée -->
<xsl:variable name="seqEclairee" as="xs:string*">
    <xsl:sequence select="('&#x1D538;','&#x1D539;','&#x02102;','&#x1D53B;','&#x1D53C;','&#x1D53D;','&#x1D53E;','&#x0210D;','&#x1D540;','&#x1D541;','&#x1D542;','&#x1D543;','&#x1D544;','&#x02115;','&#x1D546;','&#x02119;','&#x0211A;','&#x0211D;','&#x1D54A;','&#x1D54B;','&#x1D54C;','&#x1D54D;','&#x1D54E;','&#x1D54F;','&#x1D550;','&#x02124;')"/>
</xsl:variable>

<!--Alphabet noir majuscule -->
<xsl:variable name="seqAlphabetNoirMajuscule" as="xs:string*">
    <xsl:sequence select="('A','B','C','D','E','F','G','H','I','J','K','L','M','N','O','P','Q','R','S','T','U','V','W','X','Y','Z')"/>
</xsl:variable>



<xsl:variable name="caractereBrailleSeq" select="($exposant,$indice)"/>
<xsl:variable name="caractereBraille" select="concat($exposant,$indice)"/>
<xsl:variable name="remplacement" select="concat($exposant,$exposant)"/>

</xsl:stylesheet>