<?xml version='1.1' encoding="UTF-8" ?>
<!--
 * NAT - An universal Translator
 * Copyright (C) 2005 Bruno Mascret
 * Contact: bmascret@free.fr
 * 
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
-->

<!-- version: 1.1 -->
<!DOCTYPE xsl:stylesheet SYSTEM "nat://system/xsl/mmlents/windob.dtd">

<xsl:stylesheet version="2.0"
xmlns:xsl='http://www.w3.org/1999/XSL/Transform' 
xmlns:saxon='http://icl.com/saxon'
xmlns:m='http://www.w3.org/1998/Math/MathML'
xmlns:lit='espacelit'
xmlns:doc='espaceDoc'>

<xsl:output
   method="xhtml"
   doctype-public="-//W3C//DTD XHTML 1.1 plus MathML 2.0//EN"
   indent="yes"
   encoding="UTF-8"
   media-type="text/html"
/>

<!-- ****************************************************
	Outils pour les majuscules
************************************* -->
<xsl:template name="dernierMotPassageM">
	<xsl:param name="noeud"/>
	<xsl:param name="pos" select="1"/>
	<xsl:choose>
		<xsl:when test=".='test'">
			
		</xsl:when>
	</xsl:choose>
</xsl:template>
<xsl:template name="donnePosition1MotMaj">
	<!-- 
		renvoie 0 si le mot n'est pas dans un passage en majuscule
		renvoie 1 si le mot est le premier d'un passage en majuscule
		renvoie 2 si le mot est le dernier d'un passage en majuscule
		renvoie 3 si le mot précédent est en majuscule -->
	<xsl:param name="position"/>
	<xsl:choose>
		<!-- on cherche le premier mot en majuscule précédent si il existe, ou si il y a un mot en minuscule avant -->
		<xsl:when test="position() = 1">
		<!-- c'est le debut du paragraphe -->
			<xsl:value-of select="1"/>
		</xsl:when>
		<xsl:when test="not(local-name(preceding::*[$position])='mot') or (local-name(preceding::*[$position])='mot' and string(preceding::*[$position]) = $chaine_vide)">
		<!--pas intéressant-->
			<xsl:call-template name="donnePosition1MotMaj">
				<xsl:with-param name="position" select="$position + 1"/>
			</xsl:call-template>
		</xsl:when>
		<!-- interessant et pas en majuscule -->
		<xsl:when test="contains(translate(preceding::*[$position],'°&sup2;&sup3;0123456789abcdefghijklmnopqrstuvwxyzàâéèêëîïôùûü&ccedil;&aelig;&oelig;','aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa'),'a')">
 			<xsl:value-of select="1"/>
		</xsl:when>
		<!-- intéressant, en majuscule? -->
		<xsl:when test="contains(translate(preceding::*[$position],'ABCDEFGHIJKLMNOPQRSTUVWXYZÀÂÉÈÊËÎÏÔÙÛÜ&Ccedil;&AElig;&OElig;','AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA'),'A')">
 			<xsl:value-of select="0"/>
		</xsl:when>
		<xsl:otherwise>
			<!-- intéressant, ni en majuscule ni en minuscule -->
			<xsl:value-of select="0"/>
			<!--<xsl:call-template name="donnePosition1MotMaj">
				<xsl:with-param name="position" select="$position + 1"/>
			</xsl:call-template>-->
		</xsl:otherwise>
	</xsl:choose>
</xsl:template>

<xsl:template name="donnePositionDMotMaj">
	<!-- 
		renvoie 0 si le mot n'est pas le dernier d'un passage en majuscule
		renvoie 1 si le mot est le dernier d'un passage en majusculee -->
	<xsl:param name="position"/>
	<xsl:choose>
		<!-- on cherche le premier mot en majuscule suivant si il existe, ou si il y a un mot en minuscule avant -->
		<xsl:when test="$position + position() &gt; last()">
		<!-- c'est la fin du document -->
			<xsl:value-of select="1"/>
		</xsl:when>
		<xsl:when test="not(local-name(following::*[$position])='mot') or (local-name(following::*[$position])='mot' and string(following::*[$position]) = $chaine_vide)">
		<!--pas intéressant-->
			<xsl:call-template name="donnePositionDMotMaj">
				<xsl:with-param name="position" select="$position + 1"/>
			</xsl:call-template>
		</xsl:when>
		<!-- interessant et pas en majuscule -->
		<xsl:when test="contains(translate(following::*[$position],'°&sup2;&sup3;0123456789abcdefghijklmnopqrstuvwxyzàâéèêëîïôùûü&ccedil;&aelig;&oelig;','aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa'),'a')">
 			<xsl:value-of select="1"/>
		</xsl:when>
		<!-- intéressant, en majuscule? -->
		<xsl:when test="contains(translate(following::*[$position],'ABCDEFGHIJKLMNOPQRSTUVWXYZÀÂÉÈÊËÎÏÔÙÛÜ&Ccedil;&AElig;&OElig;','AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA'),'A')">
 			<xsl:value-of select="0"/>
		</xsl:when>
		<xsl:otherwise>
			<!-- intéressant, ni en majuscule ni en minuscule -->
			<!--<xsl:value-of select="0"/>-->
			<xsl:call-template name="donnePositionDMotMaj">
				<xsl:with-param name="position" select="$position + 1"/>
			</xsl:call-template>
		</xsl:otherwise>
	</xsl:choose>
</xsl:template>

<xsl:template name="estSuiviMaj">
	<xsl:param name="nb"/>
	<xsl:param name="position"/>
	<!-- renvoie 1 si il y a nb mot en majuscule au moins après le mot -->
	<xsl:choose>
		<xsl:when test="$nb = 0">
			<xsl:value-of select="1"/>
		</xsl:when>
		<xsl:when test="$position + position() + $nb &gt; last()">
		<!-- c'est la fin du paragraphe -->
			<xsl:value-of select="0"/>
		</xsl:when>
		<xsl:when test="not(local-name(following::*[$position])='mot') or (local-name(following::*[$position])='mot' and string(following::*[$position]) = $chaine_vide)">
		<!--pas intéressant-->
			<xsl:choose>
				<xsl:when test="local-name(following::*[$position])='mot' and string(following::*[$position]) = $chaine_vide">
				<!-- on ajoute un mot pour ignorer le mot vide -->
					<xsl:call-template name="estSuiviMaj">
						<xsl:with-param name="position" select="$position + 1"/>
						<xsl:with-param name="nb" select="$nb + 1"/>
					</xsl:call-template>
				</xsl:when>
				<xsl:otherwise>
					<xsl:call-template name="estSuiviMaj">
						<xsl:with-param name="position" select="$position + 1"/>
						<xsl:with-param name="nb" select="$nb"/>
					</xsl:call-template>
				</xsl:otherwise>
			</xsl:choose>
		</xsl:when>
		<!-- interessant et pas en majuscule -->
		<xsl:when test="contains(translate(following::*[$position],'°&sup2;&sup3;0123456789abcdefghijklmnopqrstuvwxyzàâéèêëîïôùûü&ccedil;&aelig;&oelig;','aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa'),'a')">
 			<xsl:value-of select="0"/>
		</xsl:when>
		<!-- intéressant, en majuscule? -->
		<xsl:when test="contains(translate(following::*[$position],'ABCDEFGHIJKLMNOPQRSTUVWXYZÀÂÉÈÊËÎÏÔÙÛÜ&Ccedil;&AElig;&OElig;','AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA'),'A')">
 			<xsl:call-template name="estSuiviMaj">
				<xsl:with-param name="position" select="$position + 1"/>
				<xsl:with-param name="nb" select="$nb - 1"/>
			</xsl:call-template>
		</xsl:when>
		<xsl:otherwise>
			<xsl:call-template name="estSuiviMaj">
				<xsl:with-param name="position" select="$position + 1"/>
				<xsl:with-param name="nb" select="$nb"/>
			</xsl:call-template>
		</xsl:otherwise>
	</xsl:choose>
</xsl:template>

<xsl:template name="estPrecedeMaj">
	<xsl:param name="nb"/>
	<xsl:param name="position"/>
	<!-- renvoie 1 si il y a nb mot en majuscule au moins après le mot -->
	<xsl:choose>
		<xsl:when test="$nb = 0">
			<xsl:value-of select="1"/>
		</xsl:when>
		<xsl:when test="position() - $position - $nb &lt; 0">
		<!-- c'est le début du document -->
			<xsl:value-of select="0"/>
		</xsl:when>
		<xsl:when test="not(local-name(preceding::*[$position])='mot') or (local-name(preceding::*[$position])='mot' and string(preceding::*[$position]) = $chaine_vide)">
		<!--pas intéressant-->
			<xsl:choose>
				<xsl:when test="local-name(preceding::*[$position])='mot' and string(preceding::*[$position]) = $chaine_vide">
				<!-- on ajoute un mot pour ignorer le mot vide -->
					<xsl:call-template name="estPrecedeMaj">
						<xsl:with-param name="position" select="$position + 1"/>
						<xsl:with-param name="nb" select="$nb + 1"/>
					</xsl:call-template>
				</xsl:when>
				<xsl:otherwise>
					<xsl:call-template name="estPrecedeMaj">
						<xsl:with-param name="position" select="$position + 1"/>
						<xsl:with-param name="nb" select="$nb"/>
					</xsl:call-template>
				</xsl:otherwise>
			</xsl:choose>
		</xsl:when>
		<!-- interessant et pas en majuscule -->
		<xsl:when test="contains(translate(preceding::*[$position],'°&sup2;&sup3;0123456789abcdefghijklmnopqrstuvwxyzàâéèêëîïôùûü&ccedil;&aelig;&oelig;','aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa'),'a')">
 			<xsl:value-of select="0"/>
		</xsl:when>
		<!-- intéressant, en majuscule? -->
		<xsl:when test="contains(translate(preceding::*[$position],'ABCDEFGHIJKLMNOPQRSTUVWXYZÀÂÉÈÊËÎÏÔÙÛÜ&Ccedil;&AElig;&OElig;','AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA'),'A')">
 			<xsl:call-template name="estPrecedeMaj">
				<xsl:with-param name="position" select="$position + 1"/>
				<xsl:with-param name="nb" select="$nb - 1"/>
			</xsl:call-template>
		</xsl:when>
		<xsl:otherwise>
			<xsl:call-template name="estPrecedeMaj">
				<xsl:with-param name="position" select="$position + 1"/>
				<xsl:with-param name="nb" select="$nb"/>
			</xsl:call-template>
		</xsl:otherwise>
	</xsl:choose>
</xsl:template>

<!-- **************************************************
		Outils pour les mises en evidence
	**********************************************
-->
<xsl:template name="donnePosition1MotME">
	<!-- 
		renvoie 0 si le mot n'est le 1er d'un passage en évidence
		renvoie 1 si le mot est le premier d'un passage en évidence
	-->
	<xsl:param name="position"/>
	<xsl:choose>
		<!-- on cherche le premier mot en évidence précédent si il existe, ou si il y a un mot pas en évidence avant -->
		<xsl:when test="position() - $position + 1 = 1">
		<!-- c'est le debut du þ -->
			<xsl:value-of select="1"/>
		</xsl:when>
		<xsl:when test="not(local-name(preceding::*[$position])='mot') or (local-name(preceding::*[$position])='mot' and string(preceding::*[$position]) = $chaine_vide)">
		<!--pas intéressant-->
			<xsl:call-template name="donnePosition1MotME">
				<xsl:with-param name="position" select="$position + 1"/>
			</xsl:call-template>
		</xsl:when>
		<!-- intéressant, en évidence? -->
		<xsl:when test="preceding::*[$position]/@mev=('1','2','3')">
 			<xsl:value-of select="0"/>
		</xsl:when>
		<xsl:otherwise>
			<xsl:value-of select="1"/>
		</xsl:otherwise>
	</xsl:choose>
</xsl:template>

<xsl:template name="donnePositionDMotME">
	<!-- 
		renvoie 0 si le mot n'est pas le dernier d'un passage en évidence
		renvoie 1 si le mot est le dernier d'un passage en évidence -->
	<xsl:param name="position"/>
	<xsl:choose>
		<!-- on cherche le premier mot en évidence suivant si il existe, ou si il y a un mot pas en évidence avant -->
		<xsl:when test="$position + position() &gt; last()">
		<!-- c'est la fin du document -->
			<xsl:value-of select="1"/>
		</xsl:when>
		<xsl:when test="not(local-name(following::*[$position])='mot') or (local-name(following::*[$position])='mot' and string(following::*[$position]) = $chaine_vide)">
		<!--pas intéressant-->
			<xsl:call-template name="donnePositionDMotME">
				<xsl:with-param name="position" select="$position + 1"/>
			</xsl:call-template>
		</xsl:when>
		<!-- intéressant, en évidence? -->
		<xsl:when test="following::*[$position]/@mev=('1','2','3')">
 			<xsl:value-of select="0"/>
		</xsl:when>
		<xsl:otherwise>
			<xsl:value-of select="1"/>
		</xsl:otherwise>
	</xsl:choose>
</xsl:template>

<xsl:template name="estSuiviME">
	<xsl:param name="nb"/>
	<xsl:param name="position"/>
	<!-- renvoie 1 si il y a nb mot en évidence au moins après le mot -->
	<xsl:choose>
		<xsl:when test="$nb = 0">
			<xsl:value-of select="1"/>
		</xsl:when>
		<xsl:when test="$position + position() + $nb &gt; last()">
		<!-- c'est la fin du document -->
			<xsl:value-of select="0"/>
		</xsl:when>
		<xsl:when test="not(local-name(following::*[$position])='mot') or (local-name(following::*[$position])='mot' and string(following::*[$position]) = $chaine_vide)">
		<!--pas intéressant-->
			<xsl:choose>
				<xsl:when test="local-name(following::*[$position])='mot' and string(following::*[$position]) = $chaine_vide">
				<!-- on ajoute un mot pour ignorer le mot vide -->
					<xsl:call-template name="estSuiviME">
						<xsl:with-param name="position" select="$position + 1"/>
						<xsl:with-param name="nb" select="$nb + 1"/>
					</xsl:call-template>
				</xsl:when>
				<xsl:when test="local-name(following::*[$position])='ponctuation'">
				<!-- on ajoute un mot pour ignorer le mot vide -->
					ponct
				</xsl:when>
				<xsl:otherwise>
					<xsl:call-template name="estSuiviME">
						<xsl:with-param name="position" select="$position + 1"/>
						<xsl:with-param name="nb" select="$nb"/>
					</xsl:call-template>
				</xsl:otherwise>
			</xsl:choose>
		</xsl:when>
		<!-- intéressant, en évidence? -->
		<xsl:when test="following::*[$position]/@mev=('1','2','3')"> 
 			<xsl:call-template name="estSuiviME">
				<xsl:with-param name="position" select="$position + 1"/>
				<xsl:with-param name="nb" select="$nb - 1"/>
			</xsl:call-template>
		</xsl:when>
		<xsl:otherwise>
			<xsl:value-of select="0"/>
		</xsl:otherwise>
	</xsl:choose>
</xsl:template>

<xsl:template name="estPrecedeME">
	<xsl:param name="nb"/>
	<xsl:param name="position"/>
	<!-- renvoie 1 si il y a nb mot en évidence au moins après le mot -->
	<xsl:choose>
		<xsl:when test="$nb = 0">
			<xsl:value-of select="1"/>
		</xsl:when>
		<xsl:when test="position() - $position - $nb &lt; 0">
		<!-- c'est la fin du document -->
			<xsl:value-of select="0"/>
		</xsl:when>
		<xsl:when test="not(local-name(preceding::*[$position])='mot') or (local-name(preceding::*[$position])='mot' and string(preceding::*[$position]) = $chaine_vide)">
		<!--pas intéressant-->
			<xsl:choose>
				<xsl:when test="local-name(preceding::*[$position])='mot' and string(preceding::*[$position]) = $chaine_vide">
				<!-- on ajoute un mot pour ignorer le mot vide -->
					<xsl:call-template name="estPrecedeME">
						<xsl:with-param name="position" select="$position + 1"/>
						<xsl:with-param name="nb" select="$nb + 1"/>
					</xsl:call-template>
				</xsl:when>
				<xsl:otherwise>
					<xsl:call-template name="estPrecedeME">
						<xsl:with-param name="position" select="$position + 1"/>
						<xsl:with-param name="nb" select="$nb"/>
					</xsl:call-template>
				</xsl:otherwise>
			</xsl:choose>
		</xsl:when>
		<!-- intéressant, en évidence? -->
		<xsl:when test="preceding::*[$position]/@mev=('1','2','3')"> 
 			<xsl:call-template name="estPrecedeME">
				<xsl:with-param name="position" select="$position + 1"/>
				<xsl:with-param name="nb" select="$nb - 1"/>
			</xsl:call-template>
		</xsl:when>
		<xsl:otherwise>
			<xsl:value-of select="0"/>
		</xsl:otherwise>
	</xsl:choose>
</xsl:template>
</xsl:stylesheet>