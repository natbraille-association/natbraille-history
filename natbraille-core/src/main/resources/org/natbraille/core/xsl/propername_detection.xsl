<?xml version='1.1' encoding="UTF-8" ?>
<!--
 * NAT - An universal Translator
 * Copyright (C) 2005 Bruno Mascret
 * Contact: bmascret@free.fr
 * 
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
-->

<!DOCTYPE xsl:stylesheet SYSTEM "nat://system/xsl/mmlents/windob.dtd">

<xsl:stylesheet version="2.0"
xmlns:xsl='http://www.w3.org/1999/XSL/Transform' 
xmlns:saxon='http://icl.com/saxon'
xmlns:m='http://www.w3.org/1998/Math/MathML'
xmlns:fn='http://www.w3.org/2005/xpath-functions'
xmlns:functx='http://www.functx.com'
xmlns:xs='http://www.w3.org/2001/XMLSchema'
xmlns:xhtml='http://www.w3.org/1999/xhtml'
xmlns:nat='http://natbraille.free.fr/xsl'
xmlns:lit='espacelit'
xmlns:doc='espaceDoc'>
	
	<xsl:import href="nat://system/xsl/nat-functs.xsl" />

	<xsl:output method="text" encoding="UTF-8"/>
	<xsl:param name="allPonct" select="false()"/>
	<xsl:variable name="l_maj" as="xs:string">ABCDEFGHIJKLMNOPQRSTUVWXYZÀÂÉÈÊËÎÏÔÙÛÜÁÍÓÚÑÌÒÄÖ&Ccedil;&AElig;&OElig;</xsl:variable>
	<xsl:variable name="s_maj" as="xs:string*" select="functx:chars($l_maj)"/>
	<xsl:variable name="l_maj_A" as="xs:string">AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA</xsl:variable>
	<xsl:variable name="l_min" as="xs:string">abcdefghijklmnopqrstuvwxyzàâéèêëîïôùûüáíóúñìòäö&cedil;&aelig;&oelig;</xsl:variable>
	<xsl:variable name="l_min_a" as="xs:string">aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa</xsl:variable>
	<xsl:variable name="l_num" as="xs:string">0123456789+-×÷=,.&sup2;&sup3;</xsl:variable>
	<xsl:variable name="l_alphabet" as="xs:string">abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ</xsl:variable>
	<xsl:variable name="l_punct_fin" as="xs:string">
		<xsl:value-of select="concat('–…...?!&quot;«»“”()¡¿[]{}', if($allPonct) then ';:' else '')"/>
	</xsl:variable>
	<xsl:variable name="l_punct" as="xs:string">–…...,;:.?!&quot;«»“”‘’&lsquo;&rsquo;&acute;&prime;()¡¿[]{}</xsl:variable>
	<xsl:variable name="ambi" as="xs:string*" select="('LE','LA', 'DE', 'DU')"/>
	<xsl:strip-space elements="*"/>

	<xsl:template match="doc:doc">
		<xsl:variable name="proper1" as="element()*">
			<xsl:apply-templates select="*/lit/mot" />
		</xsl:variable>
		<!-- tri des mots -->
		<xsl:variable name="ordered" as="element()*">
			<xsl:for-each select="$proper1">
				<xsl:sort select="upper-case(string(.))"/>
				<xsl:copy-of select="."/>
			</xsl:for-each>
		</xsl:variable>
		<!-- suppression des doublons -->
		<xsl:variable name="proper2" as="element()*">
			<xsl:for-each select="$ordered">
				<xsl:variable name="next_item" select="position()+1"/>
				<xsl:if test="not($ordered[$next_item]=.)"><xsl:copy-of select="."/>
				<!--<xsl:message select=".,':',$ordered[$next_item]"/>-->
				</xsl:if>
			</xsl:for-each>
		</xsl:variable>
		<!-- noms propres "sûrs" -->
		<xsl:variable name="ok" as="xs:string*">
			<xsl:for-each select="$proper2[@confidence=1]">
				<!--<xsl:value-of select="concat(.,';',@avant, ';', @après, '&#10;')"/>-->
				<xsl:value-of select="."/>
			</xsl:for-each>
		</xsl:variable>
		<!-- noms propres "à vérifier" -->
		<xsl:variable name="ask" as="xs:string*">
			<xsl:for-each select="$proper2[@confidence=0.5]">
				<!--<xsl:value-of select="concat(.,';',@avant, ';', @après, '&#10;')"/>-->
				<xsl:value-of select="."/>
			</xsl:for-each>
		</xsl:variable>
		<!-- noms propres "pas sûrs" -->
		<xsl:variable name="verif" as="xs:string*">
			<xsl:for-each select="$proper2[@confidence=0]">
				<!--<xsl:value-of select="concat(.,';',@avant, ';', @après, '&#10;')"/>-->
				<xsl:value-of select="."/>
			</xsl:for-each>
		</xsl:variable>
		<!--<xsl:value-of select="('proper1:',count($proper1), '; proper2:', count($proper2), ';sûr:', count($ok), ';pas sûr:', count($verif))"/>-->
		<xsl:value-of select="($ok,'; ',$ask,'; ',$verif)"/>
	</xsl:template>

	<xsl:template match="mot">
		<!-- commence par une majuscule (13397), n'est pas précédé d'une ponctuation (7566) finale (8569) ., n'est pas le premier mot d'un paragraphe
			n'est pas (tout en maj et précédé d'un mot tout en maj)
			OU est précédé par M, MMe, etc
		-->
		<xsl:variable name="titres" as="xs:string*" select="('M','MR','MME','MLLE')"/>
		<xsl:if test="nat:starts-with-any-of(.,$s_maj) and (
			not(contains($l_punct_fin,preceding-sibling::*[1]))
			and not(count(preceding-sibling::mot[1])=0)
			and not(upper-case(string(.))=. and upper-case(preceding-sibling::mot[1])=preceding-sibling::mot[1])
			or (functx:is-value-in-sequence(upper-case(preceding-sibling::mot[1]),$titres) and preceding-sibling::*[1]='.')
			)
		">
			<xsl:variable name="enMin" select="translate(lower-case(.),$l_punct,'')" as="xs:string"/>
			<!-- première lettre en maj, reste en minuscule -->
			<xsl:variable name="majMin" select="concat(upper-case(functx:chars($enMin)[1]), string-join(functx:chars($enMin)[position() &gt; 1],''))"/>
			<!-- confiance dans le mot -->
			<xsl:element name="mot">
				<xsl:choose>
					<!-- quasi sûr 1 -->
					<xsl:when test="@abrege='false' and not(functx:is-value-in-sequence(upper-case(.),$ambi))">
						<xsl:attribute name="confidence">1</xsl:attribute>
					</xsl:when>
					<!-- quasi sûr 2 -->
					<xsl:when test="functx:is-value-in-sequence(upper-case(preceding-sibling::mot[1]),$titres) and not(functx:is-value-in-sequence(upper-case(.),$ambi))">
						<xsl:attribute name="confidence">1</xsl:attribute>
					</xsl:when>
					<!-- plutôt sûr -->
					<xsl:when test="string-length(.)>3">
						<xsl:attribute name="confidence">0.5</xsl:attribute>
					</xsl:when>
					<!-- pas sûr -->
					<xsl:otherwise>
						<xsl:attribute name="confidence">0</xsl:attribute>
					</xsl:otherwise>
				</xsl:choose>
				<!--<xsl:attribute name="avant" select="preceding-sibling::*[1]"/>
				<xsl:attribute name="après" select="following-sibling::*[1]"/>-->
				<xsl:value-of select="$majMin"/>
			</xsl:element>
		</xsl:if>
	</xsl:template>

</xsl:stylesheet>
