/*
 * NatBraille - An universal Translator - Braille font generator
 * Copyright (C) 2014 Bruno Mascret
 * Contact: bmascret@free.fr
 * 
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */
package org.natbraille.font.Data;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.util.ArrayList;
import java.util.List;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.TransformerConfigurationException;
import javax.xml.transform.TransformerException;
import org.natbraille.brailletable.BrailleTable;
import org.natbraille.brailletable.BrailleTableException;
import org.natbraille.brailletable.BrailleTables;
import org.natbraille.brailletable.system.SystemBrailleTables;
import org.natbraille.font.BrailleFontDefinition;
import org.natbraille.font.svg.SvgBrailleFont;

/**
 * Standard Braille cell sizes values.
 * <p>
 * values from : http://www.tiresias.org/research/reports/braille_cell.htm
 * <p>
 * @author vivien
 */
public class DotStandards {

    private static class AStandard {

        public String name;
        public String xInterPoint;
        public String yInterPoint;
        public String xInterCell;
        public String yInterCell;
        public String diameter;
        public String height;

        public Integer nunu(String s) {
            Integer parsed = (int) (100 * Float.parseFloat(s));
            return parsed;
        }

        public AStandard(String name, String xInterPoint, String yInterPoint, String xInterCell, String yInterCell, String diameter, String height) {
            this.name = name;
            this.xInterPoint = xInterPoint;
            this.yInterPoint = yInterPoint;
            this.xInterCell = xInterCell;
            this.yInterCell = yInterCell;
            this.diameter = diameter;
            this.height = height;
        }

        @Override
        public String toString() {
            return new StringBuilder(String.format("%1$55s", name))
                    .append("\t").append(xInterCell)
                    .append("\t").append(yInterCell)
                    .append("\t").append(xInterPoint)
                    .append("\t").append(yInterPoint)
                    .append("\t").append(diameter)
                    .append("\t").append(height)
                    .toString();
        }

    }

    // distances in millimeter
    private static final AStandard knownStandards[] = new AStandard[]{
        new AStandard("American Library of Congress", "2.5", "2.5", "6.25", "10.0", " ", "0.5"),
        new AStandard("American National Library for the Blind", "2.28", "2.28", "6.09", "10.16", " ", "0.5"),
        new AStandard("American Standard Sign", "2.3 - 2.5", "2.3 - 2.5", "6.1 - 7.6", "10.0 - 10.1", "1.5 - 1.6", "0.6 - 0.9"),
        new AStandard("Australia Sign", "2.29 - 2.50", "2.29 - 2.54", "6.00 - 6.10", "10.16 - 10.41", "1.40 - 1.50", "0.46 - 0.53"),
        new AStandard("Californian Sign", "2.54", "2.54", "5.08", " ", " ", "0.64"),
        new AStandard("ECMA Euro Braille", "2.5", "2.5", "6.0", "10.0", "1.3", "0.5"),
        new AStandard("Electronic Braille", "2.4", "2.4", "6.4", " ", " ", "0.8"),
        new AStandard("English Interline", "2.29", "2.54", "6.00", "12.70", "1.4 - 1.5", "0.46"), // (alternate print/braille)
        new AStandard("English Interpoint", "2.29", "2.54", "6.00", "10.41", "1.4 - 1.5", "0.46"),// (braille on both sides of the paper)
        new AStandard("English Giant Dot", "3.25", "3.25", "9.78", "17.02", "1.9", "0.81"),
        new AStandard("Enlarged American", "2.54", "2.54", "7.24", "12.70", " ", " "),
        new AStandard("Enhanced Line Spacing", "2.29", "2.29", "6.1", "15.24", " ", " "),
        new AStandard("French", "2.5 - 2.6", "2.5 - 2.6", " ", ">10", "1.2", "0.8 - 1.0"),
        new AStandard("German", "2.5", "2.5", "6.0", "10.0", "1.3 - 1.6", "≥0.5"),
        new AStandard("International Building Standard", "2.5", "2.5", "6.1 - 7.6", "10.0 - 10.1", "1.5 - 1.6", "0.6 - 0.9"),
        new AStandard("Italian", "2.2 - 2.5", "2.2 - 2.5", " ", " ", "1.0", "0.5"),
        new AStandard("Japanese", "2.13", "2.37", "5.4", "13.91", "1.43", "0.5"),
        new AStandard("Jumbo American", "2.92", "2.92", "8.76", "12.70", "1.7", "0.53"),
        new AStandard("Korean", "2.0", "2.0", "5.0", "6.0", "1.5", "0.6"),
        new AStandard("Latvian", "2.5", "2.5", "5", "10.0", "1.6", "0.45"),
        new AStandard("Marburg Medium", "2.5", "2.5", "6.0", "10.0", "1.3 - 1.6", " "),
        new AStandard("Marburg Large", "2.7", "2.7", "6.6", "10.8", "1.5 - 1.8", " "),
        new AStandard("Portuguese", "2.29", "2.54", "6.0", "10.41", "1.4", " "),
        new AStandard("Small English", "2.03", "2.03", "5.38", "8.46", "1.4 - 1.5", "0.33"),
        new AStandard("Spanish", "2.5", "2.5", "6.0", "10.0", "1.2", " "),
        new AStandard("Standard American", "2.34", "2.34", "6.22", "10.16", "1.45", "0.48"),
        new AStandard("Swedish", "2.5", "2.5", "6.0", "10.0", "1.0", "0.25")
    };

    /**
     * Get standard name starting with given string (case insensitive).
     * <p>
     * @param loName the looked up standard name;
     * @return
     */
    private static List<AStandard> forName(String loName) {
        List<AStandard> resu = new ArrayList<>();
        for (AStandard as : knownStandards) {
            if (as.name.toLowerCase().startsWith(loName.toLowerCase())) {
                resu.add(as);
            }
        }
        return resu;
    }

    public static void main(String args[]) throws ParserConfigurationException, TransformerException, FileNotFoundException, TransformerConfigurationException, BrailleTableException {
        for (AStandard s : forName("")) {
            //System.out.println(s);
        }

        String name = "";
        for (AStandard s : forName(name)) {

            try {
                BrailleFontDefinition def = new BrailleFontDefinition();

                def.setShowEmpty(true);
                def.setxInterCell(s.nunu(s.xInterCell));
                def.setyInterCell(s.nunu(s.yInterCell));
                def.setxInterPoint(s.nunu(s.xInterPoint));
                def.setyInterPoint(s.nunu(s.yInterPoint));
                def.setRadius(s.nunu(s.diameter)/2);

                //BrailleTable fontBrailleTable = BrailleTables.forName(SystemBrailleTables.DuxCBfr1252);
                BrailleTable fontBrailleTable = BrailleTables.forName(SystemBrailleTables.CodeUS);
                def.setBrailleTable(fontBrailleTable);
                SvgBrailleFont svgBrailleFont = new SvgBrailleFont(def);
                String nsName = s.name.replaceAll(" ", "_");
                String filename = "test/fonts/" + nsName + "_" + def.getBrailleTable().getName() + ".svg";
                svgBrailleFont.write(new FileOutputStream(new File(filename)));

                System.out.println("[OK] " + s);
            } catch (Exception e) {

                System.out.println("[!!] " + s);
            }
        }

    }

}


/* ORIGINAL (unmodified) TABLE
 "American Library of Congress", "2.5", "2.5", "6.25", "10.0", " ", "0.5
 "American National Library for the Blind", "2.28", "2.28", "6.09", "10.16", " ", "0.5
 "American Standard Sign", "2.3 - 2.5", "2.3 - 2.5", "6.1 - 7.6", "10.0 - 10.1", "1.5 - 1.6", "0.6 - 0.9
 "Australia Sign", "2.29 - 2.50", "2.29 - 2.54", "6.00 - 6.10", "10.16 - 10.41", "1.40 - 1.50", "0.46 - 0.53
 "Californian Sign", "2.54", "2.54", "5.08", " ", " ", "0.64
 "ECMA Euro Braille", "2.5", "2.5", "6.0", "10.0", "1.3", "0.5
 "Electronic Braille", "2.4", "2.4", "6.4", " ", " ", "0.8
 "English Interline (alternate print/braille)", "2.29", "2.54", "6.00", "12.70", "1.4 - 1.5", "0.46
 "English Interpoint (braille on both sides of the paper)", "2.29", "2.54", "6.00", "10.41", "1.4 - 1.5", "0.46
 "English Giant Dot", "3.25", "3.25", "9.78", "17.02", "1.9", "0.81
 "Enlarged American", "2.54", "2.54", "7.24", "12.70", " ", " 
 "Enhanced Line Spacing", "2.29", "2.29", "6.1", "15.24", " ", " 
 "French", "2.5 - 2.6", "2.5 - 2.6", " ", ">10", "1.2", "0.8 - 1.0
 "German", "2.5", "2.5", "6.0", "10.0", "1.3 - 1.6", "≥0.5
 "International Building Standard", "2.5", "2.5", "6.1 - 7.6", "10.0 - 10.1", "1.5 - 1.6", "0.6 - 0.9
 "Italian", "2.2 - 2.5", "2.2 - 2.5", " ", " ", "1.0", "0.5
 "Japanese", "2.13", "2.37", "5.4", "13.91", "1.43", "0.5
 "Jumbo American", "2.92", "2.92", "8.76", "12.70", "1.7", "0.53
 "Korean", "2.0", "2.0", "5.0", "6.0", "1.5", "0.6
 "Latvian", "2.5", "2.5", "5", "10.0", "1.6", "0.45
 "Marburg Medium", "2.5", "2.5", "6.0", "10.0", "1.3 - 1.6", " 
 "Marburg Large", "2.7", "2.7", "6.6", "10.8", "1.5 - 1.8", " 
 "Portuguese", "2.29", "2.54", "6.0", "10.41", "1.4", " 
 "Small English", "2.03", "2.03", "5.38", "8.46", "1.4 - 1.5", "0.33
 "Spanish", "2.5", "2.5", "6.0", "10.0", "1.2", " 
 "Standard American", "2.34", "2.34", "6.22", "10.16", "1.45", "0.48
 "Swedish", "2.5", "2.5", "6.0", "10.0", "1.0", "0.25")
 */
