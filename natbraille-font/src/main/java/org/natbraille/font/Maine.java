/*
 * NatBraille - An universal Translator - Braille font generator
 * Copyright (C) 2014 Bruno Mascret
 * Contact: bmascret@free.fr
 * 
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */
package org.natbraille.font;

import java.io.File;
import java.io.FileOutputStream;
import org.natbraille.brailletable.BrailleTables;
import org.natbraille.brailletable.system.SystemBrailleTables;
import org.natbraille.font.svg.SvgBrailleFont;

/**
 *
 * @author vivien
 */
public class Maine {

    /**
     * @param args the command line arguments
     * @throws java.lang.Exception
     */
    public static void main(String[] args) throws Exception {

        BrailleFontDefinition def = new BrailleFontDefinition();        
        def.setEightDots(false);
        def.setRadius(def.getRadius());
        def.setShowEmpty(true);
        def.setxInterPoint(def.getxInterPoint()*2);
        def.setyInterPoint(def.getyInterPoint()*2);
        
        def.setBrailleTable(BrailleTables.forName(SystemBrailleTables.CodeUS));
        SvgBrailleFont bf = new SvgBrailleFont(def);
        
        bf.write(new FileOutputStream(new File("fonts/bfont.svg")));

    }
}
