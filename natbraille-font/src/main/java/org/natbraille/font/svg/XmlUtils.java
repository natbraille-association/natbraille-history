/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.natbraille.font.svg;

import java.io.OutputStream;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.OutputKeys;
import javax.xml.transform.Result;
import javax.xml.transform.Source;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerConfigurationException;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import org.w3c.dom.Document;
import org.w3c.dom.Element;

/**
 *
 * @author vivien
 */
public class XmlUtils {

    public static void documentToStream(Document fontDocument, OutputStream os) throws TransformerConfigurationException, TransformerException {
        TransformerFactory transFactory = TransformerFactory.newInstance();
        Transformer idTransform = transFactory.newTransformer();
        idTransform.setOutputProperty(OutputKeys.METHOD, "xml");
        idTransform.setOutputProperty(OutputKeys.INDENT, "yes");
        Source input = new DOMSource(fontDocument);
        Result output = new StreamResult(os);
        idTransform.transform(input, output);

    }

    public static Document newSvgDocument() throws ParserConfigurationException {
        Document document;
        DocumentBuilderFactory fabrique;
        fabrique = DocumentBuilderFactory.newInstance();
        DocumentBuilder builder = fabrique.newDocumentBuilder();
        document = builder.newDocument();
        Element racine = (Element) document.createElement("svg");
        document.appendChild(racine);
        return document;
    }
}
