# SOME DESCRIPTIVE TITLE.
# Copyright (C) YEAR THE PACKAGE'S COPYRIGHT HOLDER
# This file is distributed under the same license as the PACKAGE package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2014-10-13 09:48+0200\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"Language: \n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=CHARSET\n"
"Content-Transfer-Encoding: 8bit\n"

#: org/natbraille/font/ui/BrailleFontCli.java:47
msgid "set font ascent (default : auto)"
msgstr ""

#: org/natbraille/font/ui/BrailleFontCli.java:49
msgid "set font descent (default : auto)"
msgstr ""

#: org/natbraille/font/ui/BrailleFontCli.java:51
msgid "set the Braille cell dimensions norm"
msgstr ""

#: org/natbraille/font/ui/BrailleFontCli.java:53
msgid "set the Braille table"
msgstr ""

#: org/natbraille/font/ui/BrailleFontCli.java:55
msgid "set the dot radius  (overrides --norm)"
msgstr ""

#: org/natbraille/font/ui/BrailleFontCli.java:57
msgid ""
"set the horizontal distance between two Braille cells  (overrides --norm)"
msgstr ""

#: org/natbraille/font/ui/BrailleFontCli.java:59
msgid "set the vertical distance between two Braille cells  (overrides --norm)"
msgstr ""

#: org/natbraille/font/ui/BrailleFontCli.java:61
msgid ""
"set the vertical distance between two Braille dots in a cell(overrides --"
"norm)"
msgstr ""

#: org/natbraille/font/ui/BrailleFontCli.java:63
msgid ""
"set the horizontal distance between two Braille dots in a cell(overrides --"
"norm)"
msgstr ""

#: org/natbraille/font/ui/BrailleFontCli.java:65
msgid "list braille tables and cell dimensions norms"
msgstr ""

#: org/natbraille/font/ui/BrailleFontCli.java:67
#: org/natbraille/font/ui/BrailleFontCli.java:69
msgid "display help"
msgstr ""
