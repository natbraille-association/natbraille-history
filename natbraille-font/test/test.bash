#!/bin/bash

NATBRAILLEPDF="java -jar ../../natbraille-braillepdf/target/natbraille-braillepdf-1.0-SNAPSHOT-jar-with-dependencies.jar"

for i in fonts/*svg ;
do 
    ../svg2ttf/svg2ttf.bash $i $i.ttf

    name="A_"`basename $i`".txt"
    cp testdocs/codeUS-UTF8.txt $name
    $NATBRAILLEPDF -f $i.ttf  $name
    rm $name

    name="B_"`basename $i`".txt"
    cp testdocs/line.txt $name
    $NATBRAILLEPDF -f $i.ttf $name
    rm $name

done;