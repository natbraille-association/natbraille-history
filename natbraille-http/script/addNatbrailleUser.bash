#!/bin/bash

USERNAME=$1;
PASSWORD=$2;

#
# check command parameters
#

if  [ -e $USERNAME ] ; then
    echo "username ?"
    echo "usage : addNatbrailleUser.bash username password" ; 
    exit;  
fi;
if  [ -e $PASSWORD ] ; then
    echo "password ?"
    echo "usage : addNatbrailleUser.bash username password" ; 
    exit;  
fi;

#
# check if user already exists
#

EXISTS=`echo '
use natbrailleRealm;
db.user.find({userName:"'$USERNAME'"});
' | mongo | grep $USERNAME | wc -l`

if [ $EXISTS == 0 ] ; then 
    echo adding user \"$1\" with password \"$2\";


#
# insert user
#

    echo '
   use natbrailleRealm;
     newUser = {
     	"userName" : "'$USERNAME'",	
     	"password" : "'$PASSWORD'",
     	"roles" : [{
     			"_id" : ObjectId(),
     			"name" : "user"
    	}]
     };
   db.user.insert(newUser);
' |  mongo



else
    echo user \"$USERNAME\" already exists 
    exit;
fi
