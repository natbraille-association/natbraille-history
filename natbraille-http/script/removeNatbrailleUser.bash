#!/bin/bash

USERNAME=$1;


#
# check command parameters
#

if  [ -e $USERNAME ] ; then
    echo "username ?"
    echo "usage : removeNatbrailleUser.bash username" ; 
    exit;  
fi;

#
# check if user already exists
#

EXISTS=`echo '
use natbrailleRealm;
db.user.find({userName:"'$USERNAME'"});
' | mongo | grep $USERNAME | wc -l`

if [ $EXISTS == 0 ] ; then 
    echo no user with userName : \"$1\" ;
else
#
# remove user
#
echo '
use natbrailleRealm;
db.user.remove({userName:"'$USERNAME'"});
' | mongo > /dev/null


fi
