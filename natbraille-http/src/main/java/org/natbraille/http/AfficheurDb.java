/*
 * NatBraille Http Server - An universal Translator over http
 * Copyright (C) 2013 Vivien Guillet
 * Contact: vivien.guillet@univ-lyon1.fr
 * 
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */



package org.natbraille.http;
import org.natbraille.core.gestionnaires.Afficheur;
import org.natbraille.core.gestionnaires.LogMessage;
import org.natbraille.http.dbDocument.DbTransDoc;
import org.natbraille.http.dbDocument.DbTransStatusDoc;
import org.natbraille.http.dbDocument.NatLibrary;
/**
 * Write transcription log to db.
 * @author vivien
 */
public class AfficheurDb extends Afficheur {
	
	private final NatLibrary natLibrary;
	private final DbTransDoc dbTranscriptionDocument;

	@Override
	public void afficheMessage(LogMessage logMessage) {
		
		// create a db document from the log message
		DbTransStatusDoc dbStatusDoc = new DbTransStatusDoc(logMessage,getI18n());
		
		// author of the log message is author of the transcription
		dbStatusDoc.stamp(dbTranscriptionDocument.getMetadataDocument().getAuthor());
		
		// set the transcription id
		dbStatusDoc.setDbTranscriptionDocumentId(dbTranscriptionDocument.getId());
		
		// write to the library
		dbStatusDoc.write(natLibrary.getDb());
	}
	public AfficheurDb(DbTransDoc dbTranscriptionDocument, NatLibrary natLibrary){
		this.dbTranscriptionDocument = dbTranscriptionDocument;
		this.natLibrary = natLibrary;
		this.setLogLevel(dbTranscriptionDocument.getLogLevel());
	}
	
}
