/*
 * NatBraille Http Server - An universal Translator over http
 * Copyright (C) 2013 Vivien Guillet
 * Contact: vivien.guillet@univ-lyon1.fr
 * 
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */
package org.natbraille.http;

import com.mongodb.BasicDBObject;
import com.mongodb.DB;
import com.mongodb.DBCollection;
import com.mongodb.DBCursor;
import com.mongodb.MongoException;

/**
 * Test database.
 */
public class Mongotest {

    public static Boolean test(DB db) {
        Boolean ok = false;
        try {

			// Get collection from MongoDB, database named "yourDB"
            // if collection doesn't exists, mongoDB will create it
            // automatically
            DBCollection collection = db.getCollection("test");

            // create a document to store key and value
            BasicDBObject document = new BasicDBObject();
            document.put("par", "fait");

            // save it into collection named "yourCollection"
            collection.insert(document);

            // search query
            BasicDBObject searchQuery = new BasicDBObject();
            searchQuery.put("par", "fait");

            // query it
            DBCursor cursor = collection.find(searchQuery);

            ok = cursor.hasNext();

            collection.remove(document);
            //test2(db);
        } catch (MongoException e) {
            e.printStackTrace();
        }
        return ok;
    }
}
