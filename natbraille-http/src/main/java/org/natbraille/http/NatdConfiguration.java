/*
 * NatBraille Http Server - An universal Translator over http
 * Copyright (C) 2013 Vivien Guillet
 * Contact: vivien.guillet@univ-lyon1.fr
 * 
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */
package org.natbraille.http;

import java.util.ArrayList;

/**
 * nat web server configuration backed by a json file
 *
 * @author vivien
 *
 */

public class NatdConfiguration {

    private ArrayList<String> systemFilterConfigurationPaths = new ArrayList<>();
//    private String documentStorageDir = null;
    private String version = null;
    private String infos = null;
    private String db_name = null;
    private String db_host = null;
    private String db_port = null;
    private int transcriptionPoolSize = 20;

    /**
     * 
     */

//    /**
//     * @return the documentStorageDir
//     */
//    public String getDocumentStorageDir() {
//        return documentStorageDir;
//    }
//
//    /**
//     * @param documentStorageDir the documentStorageDir to set
//     */
//    public void setDocumentStorageDir(String documentStorageDir) {
//        this.documentStorageDir = documentStorageDir;
//    }

    /**
     * @return the systemFilterConfigurationPaths
     */
    public ArrayList<String> getSystemFilterConfigurationPaths() {
        return systemFilterConfigurationPaths;
    }

    /**
     * @param systemFilterConfigurationPaths the systemFilterConfigurationPaths
     * to set
     */
    public void setSystemFilterConfigurationPaths(ArrayList<String> systemFilterConfigurationPaths) {
        this.systemFilterConfigurationPaths = systemFilterConfigurationPaths;
    }

    /**
     * @return the version
     */
    public String getVersion() {
        return version;
    }

    /**
     * @param version the version to set
     */
    public void setVersion(String version) {
        this.version = version;
    }

    /**
     * @return the infos
     */
    public String getInfos() {
        return infos;
    }

    /**
     * @param infos the infos to set
     */
    public void setInfos(String infos) {
        this.infos = infos;
    }

    /**
     * @return the db_name
     */
    public String getDb_name() {
        return db_name;
    }

    /**
     * @param db_name the db_name to set
     */
    public void setDb_name(String db_name) {
        this.db_name = db_name;
    }

    /**
     * @return the db_host
     */
    public String getDb_host() {
        return db_host;
    }

    /**
     * @param db_host the db_host to set
     */
    public void setDb_host(String db_host) {
        this.db_host = db_host;
    }

    /**
     * @return the db_port
     */
    public String getDb_port() {
        return db_port;
    }

    /**
     * @param db_port the db_port to set
     */
    public void setDb_port(String db_port) {
        this.db_port = db_port;
    }

    public int getTranscriptionPoolSize() {
        return transcriptionPoolSize;
    }

    public void setTranscriptionPoolSize(int transcriptionPoolSize) {
        this.transcriptionPoolSize = transcriptionPoolSize;
    }

}
