package org.natbraille.http.dbDocument;

import java.net.URISyntaxException;
import java.nio.ByteBuffer;
import java.nio.charset.Charset;
import org.natbraille.brailletable.BrailleTables;
import org.natbraille.core.document.NatDocument;
import org.natbraille.core.document.NatDocumentException;
import org.natbraille.http.dbDocument.base.DbDoc;

public abstract class DbNatDoc extends DbDoc {

	public class Key {
		public final static String  ORIGINAL_FILENAME = "original_filename";
		public final static String  CHARSET = "charset";
		public final static String  MIMETYPE = "doctype";
		public final static String  BRAILLE_TABLE = "braille_table";
		public final static String  BRAILLE_ENCODING = "braille_encoding";
		public final static String  DATA = "data";
	}

	public DbNatDoc(NatDocument natDoc) throws NatDocumentException {
		setOriginalFileName(natDoc.getOriginalFileName());
		setCharset(natDoc.getOrGuessCharset().name());
		setMimeType(natDoc.getOrGuessContentType());
		try {
			setBrailleTable(natDoc.getBrailletable().getName());
			setBrailleEncoding(natDoc.getOrGuessCharset().name());
		} catch (Exception e){
			// getBrailleTable can be null;
		}
		setData(natDoc.getBytebuffer().array());
	}

	public NatDocument toNatDocument() throws URISyntaxException, NatDocumentException{
		NatDocument natDoc = new NatDocument();
		try {
		natDoc.setBrailletable(BrailleTables.forName(getBrailleTable()));
		/*URI uri = new URI(getBrailleTable());
		String name = null;
		Charset charset = Charset.forName(getBrailleEncoding());		
                        */
		} catch (Exception e){
			// not illegal not to have a braille table
		}
		natDoc.setByteBuffer(ByteBuffer.wrap(getData()));
		natDoc.setOriginalFileName(getOriginalFileName());
		natDoc.forceCharset(Charset.forName(getCharset()));
		natDoc.forceContentType(getMimeType());
		return natDoc;
	}
	
	public DbNatDoc() {
	}

	public String getOriginalFileName(){
		return getDbo().getString(Key.ORIGINAL_FILENAME);
	}
	public String getCharset(){
		return getDbo().getString(Key.CHARSET);
	}
	public String getMimeType(){
		return getDbo().getString(Key.MIMETYPE);
	}
	public String getBrailleTable(){
		return getDbo().getString(Key.BRAILLE_TABLE);
	}
	public String getBrailleEncoding(){
		return getDbo().getString(Key.BRAILLE_ENCODING);
	}
	public byte[] getData(){
		return (byte[]) getDbo().get(Key.DATA);
	}
	protected void setOriginalFileName(String originalFileName){
		getDbo().remove(Key.ORIGINAL_FILENAME);
		getDbo().put(Key.ORIGINAL_FILENAME,originalFileName);		
	}
	protected void setCharset(String charset){
		getDbo().remove(Key.CHARSET);
		getDbo().put(Key.CHARSET,charset);
	}
	protected void setMimeType(String mimeType){
		getDbo().remove(Key.MIMETYPE);
		getDbo().put(Key.MIMETYPE,mimeType);
	}
	protected void setBrailleTable(String brailleTable){
		getDbo().remove(Key.BRAILLE_TABLE);
		getDbo().put(Key.BRAILLE_TABLE,brailleTable);
	}
	protected void setBrailleEncoding(String brailleEncoding){
		getDbo().remove(Key.BRAILLE_ENCODING);
		getDbo().put(Key.BRAILLE_ENCODING,brailleEncoding);
	}
	protected void setData(byte[] data){
		getDbo().remove(Key.DATA);
		getDbo().put(Key.DATA,data);
	}

	
}
