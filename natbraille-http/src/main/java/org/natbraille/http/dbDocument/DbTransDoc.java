/*
 * NatBraille Http Server - An universal Translator over http
 * Copyright (C) 2013 Vivien Guillet
 * Contact: vivien.guillet@univ-lyon1.fr
 * 
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */
package org.natbraille.http.dbDocument;

import java.util.ArrayList;
import java.util.List;
import org.natbraille.core.gestionnaires.GestionnaireErreur;
import org.natbraille.http.TranscriptionState;
import org.natbraille.http.dbDocument.base.DbDoc;


public class DbTransDoc extends DbDoc implements Comparable<DbTransDoc> {
	private final String COLLECTION_NAME = "transcription documents";
	protected String getCollectionName(){
		return COLLECTION_NAME;
	}
	public class Key {
		public final static String INPUT_DOC_ID = "input_doc_id";
		public final static String CONFIGURATION_DOC_ID = "configuration_doc_id";
		public final static String OUTPUT_DOC_ID = "output_doc_id";
		public final static String STATE = "state";
		public final static String LOG_LEVEL = "log_level";
	}
	public String getState() {
		return getDbo().getString(Key.STATE);
	}	
	public String getInputDocId() {
		return getDbo().getString(Key.INPUT_DOC_ID);
	}
	public String getConfigurationDocId() {
		return getDbo().getString(Key.CONFIGURATION_DOC_ID);
	}
	public String getOutputDocId() {
		return getDbo().getString(Key.OUTPUT_DOC_ID);
	}
	public int getLogLevel() {
		return getDbo().getInt(Key.LOG_LEVEL);
	}

	private void setInputDocId(String inputDocId){
		//		getDbo().remove(Key.INPUT_DOC_ID);
		getDbo().put(Key.INPUT_DOC_ID,inputDocId);
	}

	private void setConfigurationDocId(String configurationDocId){
		//		getDbo().remove(Key.INPUT_DOC_ID);
		getDbo().put(Key.CONFIGURATION_DOC_ID,configurationDocId);
	}
	
	public void setOutputDocId(String outputDocId){
		//		getDbo().remove(Key.INPUT_DOC_ID);
		getDbo().put(Key.OUTPUT_DOC_ID,outputDocId);
	}

	private void setLogLevel(int logLevel){
		//		getDbo().remove(Key.LOG_LEVEL);
		getDbo().put(Key.LOG_LEVEL,logLevel);
	}
	public void setState(TranscriptionState state) {
		getDbo().put(Key.STATE,state.name());
	}

	private GestionnaireErreur ge;
	public GestionnaireErreur getGestionnaireErreur(){
		return ge;
	}

	public DbTransDoc(String dbInputNatDocId, String dbConfigurationDocId, int logLevel){
		setInputDocId(dbInputNatDocId);
		setConfigurationDocId(dbConfigurationDocId);
		setLogLevel(logLevel);
		this.ge = new GestionnaireErreur();
                
	}
	public DbTransDoc() {
		// TODO Auto-generated constructor stub
	}
	@Override
	public int compareTo(DbTransDoc other) {
		Integer thisOrder = getMetadataDocument().getCreationOrder();
		Integer otherOrder = other.getMetadataDocument().getCreationOrder();
		return thisOrder.compareTo(otherOrder);
	}

	public ArrayList<DbTransStatusDoc> getAssociatedDbStatusDocuments(NatLibrary natLibrary){
		ArrayList<DbTransStatusDoc> 	status = new ArrayList<>();
		try {
			// build model
			DbTransStatusDoc dbStatusDocModel = new DbTransStatusDoc();
			dbStatusDocModel.setDbTranscriptionDocumentId(getId());

			// retrieve transcription status documents
			List<DbDoc> dbStatusDocList;
			dbStatusDocList = dbStatusDocModel.getMatching(natLibrary.getDb());

			// add
			for (DbDoc dbDoc : dbStatusDocList){
				DbTransStatusDoc dbTranscriptionStatusDoc = (DbTransStatusDoc) dbDoc;
				status.add(dbTranscriptionStatusDoc);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return status;
	}
	
}
