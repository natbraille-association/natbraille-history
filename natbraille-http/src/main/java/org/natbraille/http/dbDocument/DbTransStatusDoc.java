/*
 * NatBraille Http Server - An universal Translator over http
 * Copyright (C) 2013 Vivien Guillet
 * Contact: vivien.guillet@univ-lyon1.fr
 * 
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */
package org.natbraille.http.dbDocument;

import org.natbraille.core.gestionnaires.LogMessage;
import org.natbraille.http.dbDocument.base.DbDoc;
import org.xnap.commons.i18n.I18n;


public class DbTransStatusDoc extends DbDoc {
	
	private final String COLLECTION_NAME = "transcription status documents";

	protected String getCollectionName(){
		return COLLECTION_NAME;
	}


	public class Key {
		public final static String  TRANSCRIPTION_ID = "transcription_id";
		public final static String  MESSAGE_KIND= "message_kind";
		public final static String  LOG_LEVEL = "log_level";
		public final static String  TEXT = "text";
		public final static String  DATE = "date";

	}
	public String getDbTranscriptionDocumentId(){
		return getDbo().getString(Key.TRANSCRIPTION_ID);
	}	
	public String getMessageKind(){
		return getDbo().getString(Key.MESSAGE_KIND);
	}
	public int getLogLevel(){
		return getDbo().getInt(Key.LOG_LEVEL);
	}
	public String getText(){
		return getDbo().getString(Key.TEXT);
	}
	public String getDate(){
		return getDbo().getString(Key.DATE);
	}

	public void setDbTranscriptionDocumentId(String id) {
		getDbo().put(Key.TRANSCRIPTION_ID,id);
	}
	
	private void setText(String text){
	//	getDbo().remove(Key.TEXT);
		getDbo().put(Key.TEXT,text);
		
	}	
	private void setMessageKind(String messageKind){
		getDbo().remove(Key.MESSAGE_KIND);
		getDbo().put(Key.MESSAGE_KIND,messageKind);
		
	}
	private void setLogLevel(int logLevel){
		getDbo().remove(Key.LOG_LEVEL);
		getDbo().put(Key.LOG_LEVEL,logLevel);
	}
	private void setDate(String date){
		getDbo().remove(Key.DATE);
		getDbo().put(Key.DATE,date);
		
	}
	/**
	 * constrcutrs a log message
	 * @return
	 */
//	public LogMessage toLogMessage(){
//		//MessageContents mc = MessageContents.Tr(text)
//		return new LogMessage(
//				MessageKind.valueOf(getMessageKind()),
//				getText(),
//				getLogLevel()); 
//	}

	/**
	 * constructor from a log message
	 * @param logMessage
	 */
	public DbTransStatusDoc(LogMessage logMessage,I18n i18n){
		setMessageKind(logMessage.getKind().name());
		setLogLevel(logMessage.getLevel());
		setText(logMessage.getTrContents(i18n));	
		setDate(logMessage.getDate().toString());
	}
	public DbTransStatusDoc() {
		
	}

	
	
}
