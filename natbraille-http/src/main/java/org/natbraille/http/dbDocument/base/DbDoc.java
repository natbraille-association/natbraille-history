package org.natbraille.http.dbDocument.base;


import com.mongodb.BasicDBObject;
import com.mongodb.DB;
import com.mongodb.DBCollection;
import com.mongodb.DBCursor;
import com.mongodb.DBObject;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import org.bson.BasicBSONObject;
import org.bson.types.ObjectId;



public abstract class DbDoc {

	protected abstract String getCollectionName();

	public class Key {
		public final static String ID = "_id";
		public final static String METADATA = "metadata";
	}
	
	private DbMetadataDoc metadataDocument = new DbMetadataDoc();

	/**
	 * 
	 */
	private BasicBSONObject bbo = new BasicBSONObject();

	/**
	 * @return the dbo
	 */
	protected BasicBSONObject getDbo() {
		return bbo;
	}
	private void setDbo(BasicBSONObject dbo) {
		
		// sets the dbo
		this.bbo = dbo;

		// retrieves metadata of set dbo
		Map<?, ?> md = getDboMetaData();
		
		// create and set the DbMetaDataDocument
		setMetaDataDocument(new DbMetadataDoc(md));

	}

	public String getId(){
		return getDbo().get(Key.ID).toString();
	}

	public void setId(String id){
		getDbo().removeField(Key.ID);
		getDbo().append(Key.ID, new ObjectId(id));
	}


	private void newId(){
		getDbo().removeField(Key.ID);
		getDbo().append(Key.ID, new ObjectId());
	}

	public String buildURI(){
		return "";
	}

	// not in interface
	private Map<?,?> getDboMetaData(){
		return (Map<?,?>) getDbo().get(Key.METADATA);
	}

	// not in interface
	private void setDboMetaData(Map<?,?> metadata){
		getDbo().removeField(Key.METADATA);
		getDbo().append(Key.METADATA, metadata);
	}

	/**
	 * 
	 */

	public DbMetadataDoc getMetadataDocument() {
		return metadataDocument;
	}

	public void setMetaDataDocument(DbMetadataDoc metadataDocument) {
		this.metadataDocument = metadataDocument;
	}

	public void stamp(String author){
		newId();
		getMetadataDocument().timeStamp();
		getMetadataDocument().setAuthor(author);
	}

	public void write(DB db) {
	//	System.out.println("[DB] write in collection '"+getCollectionName()+"' : "+getDbo());

		// get collection from document class
		DBCollection collection = db.getCollection(getCollectionName());

		// set meta data in database object
		setDboMetaData(getMetadataDocument().getDbo());

		// insert in the collection
		collection.save(new BasicDBObject(getDbo().toMap()));

	//	documentsInCollection(db);
	}
	private DBObject getAsQuery(){
		BasicDBObject baseQuery = new BasicDBObject(getDbo().toMap());
		
		BasicBSONObject mdm = getMetadataDocument().getDbo();		
		for (Object a : mdm.keySet()){
			if (mdm.get(a) != null){
				String dotName = Key.METADATA+"."+(String) a;
				String val = (String) mdm.get(a);
				baseQuery.append(dotName, val);
			}
		}
		
		return baseQuery;
	}
	public void documentsInCollection(DB db){
		DBCollection collection = db.getCollection(getCollectionName());
		DBCursor cur = collection.find();
		System.out.println(">[DB] Collection "+getCollectionName()+" has documents ");
		while (cur.hasNext()){
			System.out.println(" - "+cur.next());
		}
	}
	public void remove(DB db){
		// get collection from document class
		DBCollection collection = db.getCollection(getCollectionName());
		// build search object
		DBObject query = getAsQuery();
		
		//remove
		collection.remove(query);
	}
	public void read (DB db) {

		//	documentsInCollection(db);

		// get collection from document class
		DBCollection collection = db.getCollection(getCollectionName());

		// build search object
		DBObject query = getAsQuery();

		// result 
		DBObject readDbo = collection.findOne(query);

		// set the BasicBSONObject
		setDbo(new BasicBSONObject(readDbo.toMap()));				

		// set the DbMetaData
//		setMetaDataDocument(new DbMetaDataDocument(getDboMetaData()));

		//		System.out.println("[DB] read in collection '"+getCollectionName()+"' : "+getDbo());

	}
	
	public void readId (DB db, String DbDocumentId) {
		setId(DbDocumentId);
		read(db);		
	}
	
	
	public List<DbDoc> getMatching(DB db) throws Exception{

		ArrayList<DbDoc> resu = new ArrayList<>();
		
		// get collection from document class
		DBCollection collection = db.getCollection(getCollectionName());

		
		DBObject query = getAsQuery();

	//	query = new BasicDBObject("metadata.author","natbraille");
		
		// find all
		DBCursor cur = collection.find(query);
		
		while (cur.hasNext()){
			DBObject readDbo = cur.next();

			// create a DbDocument of the good type
			//DbDoc item = getClass().newEmpty();
			DbDoc item = getClass().newInstance(); 
			
			// set the BasicBSONObject
			item.setDbo(new BasicBSONObject(readDbo.toMap()));				

			//System.out.println("[DB] read item of list collection '"+getCollectionName()+"' : "+item.getDbo());
		
			resu.add(item);
		}
		return resu;
	}
}
	
