package org.natbraille.http.dbDocument.base;

import java.util.Date;
import java.util.Map;
import org.bson.BasicBSONObject;
import org.bson.types.BSONTimestamp;

public class DbMetadataDoc {

    private BasicBSONObject dbo = new BasicBSONObject();

    public BasicBSONObject getDbo() {
        return dbo;
    }

    private void setDbo(BasicBSONObject dbo) {
        this.dbo = dbo;
    }

    public class Key {

        public final static String CREATION_ORDER = "creation_order";
        public final static String CREATION_DATE = "creation_date";
        public final static String AUTHOR = "author";
    }

    public void timeStamp() {
        BSONTimestamp timestamp = new BSONTimestamp();

        Date date = new Date();

        setCreationDate(date);
        setCreationOrder(timestamp.getInc());

    }

    public DbMetadataDoc(Map<?, ?> md) {
        setDbo(new BasicBSONObject(md));
    }

    public DbMetadataDoc() {
    }

    public int getCreationOrder() {
        return dbo.getInt(Key.CREATION_ORDER);
    }

    public Date getCreationDate() {
        return dbo.getDate(Key.CREATION_DATE);
    }

    public String getAuthor() {
        return dbo.getString(Key.AUTHOR);
    }

    private void setCreationOrder(int creationOrder) {
        dbo.remove(Key.CREATION_ORDER);
        dbo.put(Key.CREATION_ORDER, creationOrder);
    }

    private void setCreationDate(Date d) {
        dbo.remove(Key.CREATION_DATE);
        dbo.put(Key.CREATION_DATE, d);
    }

    public void setAuthor(String author) {
        dbo.remove(Key.AUTHOR);
        dbo.put(Key.AUTHOR, author);
    }

}
