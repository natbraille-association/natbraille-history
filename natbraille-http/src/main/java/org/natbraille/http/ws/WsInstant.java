/*
 * NatBraille Http Server - An universal Translator over http
 * Copyright (C) 2013 Vivien Guillet
 * Contact: vivien.guillet@univ-lyon1.fr
 * 
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */
package org.natbraille.http.ws;

import java.io.IOException;
import java.nio.charset.Charset;
import javax.servlet.ServletContext;
import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.SecurityContext;
import javax.xml.transform.TransformerException;
import org.natbraille.brailletable.system.SystemBrailleTables;
import org.natbraille.core.document.NatDocument;
import org.natbraille.core.document.NatDocumentException;
import org.natbraille.core.filter.NatFilterChain;
import org.natbraille.core.filter.NatFilterException;
import org.natbraille.core.filter.NatFilterConfigurator;
import org.natbraille.core.filter.NatTransformerFilterResolver;
import org.natbraille.core.filter.convertisseur.ConvertisseurXML;
import org.natbraille.core.filter.options.NatFilterOption;
import org.natbraille.core.filter.options.NatFilterOptions;
import org.natbraille.core.filter.presentateur.BrailleTableModifierJava;
import org.natbraille.core.filter.presentateur.PresentateurMEP;
import org.natbraille.core.filter.transcodeur.TranscodeurNormal;
import org.natbraille.core.gestionnaires.AfficheurConsole;
import org.natbraille.core.gestionnaires.AfficheurLogMessageBuffer;
import org.natbraille.core.gestionnaires.GestionnaireErreur;
import org.natbraille.core.gestionnaires.LogLevel;
import org.natbraille.core.gestionnaires.MessageKind;
import org.natbraille.http.Natd;
import org.natbraille.http.dbDocument.NatLibrary;
import org.natbraille.http.ws.formats.in.FoInstantRequest;
import org.natbraille.http.ws.formats.out.FoInstantResponse;

@Path("/private/~{user}/instant")
public class WsInstant {

    @Context
    ServletContext context;
    @Context
    SecurityContext securityContext;
    @PathParam("user")
    String ppUser;

    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public FoInstantResponse instant(FoInstantRequest instantRequest) {

        Natd natd = Natd.getInstance(context);
        NatLibrary natLibrary = natd.getNatLibrary();

        natLibrary.checkUser(securityContext, ppUser);

        FoInstantResponse response = new FoInstantResponse();

        AfficheurLogMessageBuffer afficheur = new AfficheurLogMessageBuffer(LogLevel.SILENT);


        GestionnaireErreur ge = new GestionnaireErreur();

        // error handling
        try {
            System.out.println(instantRequest.getText());

            // document
            NatDocument natDoc = new NatDocument();
            natDoc.setString(instantRequest.getText());
            System.err.println("string is '" + instantRequest.getText() + "'");
            natDoc.forceCharset(Charset.forName("UTF-8"));
            natDoc.forceContentType("text/plain");

            ge.addAfficheur(afficheur);
            ge.addAfficheur(new AfficheurConsole(LogLevel.DEBUG));

            // filters
            NatFilterOptions options = new NatFilterOptions();

            // set debug options
            options.setOption(NatFilterOption.debug_document_show, "true");
            options.setOption(NatFilterOption.debug_document_show_max_char, "300000");
            /*
            options.setOption(NatFilterOption.debug_document_write_options_file, "true");
            options.setOption(NatFilterOption.debug_document_write_temp_file, "true");
            options.setOption(NatFilterOption.debug_dyn_ent_res_show, "true");
            options.setOption(NatFilterOption.debug_dyn_res_show, "true");
            options.setOption(NatFilterOption.debug_dyn_xsl_show, "true");
            options.setOption(NatFilterOption.debug_dyn_xsl_write_temp_file, "true");
            options.setOption(NatFilterOption.debug_xsl_processing, "true");
*/
            // filter options
            options.setOption(NatFilterOption.preserveTags, "true");
            options.setOption(NatFilterOption.FORMAT_OUTPUT_ENCODING, "UTF-8");
            options.setOption(NatFilterOption.FORMAT_OUTPUT_BRAILLE_TABLE, SystemBrailleTables.CBFr1252);
            //options.setOption(NatFilterOption.LAYOUT_PAGE_TOC,"true");
            options.setOption(NatFilterOption.MODE_G2,"true");
            options.setOption(NatFilterOption.MODE_ARRAY_LINEARIZE_ALWAYS,"true");
            options.setOption(NatFilterOption.MODE_ARRAY_2D_MIN_CELL_NUM,"3");

            NatTransformerFilterResolver ntfr = new NatTransformerFilterResolver(options, ge);
            NatFilterConfigurator ff = new NatFilterConfigurator().set(options).set(ntfr).set(ge);

            NatFilterChain chain = ff.newFilterChain();
            
            //chain.addNewFilter(ConvertisseurTexte.class);
            chain.addNewFilter(ConvertisseurXML.class);

            chain.addNewFilter(TranscodeurNormal.class);
            chain.addNewFilter(PresentateurMEP.class);
            chain.addNewFilter(BrailleTableModifierJava.class);

            NatDocument outputNatDoc = chain.run(natDoc);
            response.setText(outputNatDoc.getString());
            //response.setText("caouter");
            //response.setMessages(afficheur.getMessages());
            ge.removeAfficheur(afficheur);
            return response;
        } catch (NatDocumentException | NatFilterException | TransformerException | IOException e) {
            ge.afficheMessage(MessageKind.ERROR, e);
        }
        
        return response;
    }

}
