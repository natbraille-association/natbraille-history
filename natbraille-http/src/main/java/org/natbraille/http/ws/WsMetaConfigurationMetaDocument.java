package org.natbraille.http.ws;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import org.natbraille.http.ws.formats.out.FoNatFilterOptionsDetail;

@Path("/public/metaconfig")
public class WsMetaConfigurationMetaDocument {


	@GET
	@Path("/format/")
	@Produces(MediaType.APPLICATION_JSON)
	public FoNatFilterOptionsDetail get() {
		return new FoNatFilterOptionsDetail();
	}
}