/*
 * NatBraille Http Server - An universal Translator over http
 * Copyright (C) 2013 Vivien Guillet
 * Contact: vivien.guillet@univ-lyon1.fr
 * 
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */
package org.natbraille.http.ws;

import java.util.ArrayList;
import java.util.List;
import javax.servlet.ServletContext;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.SecurityContext;
import org.natbraille.http.Natd;
import org.natbraille.http.dbDocument.DbOutputNatDoc;
import org.natbraille.http.dbDocument.NatLibrary;
import org.natbraille.http.dbDocument.base.DbDoc;
import org.natbraille.http.ws.formats.out.FoOutputNatDocument;
import org.natbraille.http.ws.httpexception.Exception404;

/**
 * Web services of natbraille web server - destination document management
 *
 * @author vivien
 *
 */
@Path("/private/~{user}/resu")
public class WsResuDocument {

    @Context
    ServletContext context;
    @Context
    SecurityContext securityContext;
    @PathParam("user")
    String ppUser;

    /**
     * List the transcription results
     *
     * @return the list
     */
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public ArrayList<FoOutputNatDocument> liste() {

        Natd natd = Natd.getInstance(context);
        NatLibrary natLibrary = natd.getNatLibrary();
        natLibrary.checkUser(securityContext, ppUser);

        ArrayList<FoOutputNatDocument> resu = new ArrayList<>();;
        try {
            DbOutputNatDoc request = new DbOutputNatDoc();
            request.getMetadataDocument().setAuthor(ppUser);
            List<DbDoc> listeDocs = request.getMatching(natLibrary.getDb());
            for (DbDoc doc : listeDocs) {
                resu.add(new FoOutputNatDocument((DbOutputNatDoc) doc));
            }
        } catch (Exception e) {
            throw new Exception404("cannot find '" + ppUser + "' output document");
        }

        return resu;
    }

    /**
     * Delete the transcription result by id.
     *
     * @param destDocumentId
     */
    @Path("/{destDocumentId}/")
    @DELETE
    @Produces(MediaType.APPLICATION_JSON)
    public void deleteId(
            @PathParam("destDocumentId") String destDocumentId) {

        try {
            Natd natd = Natd.getInstance(context);
            NatLibrary natLibrary = natd.getNatLibrary();

            natLibrary.checkUser(securityContext, ppUser);

            // get resu document
            DbOutputNatDoc outDoc = new DbOutputNatDoc();
            outDoc.readId(natLibrary.getDb(), destDocumentId);
            // remove resu
            DbOutputNatDoc outDocModel = new DbOutputNatDoc();
            outDocModel.setId(outDoc.getId());
            outDocModel.remove(natLibrary.getDb());

        } catch (Exception e) {
            throw new Exception404("Cannot delete document " + destDocumentId);
        }
    }

}
