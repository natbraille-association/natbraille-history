/*
 * NatBraille Http Server - An universal Translator over http
 * Copyright (C) 2013 Vivien Guillet
 * Contact: vivien.guillet@univ-lyon1.fr
 * 
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */


package org.natbraille.http.ws;


import javax.servlet.ServletContext;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.SecurityContext;
import org.natbraille.http.ws.formats.out.FoUserDocument;

@Path("/private/user")
public class WsUserTest {

	@Context ServletContext context;
	@Context SecurityContext securityContext;


	@GET
	@Produces(MediaType.APPLICATION_JSON)
	public FoUserDocument testit(){
//		Natd natd = Natd.getFromContext(context);
//		NatLibrary natLibrary = natd.getNatLibrary();		
//		natLibrary.checkUser(securityContext,ppUser);
//		System.err.println("usere principal"+securityContext.getUserPrincipal());
		FoUserDocument resu = new FoUserDocument(securityContext.getUserPrincipal().getName());
		return resu;
	}

}
