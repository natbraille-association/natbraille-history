/*
 * NatBraille Http Server - An universal Translator over http
 * Copyright (C) 2013 Vivien Guillet
 * Contact: vivien.guillet@univ-lyon1.fr
 * 
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */

package org.natbraille.http.ws.formats.out;

import org.natbraille.http.dbDocument.DbTransStatusDoc;

public class FoTranscriptionStatusDocument  {
	

	private String kind;
	private String contents;
	private int logLevel;
	
	public String getKind() {
		return kind;
	}
	public String getContents() {
		return contents;
	}
	public int getLogLevel() {
		return logLevel;
	}
	private void setKind(String kind) {
		this.kind = kind;
	}
	private void setContents(String contents) {
		this.contents = contents;
	}
	private void setLogLevel(int level) {
		this.logLevel = level;
	}
	public FoTranscriptionStatusDocument(DbTransStatusDoc dbts) {
		setKind(dbts.getMessageKind());
		setContents(dbts.getText());
		setLogLevel(dbts.getLogLevel());
	}

}
