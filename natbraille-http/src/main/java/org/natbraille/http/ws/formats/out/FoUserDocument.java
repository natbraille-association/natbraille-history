package org.natbraille.http.ws.formats.out;

/**
 * webservice format description for user
 * <p>
 * Always constructed server-side, must be serializable by a json jersey mapper.
 *
 * @author vivien
 *
 */
public class FoUserDocument {

    String name;
    public String lastMetaConfigId;

    public String getLastMetaConfigId() {
        return lastMetaConfigId;
    }

    public void setLastMetaConfigId(String lastMetaConfigId) {
        this.lastMetaConfigId = lastMetaConfigId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public FoUserDocument(String name) {
        this.name = name;
        this.lastMetaConfigId = "";
        
    }
}
