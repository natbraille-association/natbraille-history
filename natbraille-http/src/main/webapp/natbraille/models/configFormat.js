// {
//     debug_dyn_xsl_write_temp_file: {
// 	comment: "write generated xsl transformation stylesheets to temporary file",
// 	defaultValue: "false",
// 	hidden: "true",
// 	possibleValues: [],
// 	categories: ["Debug"],
// 	type: "Boolean"
//     },
//     MODE_LIT_FR_MAJ_MELANGE: {
// 	comment: "take account of mixed upcase lowcase word",
// 	defaultValue: "true",
// 	hidden: "",
// 	possibleValues: [],
// 	categories: ["Mode", "ModeIntegralMaj"],
// 	type: "Boolean"
//     },
//     MODE_LIT_FR_MAJ_PASSAGE: {
// 	comment: "use special rule if a whole text passage is upcase",
// 	defaultValue: "true",
// 	hidden: "",
// 	possibleValues: [],
// 	categories: ["Mode", "ModeIntegralMaj"],
// 	type: "Boolean"
//     },
//     LAYOUT_PAGE_HEADING_LEVEL: {
// 	comment: "comma separated mapping between input document and braille output heading level in input document heading level order \"1,2,3,4,5,5,5,5,5\"",
// 	defaultValue: "1,2,3,4,5,5,5,5,5",
// 	hidden: "",
// 	possibleValues: [],
// 	categories: ["Layout", "LayoutPageTitles"],
// 	type: "String"
//     },
//     debug_xsl_processing: {
// 	comment: "debug the transcodeur (boolean)",
// 	defaultValue: "false",
// 	hidden: "",
// 	possibleValues: [],
// 	categories: ["Debug"],
// 	type: "Boolean"
//     },
//     CONFIG_DESCRIPTION: {
// 	comment: "configuration description",
// 	defaultValue: "default description",
// 	hidden: "",
// 	possibleValues: [],
// 	categories: ["Infos"],
// 	type: "String"
//     },
//     LAYOUT_LIT_HYPHENATION: {
// 	comment: "activation of literary line breaking",
// 	defaultValue: "true",
// 	hidden: "",
// 	possibleValues: [],
// 	categories: ["Layout", "LayoutBase"],
// 	type: "Boolean"
//     },
//     LAYOUT_PAGE_EMPTY_LINES_MODE: {
// 	comment: "empty line mode. \"0\" means \"as in source\" ; \"1\" means  \"customized\" (see mepminligneN) ; \"2\" means \"no empty lines\" ; \"3\" means,  as in French braille norm, 'aÃ©rÃ©' ; \"4\" means, as in French braille norm,  'compact' ; \"5\" means double line spacing ",
// 	defaultValue: "0",
// 	hidden: "",
// 	possibleValues: ["0", "1", "2", "3", "4", "5"],
// 	categories: ["Layout", "LayoutEmptyLines"],
// 	type: "String"
//     },
//     FORMAT_LINE_LENGTH: {
// 	comment: "Line length of an output braille page",
// 	defaultValue: "40",
// 	hidden: "",
// 	possibleValues: [],
// 	categories: ["Format"],
// 	type: "Number"
//     },
//     MODE_LIT: {
// 	comment: "convert literary expressions",
// 	defaultValue: "true",
// 	hidden: "",
// 	possibleValues: [],
// 	categories: ["Mode", "ModeBase"],
// 	type: "Boolean"
//     },
//     MODE_LIT_FR_MAJ_DOUBLE: {
// 	comment: "use double prefix rule for upcase words",
// 	defaultValue: "true",
// 	hidden: "",
// 	possibleValues: [],
// 	categories: ["Mode", "ModeIntegralMaj"],
// 	type: "Boolean"
//     },
//     MODE_MUSIC: {
// 	comment: "convert musical expressions (not used)",
// 	defaultValue: "false",
// 	hidden: "true",
// 	possibleValues: [],
// 	categories: ["Mode", "ModeBase"],
// 	type: "Boolean"
//     },
//     LAYOUT_PAGE_BREAKS_MIN_LINES: {
// 	comment: "minimum line number for page break",
// 	defaultValue: "5",
// 	hidden: "",
// 	possibleValues: [],
// 	categories: ["Layout", "LayoutPageBreak"],
// 	type: "Number"
//     },
//     sens: {
// 	comment: "",
// 	defaultValue: "false",
// 	hidden: "",
// 	possibleValues: [],
// 	categories: ["Advanced"],
// 	type: "Boolean"
//     },
//     debug_dyn_ent_res_show: {
// 	comment: "show detail of dynamic entities resolution for debug",
// 	defaultValue: "false",
// 	hidden: "",
// 	possibleValues: [],
// 	categories: ["Debug"],
// 	type: "Boolean"
//     },
//     preserveXhtmlTags: {
// 	comment: "",
// 	defaultValue: "false",
// 	hidden: "",
// 	possibleValues: [],
// 	categories: ["Advanced"],
// 	type: "Boolean"
//     },
//     MODE_LIT_EMPH: {
// 	comment: "process word emphasis",
// 	defaultValue: "true",
// 	hidden: "",
// 	possibleValues: [],
// 	categories: ["Mode", "ModeIntegralEmph"],
// 	type: "Boolean"
//     },
//     LAYOUT_PAGE_EMPTY_LINES_CUSTOM_3: {
// 	comment: "number of consecutive empty lines in input outputing three empty braille lines",
// 	defaultValue: "4",
// 	hidden: "",
// 	possibleValues: [],
// 	categories: ["Layout", "LayoutEmptyLines"],
// 	type: "Number"
//     },
//     XSL_FR_HYPH_RULES: {
// 	comment: "hyphenation rules",
// 	defaultValue: "",
// 	hidden: "true",
// 	possibleValues: [],
// 	categories: ["Advanced"],
// 	type: "Url"
//     },
//     LAYOUT_PAGE_EMPTY_LINES_CUSTOM_2: {
// 	comment: "number of consecutive empty lines in input outputing two empty braille lines",
// 	defaultValue: "3",
// 	hidden: "",
// 	possibleValues: [],
// 	categories: ["Layout", "LayoutEmptyLines"],
// 	type: "Number"
//     },
//     XSL_musique: {
// 	comment: "url of Braille music conversion xsl stylesheet",
// 	defaultValue: "nat://system/xsl/musique.xsl",
// 	hidden: "true",
// 	possibleValues: [],
// 	categories: ["Advanced"],
// 	type: "Url"
//     },
//     LAYOUT_PAGE_EMPTY_LINES_CUSTOM_1: {
// 	comment: "number of consecutive empty lines in input outputing one empty braille line",
// 	defaultValue: "2",
// 	hidden: "",
// 	possibleValues: [],
// 	categories: ["Layout", "LayoutEmptyLines"],
// 	type: "Number"
//     },
//     debug_document_show: {
// 	comment: "show generated documents for debug",
// 	defaultValue: "false",
// 	hidden: "",
// 	possibleValues: [],
// 	categories: ["Debug"],
// 	type: "Boolean"
//     },
//     XSL_maths: {
// 	comment: "url of mathematical Braille conversion xsl stylesheet",
// 	defaultValue: "nat://system/xsl/fr-maths.xsl",
// 	hidden: "true",
// 	possibleValues: [],
// 	categories: ["Advanced"],
// 	type: "Url"
//     },
//     LAYOUT_NOLAYOUT_OUTPUT_TAGS: {
// 	comment: "list of string to be added before/after (in that order) : document, paragraph, line, mathematis, lit, music",
// 	defaultValue: "'','','','','','','','','','','',''",
// 	hidden: "",
// 	possibleValues: [],
// 	categories: ["Layout", "LayoutNoLayout"],
// 	type: "String"
//     },
//     LAYOUT_PAGE_TOC: {
// 	comment: "builds an index table",
// 	defaultValue: "false",
// 	hidden: "",
// 	possibleValues: [],
// 	categories: ["Layout", "LayoutPageIndexTable"],
// 	type: "Boolean"
//     },
//     MODE_MATH_SPECIFIC_NOTATION: {
// 	comment: "use specific trigonometry notation",
// 	defaultValue: "false",
// 	hidden: "",
// 	possibleValues: [],
// 	categories: ["Mode", "ModeBase"],
// 	type: "Boolean"
//     },
//     LAYOUT_NOLAYOUT_TR_OUTPUT: {
// 	comment: "replace string to",
// 	defaultValue: "",
// 	hidden: "",
// 	possibleValues: [],
// 	categories: ["Layout", "LayoutNoLayout"],
// 	type: "String"
//     },
//     Stylist: {
// 	comment: "url of the xml layout style document",
// 	defaultValue: "nat://system/xsl/ressources/styles_default.xml",
// 	hidden: "",
// 	possibleValues: [],
// 	categories: ["Format"],
// 	type: "Url"
//     },
//     MODE_G2_HEADING_LEVEL: {
// 	comment: "use grade 2 braille for titles starting for this heading level",
// 	defaultValue: "3",
// 	hidden: "",
// 	possibleValues: [],
// 	categories: ["Mode", "ModeAbrv"],
// 	type: "String"
//     },
//     MODE_G2_IVB_AMB_WORDS: {
// 	comment: "la liste des mots parfois en ivb, sÃ©parÃ©s par des virgules",
// 	defaultValue: "",
// 	hidden: "",
// 	possibleValues: [],
// 	categories: ["Mode", "ModeAbrv"],
// 	type: "String"
//     },
//     MODE_DETRANS_KEEP_HYPHEN: {
// 	comment: "preserve hyphenation found in original braille document",
// 	defaultValue: "false",
// 	hidden: "",
// 	possibleValues: [],
// 	categories: ["Mode", "ModeDetrans"],
// 	type: "Boolean"
//     },
//     FORMAT_OUTPUT_ENCODING: {
// 	comment: "output document encoding",
// 	defaultValue: "UTF-8",
// 	hidden: "",
// 	possibleValues: [],
// 	categories: ["Format"],
// 	type: "String"
//     },
//     MODE_G2: {
// 	comment: "convert literary expressions to grade 2 Braille",
// 	defaultValue: "false",
// 	hidden: "",
// 	possibleValues: [],
// 	categories: ["Mode", "ModeAbrv"],
// 	type: "Boolean"
//     },
//     LAYOUT_NUMBERING_MODE: {
// 	comment: "page numbering position (one of 'bs' 'hs' 'bb' 'hb' 'nn')  letter : b means bottom of the page ; h means top of the page. Second  letter : s means the page number has its own line ; b means the line  number is integrated with the text",
// 	defaultValue: "'bs'",
// 	hidden: "",
// 	possibleValues: ["'bs'", "'hs'", "'bb'", "'hb'", "'nn'"],
// 	categories: ["Layout", "LayoutNumbering"],
// 	type: "String"
//     },
//     XSL_FR_HYPH_DIC: {
// 	comment: "hyphenation dictionnary",
// 	defaultValue: "nat://system/xsl/dicts/hyph_fr_nat.dic",
// 	hidden: "true",
// 	possibleValues: [],
// 	categories: ["Advanced"],
// 	type: "Url"
//     },
//     debug_dyn_xsl_show: {
// 	comment: "show generated xsl stylesheets for debug",
// 	defaultValue: "false",
// 	hidden: "",
// 	possibleValues: [],
// 	categories: ["Debug"],
// 	type: "Boolean"
//     },
//     FORMAT_OUTPUT_BRAILLE_TABLE: {
// 	comment: "output braille table URI",
// 	defaultValue: "brailleUTF8",
// 	hidden: "",
// 	possibleValues: ["brailleUTF8", "brailleUTF8web", "CBFr1252", "CBISF", "CodeUS", "DuxCBfr1252", "DuxTbFr2007", "IBM850", "TbFr2007"],
// 	categories: ["Format"],
// 	type: "String"
//     },
//     MODE_LIT_FR_EMPH_IN_PASSAGE: {
// 	comment: "process emphasis in a passage",
// 	defaultValue: "true",
// 	hidden: "",
// 	possibleValues: [],
// 	categories: ["Mode", "ModeIntegralEmph"],
// 	type: "Boolean"
//     },
//     MODE_G2_IVB_WORDS: {
// 	comment: "la liste des mots en ivb, sÃ©parÃ©s par des virgules ",
// 	defaultValue: "",
// 	hidden: "",
// 	possibleValues: [],
// 	categories: ["Mode", "ModeAbrv"],
// 	type: "String"
//     },
//     CONFIG_NAME: {
// 	comment: "configuration name",
// 	defaultValue: "default name",
// 	hidden: "false",
// 	possibleValues: [],
// 	categories: ["Infos"],
// 	type: "String"
//     },
//     MODE_IMAGES: {
// 	comment: "process images (not used)",
// 	defaultValue: "false",
// 	hidden: "true",
// 	possibleValues: [],
// 	categories: ["Mode", "ModeBase"],
// 	type: "Boolean"
//     },
//     LAYOUT_DIRTY_HYPHENATION: {
// 	comment: "activation of quirky line breaking",
// 	defaultValue: "false",
// 	hidden: "",
// 	possibleValues: [],
// 	categories: ["Layout", "LayoutBase"],
// 	type: "Boolean"
//     },
//     MODE_G2_G1_AMB_WORDS: {
// 	comment: "la liste des mots Ã  conserver parfois en intÃ©gral, sÃ©parÃ©s par des virgules",
// 	defaultValue: "",
// 	hidden: "",
// 	possibleValues: [],
// 	categories: ["Mode", "ModeAbrv"],
// 	type: "String"
//     },
//     debug_dyn_res_show: {
// 	comment: "show detail of dynamic resolution for debug",
// 	defaultValue: "false",
// 	hidden: "",
// 	possibleValues: [],
// 	categories: ["Debug"],
// 	type: "Boolean"
//     },
//     LAYOUT_PAGE_BREAKS_KEEP_ORIGINAL: {
// 	comment: "keep the page breaks of the original document",
// 	defaultValue: "false",
// 	hidden: "",
// 	possibleValues: [],
// 	categories: ["Layout", "LayoutPageBreak"],
// 	type: "Boolean"
//     },
//     LAYOUT: {
// 	comment: "Do final layout",
// 	defaultValue: "true",
// 	hidden: "",
// 	possibleValues: [],
// 	categories: ["Layout", "LayoutBase"],
// 	type: "Boolean"
//     },
//     imageMagickDir: {
// 	comment: "image magick dir for image transcription (not used)",
// 	defaultValue: "",
// 	hidden: "true",
// 	possibleValues: [],
// 	categories: ["Advanced"],
// 	type: "String"
//     },
//     MODE_MATH: {
// 	comment: "convert mathematical expressions",
// 	defaultValue: "true",
// 	hidden: "",
// 	possibleValues: [],
// 	categories: ["Mode", "ModeBase"],
// 	type: "Boolean"
//     },
//     LAYOUT_PAGE_BREAKS: {
// 	comment: "generate page breaks",
// 	defaultValue: "false",
// 	hidden: "",
// 	possibleValues: [],
// 	categories: ["Layout", "LayoutPageBreak"],
// 	type: "Boolean"
//     },
//     debug_document_write_options_file: {
// 	comment: "write transcription options to temporary file",
// 	defaultValue: "false",
// 	hidden: "true",
// 	possibleValues: [],
// 	categories: ["Debug"],
// 	type: "Boolean"
//     },
//     XSL_G2_DICT: {
// 	comment: "grade 2 dictionnary",
// 	defaultValue: "nat://system/xsl/dicts/fr-g2.xml",
// 	hidden: "true",
// 	possibleValues: [],
// 	categories: ["Advanced"],
// 	type: "Url"
//     },
//     MathPrefixAlways: {
// 	comment: "always put mathematical prefix",
// 	defaultValue: "false",
// 	hidden: "",
// 	possibleValues: [],
// 	categories: ["Advanced"],
// 	type: "Boolean"
//     },
//     XSL_chimie: {
// 	comment: "url of Chemical music conversion xsl stylesheet",
// 	defaultValue: "nat://system/xsl/fr-chimie.xsl",
// 	hidden: "true",
// 	possibleValues: [],
// 	categories: ["Advanced"],
// 	type: "Url"
//     },
//     debug_document_show_max_char: {
// 	comment: "size of tail of generated documents display for debug",
// 	defaultValue: "200",
// 	hidden: "",
// 	possibleValues: [],
// 	categories: ["Debug"],
// 	type: "Number"
//     },
//     CONFIG_VERSION: {
// 	comment: "configuration format version",
// 	defaultValue: "5",
// 	hidden: "true",
// 	possibleValues: [],
// 	categories: ["Infos", "Advanced"],
// 	type: "String"
//     },
//     LAYOUT_NOLAYOUT_TR_INPUT: {
// 	comment: "replace string from",
// 	defaultValue: "",
// 	hidden: "",
// 	possibleValues: [],
// 	categories: ["Layout", "LayoutNoLayout"],
// 	type: "String"
//     },
//     MODE_LIT_FR_EMPH_IN_WORD: {
// 	comment: "process emphasis in a word",
// 	defaultValue: "true",
// 	hidden: "",
// 	possibleValues: [],
// 	categories: ["Mode", "ModeIntegralEmph"],
// 	type: "Boolean"
//     },
//     XSL_g1: {
// 	comment: "url of Grade 1 Braille conversion xsl stylesheet",
// 	defaultValue: "nat://system/xsl/fr-g1.xsl",
// 	hidden: "true",
// 	possibleValues: [],
// 	categories: ["Advanced"],
// 	type: "Url"
//     },
//     XSL_g2: {
// 	comment: "url of Grade 2 Braille conversion xsl stylesheet",
// 	defaultValue: "nat://system/xsl/fr-g2.xsl",
// 	hidden: "true",
// 	possibleValues: [],
// 	categories: ["Advanced"],
// 	type: "Url"
//     },
//     LAYOUT_NUMBERING_START_PAGE: {
// 	comment: "layout ; start numbering at page (number)",
// 	defaultValue: "2",
// 	hidden: "",
// 	possibleValues: [],
// 	categories: ["Layout", "LayoutNumbering"],
// 	type: "Number"
//     },
//     IvbMajSeule: {
// 	comment: "always true",
// 	defaultValue: "true",
// 	hidden: "true",
// 	possibleValues: [],
// 	categories: ["Mode", "ModeAbrv"],
// 	type: "Boolean"
//     },
//     MODE_CHEMISTRY: {
// 	comment: "convert chemical expressions",
// 	defaultValue: "true",
// 	hidden: "",
// 	possibleValues: [],
// 	categories: ["Mode", "ModeBase"],
// 	type: "Boolean"
//     },
//     XSL_g2_Rules: {
// 	comment: "rules for grade 2",
// 	defaultValue: "nat://temp/xsl/fr-g2-rules.xsl",
// 	hidden: "true",
// 	possibleValues: [],
// 	categories: ["Advanced"],
// 	type: "Url"
//     },
//     MODE_ARRAY_2D_MIN_CELL_NUM: {
// 	comment: "minimum table cell number for 2D (non linear) table",
// 	defaultValue: "4",
// 	hidden: "",
// 	possibleValues: [],
// 	categories: ["Mode", "ModeTable"],
// 	type: "Number"
//     },
//     preserveTags: {
// 	comment: "keep xml tags",
// 	defaultValue: "false",
// 	hidden: "",
// 	possibleValues: [],
// 	categories: ["Advanced"],
// 	type: "Boolean"
//     },
//     MODE_G2_G1_WORDS: {
// 	comment: "liste des mots Ã  conserver en intÃ©gral, sÃ©parÃ©s par des virgules",
// 	defaultValue: "",
// 	hidden: "",
// 	possibleValues: [],
// 	categories: ["Mode", "ModeAbrv"],
// 	type: "String"
//     },
//     LAYOUT_PAGE_TOC_NAME: {
// 	comment: "index table name",
// 	defaultValue: "Table des MatiÃ¨res",
// 	hidden: "false",
// 	possibleValues: [],
// 	categories: ["Layout", "LayoutPageIndexTable"],
// 	type: "String"
//     },
//     debug_document_write_temp_file: {
// 	comment: "write generated documents to temporary file",
// 	defaultValue: "false",
// 	hidden: "true",
// 	possibleValues: [],
// 	categories: ["Debug"],
// 	type: "Boolean"
//     },
//     MODE_ARRAY_LINEARIZE_ALWAYS: {
// 	comment: "always linearize tables",
// 	defaultValue: "false",
// 	hidden: "",
// 	possibleValues: [],
// 	categories: ["Mode", "ModeTable"],
// 	type: "Boolean"
//     },
//     FORMAT_PAGE_LENGTH: {
// 	comment: "Line number of an output braille page",
// 	defaultValue: "40",
// 	hidden: "",
// 	possibleValues: [],
// 	categories: ["Format"],
// 	type: "Number"
//     }
// }
var ConfigFormat = Backbone.Model.extend({
    urlRoot: function () {
        return NatbrailleConfig.restRoot+'/public/metaconfig/metainfo';
    },

    // returns options names indexed by category
    categories : function(){
	var categories = {};
	_.each(this.attributes, function (formatOption, name) {
	    _.each(formatOption.categories, function (category) {
		if (categories[category] == undefined){
		    categories[category] = [];
		}
		categories[category].push(name);

	    });
	});
 	return categories;
    }
});
