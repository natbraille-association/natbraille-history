// {
//     "metadata":{
// 	"creationDate":"Sun Sep 22 22:42:58 CEST 2013",
// 	"publicUri":null,
// 	"author":"vivien"
//     },
//     "date":"Sun Sep 22 22:42:58 CEST 2013",
//     "displayName":"Berenice brouillon 3-3 070613.odt",
//     "contentType":"application/vnd.oasis.opendocument.text",
//     "charset":"UTF-8",
//     "uri":"~vivien/input documents/523f5652e4b01fc1a20e6b3d",
//     "id":"523f5652e4b01fc1a20e6b3d",
//     "brailleTable":null,
//     "brailleEncoding":null
// }

var Input = Backbone.Model.extend({
    urlRoot :  function() {
	return NatbrailleConfig.restRoot+'/private/~'+Natbraille.userName()+'/source';
    },
    resume : function(){
	return this.get("displayName");
    },
});

var InputColl = Backbone.Collection.extend({
    model : Input,  
    url : function(){
	return NatbrailleConfig.restRoot+'/private/~'+Natbraille.userName()+'/source/';
    },
    
});
