var ResuTextModel = Backbone.Model.extend({
    
    initialize : function(options){
    },

    url : function(){
	var url = "";
	var resu  = this.get('resu');
	if (resu != undefined) {
	    var id = resu.get('id');
	    var displayName = resu.get('displayName');
	    url = NatbrailleConfig.restRoot+'/public/~'+Natbraille.userName()+'/resu/'+id+'/'+displayName;
	} else {
	    url = null;
	}
	return url;

    },
    
    fetch : function(options){
	var that = this;
	options = options || {};
    	$.ajax({
    	    type : "GET",
    	    url : that.url(),
    	    error : function(object,textStatus, jqXHR){
		if (options.error != undefined){
	    	    options.error(object,textStatus, jqXHR);
		}
    	    },
    	    success : function(object,textStatus, jqXHR){
	    	that.set({
		    'text' :jqXHR.responseText,
		    'contentType':jqXHR.getResponseHeader('Content-Type'),
		});
 		if (options.success != undefined){
	    	    options.success(object,textStatus, jqXHR);
		}
    	    }
	});
    },
    
    getDownloadUrl : function(){
	var resu  = this.get('resu');
	if (resu != undefined) {
	    var id = resu.get('id');
	    var displayName = resu.get('displayName');
	    return NatbrailleConfig.restRoot+'/public/~'+Natbraille.userName()+'/resu/'+id+'/attachment/'+displayName;
	} else {
	    return "missing";
	}
    },
	     
});
