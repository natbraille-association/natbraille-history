/*
[
    {
	"metadata": {
	    "creationDate":"Sun Sep 22 18:05:23 CEST 2013",
	    "publicUri":null,
	    "author":"vivien",
	},
	"sourceDocumentId":"522ecfeee4b00f0ea65dd9b1",
	"metaConfigurationId":"51541df0e4b0a51456cb6020",
	"id":"523f1543e4b0aa7a50535684",
	"destDocumentId":null
    }
]
*/

var Transcription = Backbone.Model.extend({

    urlRoot :  function() {
	return NatbrailleConfig.restRoot+'/private/~'+Natbraille.userName()+'/transcription';
    },


    newInterval : function(){
	var time = 1000;
	if (this.retry > 6){
	    time = 3000;
	    if (this.retry > 30){
		time = 10000;
	    }
	}
 	this.refreshIntervalId = setInterval(this.fetchOrStop, time);
    },

    initialize : function(){
	_.bindAll(this, 'fetchOrStop');
	this.retry = 0;
	this.newInterval();
    },
    
    fetchOrStop : function(){
	var that = this;
	var timerId = this.refreshIntervalId;

	clearInterval(that.refreshIntervalId);

 	this.fetch({
	    success : function(){
		that.retry++;
		if ((that.get('state') == 'success')||(that.get('state') == 'error')){
		} else {
		    that.newInterval();
		}
	    }
	});
    },
    
    getLastStep : function(){
	var lastStep = null;
	_(this.get('status')).each(function(item){
	    if (item.kind == 'STEP'){
		lastStep = item.contents;
	    }
	}, this);
	return lastStep;
    }
});

/*
{
   "metaConfigurationId" : "51541df0e4b0a51456cb6020",
   "status" : [
      {
      "kind$poiuy
." : "INFO",
         "logLevel" : 1,
         "contents" : "la transcription va dÃ©buter"
      }
   ],
   "id" : "522ed087e4b00f0ea65dd9b2",
   "metadata" : {
      "creationDate" : "Tue Sep 10 09:55:51 CEST 2013",
      "author" : "vivien"
   },
   "destDocumentId" : null,
   "sourceDocumentId" : "522ecfeee4b00f0ea65dd9b1"
}
*/
	    
var TranscriptionColl =  Backbone.Collection.extend({
    model : Transcription,
    url : function(){
	return NatbrailleConfig.restRoot+'/private/~'+Natbraille.userName()+'/transcription/';
    }
});
