var User = Backbone.Model.extend({
    url : NatbrailleConfig.restRoot+'/private/user',
    defaults: {
        name : ''
    }
});
