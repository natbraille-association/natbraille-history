var ConfigOptionView = Backbone.View.extend({
    templateId : "config_option_template",
    tagName: 'option', // name of tag to be created
  
    initialize: function(){
	_.bindAll(this, 'render', 'unrender');
	this.model.bind('change', this.render);
	this.model.bind('remove', this.unrender);
    },
    render: function(){
	var template_html = $('#'+this.templateId).html();
 	var template = _.template( template_html, this.model.attributes );
	this.$el.html(template);
	this.$el.attr('value',this.model.get('id'));
	return this;
    },

    unrender: function(){
  	$(this.el).remove();
    },   
});
 
var ConfigSelectView =  Backbone.View.extend({
    initialize: function(){
	_.bindAll(this, 'render',  'appendItem'); // every function that uses 'this' as the current object should be in here
	this.model.bind('add', this.appendItem); // collection event binder
	this.render();
    },
    selectedId : function(){
	return 	$('select option', this.el).filter(":selected").attr("value");
    },
    render: function(){
	var self = this;
	this.$el.append("<select></select>");
	_(this.model.models).each(function(item){ // in case collection is not empty
            self.appendItem(item);
	}, this);
    },
    appendItem: function(item){
	var configOptionView = new ConfigOptionView({
            model: item
	});
	$('select', this.el).append(configOptionView.render().el);
    }  
});

