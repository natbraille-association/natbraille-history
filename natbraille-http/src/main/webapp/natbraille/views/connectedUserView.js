var ConnectedUserView = Backbone.View.extend({
    
    templateId : 'connected_user_template',

    initialize : function(){
	this.listenTo(this.model,"change",this.render);
    },
    
    render : function(){
	var template_html = $('#'+this.templateId).html();
	var template = _.template(template_html,this.model.attributes );
	this.$el.html(template);
    }
});

