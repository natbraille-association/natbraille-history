var InputUploadView = Backbone.View.extend({
    templateId : "input_upload_template",
    events : {
	'click input' : 'newUpload'
    },
    initialize : function(){
	this.render();
	this.listenTo(Natbraille.getUser(),'change:name',this.render);
    },
    enableSubmit : function(enable){
	if (enable == false){
	    $('input[type=submit]', this.el).attr('disabled', 'disabled');
	    $('input[type=file]', this.el).attr('disabled', 'disabled');
	} else {
	    $('input[type=submit]', this.el).removeAttr('disabled');
	    $('input[type=file]', this.el).removeAttr('disabled');
	}
    },

    newUpload : function(){
	this.unsetProgress();
	this.unsetFailedUpload();
    },

    endUpload : function(ok){
	this.enableSubmit(true);
	this.unsetProgress();
	if (!ok){
	    this.setFailedUpload();
	} else {
	    $('input[type=file]',this.el).val();
	    this.options.router.navigate("#convert",{trigger:true});
	}
    },

    setProgress : function(percentComplete){
	$('.progress',this.el).text(percentComplete+" % ");
    },
    unsetProgress : function(){
	$('.progress',this.el).text("");
	$('.failed',this.el).text("");
    },
    setFailedUpload : function(){
	$('.failed',this.el).text("transfer failed");
    },
    unsetFailedUpload : function(){
	$('.failed',this.el).text("");
    },

    render: function(){

	var template_html = $('#'+this.templateId).html();
 	var template = _.template( template_html, this.model.attributes );
	this.$el.html(template);

	var that = this;

	$('form', this.el).ajaxForm({
	    url : NatbrailleConfig.restRoot+"/private/~"+Natbraille.userName()+"/source",
	    beforeSubmit : function(){
		that.enableSubmit(false);
	    },

	    uploadProgress : function(event,position,total,percentComplete){
		that.setProgress(percentComplete);
	    },
	    success : function(json,statusText,xhr) {
		if (xhr.status == 200){
		    var newInput = new Input(json);
		    that.model.push(newInput);
		    that.endUpload(true);
		} else {
		    that.endUpload(false);
		}
	    },
	    error : function(what){
		that.endUpload(false);
	    }
	});


	return this;
    },


});
