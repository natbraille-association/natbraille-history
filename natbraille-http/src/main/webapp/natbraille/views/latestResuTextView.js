var LatestResuTextView = Backbone.View.extend({
    templateId : "latest_resu_text_template",
    initialize: function(){
	_.bindAll(this, 'render','changeModel'); // every function that uses 'this' as the current object should be in here
	this.changeModel(this.options.resu);
    },

    
    changeModel : function(resu){

	var resuText = new ResuTextModel({'resu': resu});

	this.stopListening();
	this.options.resu = resu;
	this.resuText = resuText;
	this.listenTo(resuText, "change", this.render);
	this.listenTo(this.model, "add", this.changeModel);

	this.resuText.fetch({});

    },

    render : function(){
	var that = this;

	var pass = this.resuText.attributes;
	var template_html = $('#'+that.templateId).html();
 	var template = _.template( template_html, pass );
	this.$el.html(template);
    },


})