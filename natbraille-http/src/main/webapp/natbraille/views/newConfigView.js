/*
 * view for one option
 */
var NewConfigOptionView = Backbone.View.extend({
    tagName: 'span',
    //    templateId: 'new_config_option_template',

    baseEvents: {
        "click button[name=defaultOption]": "resetDefaults",
	"click button[name=templateOption]": "resetTemplate",
    },

    additionalEvents: {},
    
    events : function() {
	return _.extend({},this.baseEvents,this.additionalEvents);
    },
    listenToCategoriesShow : function(){
	if (this.options.categoriesShow.get('ready')){
	    this.listenTo(this.options.categoriesShow, "change",this.showChange);
	} else {
	    this.listenTo(this.options.categoriesShow, "change:ready", this.listenToCategoriesShow);
	}
    },
    initialize: function () {
        // configuration changes for option name -> set in ui
        this.listenTo(this.model.getConfig(), "change:" + this.options.optionName, this.configChange);
	
	// template configuration changes -> complete rerender
	this.listenTo(this.model, "change:templateConfig", this.templateChange);

	this.listenToCategoriesShow();
    },

    showChange : function(e){
	var common = [];
	var thisCategs = this.model.getFormat().get(this.options.optionName).categories;
	// if (e != undefined){
	//     var changedCategs = _.keys(e.changed);
	//     var common = _.intersection(changedCategs,thisCategs);
	// } else {
	    common = thisCategs;
//	}

	if (common.length>0){
	    var that = this;
	    var show = _.reduce(
		common,
		function(memo, name){ 
		    return (memo || that.options.categoriesShow.get(name)); 
		}, 
		false);
	    if (show){
		this.$el.show();
	    } else {
		this.$el.hide();
	    }
	    
	}
    },
    
    // getters setters
    configValue: function (val) {
        var config = this.model.getConfig();
        if (val == undefined) {
            return config.get(this.options.optionName);
        }  else {
            config.set(this.options.optionName, val);
        }
    },
    defaultValue: function () {
        return this.model.getFormat().get(this.options.optionName).defaultValue;
    },
    templateValue: function () {
	var config = this.model.getTemplateConfig();
        if (config != undefined) {
            return config.get(this.options.optionName);
        }  else {
            return undefined;
        }
    },

    // event handlers
    resetDefaults: function () {
        this.configValue(this.defaultValue());
    },
    resetTemplate: function () {
        this.configValue(this.templateValue());
    },
    
    configChange: function () {

        this.viewValue(this.configValue());
	var hasClass = false;
	if (this.configValue() == this.defaultValue()){
	    this.$el.addClass('config-default-value');
	    hasClass = true;
	} else {
	    this.$el.removeClass('config-default-value');
	}
	if (this.configValue() == this.templateValue()){
	    this.$el.addClass('config-template-value');
	    hasClass = true;
	} else {
	    this.$el.removeClass('config-template-value');
	}

	if (hasClass){
	    this.$el.removeClass('config-changed-value');
	} else {
	    this.$el.addClass('config-changed-value');
	}


    },
    templateChange : function(){
	this.render();
    },
    input: function(){
        this.configValue(this.viewValue());
    },

    // render
    render: function () {
        var template = _.template($('#' + this.templateId).html(), {
            optionName: this.options.optionName,
	    defaultValue : this.defaultValue(),
	    configValue : this.configValue(),
	    templateValue : this.templateValue(),
	    optionFormat : this.model.getFormat().get(this.options.optionName),
        });

        this.$el.html(template);

	var templateCategs = _.template($('#new_config_option_categs_template').html(),{
	    categories : this.model.getFormat().get(this.options.optionName).categories
	});

	this.$el.append(templateCategs);
	this.configChange();
	this.showChange();
        return this.$el;
    },

});

/*
 * view for one text input option
 */
var NewConfigOptionInputView = NewConfigOptionView.extend({
    templateId: 'new_config_option_input_template',
    additionalEvents: {
        "input input": "input",
    },
    viewValue: function(val){
        var inputEl = this.$el.find("input");
        if (val == undefined) {
            return inputEl.val()
        } else {
            inputEl.val(val);
        }
    },
});

/*
 * view for one select/option option
 */
var NewConfigOptionSelectView = NewConfigOptionView.extend({
    templateId: 'new_config_option_select_template',
    additionalEvents: {
        "change select": "input",
    },
    viewValue: function(val){
        if (val == undefined) {
	    return this.$el.find( "select option:selected").val();
        } else {
	    this.$el.find($("select option")).removeAttr("selected");
	    this.$el.find($("select option[value='"+val+"']")).attr("selected",true);
	}
    },
});


/*
 * view for one checbox option
 */
var NewConfigOptionCheckBoxView = NewConfigOptionView.extend({
    templateId: 'new_config_option_checkbox_template',
    additionalEvents: {
        "change input": "input",
    },

    viewValue: function(val){
        

	var inputEl = this.$el.find("input");
        if (val == undefined) {
	    if (inputEl.attr('checked') != undefined){
		return 'true';
	    } else {
		return 'false';
	    }
        } else {
	    if (val == 'true'){
		inputEl.attr('checked','true');
	    } else {
		inputEl.removeAttr('checked');
	    }
	}
    },
});


/*
 * model for bar showing categories and category show/view buttons 
 */
var ConfigCategoriesShow = Backbone.Model.extend({

    initialize : function(){
	this.set('ready',false);
	this.mustRenewFormat = true;
	this.changeFormat();
	this.listenTo(this.get('newConfig'),"change:format",this.formatChanged);
	this.listenTo(this.get('newConfig'),"change:ready",this.changeFormat);
	this.listenTo(this,'change',this.selfChange);

    },
    formatChanged : function(){
	this.mustRenewFormat = true;
    },
    // helper
    categories : function(){
	return this.get('newConfig').getFormat().categories();
    },

    _categInclusions : undefined,
    categInclusions : function(){
	if (this._categInclusions != undefined){
	    return this._categInclusions;
	}
	var categories = this.categories();
	var kCategs  = _.keys(categories);
	var inclusions = {};
	_.each(categories,function(va,ka){
	    var aincludes = [];
	    _.each(categories,function(vb,kb){
		if (ka != kb){
		    if (_.difference(vb,va).length == 0){
			aincludes.push(kb);
		    }
		}
	    });
	    if (aincludes.length>0){
		inclusions[ka] = aincludes;
	    }
	});
	this._categInclusions = inclusions;
	return inclusions;
    },
    _categReverseInclusions : undefined,
    categReverseInclusions : function(){
	if (this._categReverseInclusions != undefined){
	    return this._categReverseInclusions;
	}
	var inclusions = this.categInclusions();
	var reverseInclusions = {};
	_.each(inclusions,function(kIncludeds,kInclude){
	    _.each(kIncludeds,function(kIncluded){
		if (reverseInclusions[kIncluded] == undefined){
		    reverseInclusions[kIncluded] = [];
		}
		reverseInclusions[kIncluded].push(kInclude);
	    },this);
	}, this);

//	this._categReverseInclusions = reverseInclusions;
	this._reverseInclusions = reverseInclusions;
	return reverseInclusions;
    },


    showAll : function(){
	var params = {};
	_.each(_.keys(this.categories()),function(k){
	    params[k] = true;
	});
	this.set(params);
    },
    showNone : function(){
	var params = {};
	_.each(_.keys(this.categories()),function(k){
	    params[k] = false;
	});
	this.set(params);
    },
    selfChange : function(event){
	var categories = this.categories();
	var kChanged = _.keys(event.changed);
	var kCategs  = _.keys(categories);
	var kChangedCategs = _.intersection(kChanged,kCategs);

	var inclusions = this.categInclusions();
	var reverseInclusions = this.categReverseInclusions();

	var setValues = {};
	_.each(kChangedCategs,function(k){
	    if (this.get(k)){
		if (inclusions[k] != undefined){
		    _.each(inclusions[k],function(impliedK){
			setValues[impliedK] = true;
		    },this);
		}
	    } else {
		if (reverseInclusions[k] != undefined){
		    _.each(reverseInclusions[k],function(impliedK){
			setValues[impliedK] = false;
		    },this);
		}
	    }
	},this);
	this.set(setValues);
    },
    toggle : function(name){
	this.set(name,!this.get(name));
    },

    changeFormat : function(){
	var kCategs  = _.keys(this.categories());

	if ((kCategs.length>0)&&(this.mustRenewFormat)){
	    var inclusions = this.categInclusions();
	    _.each(this.categories(),
		   function(optionNames,name){
		       this.set(name,false);
		   },this);
	    this.mustRenewFormat = false;
	    this.set('ready',true);


	} else {

	}
    }
    
});

/*
 * view for bar showing categories and category show/view buttons 
 */
var ConfigCategoryBarView = Backbone.View.extend({
/*

	var configCategoryBarView = new ConfigCategoryBarView({
	    el: $('[name=config_new_categs_bar]', this.el),
	    model: this.model,
	    categories : this.model.getFormat().categories(),
	    categoriesShow : this.categoriesShow,
	    
	});
*/
    tagName : "span",
    templateId : 'new_config_option_categs_bar_template',
    events : {
	"click button" : "click",
	"click button[name=show_all]" : "showAll",
	"click button[name=show_none]" : "showNone",
    },
    showAll : function(){
	this.options.categoriesShow.showAll();
    }, 
    showNone : function(){
	this.options.categoriesShow.showNone();
    },
    initialize : function() {

	if (this.options.categoriesShow.get('ready')){
	    this.categoriesShowReady();
	} else {
	    this.listenTo(this.options.categoriesShow,'change:ready',this.categoriesShowReady);
	}
	
	this.listenTo(this.options.categoriesShow,'change',this.setViewStates);
    },
    categoriesShowReady : function(){
	if (this.options.categoriesShow.get('ready')){
	    this.render();
	}
    },

    setViewState : function(name,value){
	var el = $('button[name="'+name+'"]',this.el);

	var hasSub = (this.options.categoriesShow.categInclusions()[name] != undefined);
	var isSub = (this.options.categoriesShow.categReverseInclusions()[name] != undefined);

	if (hasSub){
	    el.addClass('categ_group');
	}
	if (isSub){
	    el.addClass('categ_sub');
	}
	if (this.options.categoriesShow.get(name)){
	    el.addClass('categ_shown');
	    el.removeClass('categ_not_shown');
	} else {
	    el.addClass('categ_not_shown');
	    el.removeClass('categ_shown');
	}
	
    },
    setViewStates : function(event){
	if  (this.options.categoriesShow.get("ready")){
	    var changedOptions = [];
	    if (event != undefined){
		changedOptions = _.intersection(
		    _.keys(event.changed),
		    _.keys(this.model.getFormat().categories()));
	    } else {
		changedOptions = _.keys(this.model.getFormat().categories());
	    }
	    _.each(changedOptions,function(name){
		var val = this.options.categoriesShow.get(name);
		this.setViewState(name,val);
	    },this);
	} else {
	}
    },

    click : function(e){
	var name = $(e.target).attr('name');
	if (_.contains(_.keys(this.model.getFormat().categories()),name)){
	    this.options.categoriesShow.toggle(name);
	}
    },

    render : function(){
	if (this.options.categoriesShow.get('ready')){

	    var template = _.template($('#'+this.templateId).html(), {
		categories : this.model.getFormat().categories(),
	    });
	    this.$el.html(template);
	    this.setViewStates();

	}
	return this;
	
    },
});


/*
 * general new config view
 */
var NewConfigView = Backbone.View.extend({
    templateId: "new_config_template",
    //   tagName: 'p', // name of tag to be created
    
    events: {
	"click div[name=newconfig_model] button": "setTemplateConfig",
  	"click button[name=save]":"save",
  	"click button[name=hideDefault]":"hideDefault",
  	"click button[name=showDefault]":"showDefault",
    },

    hideDefault : function() {
	$(".config-default-value").hide();
    },
    showDefault : function() {
	$(".config-default-value").show();
    },

    initialize: function () {
	_.bindAll(this, 'render', 'unrender');


	this.model = new NewConfig({});
	this.categoriesShow = new ConfigCategoriesShow({newConfig:this.model});

	this.listenTo(this.model,"change:ready",function(){
	    if (this.model.get('ready')){
		this.render();
	    }
	});

	this.listenTo(this.model,"change:templateConfig",function(){
	});
    },
    
    setTemplateConfig: function () {
	var configId = this.templateConfigSelectView.selectedId();
	var config = this.options.configColl.get(configId);
	var that = this;
	config.fetch({
	    success: function () {
		that.model.setTemplateConfig(config);
	    }
	})
    },

    render: function () {
	//	alert('i do render once');
	var template_html = $('#' + this.templateId).html();

	var hasConfig = (this.model.getConfig() != undefined);
	var hasTemplateConfig = (this.model.getTemplateConfig() != undefined);
	var hasConfigFormat = (this.model.getFormat() != undefined);


	
	var attributes = {
	    config: (hasConfig) ? this.model.getConfig().attributes : undefined,
	    templateConfig: (hasTemplateConfig) ? this.model.getTemplateConfig().attributes : undefined,
	    configFormat: (hasConfigFormat) ? this.model.getFormat().attributes : undefined,
	};
	
	var template = _.template(template_html, attributes);
	this.$el.html(template);

	// config template selector
	this.templateConfigSelectView = new ConfigSelectView({
	    el: $('[name=config_new_template]', this.el),
	    model: this.options.configColl,
	});

	// config category bar
	var configCategoryBarView = new ConfigCategoryBarView({
	    el: $('[name=config_new_categs_bar]', this.el),
	    model: this.model,
	    categoriesShow : this.categoriesShow,
	    
	});

	
	// all options
	var options_el = $('[name=config_new_option_list]',this.el);

	if (hasConfigFormat) {
//	    _.sortBy(_.keys(this.model.getFormat().attributes),function
	    _.each(this.model.getFormat().attributes, function (formatOption, name) {
		
		var formatOption = this.model.getFormat().get(name);
		var patron = {
		    model: this.model,
		    optionName: name,
		    categoriesShow : this.categoriesShow,
		};
		
		var newConfigOptionView;
		if (formatOption.possibleValues.length == 0){
		    if ( formatOption.type == 'Boolean'){
			newConfigOptionView = new NewConfigOptionCheckBoxView(patron);
		    } else {
			newConfigOptionView = new NewConfigOptionInputView(patron);
		    }
		} else {
		    newConfigOptionView = new NewConfigOptionSelectView(patron);
		}
		
		if (newConfigOptionView != undefined){
		    options_el.append(newConfigOptionView.render());
		}

	    }, this);
	}


	return this;

    },
    save : function(){
	var that = this;
	this.model.save({
	    success: function(config){
		console.error('save success',config);
		that.options.configColl.push(config);
	    },
	    error : function(xhr){
		console.error('error',xhr);
		alert('not saved');
	    }
	});
    },
    unrender: function () {
	$(this.el).remove();
    },
});
