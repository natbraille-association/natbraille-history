function get_random_color() {
    var letters = '0123456789ABCDEF'.split('');
    var color = '#';
    for (var i = 0; i < 6; i++ ) {
        color += letters[Math.round(Math.random() * 15)];
    }
    return color;
}

var Stars = new Object({
    init : function(){
	var that = this;
	$(":root").on('mousemove',function(event){that.onmousemouve(event);});
	this.armStep();
	this.armFall();
	this.counter = 0;
	this.maximum = 50;
	this.stopit = false;
	
	this.x = null;
	this.y = null;
    },

    go : function (){
	this.stopit = false;
	var that = this;
//	setTimeout(function(toEvent){that.stop()},4000);
    },

    stop : function(){
	this.stopit = true;
//	$('.star').remove();
    },

    armStep : function(){
	    var that = this;
	    setTimeout(function(toEvent){that.step(toEvent)},1000/30);
    },
    armFall : function(){
	    var that = this;
	    setTimeout(function(toEvent){that.fall()},1000/30);
    },
    
    onmousemouve : function(e){
	this.oldx = this.x;
	this.oldy = this.y;
	this.x = e.pageX;
	this.y = e.pageY;
    },

    fall : function(){
	var that  = this;
	$('.star').each(function () {

	    var top = $(this).css('top').replace(/(px)/,'') * 1.0;
	    var left = $(this).css('left').replace(/(px)/,'') * 1.0;
	    var poids = $(this).attr('poids') * 1.0;

	    var dx = $(this).attr('dx') * 1.0;
	    var dy = $(this).attr('dy') * 1.0;

//	    $(this).css('top',poids*1.0+top*1.0);//Math.random() * 10);
	    var newtop = top + dy/2;
	    var newleft = left + dx/2;
	    var cheight =  $(this).css('height').replace(/px/,'')*1.0;
	    var wbottom = $(window).height() - cheight;


	    if ((newtop+cheight) > (($(window).height()))){
		dy *= -1.0;
		newtop = ($(window).height())-cheight-1;
	    }
	    if (newleft > ($(window).width()-30)){
		dx *= -1.0;
		newleft = $(window).width()-30;
	    }
	    if (newleft <10){
		dx *= -1.0;
		newleft = 10;
	    }



	    dy *= 0.95;
	    dy += poids/3;
	    if (dy> 50){
		dy = 50;
	    }
	    dx = dx*0.95

	    $(this).css('top',newtop);
	    $(this).css('left',newleft);
	    $(this).attr('dx',dx*0.95);
	    $(this).attr('dy',dy);

	    that.oldx = that.x;
	    that.oldy = that.y;

	});

	this.armFall();

    },
    step : function(){
	var x = this.x;
	var y = this.y;
	this.counter %= this.maximum;
	var id = "star_"+this.counter;
	
	var el = $("#"+id);


	    if (!el.hasClass('star')){
		$(':root').append('<p id="'+id+'">*</div>');
		el = $("#"+id);
		el.addClass('star');
		el.css('position','absolute');
	    }
	var poids = 1.0+Math.random()*5;
 	el.css('color',get_random_color());
	
	var fontsize = 2.0+poids*6.0;

	    el.attr('dx',this.x-this.oldx+Math.random()*55.0-27.5);
	    el.attr('dy',this.y-this.oldy+Math.random()*30.0-20);
	    
	  //  el.attr('dx',Math.random()*50.0-25);
	   // el.attr('dy',Math.random()*30.0-20);
	    
	    el.attr('poids',poids);
	    el.css('top',y-fontsize/2);
	    el.css('left',x-fontsize/2);
	    el.css('font-size', fontsize+'px');
	    el.mouseover(function(){
		
		el.attr('dx',Math.random()*50.0-25);
		el.attr('dy',Math.random()*30.0-20);
		
	    });
	if (this.stopit){
	    el.hide();
	} else {
	    el.show();
	}
	this.counter++;
	this.armStep();
    },
});
