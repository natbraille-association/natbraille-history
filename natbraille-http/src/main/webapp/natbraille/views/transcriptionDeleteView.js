var TranscriptionDeleteOptionView = SomethingOptionView.extend({
    templateId : "transcription_option_template",
    templateAttributes : function(){
	var input = this.options.inputColl.get(this.model.get('sourceDocumentId'));
	var conf = this.options.configColl.get(this.model.get('metaConfigurationId'));
	var attributes = {
	    transcription : this.model.attributes,
	    input : (input != undefined)?(input.attributes):null,
	    conf : (conf != undefined)?(conf.attributes):null,
	};
	return attributes;
    }
});

var TranscriptionDeleteView =  SomethingDeleteView.extend({
    templateId : "transcription_delete_template",
    somethingViewProto : TranscriptionDeleteOptionView,
    newOptionViewOptions : function(item) {
	var options = {
            model: item,
	    configColl:this.options.configColl,
	    inputColl:this.options.inputColl,
	};
	return options
    },
});
