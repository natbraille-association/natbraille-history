var TranscriptionSummaryView = Backbone.View.extend({
    templateId : 'transcription_summary_template',
    tagName: 'li', // name of tag to be created
    className : 'transcription_summary',

    events : {
	'click .delete' : 'viremoi',
	'click .showdetail' : 'toggleDetail',
    },

    viremoi : function(){
	this.model.destroy();
    },


    initialize: function(){
	_.bindAll(this, 'render', 'unrender');
	this.model.bind('change', this.render);
	this.model.bind('remove', this.unrender);
	this.showDetail = false;
	this.triedFetchingResu = false;


    },


    stepAsPercent : function() {
	var state = this.model.get('state');
	if ((state == undefined)||(state == null)||(state = '')){
	    return 0;
	}

	//	if ( state == 'pending'){
	var step = this.model.getLastStep();
	var num = 0;
	var den = 100;
	if (step != undefined){
	    step.replace(/(\d+)/,function(v) { 
		num = v;
	    }).replace((/(\d+)/),function(v){
		den = v;
	    });
	}
	var percent  = (100*num/den);
	if (percent>100){
	    percent = 100;
	} else if (percent<0){
	    percent = 0;
	}
	return percent;
    },

    getResuTextView : function(el){
	var resuId = this.model.get('destDocumentId');
	var relResu  = this.options.resuColl.get(resuId);
	if (relResu != null){
	    this.options.resuTextView = new ResuTextView({
		resu : relResu,
		'el' : el,
	    });
	}
	return this.options.resuTextView;
    },

    render: function(){
	this.stepAsPercent();
	// get linked models
	var sourceDocumentId = this.model.get('sourceDocumentId');
	var metaConfigurationId = this.model.get('metaConfigurationId');
	var resuId = this.model.get('destDocumentId');
		
	var relInput  = this.options.inputColl.get(sourceDocumentId);
	var relConfig  = this.options.configColl.get(metaConfigurationId);
	var relResu  = this.options.resuColl.get(resuId);

	var inputAttr = (relInput != undefined)?relInput.attributes:null;
	var configAttr = (relConfig != undefined)?relConfig.attributes:null;
	var resuAttr = (relResu != undefined)?relResu.attributes:null;

	var template_html = $('#'+this.templateId).html();
	var passed = new Object({
	    'transcription' : this.model.attributes,
	    'lastStep' : this.model.getLastStep(),
	    'lastStepPercent' : this.stepAsPercent(),
	    'input' : inputAttr ,
	    'config' : configAttr,
	    'resu' : resuAttr,
	});
	
	// if the resu exists but the model is not in 
	// resu collection, refresh (fetch) resu collection and render
	if ((resuId != null)&&(resuAttr == null)){
	    var that = this;
	    if (!this.triedFetchingResu){
		// try only once
		this.triedFetchingResu = true;
		this.options.resuColl.fetch({
		    success : function(){
			that.render();
		    }
		});
	    }
	}

 	var template = _.template( template_html, passed );
	
	this.$el.html(template);
	var domId = "result_"+this.model.get('id');
	this.$el.attr('id',domId);
	this.$el.addClass(this.model.get('state'));
	this.$el.addClass('state');
	if (! this.showDetail){
	    this.hideDetail();
	}

	var resuTextView = this.getResuTextView($("span",this.el));

	
	return this;
    },

    toggleDetail : function(){
	if (this.showDetail){
	    $('ol',this.el).hide();
	    this.showDetail = false
	} else {
	    $('ol',this.el).show();
	    this.showDetail = true;
	}
    },

    hideDetail : function (){
	this.showDetail = true;
	this.toggleDetail();
	
    },
    unrender: function(){
	$(this.el).remove();
    },   
});

var TranscriptionListView = Backbone.View.extend({
    templateId : 'transcription_list_template',
    tagName : 'ul',
    
    initialize: function(){
	_.bindAll(this, 'render',  'appendItem'); // every function that uses 'this' as the current object should be in here
	this.model.bind('add', this.appendItem); // collection event binder
	this.render();
    },
    render: function(){
	var template_html = $('#'+this.templateId).html();
 	var template = _.template( template_html, this.model.attributes );
	this.$el.html(template);
	
	var self = this;
	_(this.model.models).each(function(item){ // in case collection is not empty
            self.appendItem(item);
	}, this);
    },
    appendItem: function(item){
	var transcriptionSummaryView = new TranscriptionSummaryView({
            model: item,
	    inputColl : this.options.inputColl,
	    configColl : this.options.configColl,
	    resuColl : this.options.resuColl,
	});
	this.$el.append(transcriptionSummaryView.render().el);
    }  
});