var TranscriptionRequestView = Backbone.View.extend({
    templateId : "transcription_request_template",
    
    events : {
	'click  button' : 'uploadOrNewRequest',
//	'change #input_select_option select'  : 'uploadIfNew',
    },
    initialize : function(){
	this.setTranscription(null);


	
	this.render();
    },
   
    enableSubmit : function(enable){
	if (enable == false){
	    $('button', this.el).attr('disabled', 'disabled');
	} else {
	    $('button', this.el).removeAttr('disabled');
	}
    },
    postData : function(){
	var configId = this.options.configView.selectedId();
	var inputId = this.options.inputView.selectedId();
	var data = JSON.stringify({
	    logLevel:"2",
	    metaConfigurationId:configId,
	    sourceDocumentId:inputId,
	});
	return data;
    },

    setTranscription : function(transcription){
	this.options.transcription = transcription;
    },

    getTranscription : function(){
	return this.options.transcription;
    },
/*
   uploadIfNew : function(){ 
	alert('ol');
	if (this.options.inputView.selectedId() == 'new'){
	    this.options.router.navigate("upload",{trigger:true});
	}
   },
*/
    uploadOrNewRequest : function(){
	if (this.options.inputView.selectedId() == 'new'){
	    this.options.router.navigate("upload",{trigger:true});
	} else {
	    this.newRequest();
	}
	    
    },
    newRequest : function(){
	var that = this
	$.ajax({
	    type: "POST",
	    url: NatbrailleConfig.restRoot+"/private/~"+Natbraille.userName()+"/transcription/",
	    data: that.postData(),
	    contentType: 'application/json',
	    dataType: 'json',
	    success : function(json){
		var newTranscription = new Transcription(json);
		that.model.unshift(newTranscription);
		that.setTranscription(newTranscription);
		that.render();
		that.options.router.navigate("#result_"+newTranscription.get('id'),{trigger:true});
	    },
	});
    },

    endUpload : function(ok){
	this.enableSubmit(true);
    },

    render: function(){
	
	var template_html = $('#'+this.templateId).html();
	var values = {
	    transcription : this.getTranscription()
	};
 	var template = _.template( template_html, values );
	this.$el.html(template);
	return this;
    }




});



