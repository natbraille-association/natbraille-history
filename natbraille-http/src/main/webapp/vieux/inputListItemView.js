var InputView = Backbone.View.extend({
    templateId : "input_listitem_template",
    tagName: 'li', // name of tag to be created
  
    initialize: function(){
	_.bindAll(this, 'render', 'unrender');
	this.model.bind('change', this.render);
	this.model.bind('remove', this.unrender);
    },
    render: function(){
	var template_html = $('#'+this.templateId).html();
 	var template = _.template( template_html, this.model.attributes );
	this.$el.html(template);
	return this;
    },

    unrender: function(){
	$(this.el).remove();
    },   
});

var InputCollView =  Backbone.View.extend({
    initialize: function(){
	_.bindAll(this, 'render',  'appendItem'); // every function that uses 'this' as the current object should be in here
	this.model.bind('add', this.appendItem); // collection event binder
	this.render();
    },
    render: function(){
	var self = this;
	this.$el.append("<button id='add'>Add list item</button>");
	this.$el.append("<ul></ul>");
	_(this.model.models).each(function(item){ // in case collection is not empty
            self.appendItem(item);
	}, this);
    },
    appendItem: function(item){
	var inputView = new InputView({
            model: item
	});
	$('ul', this.el).append(inputView.render().el);
    }  
});

