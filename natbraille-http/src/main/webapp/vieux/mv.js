function app(quoi){
    $('#ici').append("<p>"+quoi);   
}
// "add" (model, collection, options) — when a model is added to a collection.
// "remove" (model, collection, options) — when a model is removed from a collection.
// "reset" (collection, options) — when the collection's entire contents have been replaced.
// "sort" (collection, options) — when the collection has been re-sorted.
// "change" (model, options) — when a model's attributes have changed.
// "change:[attribute]" (model, value, options) — when a specific attribute has been updated.
// "destroy" (model, collection, options) — when a model is destroyed.
// "request" (model, xhr, options) — when a model (or collection) has started a request to the server.
// "sync" (model, resp, options) — when a model (or collection) has been successfully synced with the server.
// "error" (model, xhr, options) — when a model's save call fails on the server.
// "invalid" (model, error, options) — when a model's validation fails on the client.
// "route:[name]" (params) — Fired by the router when a specific route is matched.
// "route" (router, route, params) — Fired by history (or router) when any route has been matched.
// "all" — this special event fires for any triggered event, passing the event name as the first argument.

//  model ;

var NatUser = Backbone.Model.extend({

    defaults: {
        name: '',
        password: '',
        connected: false,
	locked: false,
	rememberme : true,
    },
    initialize: function(){
        this.bind("change", function(model){
	});
        this.bind("change:connected", function(model){
	    
	});
        this.bind("change:name", function(model){
	});
	this.bind("change:password", function(model){
	});
    },
    connecte : function(){
	var connectionOk = true;
	this.set({locked:true});

	var co = this;
	setTimeout(function() {
	    co.set({locked:false});
	    if (connectionOk == true){
		co.set({connected:true});
	    } else {
	    }
	},1000);

    },
    deconnecte : function(){
	var deconnectionOk = true;
	this.set({locked:true});
	var co = this;
	setTimeout(function() {
	    co.set({locked:false});
	    if (deconnectionOk == true){
		co.set({connected:false});	
	    } else {
	    }
	},1000);
    },

});


var FormLogin = Backbone.View.extend({

    events: {
	"click button#connecte":"connecte",
    },

    initialize: function(){
	this.condecon();
	this.locked();
	this.listenTo(this.model, "change", this.render);
	this.listenTo(this.model, "change:connected", this.condecon);
	this.listenTo(this.model, "change:locked", this.locked);
        this.render();
    },
    condecon : function(){

	if (this.model.get('connected')){
	    $('#connection').hide();
	} else {
	    $('#connection').show();
	}
    },
    locked : function(){

	if (this.model.get('locked')){
	    $('button#connecte').attr("disabled", "disabled");
	    $('#jetourne').show();
	} else {
	    $('button#connecte').removeAttr("disabled");
	    $('#jetourne').hide();
	}
    },
    render: function(){
    	$('#form_name').val(this.model.get("name"));
    	$('#form_password').val(this.model.get("password"));
	$('#form_rememberme').attr('checked',this.model.get("rememberme"));
    },

    connecte: function( event ){
	//	Button clicked, you can access the element that was clicked with event.currentTarget
	this.model.set({
	    name : $('#form_name').val(),
	    password : $('#form_password').val(),
	    rememberme : $('#form_rememberme').is(':checked'),
	});
	this.model.connecte();
    }
 

});


// deconnecte
var LoggedUser = Backbone.View.extend({
    events : {
	"click button#deconnecte" :  "deconnecte",
    },
    initialize: function(){
	this.condecon();
	this.locked();
	this.listenTo(this.model, "change", this.render);
	this.listenTo(this.model, "change:connected", this.condecon);
	this.listenTo(this.model, "change:locked", this.locked);
        this.render();
    },

    locked : function(){
	if (this.model.get('locked')){
	    $('button#deconnecte').attr("disabled", "disabled");
	} else {
	    $('button#deconnecte').removeAttr("disabled");
	}
    },

    condecon : function(){
	if (this.model.get('connected')){
	    this.$el.show();
	} else {
	    this.$el.hide();
	}
    },

    render: function(){
	$('#display_name').text("nam:"+this.model.get("name"));
     	$('#display_password').text("pwd:"+this.model.get("password"));
	if (this.model.get("rememberme")){
	    $('#display_rememberme').text("rememberme");
	} else {
	    $('#display_rememberme').text("dontrememberme");
	}
     },

    deconnecte: function( event ){
	this.model.deconnecte();
    }
});


var CookieUser = Backbone.View.extend({
    
    cookiename : "natbraille", 
    
    initialize: function(){
	// for direct json manipulation in jquery-cookie
	$.cookie.json = true;
	
	var cookie_name = this.cookiename;
	var foundCookie = $.cookie(cookie_name);

	if ((typeof foundCookie === "undefined")||(foundCookie == null)){
	} else {
	    this.model.set('name',foundCookie.login);
	    this.model.set('password',foundCookie.pwd);
	    this.model.set('rememberme',true);
	}

	this.listenTo(this.model, "change:connected", this.condecon);
	this.model.connecte();
	this.render();

    },

    condecon : function(){
	var cookie_name = "natbraille";
	if (this.model.get('connected') 
	    && this.model.get("rememberme")){
	    $.cookie(cookie_name, { 
		login: this.model.get("name"),
		pwd: this.model.get("password")
	    });
	} else {
	    $.cookie(cookie_name,null);
	}
	this.render();
    },

    render: function(){
	$('#cookie_name').text("cookie_nam:"+this.model.get("name"));
     	$('#cookie_password').text("cookie_pwd:"+this.model.get("password"));
    }
});

//natUser.set({name:"oif",password:"oioio"});

			