
// GET private/~{username}/metaconfig/~{configId}

// request
// -----------

// 	curl -s -u user:password \
// 		http://localhost:8080/natbraille-\
// 		http/rest/private/~username/metaconfig/51541df0e4b0a51456cb601e

// response content
// -----------

// 	{
// 	   "FORMAT_LINE_LENGTH" : "32",
// 	   "LAYOUT_NUMBERING_MODE" : "'nn'",
// 	   "CONFIG_NAME" : "Plage braille 1",
// 	   "CONFIG_DESCRIPTION" : "(codage ANSI : Braille Sense, Iris...), 32 car. par ligne",
// 	   "FORMAT_OUTPUT_ENCODING" : "cp1252",
// 	   "FORMAT_OUTPUT_BRAILLE_TABLE" : "TbFr2007"
// 	}

var Configuration = Backbone.Model.extend({
    urlRoot : function () {
	return '/natbraille-http/rest/private/~vivien/metaconfig';
    },
});


var ConfigurationView = Backbone.View.extend({
    initialize: function(){
	this.listenTo(this.model, "change", this.render);
    },

    render: function(){ 
	//Pass variables in using Underscore.js Template
        var variables = { 
	    config_name : this.model.get('CONFIG_NAME'),
	    config_tout : this.model.attributes.toSource()
	}; 
//	alert($("#config_template").html());
        // Compile the template using underscore
        var template = _.template( $("#config_template").html(), variables );

        // Load the compiled HTML into the Backbone "el"
        this.$el.html( template );
	
    }
});

