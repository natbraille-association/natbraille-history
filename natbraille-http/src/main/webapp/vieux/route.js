function natbrailleroutes(){

    var AppRouter = Backbone.Router.extend({

        routes: {
	    "convert" : "convert",
	    "upload" : "upload",
	    "newconf" : "newconf",
	    "clean" : "clean",
	    "result_:id" : "resultId",
	    "result" : "result",
	    "about" : "about",
            "*actions": "defaultRoute" // matches http://example.com/#anything-here
        },
	// routes: {
	//     "help":                 "help",    // #help
	//     "search/:query":        "search",  // #search/kiwis
	//     "search/:query/p:page": "search"   // #search/kiwis/p7
	// },
	// search: function(query, page) {
	//     alert("search"+query+page);
	// }
	showonly : function(what){
	    $('div#tous div').hide("");
	    $('div#menubar a').removeClass("selectedUrl");
	    $('div#tous div#'+what).show("");
	    $("div#menubar a[href|='#"+what+"']").addClass("selectedUrl");
	},
	upload : function(){
	    this.showonly('upload');
	},
	convert : function(){
	    this.showonly('convert');
	},
	newconf : function(){
	    this.showonly('newconf');
	},
	clean : function(){
	    this.showonly('clean');
	},
	resultId : function(id){
	    this.showonly('result');
	    $('div#tous div#result_'+id).show();
	},
	result : function(){
	    this.showonly('result');
	},
	about : function(){
	    this.showonly('about');
	},
	defaultRoute : function(){
	    this.convert();
	}
    });

    $('#upload button').click(function(){
	app_router.navigate("#convert", {trigger: true});
    });
    $('#clean button').click(function(){
	alert("confirmez la suppression de ...");
    });

    // Initiate the router
    var app_router = new AppRouter;
    $('#convert button').click(function(){
	app_router.navigate("#result", {trigger: true});
    });

    app_router.on('route:defaultRoute', function(actions) {
//	app_router.navigate("#convert", {trigger: true});
	//        alert(" ??? "+actions);
    })

    // Start Backbone history a necessary step for bookmarkable URL's
    Backbone.history.start();
//    app_router.navigate("#convert", {trigger: true});

}