 Introduction to Braille Music Transcription
============================================

IBMT method, by Mary Turner De Garmo

the IBMT method exemple are used as test exemples.
They are extracted from annotated text version of
the method by running the Perl script

    perl ./script/extractExamples.pl

Details
=======

- pdf versions (original) are in pdf/
- pdf version have been converted to text (pdftotext) in txt/
- text version exemples have been annotated using the following two rules :
  - exemple start is preceded by four 'plus signs' on a line start followed by an exemple name starting with a digit, then eol
  - exemple end is followed by  four 'plus signs' on a line start followed by optional spaces, then eol
  - (see "more details")
- exemples are generated in generated.examples/ in codeus (original encoding) and unicode

More details
============

original text
-------------
   
    [...] recognize it easily, for purposes of 
    proofreading, not to reproduce it again in 
    print.

    Example 2-1

    #A HXF GXI HXG FXX FXD EXG FXE DXX EXG
     FXH GHI HXX IXG HXF GFE DXX<K

    4

annotated text
--------------

    [...] recognize it easily, for purposes of 
    proofreading, not to reproduce it again in 
    print.

    Example 2-1
    ++++ 2-1
    #A HXF GXI HXG FXX FXD EXG FXE DXX EXG
    FXH GHI HXX IXG HXF GFE DXX<K
    ++++
    4


