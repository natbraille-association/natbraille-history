#!/usr/bin/perl
#
# run from parent directory
# (no parameters)
#
# eh oui :
my $jar="../../../natbraille-brailletable/target/natbraille-brailletable-1.0a-jar-with-dependencies.jar";

use strict;
use warnings;
use Encode;

my @books = (
    "degarmo-ch01-06.txt",
    "degarmo-ch07-12.txt",
    "degarmo-ch13-16.txt",
    "degarmo-ch17-20.txt",
    "degarmo-ch21-23.txt",
    "degarmo-ch24-28.txt"
    );

my $outRoot = 'generated.examples';

`mkdir -p $outRoot`;


my %weirdut8 = ( 

    
    0x32 => 0x32,   #
    0x0a => 0x0a,   #
    0x1b => 0x283c, # 3456
    0x20 => 0x2800, # -
    0x0d => 0x2801, # 1
    0x13 => 0x2817, # 1235
    0x1a => 0x2837, # 12356
    0x18 => 0x2823, # 126
    0x0f => 0x2805, # 13
    0x11 => 0x280d, # 134
    0x16 => 0x281d, # 1345
    0x1c => 0x283d, # 13456
    0x12 => 0x280e, # 234
    0x17 => 0x281e, # 2345
    0x1d => 0x283e, # 23456
    0x19 => 0x282e, # 2346
#    20   => 0x2811, # 56
    20   => 0x2818, # 45 
    16   => 0x2809, #14

    );


sub weird_to_brailleUTF8 {
    my    $exemple = shift;
    my $lines = $exemple->{'lines'};
    my @resu;
    my $i = 0;
    foreach my $l (@$lines){
	my @newline;
	foreach my $c (split(".",$l)){
	    $c = substr($l, $i, 1);
	    $i++;
	    if (exists ($weirdut8{ord($c)})){
		push @newline, chr($weirdut8{ord($c)});
	    } else {
		die join(" ","in exemple",$exemple->{'ref'},"pos ",$i," no unicode char for ord : ",ord($c)," ");
	    }

	}
	push @resu, join("",@newline);
    }
    return \@resu;
}

sub write_exemple {
    my $exemple = shift;
    my $source = $exemple->{'source'};
    my $ref = $exemple->{'ref'};
    my $encoding = $exemple->{'encoding'}    ;

    my $lines;
    if ($encoding eq 'weird'){
	open A, ">$outRoot/$ref.weird";
	print A join("",@{$exemple->{'lines'}});
	close A;
	$lines = weird_to_brailleUTF8($exemple);
	open A, ">$outRoot/$ref.unicode";
	print A encode('utf8',join("",@$lines));
	print (encode('utf8',join("",$ref," --- ",$encoding,"\n",@$lines)));
	close A;
	my $cmd = join(" ",
		       "java -jar $jar  -f ",
		       "-c UTF-8",
		       "-C UTF-8",
		       "-t brailleUTF8",
		       "-T CodeUS",
		       "-o codeUS",
		       "'$outRoot/$ref.unicode'");
	`$cmd`;
	`mv "$outRoot/$ref.unicode.codeUS" "$outRoot/$ref.codeUS"`

    } else {
	$lines = [map {uc($_)} @{$exemple->{'lines'}}];
	

	open A, ">$outRoot/$ref";
	print A join("",@$lines);
	print join("",$ref," --- ",$encoding,"\n",@$lines);
	close A;

	my $cmd = join(" ",
		       "java -jar $jar  ",
		       "-c ASCII",
		       "-t CodeUS",
		       "-C UTF-8",
		       "-T brailleUTF8",
		       "-o unicode",
		       "'$outRoot/$ref'");
	`$cmd`;
	`mv '$outRoot/$ref' '$outRoot/$ref.codeUS' `;

    }

}

my $state = 'toStart';

for my $file (map {"txt/".$_} @books ) {
    open A, $file;

    my %exemple = ();
    my $linenum = 0;
    foreach my $line (<A>){
	$linenum++;
	if ($line =~ /^\+\+\+\+(\!?)\s*(\d.+?)\s*$/){
	    if ($state eq 'toStart') {
		$exemple{'ref'} = $2;
		$state = 'copy';
		if ($1 eq '!'){
		    $exemple{'encoding'} = 'weird';
		} else {
		    $exemple{'encoding'} = 'codeUS';
		}
	    } else {
		die ("error file '$file', line $linenum : ".
		     "exemple should not start here ('".$state."')");
	    }
	} elsif ($line =~ /^\+\+\+\+\s*$/){
	    if ($state eq 'copy'){
		$exemple{'source'} = $file;
		write_exemple(\%exemple);
		%exemple = ();
		$state = 'toStart';
	    } else {
		die ("error file '$file', line $linenum : ".
		     "exemple should not end here ('".$state."')");
	    }
	} else {
	    if ($state eq 'copy'){
		push @{$exemple{'lines'}}, $line;		
	    }
	}
	
    }
    close A;
}
