rm -f *~
mkdir -p crop

echo '
\header { 
    tagline = "" 
}
' > init.ly

for i in `find . -type f  | grep '\.xml$'` ;
do 

    mscore "$i" -o "$i.pdf" > "$i.pdf.log"
    pdfcrop "$i.pdf" "crop/$i.pdf" &> /dev/null
    mscore "$i" -o "$i.ly" > "$i.ly.log"
    cat init.ly "$i.ly" > "$i.ly.ly" ;
    lilypond `basename "$i.ly.ly"`  &> "$i.ly.pdf.log"
    pdfcrop "$i.ly.pdf" "crop/$i.ly.pdf"  &> /dev/null
    rm "$i.ly.ly"
done


for i in `find . -type f  | grep 'xml$'` ;
do 
    xmlindent -w $i
done

for i in `find . -type f  | grep 'unicode.pdf$'` ;
do
    pdfcrop "$i" "crop/$i" &> /dev/null
done
 
rm -f '*.xml.pdf.log'
rm -f *~
rm init.ly

# lowriter --headless --nologo  --nodefault 2-1.unicode.txt --convert-to pdf 

#java -cp /usr/share/java/pdfbox.jar:./aaaa/commons-logging-1.1.3/commons-logging-1.1.3.jar  org.apache.pdfbox.PDFBox TextToPDF  -standardFont Courier -fontSize 18 a.pdf 2-1.codeUS

#java -cp /usr/share/java/pdfbox.jar:/usr/share/java/commons-logging-1.1.3.jar  org.apache.pdfbox.PDFBox TextToPDF 2.-1.unicode.pdf 2-1.unicode

./book.pl

