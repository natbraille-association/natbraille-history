#!/usr/bin/perl
use strict;
use warnings;

#
# called by ./arrange.pl
#
#


# read lilypond generated pdf
my $readly = 0;
# read mscore generated pdf
my $readms = 1;


my $title="";


my $head = <<EOF
\\documentclass[a4paper,11pt]{article}
\\usepackage{listings}
\\usepackage{graphicx}
\\usepackage[margin=1cm]{geometry}
\\title{$title}
\\author{}

\\begin{document}
\\maketitle
EOF
    ;

my $foot = <<EOF
\\end{document}
EOF
    ;

open A, ">report.tex";

print A $head;

my $vis_head="\\begin{center}\n";
my $vis_foot="\n\\end{center}\n";

    ;

#for my $braille (`ls *.unicode`) {
for my $braille (`ls crop/*pdf`) {


    if ((($braille =~ /ly/)&&($readly == 1))
	||((!($braille =~ /ly/))&&($readms == 1))){
	    
	    if ($braille =~ /(.*)\.pdf/){
		print A "\n\n";
		print A `basename $braille`;
		print A "\n\n";
		print A $vis_head;
		
		print A "\\includegraphics[scale=0.9]{{$1}.pdf}\n";
		print A $vis_foot;
		
		print A "\n\n";

		
	    }
    }
}

print A $foot;
close A;
`pdflatex report.tex`


