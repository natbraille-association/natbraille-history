rm -f ./*~
rm -f ./*.unicode
rm -f ./*.bmxml
rm -f ./*.xml
rm -f ./*.pdf
rm -f ./*.pdf.log
rm -f ./*.ly
rm -f ./*.ly.log
rm -rf ./crop
rm -rf report.aux  report.log  report.tex
rm -rf ./tex