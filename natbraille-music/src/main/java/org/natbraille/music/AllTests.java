/*
 * NatBraille Music 
 * Copyright (C) 2014 Vivien Guillet, Bruno Mascret
 * Contact: vivien.guillet@univ-lyon1.fr
 * 
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */
package org.natbraille.music;

import java.io.File;
import java.io.IOException;
import nu.xom.Node;
import nu.xom.ParsingException;
import org.apache.pdfbox.exceptions.COSVisitorException;
import org.natbraille.music.braillesamples.IBMTExamples;
import org.natbraille.music.mbbm.DirectException;
import org.natbraille.music.mbbm.Root;
import org.natbraille.music.tbbt.Tbbt;
import org.natbraille.pdf.PageConfig;
import org.natbraille.pdf.PdfFormatter;

// TODO regular note grouping
//      irregular note grouping
/**
 *
 * @author vivien
 */
public class AllTests {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) throws ParsingException, IOException, DirectException, COSVisitorException {

        PageConfig conf = new PageConfig();
        PdfFormatter pf = new PdfFormatter(conf);

        //Root.mb_root(new File("bachxml/autres/Reunion.xml"));
        //Root.mb_root(new File("bachxml/043800B_.xml"));
        // String s = Samples.makeBrlMusique5();
        final String outputDir = "resu/";
        
        for (String exampleName : IBMTExamples.getNames()) {

            String brailleExample = IBMTExamples.get(exampleName);

            System.out.println("------ 1. braille -> braillemusicxml \n");
            XmlFuncts.write(brailleExample, new File(outputDir + exampleName));
            {
                System.out.println("------ 1.bis braille -> braille.pdf \n");
                String nameCodeUs = exampleName.replaceAll("unicode", "codeUS");
                String scodeus = IBMTExamples.get(nameCodeUs);
                pf.brailleToPdf(scodeus, outputDir + exampleName + ".pdf");
            }
            //-- 
            String name2 = exampleName + ".bmxml";
            //Node rootNode = Tbbt.tb_movement(s);
            Node rootNode = Tbbt.tb_musicLines(brailleExample, false);       /////******************
            XmlFuncts.write(rootNode, new File(outputDir + name2));
        //System.exit(0);

            //--
            System.out.println("------ 2. text -> braillemuscixml -> braille \n");
            String name3 = name2 + ".unicode";
            String sb = Tbbt.bt_root(rootNode);
            XmlFuncts.write(sb.toString(), new File(outputDir + name3));

            System.out.println("------ 3. text -> braillemuscixml -> *musicxml* \n");
            String name4 = name2 + ".xml";
            Node musicXml = Root.bm_root(rootNode);
            XmlFuncts.write(musicXml, new File(outputDir + name4));

            if (false) {

                System.out.println("------ 4. text -> braillemuscixml -> *musicxml* -> braillemusicxml \n");
                String name5 = name4 + ".bmxml";
                Node rebmxml = Root.mb_root(musicXml);
                XmlFuncts.write(rebmxml, new File(outputDir + name5));

                System.out.println("------ 5. text -> braillemuscixml -> *musicxml* -> braillemusicxml --> braille \n");
                String name6 = name5 + ".unicode";
                String rebraille = Tbbt.bt_root(rebmxml);
                XmlFuncts.write(rebraille, new File(outputDir + name6));
            }
        }

    }

}
