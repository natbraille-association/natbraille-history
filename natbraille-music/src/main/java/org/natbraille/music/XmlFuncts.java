/*
 * NatBraille Music 
 * Copyright (C) 2014 Vivien Guillet, Bruno Mascret
 * Contact: vivien.guillet@univ-lyon1.fr
 * 
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */

package org.natbraille.music;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import nu.xom.Builder;
import nu.xom.Node;
import nu.xom.Nodes;
import nu.xom.ParsingException;
import nu.xom.Serializer;

/**
 *
 * @author vivien
 */
public class XmlFuncts {
    
    public static void write(String text,File f) throws ParsingException, IOException {  
        System.err.println("writing to "+f.getAbsolutePath());
        FileWriter fw = new FileWriter(f);
        PrintWriter pw = new PrintWriter(fw);
        pw.print(text);
        pw.flush();        
    }
    
    public static void write(Node xml,File f) throws ParsingException, IOException {  
        write(xml.toXML(),f);
    }
    
    public static String format(String xml) throws ParsingException, IOException {
        ByteArrayOutputStream out = new ByteArrayOutputStream();
        Serializer serializer = new Serializer(out);
        serializer.setIndent(4);  // or whatever you like
        serializer.write(new Builder().build(xml, ""));
        return out.toString("UTF-8");
    }

    public static Node queryFirst(Node node, String query) {
        Nodes n = node.query(query);
        if (n.size() > 0) {
            return n.get(0);
        }
        return null;
    }

    public static String queryFirstValue(Node node, String query) {
        Node n = queryFirst(node, query);
        if (n != null) {
            return n.getValue();
        }
        return null;
    }
}
