/*
 * NatBraille Music 
 * Copyright (C) 2014 Vivien Guillet, Bruno Mascret
 * Contact: vivien.guillet@univ-lyon1.fr
 * 
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */

package org.natbraille.music.braille;

import java.util.Arrays;

/**
 *
 * @author vivien
 */
public class TSymbols {

    // notes and rests
    public static final String C_HEIGHTH = sfd("145");
    public static final String D_HEIGHTH = sfd("15");
    public static final String E_HEIGHTH = sfd("124");
    public static final String F_HEIGHTH = sfd("1245");
    public static final String G_HEIGHTH = sfd("125");
    public static final String A_HEIGHTH = sfd("24");
    public static final String B_HEIGHTH = sfd("245");
    public static final String REST_HEIGHTH = sfd("1346");

    public static final String C_QUARTER = sfd("1456");
    public static final String D_QUARTER = sfd("156");
    public static final String E_QUARTER = sfd("1246");
    public static final String F_QUARTER = sfd("12456");
    public static final String G_QUARTER = sfd("1256");
    public static final String A_QUARTER = sfd("246");
    public static final String B_QUARTER = sfd("2456");
    public static final String REST_QUARTER = sfd("1236");

    public static final String C_HALF = sfd("1453");
    public static final String D_HALF = sfd("153");
    public static final String E_HALF = sfd("1243");
    public static final String F_HALF = sfd("12453");
    public static final String G_HALF = sfd("1253");
    public static final String A_HALF = sfd("243");
    public static final String B_HALF = sfd("2453");
    public static final String REST_HALF = sfd("136");

    public static final String C_WHOLE = sfd("14536");
    public static final String D_WHOLE = sfd("1536");
    public static final String E_WHOLE = sfd("12436");
    public static final String F_WHOLE = sfd("124536");
    public static final String G_WHOLE = sfd("12536");
    public static final String A_WHOLE = sfd("2436");
    public static final String B_WHOLE = sfd("24536");
    public static final String REST_WHOLE = sfd("134");

    // IMBT 5.
    public static final String C_BREVE_A = sfd("13456","13");
    public static final String D_BREVE_A = sfd("1356","13");
    public static final String E_BREVE_A = sfd("12346","13");
    public static final String F_BREVE_A = sfd("123456","13");
    public static final String G_BREVE_A = sfd("12356","13");
    public static final String A_BREVE_A = sfd("2346","13");
    public static final String B_BREVE_A = sfd("23456","13");
    public static final String REST_BREVE_A = sfd("134","13");

    public static final String C_BREVE_B = sfd("13456","45","14","13456");
    public static final String D_BREVE_B = sfd("1356","45","14","1356");
    public static final String E_BREVE_B = sfd("12346","45","14","12346");
    public static final String F_BREVE_B = sfd("123456","45","14","123456");
    public static final String G_BREVE_B = sfd("12356","45","14","12356");
    public static final String A_BREVE_B = sfd("2346","45","14","2346");
    public static final String B_BREVE_B = sfd("23456","45","14","23456");
    public static final String REST_BREVE_B = sfd("134","45","14","134");

    // desambiguation of note length val
    // IBMT 15.
    public static final String VALUE_SIGN_LARGER = sfd("45","126","2");
    public static final String VALUE_SIGN_SMALLER = sfd("6","126","2");
    
    // note ornements
    public static final String DOT = sfd("3");
    public static final String TIE = sfd("4", "14");

    // measure bars
    public static final String MEASURE_BAR = sfd("");
    public static final String MEASURE_ENDING_DOUBLE_BAR = sfd("126", "13");
    public static final String MEASURE_SECTIONNAL_DOUBLE_BAR = sfd("126", "13", "3");

    // hyphen
    public static final String MUSIC_HYPHEN = sfd("5");

    // repeats
    public static final String REPEAT_FORWARD = sfd("126", "2356");
    public static final String REPEAT_BACKWARD = sfd("126", "23");

    // Octave Keys
    /*
     public static final String BR_OK_UNDER_C1 = sfd("4", "4", "1456");
     public static final String BR_K_C1 = sfd("4", "1456");
     public static final String BR_K_C2 = sfd("45", "1456");
     public static final String BR_K_C3 = sfd("456", "1456");
     public static final String BR_K_C4 = sfd("5", "1456");
     public static final String BR_K_C5 = sfd("46", "1456");
     public static final String BR_K_C6 = sfd("56", "1456");
     public static final String BR_K_C7 = sfd("6", "1456");
     public static final String BR_K_OVER_C7 = sfd("6", "6", "1456");
     */
    public static final String BR_OK_UNDER_C1 = sfd("4", "4");
    public static final String BR_OK_C1 = sfd("4");
    public static final String BR_OK_C2 = sfd("45");
    public static final String BR_OK_C3 = sfd("456");
    public static final String BR_OK_C4 = sfd("5");
    public static final String BR_OK_C5 = sfd("46");
    public static final String BR_OK_C6 = sfd("56");
    public static final String BR_OK_C7 = sfd("6");
    public static final String BR_OK_OVER_C7 = sfd("6", "6");

    // Accidentals
    public static final String ACC_NATURAL = sfd("16");
    public static final String ACC_FLAT = sfd("126");
    public static final String ACC_SHARP = sfd("146");
    public static final String ACC_DOUBLE_FLAT = sfd("126", "126");
    public static final String ACC_DOUBLE_SHARP = sfd("146", "146");

    
    
    // mesure
    // Keys On Black Score
    public static final String BK_K_C1 = sfd("345", "35", "123");
    public static final String BK_K_C2 = sfd("345", "356", "123");
    public static final String BK_K_C3 = sfd("345", "25", "123");
    public static final String BK_K_C4 = sfd("345", "36", "123");
    public static final String BK_K_G1 = sfd("345", "345", "123");
    public static final String BK_K_G2 = sfd("345", "34", "123");
    public static final String BK_K_F3 = sfd("345", "346", "123");
    public static final String BK_K_F4 = sfd("345", "3456", "123");

    // fingering
    public static final String FINGERING_FIRST = sfd("1");
    public static final String FINGERING_SECOND = sfd("12");
    public static final String FINGERING_THIRD = sfd("123");
    public static final String FINGERING_FOURTH = sfd("2");
    public static final String FINGERING_FIFTH = sfd("13");
    //public static final String FINGERING_OTHER = sfd("?"); // how about tchernlobyl ?
    public static final String FINGERING_CHANGE = sfd("14");

    // Piano music
    public static final String PF_ALT_LH = sfd("46", "345");
    public static final String PF_ALT_RH = sfd("456", "345");

    public static final String PF_PEDAL_PUSH = sfd("126", "14");
    public static final String PF_PEDAL_RELEASE = sfd("126", "16");

    // Misc
    public static final String NUMBER_SIGN = sfd("3456");
    public static final String SPACE = sfd("");

    public static final String METRONOME_EQUALS = sfd("2356");

    // Named time signature numbers
    public static final String TIME_SIG_COMMON = sfd("46", "14");
    public static final String TIME_SIG_CUT = sfd("456", "14");
    // Numbers and low time signature numbers

    public static final String NUMBER_UPPER_ZERO = sfd("245");
    public static final String NUMBER_UPPER_ONE = sfd("1");
    public static final String NUMBER_UPPER_TWO = sfd("12");
    public static final String NUMBER_UPPER_THREE = sfd("14");
    public static final String NUMBER_UPPER_FOUR = sfd("145");
    public static final String NUMBER_UPPER_FIVE = sfd("15");
    public static final String NUMBER_UPPER_SIX = sfd("124");
    public static final String NUMBER_UPPER_SEVEN = sfd("1245");
    public static final String NUMBER_UPPER_EIGHT = sfd("125");
    public static final String NUMBER_UPPER_NINE = sfd("24");

    public static final String NUMBER_LOWER_ONE = sfd("2");
    public static final String NUMBER_LOWER_TWO = sfd("23");
    public static final String NUMBER_LOWER_THREE = sfd("25");
    public static final String NUMBER_LOWER_FOUR = sfd("256");
    public static final String NUMBER_LOWER_FIVE = sfd("26");
    public static final String NUMBER_LOWER_SIX = sfd("234");
    public static final String NUMBER_LOWER_SEVEN = sfd("2356");
    public static final String NUMBER_LOWER_EIGHT = sfd("236");
    public static final String NUMBER_LOWER_NINE = sfd("35");

    // 
    public static final String TEMPO_MOOD_START = sfd("6");
    public static final String TEMPO_MOOD_END = sfd("256");

    
    // IBMT 12. Slurs
    public static final String SLUR_SINGLE = sfd("14");
    public static final String SLUR_DOUBLE_OPEN = sfd("14","14");
    public static final String SLUR_DOUBLE_CLOSE = sfd("14");
    public static final String SLUR_BRACKET_OPEN = sfd("56","12");
    public static final String SLUR_BRACKET_CLOSE = sfd("45","23");
    
    
    // 

    /**
     * "12" -> unicode char 0x2803
     * <p>
     * @param num string of black dots numbers
     * @return the braille unicode char
     */
    public static final Character cfd(String num) {
        int matrix[] = {0, 0, 0, 0, 0, 0, 0, 0};
        for (String a : num.split("")) {
            if (!a.isEmpty()) {
                try {
                    Integer u = new Integer(a);
                    matrix[u - 1] = 1;
                } catch (NumberFormatException e) {
                    return null;
                }
            }
        }
        char code = 10240;
        for (int i = 0; i < matrix.length; i++) {
            code += matrix[i] << i;
        }
        Character c = new Character(code);
        return c;
    }

    /**
     * "12","..." -> unicode string 0x2803 0x...
     * <p>
     * @param nums strings of black dots numbers
     * @return the braille unicode char
     */
    public static final String sfd(String... nums) {
        StringBuilder sb = new StringBuilder();
        for (String s : nums) {
            Character c = cfd(s);
            sb.append(c);
        }
        return sb.toString();

    }

    private static char qlReplaces[][] = new char[][]{
        {'a', cfd("1")},
        {'b', cfd("12")},
        {'c', cfd("14")},
        {'d', cfd("145")},
        {'e', cfd("15")},
        {'f', cfd("124")},
        {'g', cfd("1245")},
        {'h', cfd("125")},
        {'i', cfd("24")},
        {'j', cfd("245")},
        //
        {'k', cfd("31")},
        {'l', cfd("312")},
        {'m', cfd("314")},
        {'n', cfd("3145")},
        {'o', cfd("315")},
        {'p', cfd("3124")},
        {'q', cfd("31245")},
        {'r', cfd("3125")},
        {'s', cfd("324")},
        {'t', cfd("3245")},
        //
        {'u', cfd("361")},
        {'v', cfd("3612")},
        {'w', cfd("2456")},
        {'x', cfd("1346")},
        {'y', cfd("13456")},
        {'z', cfd("1356")}

    };

    public static final String quickLit(String noir) {
        String braille = noir;

        for (char[] replace : qlReplaces) {
            braille = braille.replace(replace[0], replace[1]);
        }

        return braille;

    }

    public static final String quickDeLit(String braille) {
        String noir = braille;

        for (char[] replace : qlReplaces) {
            noir = noir.replace(replace[1], replace[0]);
        }

        return noir;

    }

    static public void main(String argv[]) {
        System.err.println(sfd("123", "4567"));
    }

}
