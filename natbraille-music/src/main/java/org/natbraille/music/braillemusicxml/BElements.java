/*
 * NatBraille Music 
 * Copyright (C) 2014 Vivien Guillet, Bruno Mascret
 * Contact: vivien.guillet@univ-lyon1.fr
 * 
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */
package org.natbraille.music.braillemusicxml;

/**
 *
 * @author vivien
 */
public class BElements {

    public static final java.lang.String EQUALS = "EQUALS";
    public static final java.lang.String MEASURE_SECTIONNAL_CONTINUED = "MEASURE_SECTIONNAL_CONTINUED";
    public static final java.lang.String MARGINAL_MEASURE_NUMBER = "MARGINAL_MEASURE_NUMBER";
    public static final java.lang.String FINGER = "FINGER";
    public static final java.lang.String MUSIC_SEGMENT = "MUSIC_SEGMENT";
    public static final java.lang.String FINGERING = "FINGERING";
    public static final java.lang.String MEASURE_LINE_INTERRUPTION = "MEASURE_LINE_INTERRUPTION";
    public static final java.lang.String NOTE = "NOTE";
    public static final java.lang.String BRLNOTE = "BRLNOTE";
    public static final java.lang.String ANY_BUT_SPACE_AND_NEWLINE = "ANY_BUT_SPACE_AND_NEWLINE";
    public static final java.lang.String FINGERING_CHANGE_SYMBOL = "FINGERING_CHANGE_SYMBOL";
    public static final java.lang.String MEASURE = "MEASURE";
    public static final java.lang.String MEASURE_SECTIONNAL_DOUBLE_BAR = "MEASURE_SECTIONNAL_DOUBLE_BAR";
    public static final java.lang.String FULL_MEASURE_RESTS = "FULL_MEASURE_RESTS";
    public static final java.lang.String NUMBERED = "NUMBERED";
    public static final java.lang.String MUSIC_HYPHEN = "MUSIC_HYPHEN";
    public static final java.lang.String RIGHT_PADDING = "RIGHT_PADDING";
    public static final java.lang.String REST = "REST";
    public static final java.lang.String MEASURE_BAR = "MEASURE_BAR";
    public static final java.lang.String KEY_SIG = "KEY_SIG";
    public static final java.lang.String TIME_VALUE = "TIME_VALUE";
    public static final java.lang.String OCTAVE_MARK = "OCTAVE_MARK";
    public static final java.lang.String METRONOME_MARKING = "METRONOME_MARKING";
    public static final java.lang.String NAMED = "NAMED";
    public static final java.lang.String BASE_DURATION = "BASE_DURATION";
    public static final java.lang.String NEW_LINES = "NEW_LINES";
    public static final java.lang.String TEMPO_AND_MOOD_WORD = "TEMPO_AND_MOOD_WORD";
    public static final java.lang.String ALTERATION = "ALTERATION";
    public static final java.lang.String SPACE = "SPACE";
    public static final java.lang.String FRACTION = "FRACTION";
    public static final java.lang.String MELODY_LINE_INTERRUPTION = "MELODY_LINE_INTERRUPTION";
    public static final java.lang.String TIME_SIG = "TIME_SIG";
    public static final java.lang.String DOTS = "DOTS";
    public static final java.lang.String PREFIXED_UPPER_NUMBER = "PREFIXED_UPPER_NUMBER";
    public static final java.lang.String MOVEMENT = "MOVEMENT";
    public static final java.lang.String SPACES = "SPACES";
    public static final java.lang.String METRONOME_MARKING_CIRCA = "METRONOME_MARKING_CIRCA";
    public static final java.lang.String TEMPO_MOOD_END = "TEMPO_MOOD_END";
    public static final java.lang.String NEW_LINE = "NEW_LINE";
    public static final java.lang.String INLINE_LAYOUT = "INLINE_LAYOUT";
    public static final java.lang.String METRONOME_MARKING_AND_SIGNATURE = "METRONOME_MARKING_AND_SIGNATURE";
    public static final java.lang.String ACCIDENTAL = "ACCIDENTAL";
    public static final java.lang.String TEMPO_MOOD_START = "TEMPO_MOOD_START";
    public static final java.lang.String BLOC_LAYOUT = "BLOC_LAYOUT";
    public static final java.lang.String TIE = "TIE";
    public static final java.lang.String HEADING = "HEADING";
    public static final java.lang.String LEFT_PADDING = "LEFT_PADDING";
    public static final java.lang.String FRACTIONS = "FRACTIONS";
    public static final java.lang.String LOWER_NUMBER = "LOWER_NUMBER";
    public static final java.lang.String TEMPO_AND_MOOD = "TEMPO_AND_MOOD";
    public static final java.lang.String FINGER_CHANGE = "FINGER_CHANGE";
    public static final java.lang.String UPPER_NUMBER = "UPPER_NUMBER";
    public static final java.lang.String SIGNATURE_CHANGE = "SIGNATURE_CHANGE";
    public static final java.lang.String DIRECT = "DIRECT";

}
