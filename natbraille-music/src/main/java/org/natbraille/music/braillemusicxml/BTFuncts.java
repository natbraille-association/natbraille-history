/*
 * NatBraille Music 
 * Copyright (C) 2014 Vivien Guillet, Bruno Mascret
 * Contact: vivien.guillet@univ-lyon1.fr
 * 
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */

package org.natbraille.music.braillemusicxml;

import nu.xom.Attribute;
import nu.xom.Element;
import nu.xom.Node;
import nu.xom.Nodes;
import nu.xom.ParentNode;

/**
 *
 * @author vivien
 */
public class BTFuncts {

    public static String getNodePath(Node node){
        StringBuilder sb = new StringBuilder();
        
        if (node instanceof Element){
            sb.insert(0,((Element)node).getLocalName());
        }
        if (node.getParent() != null){
            ParentNode pn = node.getParent();
            int idx = pn.indexOf(node);
            sb.insert(0,"["+idx+"]");
        }
        if (node.getParent() != null){
            sb.insert(0,"/");
            sb.insert(0,getNodePath(node.getParent()));
        }
        
        return sb.toString();
    }
    //TODO
    public static Element getElementForOffset(Node rootNode, int pos) {

        Integer length = 0;

        Nodes symbolNodes = rootNode.query("//*/@symbol");
        for (int nxpIdx = 0; nxpIdx < symbolNodes.size(); nxpIdx++) {
            Node symbol = symbolNodes.get(nxpIdx);
            String symbolValue = symbolNodes.get(nxpIdx).getValue();
            Integer size = symbolValue.length();
            if ((length <= pos) && (pos < (length + size))) {
                return (Element) symbol.getParent();
            }
            length += size;
        }
        return null;
    }

    public static Integer getElementOffset(Node rootNode, Node node) {

        Integer length = 0;

        Nodes elementNodes = rootNode.query("//*");
        for (int nxpIdx = 0; nxpIdx < elementNodes.size(); nxpIdx++) {
            Element element = (Element) elementNodes.get(nxpIdx);
            if (element == node) {
                return length;
            }
            String symbolValue = element.getAttributeValue("symbol");
            if (symbolValue != null) {
                Integer size = symbolValue.length();
                length += size;
            }
        }

        return null;
    }

    public static Integer getNodeLength(Element node) {
        if (node == null){
            return null;
        }
        String symbolValue = node.getAttributeValue("symbol");
        if (symbolValue != null) {
            return symbolValue.length();
        }
        return 0;
    }
    
    public static void main(String args[]) {
        Element root = new Element("root");
        //root.addAttribute(new Attribute("symbol", "aaa"));

        Element s1 = new Element("s1");
        //s1.addAttribute(new Attribute("symbol","Proute "));
        root.appendChild(s1);

        Element s11 = new Element("s11");
        // s11.addAttribute(new Attribute("symbol","Proutes "));
        s1.appendChild(s11);

        Element s12 = new Element("s12");
        s12.addAttribute(new Attribute("symbol", "bbb"));
        s1.appendChild(s12);

        Element s2 = new Element("s2");
        s2.addAttribute(new Attribute("symbol", "ccc"));
        root.appendChild(s2);

        Element s21 = new Element("s21");
        s21.addAttribute(new Attribute("symbol", "ddd"));
        s21.appendChild("zzeze");
        s2.appendChild(s21);

        Element s22 = new Element("s22");
        s22.addAttribute(new Attribute("symbol", "eee"));
        s2.appendChild(s22);

        Element s3 = new Element("s3");
        s3.addAttribute(new Attribute("symbol", "fff"));
        root.appendChild(s3);

        System.err.println(root.toXML());

        for (int i = 0; i < 16; i++) {
            Node n = getElementForOffset(root, i);
            if (n != null) {
                Integer offset = getElementOffset(n, n);
                Integer length = getNodeLength((Element) n);
                System.err.println("off " + i + " " + n.toXML() + " starts at " + offset + ", length " + length);
            } else {
                System.err.println("off " + i + " does not exist in document");
            }
        }
    }

}
