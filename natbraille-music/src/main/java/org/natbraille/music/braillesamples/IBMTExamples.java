/*
 * NatBraille Music 
 * Copyright (C) 2014 Vivien Guillet, Bruno Mascret
 * Contact: vivien.guillet@univ-lyon1.fr
 * 
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */

package org.natbraille.music.braillesamples;

import java.io.InputStream;

/**
 *
 * @author vivien
 */
public class IBMTExamples {

    static String convertStreamToString(InputStream is, String charset) {
        java.util.Scanner s = new java.util.Scanner(is, charset).useDelimiter("\\A");
        return s.hasNext() ? s.next() : "";
    }

    /**
     * get a braille exemple located in this package ressources
     * @param exampleName the name of the braille example
     * @return the braille example as a String
     */
    public static String get(String exampleName) {
        InputStream is;
        is = IBMTExamples.class.getResourceAsStream(exampleName);
        if (is != null) {
            return convertStreamToString(is, "UTF8");
        } else {
            return null;
        }

    }
    
    public static String[] getNames(){
        return new String[]{
            "2-1.unicode",
            "2-2.unicode",
            "2-3.unicode",
            "2-4.unicode",
            "2-5.unicode",
            "2-6.unicode",
            "2-7.unicode",
            "3-1.unicode",
            "3-2.unicode",
            "3-3.unicode",
            "3-4.unicode",
            "3-5.unicode",
            "3-6.unicode",
            "4-1.unicode",
            "4-2.unicode",
            "4-3.unicode",
            
            //            "4-4.unicode", // 44 45 46 no marginal measure number
            //            "4-5.unicode",
            //            "4-6.unicode",
            "4-7.unicode",
            "4-8.unicode",
            
            //
            "5-1.unicode",
            "5-2.unicode",
            "5-3.unicode",
            "5-4.unicode",
            "5-5.unicode",
            "5-6a.unicode",
            "5-6b.unicode",
        
        
        
        };
    }

}
