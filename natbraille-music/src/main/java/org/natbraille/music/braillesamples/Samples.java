/*
 * NatBraille Music 
 * Copyright (C) 2014 Vivien Guillet, Bruno Mascret
 * Contact: vivien.guillet@univ-lyon1.fr
 * 
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */

package org.natbraille.music.braillesamples;

import org.natbraille.music.braille.TSymbols;

/**
 *
 * @author vivien
 */
public class Samples {
    
    public static String makeBrlMusique5() {
        // 7-8
        StringBuilder sb = new StringBuilder();

        //                .append(TSymbols.TIME_SIG_COMMON)
        // tempo and mood
        boolean inline = false;
        boolean header = true;
        if (header) {
            if (!inline) {
                sb.append(TSymbols.SPACE)
                        .append(TSymbols.SPACE)
                        .append(TSymbols.SPACE)
                        .append(TSymbols.SPACE)
                        .append(TSymbols.SPACE)
                        .append(TSymbols.SPACE)
                        .append(TSymbols.TEMPO_MOOD_START)
                        .append(TSymbols.quickLit("ben marcato"))
                        .append("\n")
                        .append(TSymbols.SPACE)
                        .append(TSymbols.SPACE)
                        .append(TSymbols.SPACE)
                        .append(TSymbols.SPACE)
                        .append(TSymbols.quickLit("con super trouzaille"))
                        .append(TSymbols.TEMPO_MOOD_END)
                        .append(TSymbols.SPACE)
                        .append(TSymbols.SPACE)
                        .append("\n")
                        //                  .append(" ")

                        //      .append("\n")                

                        .append(TSymbols.SPACE)
                        .append(TSymbols.SPACE)
                        .append(TSymbols.quickLit("circa."))
                        .append(TSymbols.SPACE)
                        .append(TSymbols.C_QUARTER)
                        .append(TSymbols.METRONOME_EQUALS)
                        //.append(TSymbols.NUMBER_SIGN)
                        .append(TSymbols.C_QUARTER)
                        .append(TSymbols.DOT)
                        //                .append(TSymbols.NUMBER_UPPER_ONE)
                        //                .append(TSymbols.NUMBER_UPPER_THREE)
                        //                .append(TSymbols.NUMBER_UPPER_TWO)
                        //                
                        //
                        .append(TSymbols.SPACE)
                        .append(TSymbols.NUMBER_SIGN)
                        .append(TSymbols.NUMBER_UPPER_FOUR)
                        .append(TSymbols.ACC_SHARP)
                        .append(TSymbols.NUMBER_SIGN)
                        .append(TSymbols.NUMBER_UPPER_FOUR)
                        .append(TSymbols.NUMBER_UPPER_FOUR)
                        .append(TSymbols.NUMBER_SIGN)
                        .append(TSymbols.NUMBER_UPPER_THREE)
                        .append(TSymbols.NUMBER_UPPER_FOUR);
                //                
            } else {

                sb.append(TSymbols.SPACE)
                        .append(TSymbols.SPACE)
                        .append(TSymbols.TEMPO_MOOD_START)
                        .append(TSymbols.quickLit("ben marcato"))
                        .append(TSymbols.TEMPO_MOOD_END)
                        .append(TSymbols.SPACE)
                        .append(TSymbols.C_QUARTER)
                        .append(TSymbols.METRONOME_EQUALS)
                        .append(TSymbols.C_QUARTER)
                        .append(TSymbols.DOT)
                        .append(TSymbols.SPACE)
                        .append(TSymbols.NUMBER_SIGN)
                        .append(TSymbols.NUMBER_UPPER_FOUR)
                        .append(TSymbols.ACC_SHARP)
                        .append(TSymbols.NUMBER_SIGN)
                        .append(TSymbols.NUMBER_UPPER_FOUR)
                        .append(TSymbols.NUMBER_UPPER_FOUR)
                        .append(TSymbols.NUMBER_SIGN)
                        .append(TSymbols.NUMBER_UPPER_THREE)
                        .append(TSymbols.NUMBER_UPPER_FOUR);
                //                

            }

            sb.append("\n");
        }
        sb
                .append(TSymbols.NUMBER_SIGN)
                .append(TSymbols.sfd("1"))
                .append(TSymbols.SPACE)
                //
                .append(TSymbols.BR_OK_C3)
                .append(TSymbols.C_HALF)
                .append(TSymbols.C_HALF)
                .append(TSymbols.MEASURE_BAR)
                .append(TSymbols.BR_OK_C2)
                .append(TSymbols.G_QUARTER)
                .append(TSymbols.DOT)
                .append(TSymbols.FINGERING_FIFTH)
                .append(TSymbols.FINGERING_FIFTH)
                .append(TSymbols.FINGERING_CHANGE)
                .append(TSymbols.FINGERING_FIRST)
                .append(TSymbols.BR_OK_C3)
                .append(TSymbols.E_QUARTER)
                .append(TSymbols.MEASURE_BAR)
                .append(TSymbols.MEASURE_BAR)
                .append(TSymbols.MEASURE_BAR)
                .append("\n  ")
                .append(TSymbols.ACC_FLAT)                
                .append(TSymbols.BR_OK_C3)
                .append(TSymbols.E_QUARTER)
                .append("\n  ")
                .append(TSymbols.ACC_NATURAL)                
                .append(TSymbols.E_QUARTER)
                .append(TSymbols.ACC_SHARP)                
                .append(TSymbols.E_QUARTER)
                .append(TSymbols.ACC_FLAT)                
                .append(TSymbols.E_QUARTER)
                .append(TSymbols.MEASURE_BAR)
                .append(TSymbols.E_QUARTER)
                .append(TSymbols.ACC_FLAT)
                .append(TSymbols.F_QUARTER)
                .append(TSymbols.MUSIC_HYPHEN)
                .append("\n  ")
                .append(TSymbols.BR_OK_C3)
                .append(TSymbols.E_QUARTER)
                .append(TSymbols.MEASURE_BAR)
                .append(TSymbols.E_QUARTER)
                .append(TSymbols.MEASURE_SECTIONNAL_DOUBLE_BAR)
                .append(TSymbols.MUSIC_HYPHEN)
                .append(" ")
                .append(TSymbols.BR_OK_C3)
                .append(TSymbols.E_QUARTER)
                .append(TSymbols.MEASURE_SECTIONNAL_DOUBLE_BAR)
                .append(TSymbols.MUSIC_HYPHEN)
                .append(" ")
                .append(TSymbols.NUMBER_SIGN)
                .append(TSymbols.NUMBER_UPPER_FOUR)
                .append(TSymbols.ACC_SHARP)
                .append(TSymbols.NUMBER_SIGN)
                .append(TSymbols.NUMBER_UPPER_TWO)
                .append(TSymbols.NUMBER_LOWER_FOUR)
                .append(TSymbols.NUMBER_SIGN)
                .append(TSymbols.NUMBER_UPPER_TWO)
                .append(TSymbols.NUMBER_LOWER_FOUR)
                .append(" ")
                .append(TSymbols.E_QUARTER)
                .append(TSymbols.E_QUARTER)
                .append(TSymbols.E_QUARTER)
                .append(TSymbols.MEASURE_BAR)
                .append(TSymbols.E_QUARTER)
                .append(TSymbols.E_QUARTER)
                .append(TSymbols.E_QUARTER)
                .append(" ")
                .append(TSymbols.E_QUARTER)
                .append(TSymbols.E_QUARTER)
                .append(" ")
                .append(TSymbols.E_QUARTER)
                .append(" ") //                .append(TSymbols.E_QUARTER)
                //                .append(TSymbols.NUMBER_SIGN)
                //                .append(TSymbols.NUMBER_UPPER_FOUR)
                //                .append(TSymbols.ACC_NATURAL)
                //              
                ;
        return sb.toString();
    }

    public static String makeBrlMusique4() {
        // 7-8
        StringBuilder sb = new StringBuilder();

        sb.append(TSymbols.NUMBER_SIGN)
                .append(TSymbols.sfd("1"))
                .append(TSymbols.SPACE)
                .append(TSymbols.BR_OK_C3)
                .append(TSymbols.C_HALF)
                .append(TSymbols.BR_OK_C2)
                .append(TSymbols.G_QUARTER)
                .append(TSymbols.BR_OK_C3)
                .append(TSymbols.E_QUARTER)
                .append(TSymbols.MEASURE_BAR)
                .append("");
        //1

        return sb.toString();
    }

    public static String makeBrlMusique3() {
        // 6-3
        StringBuilder sb = new StringBuilder();

        sb.append(TSymbols.NUMBER_SIGN)
                .append(TSymbols.sfd("1"))
                .append(TSymbols.SPACE) // 1
                .append(TSymbols.C_HALF)
                .append(TSymbols.ACC_FLAT)
                .append(TSymbols.B_HALF)
                .append(TSymbols.TIE)
                .append(TSymbols.MEASURE_BAR)
                .append(TSymbols.B_QUARTER) // Bb
                .append(TSymbols.C_QUARTER)
                .append(TSymbols.A_QUARTER)
                .append(TSymbols.F_QUARTER);
        return sb.toString();
    }

    public static String makeBrlMusique2() {
        // 4-7
        StringBuilder sb = new StringBuilder();

        sb
                .append(TSymbols.NUMBER_SIGN)
                .append(TSymbols.sfd("1"))
                .append(TSymbols.SPACE)
                // 1
                .append(TSymbols.G_HALF)
                .append(TSymbols.DOT)
                .append(TSymbols.MEASURE_BAR)
                //2
                .append(TSymbols.E_HALF)
                .append(TSymbols.DOT)
                .append(TSymbols.MEASURE_BAR)
                //3
                .append(TSymbols.C_HALF)
                .append(TSymbols.DOT)
                .append(TSymbols.MEASURE_BAR)
                //4
                .append(TSymbols.E_HALF)
                .append(TSymbols.DOT)
                .append(TSymbols.MEASURE_BAR)
                // 5
                .append(TSymbols.C_QUARTER)
                .append(TSymbols.D_QUARTER)
                .append(TSymbols.E_QUARTER)
                .append(TSymbols.MEASURE_BAR)
                // 6
                .append(TSymbols.G_QUARTER)
                .append(TSymbols.F_QUARTER)
                .append(TSymbols.E_QUARTER)
                .append(TSymbols.MEASURE_BAR)
                // 7
                .append(TSymbols.D_HALF)
                .append(TSymbols.DOT)
                .append(TSymbols.TIE)
                .append(TSymbols.MEASURE_BAR)
                // 7
                .append(TSymbols.D_HALF)
                .append(TSymbols.DOT)
                .append(TSymbols.MEASURE_ENDING_DOUBLE_BAR);
        ;
        return sb.toString();

    }

    public static String makeBrlMusique() {
        StringBuilder sb = new StringBuilder();

        sb.append(TSymbols.NUMBER_SIGN)
                .append(TSymbols.sfd("1"))
                .append(TSymbols.SPACE)
                // 1
                .append(TSymbols.G_HEIGHTH)
                .append(TSymbols.REST_HEIGHTH)
                .append(TSymbols.E_HEIGHTH)
                .append(TSymbols.MEASURE_BAR)
                //2
                .append(TSymbols.F_HEIGHTH)
                .append(TSymbols.REST_HEIGHTH)
                .append(TSymbols.A_HEIGHTH)
                .append(TSymbols.MEASURE_BAR)
                //3
                .append(TSymbols.G_HEIGHTH)
                .append(TSymbols.REST_HEIGHTH)
                .append(TSymbols.F_HEIGHTH)
                .append(TSymbols.MEASURE_BAR)
                //4
                .append(TSymbols.E_HEIGHTH)
                .append(TSymbols.REST_HEIGHTH)
                .append(TSymbols.REST_HEIGHTH)
                .append(TSymbols.MEASURE_BAR)
                //
                // 5
                .append(TSymbols.E_HEIGHTH)
                .append(TSymbols.REST_HEIGHTH)
                .append(TSymbols.C_HEIGHTH)
                .append(TSymbols.MEASURE_BAR)
                // 6
                .append(TSymbols.D_HEIGHTH)
                .append(TSymbols.REST_HEIGHTH)
                .append(TSymbols.F_HEIGHTH)
                .append(TSymbols.MEASURE_BAR)
                // 7
                .append(TSymbols.E_HEIGHTH)
                .append(TSymbols.REST_HEIGHTH)
                .append(TSymbols.D_HEIGHTH)
                .append(TSymbols.MEASURE_BAR)
                // 8
                .append(TSymbols.C_HEIGHTH)
                .append(TSymbols.REST_HEIGHTH)
                .append(TSymbols.REST_HEIGHTH)
                .append(TSymbols.MEASURE_BAR)
                // 9
                .append(TSymbols.D_HEIGHTH)
                .append(TSymbols.REST_HEIGHTH)
                .append(TSymbols.F_HEIGHTH)
                .append(TSymbols.MEASURE_BAR)
                //10
                .append(TSymbols.E_HEIGHTH)
                .append(TSymbols.REST_HEIGHTH)
                .append(TSymbols.G_HEIGHTH)
                .append(TSymbols.MEASURE_BAR)
                //11
                .append(TSymbols.F_HEIGHTH)
                .append(TSymbols.G_HEIGHTH)
                .append(TSymbols.A_HEIGHTH)
                .append(TSymbols.MEASURE_BAR)
                //12
                .append(TSymbols.G_HEIGHTH)
                .append(TSymbols.REST_HEIGHTH)
                .append(TSymbols.REST_HEIGHTH)
                .append(TSymbols.MEASURE_BAR)
                //
                // 13
                .append(TSymbols.A_HEIGHTH)
                .append(TSymbols.REST_HEIGHTH)
                .append(TSymbols.F_HEIGHTH)
                .append(TSymbols.MEASURE_BAR)
                // 14
                .append(TSymbols.G_HEIGHTH)
                .append(TSymbols.REST_HEIGHTH)
                .append(TSymbols.E_HEIGHTH)
                .append(TSymbols.MEASURE_BAR)
                // 15
                .append(TSymbols.F_HEIGHTH)
                .append(TSymbols.E_HEIGHTH)
                .append(TSymbols.D_HEIGHTH)
                .append(TSymbols.MEASURE_BAR)
                // 16
                .append(TSymbols.C_HEIGHTH)
                .append(TSymbols.REST_HEIGHTH)
                .append(TSymbols.REST_HEIGHTH)
                .append(TSymbols.MEASURE_ENDING_DOUBLE_BAR);
        ;

        return sb.toString();

    }
}
