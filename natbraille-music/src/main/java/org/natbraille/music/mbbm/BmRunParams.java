/*
 * NatBraille Music 
 * Copyright (C) 2014 Vivien Guillet, Bruno Mascret
 * Contact: vivien.guillet@univ-lyon1.fr
 * 
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */

package org.natbraille.music.mbbm;

import nu.xom.Element;

/**
 *
 * @author vivien
 */
public class BmRunParams {

    private Element pitch = null;
    private Integer measureNumber = null;
    private Element direction = null;
    private Element keySignature = null;
    private Element TimeSignature = null;
    private Element lastMeasure = null;

    public Element getLastMeasure() {
        return lastMeasure;
    }

    public void setLastMeasure(Element lastMeasure) {
        this.lastMeasure = lastMeasure;
    }
    
    
    
    public Element getTimeSignature() {
        return TimeSignature;
    }

    public void setTimeSignature(Element TimeSignature) {
        this.TimeSignature = TimeSignature;
    }

    public Element getKeySignature() {
        return keySignature;
    }

    public void setKeySignature(Element keySignature) {
        this.keySignature = keySignature;
    }

    public Element getDirection() {
        if (direction == null) {
            return null;
        }
        Element resu = (Element) direction.copy();
        return resu;
    }

    public void resetDirection() {
        this.direction = null;
    }
//        

    public void setDirection(Element direction) {
        this.direction = direction;
    }
//        
//

    public Element getPitch() {
        if (pitch == null) {
            Element defaultPitch = new Element("pitch");
            Element octave = new Element("octave");
            octave.appendChild("4");
            defaultPitch.appendChild(octave);
            Element step = new Element("step");
            step.appendChild("C");
            defaultPitch.appendChild(step);
            pitch = defaultPitch;
        }
        return (Element) pitch.copy();
    }

    public void setPitch(Element pitch) {
        this.pitch = pitch;
    }

    public Integer getMeasureNumber() {
        if (measureNumber == null) {
            measureNumber = 1;
        }
        return measureNumber;
    }

    public void setMeasureNumber(Integer measureNumber) {
        this.measureNumber = measureNumber;
    }
}


