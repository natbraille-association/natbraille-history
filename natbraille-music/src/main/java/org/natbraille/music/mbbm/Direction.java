/*
 * NatBraille Music 
 * Copyright (C) 2014 Vivien Guillet, Bruno Mascret
 * Contact: vivien.guillet@univ-lyon1.fr
 * 
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */

package org.natbraille.music.mbbm;

import nu.xom.Attribute;
import nu.xom.Element;
import nu.xom.Node;
import nu.xom.Nodes;
import org.natbraille.music.braille.TSymbols;

/**
 *
 * @author vivien
 */
public class Direction {
    /* TODO : tempo
     <direction directive="yes" placement="above">
     <direction-type>
     <words default-y="15" font-weight="bold">Andantino</words>
     </direction-type>
     <sound tempo="60"/>
     </direction>
     */

    public static Element bm_direction(Node bmxml) {
        Element direction = new Element("direction");
        direction.addAttribute(new Attribute("placement", "above"));
        Element directionType = new Element("direction-type");
        Element words = new Element("words");
        Nodes bDirection = bmxml.query("//TEMPO_AND_MOOD_WORD");
        for (int i = 0; i < bDirection.size(); i++) {
            Element d = (Element) bDirection.get(i);
            words.appendChild(TSymbols.quickDeLit(d.getAttribute("symbol").getValue()));
            if (i < (bDirection.size() - 1)) {
                words.appendChild(" ");
            }
        }
        directionType.appendChild(words);
        direction.appendChild(directionType);
        return direction;
    }

}
