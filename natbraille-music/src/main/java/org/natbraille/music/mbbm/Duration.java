/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.natbraille.music.mbbm;

import nu.xom.Element;
import org.natbraille.music.braille.TSymbols;
import org.natbraille.music.braillemusicxml.BElements;

/**
 *
 * @author vivien
 */
public class Duration {

    public static Integer bm_symbolicDuration(Element brlNoteOrRest) throws DirectException {
        String brlSymbol = brlNoteOrRest.getAttributeValue("symbol");
        if (brlSymbol.equals(TSymbols.REST_HEIGHTH)) {
            return 16;
        } else if (brlSymbol.equals(TSymbols.C_HEIGHTH)) {
            return 16;
        } else if (brlSymbol.equals(TSymbols.D_HEIGHTH)) {
            return 16;
        } else if (brlSymbol.equals(TSymbols.E_HEIGHTH)) {
            return 16;
        } else if (brlSymbol.equals(TSymbols.F_HEIGHTH)) {
            return 16;
        } else if (brlSymbol.equals(TSymbols.G_HEIGHTH)) {
            return 16;
        } else if (brlSymbol.equals(TSymbols.A_HEIGHTH)) {
            return 16;
        } else if (brlSymbol.equals(TSymbols.B_HEIGHTH)) {
            return 16;
            //
        } else if (brlSymbol.equals(TSymbols.REST_QUARTER)) {
            return 8;
        } else if (brlSymbol.equals(TSymbols.C_QUARTER)) {
            return 8;
        } else if (brlSymbol.equals(TSymbols.D_QUARTER)) {
            return 8;
        } else if (brlSymbol.equals(TSymbols.E_QUARTER)) {
            return 8;
        } else if (brlSymbol.equals(TSymbols.F_QUARTER)) {
            return 8;
        } else if (brlSymbol.equals(TSymbols.G_QUARTER)) {
            return 8;
        } else if (brlSymbol.equals(TSymbols.A_QUARTER)) {
            return 8;
        } else if (brlSymbol.equals(TSymbols.B_QUARTER)) {
            return 8;
            //
        } else if (brlSymbol.equals(TSymbols.REST_HALF)) {
            return 4;
        } else if (brlSymbol.equals(TSymbols.C_HALF)) {
            return 4;
        } else if (brlSymbol.equals(TSymbols.D_HALF)) {
            return 4;
        } else if (brlSymbol.equals(TSymbols.E_HALF)) {
            return 4;
        } else if (brlSymbol.equals(TSymbols.F_HALF)) {
            return 4;
        } else if (brlSymbol.equals(TSymbols.G_HALF)) {
            return 4;
        } else if (brlSymbol.equals(TSymbols.A_HALF)) {
            return 4;
        } else if (brlSymbol.equals(TSymbols.B_HALF)) {
            return 4;

        } //
        else if (brlSymbol.equals(TSymbols.REST_WHOLE)) {
            return 2;
        } else if (brlSymbol.equals(TSymbols.C_WHOLE)) {
            return 2;
        } else if (brlSymbol.equals(TSymbols.D_WHOLE)) {
            return 2;
        } else if (brlSymbol.equals(TSymbols.E_WHOLE)) {
            return 2;
        } else if (brlSymbol.equals(TSymbols.F_WHOLE)) {
            return 2;
        } else if (brlSymbol.equals(TSymbols.G_WHOLE)) {
            return 2;
        } else if (brlSymbol.equals(TSymbols.A_WHOLE)) {
            return 2;
        } else if (brlSymbol.equals(TSymbols.B_WHOLE)) {
            return 2;
        } //
        else if (brlSymbol.equals(TSymbols.REST_BREVE_A)) {
            return 1;
        } else if (brlSymbol.equals(TSymbols.C_BREVE_A)) {
            return 1;
        } else if (brlSymbol.equals(TSymbols.D_BREVE_A)) {
            return 1;
        } else if (brlSymbol.equals(TSymbols.E_BREVE_A)) {
            return 1;
        } else if (brlSymbol.equals(TSymbols.F_BREVE_A)) {
            return 1;
        } else if (brlSymbol.equals(TSymbols.G_BREVE_A)) {
            return 1;
        } else if (brlSymbol.equals(TSymbols.A_BREVE_A)) {
            return 1;
        } else if (brlSymbol.equals(TSymbols.B_BREVE_A)) {
            return 1;
        }//
        else if (brlSymbol.equals(TSymbols.REST_BREVE_B)) {
            return 1;
        } else if (brlSymbol.equals(TSymbols.C_BREVE_B)) {
            return 1;
        } else if (brlSymbol.equals(TSymbols.D_BREVE_B)) {
            return 1;
        } else if (brlSymbol.equals(TSymbols.E_BREVE_B)) {
            return 1;
        } else if (brlSymbol.equals(TSymbols.F_BREVE_B)) {
            return 1;
        } else if (brlSymbol.equals(TSymbols.G_BREVE_B)) {
            return 1;
        } else if (brlSymbol.equals(TSymbols.A_BREVE_B)) {
            return 1;
        } else if (brlSymbol.equals(TSymbols.B_BREVE_B)) {
            return 1;
        }

        throw new DirectException("unexpected symbol '" + brlSymbol + "' for element '" + BElements.BRLNOTE + "'");
    }

    //todo
    static Integer TICKS_PER_QUARTER_NOTE = 16;
    // rythm

}
