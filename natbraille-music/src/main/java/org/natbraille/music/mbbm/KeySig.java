/*
 * NatBraille Music 
 * Copyright (C) 2014 Vivien Guillet, Bruno Mascret
 * Contact: vivien.guillet@univ-lyon1.fr
 * 
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */

package org.natbraille.music.mbbm;

import nu.xom.Attribute;
import nu.xom.Element;
import nu.xom.Node;
import nu.xom.Nodes;
import org.natbraille.music.XmlFuncts;
import org.natbraille.music.braille.TSymbols;
import org.natbraille.music.braillemusicxml.BElements;
import static org.natbraille.music.mbbm.Nums.bm_upperNumber;
import static org.natbraille.music.mbbm.Nums.mb_upperNumber;

/**
 *
 * @author vivien
 */
public class KeySig {

    public static Integer pitchAlterForFifths(Integer fifths, String pitchStep) {

        switch (pitchStep) {
            case "F":
                return (fifths >= 1) ? (1) : (fifths <= -7) ? (-1) : (0);
            case "C":
                return (fifths >= 2) ? (1) : (fifths <= -6) ? (-1) : (0);
            case "G":
                return (fifths >= 3) ? (1) : (fifths <= -5) ? (-1) : (0);
            case "D":
                return (fifths >= 4) ? (1) : (fifths <= -4) ? (-1) : (0);
            case "A":
                return (fifths >= 5) ? (1) : (fifths <= -3) ? (-1) : (0);
            case "E":
                return (fifths >= 6) ? (1) : (fifths <= -2) ? (-1) : (0);
            case "B":
                return (fifths >= 7) ? (1) : (fifths <= -1) ? (-1) : (0);
        }
        return 1;
    }

    /**
     * Translates key signature from MusicXml to BrailleMusicXml.
     * <p>
     * {@code <key>} <br/>
     * {@code <fifths>-3</fifths> }</br>
     * {@code <mode>minor</mode>}</br>
     * {@code </key>} <br/>
     * <p>to
     * <p>
     * {@code <KEY_SIG>} <br/>
     * {@code  <NUMBERED symbol="⠼">} <br/>
     * {@code    <UPPER_NUMBER symbol="⠙"/>} <br/>
     * {@code    <ALTERATION symbol="⠩"/>} <br/>
     * {@code  </NUMBERED>} <br/>
     * {@code </KEY_SIG>} <br/>
     * <p>or
     * <p>
     * {@code <KEY_SIG>} <br/>
     * {@code  <DIRECT>} <br/>
     * {@code    <ALTERATION  symbol="⠩"/>} <br/>
     * {@code    <ALTERATION symbol="⠩"/>} <br/>
     * {@code  </DIRECT>} <br/>
     * {@code </KEY_SIG>} <br/>     
     * <p> the key/mode is lost
     */
    public static Node mb_KeySignature(Node mx) throws DirectException {

        String fifths = XmlFuncts.queryFirstValue(mx, "fifths");
        // not used
        //    String mode = MusicXmlFuncts.queryFirstValue(mx, "mode");

        int fifthsNum = Integer.parseInt(fifths);

        Element bx = new Element(BElements.KEY_SIG);

        if (fifthsNum == 0) {
        } else {

            Element alteration = new Element(BElements.ALTERATION);
            if (fifthsNum < 0) {
                alteration.addAttribute(new Attribute("symbol", TSymbols.ACC_FLAT));
            } else if (fifthsNum > 0) {
                alteration.addAttribute(new Attribute("symbol", TSymbols.ACC_SHARP));
            }

            if (Math.abs(fifthsNum) > 3) {
                Element numbered = new Element(BElements.NUMBERED);
                numbered.addAttribute(new Attribute("symbol", TSymbols.NUMBER_SIGN));
                numbered.appendChild(mb_upperNumber(Math.abs(fifthsNum)));
                numbered.appendChild(alteration);
                bx.appendChild(numbered);
            } else {
                Element direct = new Element(BElements.DIRECT);
                for (int i = 1; i <= Math.abs(fifthsNum); i++) {
                    direct.appendChild(alteration.copy());
                }
                bx.appendChild(direct);
            }
        }

        return bx;

    }

    /**
     * Translates key signature from BrailleMusicXml to MusicXml.
     * opposite trans of {@link #mb_KeySignature(nu.xom.Node) }
     */
    public static Node bm_KeySignature(Node bx) throws DirectException {

        Element key = new Element("key");
        Element fifths = new Element("fifths");
        //Element mode = new Element("mode");         // not used

        String kind = null;
        int number = 0;
        if (bx.query(BElements.DIRECT).size() > 0) {
            Nodes directNodes = bx.query(BElements.DIRECT+"/"+BElements.ALTERATION+"/@symbol");
            // count occurences of last same kind
            for (int i = 0; i < directNodes.size(); i++) {
                String thisKind = directNodes.get(i).getValue();
                if (!thisKind.equals(kind)) {
                    number = 0;
                }
                kind = thisKind;
                number++;
            }
        }
        if (bx.query(BElements.NUMBERED).size() > 0) {
            Node upperNumber = XmlFuncts.queryFirst(bx, BElements.NUMBERED+"/"+BElements.UPPER_NUMBER);
            number = Integer.parseInt(bm_upperNumber((Element) upperNumber));
            kind = XmlFuncts.queryFirstValue(bx, BElements.NUMBERED+"/"+BElements.ALTERATION+"/@symbol");
        }

        if (kind != null) {
            StringBuilder sb = new StringBuilder();
            int mul;
            if (kind.equals(TSymbols.ACC_FLAT)) {
                mul = -1;
            } else if (kind.equals(TSymbols.ACC_SHARP)) {
                mul = 1;
            } else if (kind.equals(TSymbols.ACC_NATURAL)) {
                mul = 0;
            } else {
                throw new DirectException("unexpected symbol '" + kind + "' in '" + bx + "'");
            }

            fifths.appendChild(Integer.toString(mul * number));
            key.appendChild(fifths);
        }
        return key;

    }
}
