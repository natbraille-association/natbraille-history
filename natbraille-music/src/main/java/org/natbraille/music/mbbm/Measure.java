/*
 * NatBraille Music 
 * Copyright (C) 2014 Vivien Guillet, Bruno Mascret
 * Contact: vivien.guillet@univ-lyon1.fr
 * 
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */
package org.natbraille.music.mbbm;

import nu.xom.Attribute;
import nu.xom.Element;
import nu.xom.Elements;
import org.natbraille.music.braille.TSymbols;
import org.natbraille.music.braillemusicxml.BElements;

/**
 *
 * @author vivien
 */
public class Measure {

    public static Element bm_signatureChange(Element bSignatureChange, BmRunParams bmRunParams) throws DirectException {

        Element attributesChange = new Element("attributes");

        Element bKeySig = bSignatureChange.getFirstChildElement(BElements.KEY_SIG);
        Element keySig = (Element) KeySig.bm_KeySignature(bKeySig);
        if (keySig != null) {
            bmRunParams.setKeySignature(keySig);
            attributesChange.appendChild(keySig);
        }

        Element bTimeSig = bSignatureChange.getFirstChildElement(BElements.TIME_SIG);
        Element timeSig = TimeSig.bm_TimeSignature(bTimeSig);
        if (timeSig != null) {
            bmRunParams.setTimeSignature(keySig);
            attributesChange.appendChild(timeSig);
        }

        return attributesChange;
    }

    public static Integer bm_accidental_value(String bSymbol) {
        Integer bAccidentalValue = null;

        if (bSymbol.equals(TSymbols.ACC_DOUBLE_FLAT)) {
            bAccidentalValue = -2;
        } else if (bSymbol.equals(TSymbols.ACC_FLAT)) {
            bAccidentalValue = -1;
        } else if (bSymbol.equals(TSymbols.ACC_NATURAL)) {
            bAccidentalValue = 0;
        } else if (bSymbol.equals(TSymbols.ACC_SHARP)) {
            bAccidentalValue = 1;
        } else if (bSymbol.equals(TSymbols.ACC_DOUBLE_SHARP)) {
            bAccidentalValue = 2;
        }
        return bAccidentalValue;
    }

    public static Element bm_accidental(Element bAccidental, BmRunParams bmRunParams) throws DirectException {

        String bSymbol = bAccidental.getAttributeValue("symbol");

        Element accidental = new Element("accidental");
        Integer bAccidentalValue = null;
        if (bSymbol != null) {
            bAccidentalValue = bm_accidental_value(bSymbol);
            if (bAccidentalValue != null) {
                String accidentalValue = null;
                switch (bAccidentalValue) {
                    case -2:
                        accidentalValue = "double-flat";
                        break;
                    case -1:
                        accidentalValue = "flat";
                        break;
                    case 0:
                        accidentalValue = "natural";
                        break;
                    case 2:
                        accidentalValue = "double-sharp";
                        break;
                    case 1:
                        accidentalValue = "sharp";
                        break;

                }

                accidental.appendChild(accidentalValue);
            }
        }
        return accidental;
    }

    /**
     * <note>
     * <rest/>
     * <duration>72</duration>
     * <voice>1</voice>
     * </note>
     *
     */
    /*
     public static Element bm_rest(Element bRest, BmRunParams bmRunParams) throws DirectException {
     Element note = new Element("note");
     note.appendChild(new Element("rest"));

     //
     //
     Element type = new Element("type");
     type.appendChild("eighth");
     note.appendChild(type);
     //
     Element duration = new Element("duration");
     duration.appendChild("1");
     note.appendChild(duration);
     //
     Element voice = new Element("voice");
     voice.appendChild("1");
     note.appendChild(voice);

     return note;
     }
     */
    public static Element bm_noteOrRest(Element bNoteOrRest, BmRunParams bmRunParams) throws DirectException {

        Element note = new Element("note");

        // REST or     // pitch/step/octave / accidental 
        if (bNoteOrRest.getLocalName().equals(BElements.REST)) {
            note.appendChild(new Element("rest"));
        } else if (bNoteOrRest.getLocalName().equals(BElements.NOTE)) {
            Element bNote = bNoteOrRest;
            // 
            //{
            Element pitch = new Element("pitch");
            // -> step
            //Element step = new Element("step");
            Element step = Pitch.bm_pitchStep(bNote.getFirstChildElement(BElements.BRLNOTE));

            pitch.appendChild(step);
            {
                // -> octave
                Element octave = null;
                if (bNote.getFirstChildElement(BElements.OCTAVE_MARK) != null) {
                    octave = Pitch.bm_octave(bNote.getFirstChildElement(BElements.OCTAVE_MARK));
                } else {
                    // infer octave mark
                    Element lastPitch = bmRunParams.getPitch();
                    Element lastOctave = lastPitch.getFirstChildElement("octave");
                    Element lastStep = lastPitch.getFirstChildElement("step");

                    if (lastStep == null) {
                        octave = (Element) lastOctave.copy();
                    } else {
                        Integer lastOctaveIntValue = Integer.parseInt(lastOctave.getValue());
                        for (int possibleOctaveOffsetValue = -1; possibleOctaveOffsetValue <= 1; possibleOctaveOffsetValue++) {
                            Integer possibleOctaveValue = lastOctaveIntValue + possibleOctaveOffsetValue;

                            Boolean needsom = Pitch.NeedsOctaveMark(
                                    lastStep.getValue(),
                                    lastOctave.getValue(),
                                    step.getValue(),
                                    possibleOctaveValue.toString());
                            if (needsom) {
                            } else {
                                octave = new Element("octave");
                                octave.appendChild(possibleOctaveValue.toString());
                            }
                        }

                    }

                }
                pitch.appendChild(octave);

            }
            String bAccidentalSymbol = null;
            {
                if (bNote.getFirstChildElement(BElements.ACCIDENTAL) != null) {
                    Element bAccidental = bNote.getFirstChildElement(BElements.ACCIDENTAL);
                    bAccidentalSymbol = bAccidental.getAttributeValue("symbol");

                    Element accidental = bm_accidental(bNote.getFirstChildElement(BElements.ACCIDENTAL), bmRunParams);
                    if (accidental != null) {
                        note.appendChild(accidental);
                    }
                }
            }
            // -> alter
            Element alter = new Element("alter");
            if (bAccidentalSymbol != null) {
                Integer bAccidentalValue = bm_accidental_value(bAccidentalSymbol);
                if (bAccidentalValue != null) {
                    alter.appendChild(bAccidentalValue.toString());
                } else {
                    alter.appendChild("0");
                }
            } else {
                if (bmRunParams.getKeySignature() != null) {
                    String fifths = bmRunParams.getKeySignature().getFirstChildElement("fifths").getValue();
                    Integer alterValue = KeySig.pitchAlterForFifths(Integer.parseInt(fifths), step.getValue());
                    alter.appendChild(alterValue.toString());
                } else {
                    alter.appendChild("0");
                }

            }

            pitch.appendChild(alter);
            note.appendChild(pitch);
            bmRunParams.setPitch(pitch);

        }

        // braille tie on first note
        // xml tie on both notes
        //TODO tie stop
//                <tie type="stop"/>
//                <notations>
//                    <tied type="stop"/>
//                </notations>
        {
            Element bTie = bNoteOrRest.getFirstChildElement(BElements.TIE);
            if (bTie != null) {

                Element tie = new Element("tie");
                tie.addAttribute(new Attribute("type", "start"));
                note.appendChild(tie);
                Element notations = new Element("notations");
                Element tied = new Element("tied");
                tied.addAttribute(new Attribute("type", "start"));
                notations.appendChild(tied);
                note.appendChild(notations);

//            
//            Note.appendChild(tie)
            }

        }
        // rythm
        {
            
            Integer symDuration = null;
            Element durationElement = null;
          
            if (bNoteOrRest.getLocalName().equals(BElements.NOTE)) {
                durationElement = bNoteOrRest.getFirstChildElement(BElements.BRLNOTE);
      
            } else if (bNoteOrRest.getLocalName().equals(BElements.REST)) {
                durationElement = bNoteOrRest;
            }
            symDuration = Duration.bm_symbolicDuration(durationElement);
            Integer durationValue = Duration.TICKS_PER_QUARTER_NOTE / (symDuration) * 8;

            Element bDots = bNoteOrRest.getFirstChildElement(BElements.DOTS);
            if (bDots != null) {

                // foreach braille note note
                String bDotsSymbol = bDots.getAttributeValue("symbol");
                for (int i = 0; i < bDotsSymbol.length(); i++) {
                    // add a musicxml dot
                    Element dot = new Element("dot");
                    note.appendChild(dot);
                    // add half of the note duration
                    durationValue += durationValue / 2;
                    if (Math.round(durationValue) != durationValue) {
                        throw new DirectException("tick " + Duration.TICKS_PER_QUARTER_NOTE + "is too narrow for " + i + " dots");
                    }
                }

            }
            // standard value
            String typeValue = null;
            switch (symDuration) {
                case 1:
                    typeValue = "breve";
                    break;
                case 2:
                    typeValue = "whole";
                    break;
                case 4:
                    typeValue = "half";
                    break;
                case 8:
                    typeValue = "quarter";
                    break;
                case 16:
                    typeValue = "eighth";
                    break;

            }
            Element type = new Element("type");
            type.appendChild(typeValue);
            note.appendChild(type);

            Element duration = new Element("duration");
            duration.appendChild(durationValue.toString());
            note.appendChild(duration);
        }

        Element voice = new Element("voice");
        voice.appendChild("1");
        note.appendChild(voice);

        return note;

    }
   
    

    

    public static Element bm_measure(Element bMeasure, BmRunParams bmRunParams) throws DirectException {

        // BEGIN MEASURE
        Element measure = new Element("measure");
        {
            Integer measureNumber = bmRunParams.getMeasureNumber();
            measure.addAttribute(new Attribute("number", measureNumber.toString()));
            if (measureNumber == 0) {
                measure.addAttribute(new Attribute("implicit", "yes"));
            }

            // TODO should not belong to bm_measure
            Element attributes = new Element("attributes");
            {
                Element divisions = new Element("divisions");
                divisions.appendChild(Duration.TICKS_PER_QUARTER_NOTE.toString());
                attributes.appendChild(divisions);
                measure.appendChild(attributes);
            }

            // set direction
            Element lDirection = bmRunParams.getDirection();
            if (lDirection != null) {
                measure.appendChild(lDirection);
                bmRunParams.resetDirection();
            }

            // measure childs
            Elements measureEls = bMeasure.getChildElements();
            for (int j = 0; j < measureEls.size(); j++) {
                Element measureEl = measureEls.get(j);
                //System.out.println("-> " + measureEl.toXML());

                if (measureEl.getLocalName().equals(BElements.SIGNATURE_CHANGE)) {

                    Element attributesChange = bm_signatureChange(measureEl, bmRunParams);
                    measure.appendChild(attributesChange);

                } else if (measureEl.getLocalName().equals(BElements.NOTE)) {

                    Element note = bm_noteOrRest(measureEl, bmRunParams);
                    if (note != null) {
                        measure.appendChild(note);
                    }
                } else if (measureEl.getLocalName().equals(BElements.REST)) {

                    Element restNote = bm_noteOrRest(measureEl, bmRunParams);
                    if (restNote != null) {
                        measure.appendChild(restNote);
                    }
                }

            }

            // increment measure number
            bmRunParams.setMeasureNumber(bmRunParams.getMeasureNumber() + 1);
            // END MEASURE
            return measure;
        }
    }
}
