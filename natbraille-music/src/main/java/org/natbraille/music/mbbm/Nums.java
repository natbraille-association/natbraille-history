/*
 * NatBraille Music 
 * Copyright (C) 2014 Vivien Guillet, Bruno Mascret
 * Contact: vivien.guillet@univ-lyon1.fr
 * 
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */

package org.natbraille.music.mbbm;

import nu.xom.Attribute;
import nu.xom.Element;
import org.natbraille.music.braille.TSymbols;
import org.natbraille.music.braillemusicxml.BElements;

/**
 *
 * @author vivien
 */
public class Nums {
    
    public static String upperDigits[] = new String[]{
        TSymbols.NUMBER_UPPER_ZERO,
        TSymbols.NUMBER_UPPER_ONE,
        TSymbols.NUMBER_UPPER_TWO,
        TSymbols.NUMBER_UPPER_THREE,
        TSymbols.NUMBER_UPPER_FOUR,
        TSymbols.NUMBER_UPPER_FIVE,
        TSymbols.NUMBER_UPPER_SIX,
        TSymbols.NUMBER_UPPER_SEVEN,
        TSymbols.NUMBER_UPPER_EIGHT,
        TSymbols.NUMBER_UPPER_NINE
    };
public static String lowerDigits[] = new String[]{
        TSymbols.NUMBER_LOWER_ONE,
        TSymbols.NUMBER_LOWER_TWO,
        TSymbols.NUMBER_LOWER_THREE,
        TSymbols.NUMBER_LOWER_FOUR,
        TSymbols.NUMBER_LOWER_FIVE,
        TSymbols.NUMBER_LOWER_SIX,
        TSymbols.NUMBER_LOWER_SEVEN,
        TSymbols.NUMBER_LOWER_EIGHT,
        TSymbols.NUMBER_LOWER_NINE
    };
    /**
     * <pre>
     * <UPPER_NUMBER symbol="⠙"/>
     * <pre>
     */
    public static Element mb_upperNumber(int number) throws DirectException {
        if (number < 0) {
            throw new DirectException("cannot build upper number with negative number " + number);
        }
        String numberString = Integer.toString(number);
        StringBuilder sb = new StringBuilder();
        for (int i = 0; i < numberString.length(); i++) {
            int digit = Integer.parseInt(numberString.substring(i, i + 1));
            sb.append(upperDigits[digit]);
        }
        Element un = new Element(BElements.UPPER_NUMBER);
        un.addAttribute(new Attribute("symbol", sb.toString()));
        return un;

    }
/**
     * <pre>
     * <LOWER_NUMBER symbol="⠙"/>
     * <pre>
     */
    public static Element mb_lowerNumber(int number) throws DirectException {
        if (number < 0) {
            throw new DirectException("cannot build lower number with negative number " + number);
        }
        String numberString = Integer.toString(number);
        StringBuilder sb = new StringBuilder();
        for (int i = 0; i < numberString.length(); i++) {
            int digit = Integer.parseInt(numberString.substring(i, i + 1));
            sb.append(lowerDigits[digit-1]);
        }
        Element un = new Element(BElements.LOWER_NUMBER);
        un.addAttribute(new Attribute("symbol", sb.toString()));
        return un;

    }

    /**
     * <pre>
     * <UPPER_NUMBER symbol="⠙"/>
     * <pre>
     */
    public static String bm_upperNumber(Element upperNumberElement) throws DirectException {
        String upperNumber = upperNumberElement.getAttribute("symbol").getValue();
        StringBuilder sb = new StringBuilder();
        for (int i = 0; i < upperNumber.length(); i++) {
            String digit = upperNumber.substring(i, i + 1);
            if (digit.equals(TSymbols.NUMBER_UPPER_ZERO)) {
                sb.append("0");
            }if (digit.equals(TSymbols.NUMBER_UPPER_ONE)) {
                sb.append("1");
            } else if (digit.equals(TSymbols.NUMBER_UPPER_TWO)) {
                sb.append("2");
            } else if (digit.equals(TSymbols.NUMBER_UPPER_THREE)) {
                sb.append("3");
            } else if (digit.equals(TSymbols.NUMBER_UPPER_FOUR)) {
                sb.append("4");
            } else if (digit.equals(TSymbols.NUMBER_UPPER_FIVE)) {
                sb.append("5");
            } else if (digit.equals(TSymbols.NUMBER_UPPER_SIX)) {
                sb.append("6");
            } else if (digit.equals(TSymbols.NUMBER_UPPER_SEVEN)) {
                sb.append("7");
            } else if (digit.equals(TSymbols.NUMBER_UPPER_EIGHT)) {
                sb.append("8");
            } else if (digit.equals(TSymbols.NUMBER_UPPER_NINE)) {
                sb.append("9");
            } else {
                throw new DirectException("unexpected symbol '" + digit
                        + "' for upper number in " + upperNumberElement);
            }
        }
        return sb.toString();
    }

    static String bm_lowerNumber(Element lowerNumberElement) throws DirectException {
        String lowerNumber = lowerNumberElement.getAttribute("symbol").getValue();
        StringBuilder sb = new StringBuilder();
        for (int i = 0; i < lowerNumber.length(); i++) {
            String digit = lowerNumber.substring(i, i + 1);
            if (digit.equals(TSymbols.NUMBER_LOWER_ONE)) {
                sb.append("1");
            } else if (digit.equals(TSymbols.NUMBER_LOWER_TWO)) {
                sb.append("2");
            } else if (digit.equals(TSymbols.NUMBER_LOWER_THREE)) {
                sb.append("3");
            } else if (digit.equals(TSymbols.NUMBER_LOWER_FOUR)) {
                sb.append("4");
            } else if (digit.equals(TSymbols.NUMBER_LOWER_FIVE)) {
                sb.append("5");
            } else if (digit.equals(TSymbols.NUMBER_LOWER_SIX)) {
                sb.append("6");
            } else if (digit.equals(TSymbols.NUMBER_LOWER_SEVEN)) {
                sb.append("7");
            } else if (digit.equals(TSymbols.NUMBER_LOWER_EIGHT)) {
                sb.append("8");
            } else if (digit.equals(TSymbols.NUMBER_LOWER_NINE)) {
                sb.append("9");
            } else {
                throw new DirectException("unexpected symbol '" + digit
                        + "' for lower number in " + lowerNumberElement);
            }
        }
        return sb.toString();
    }

}
