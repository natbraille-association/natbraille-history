/*
 * NatBraille Music 
 * Copyright (C) 2014 Vivien Guillet, Bruno Mascret
 * Contact: vivien.guillet@univ-lyon1.fr
 * 
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */

package org.natbraille.music.mbbm;

import nu.xom.Element;
import org.natbraille.music.braille.TSymbols;

/**
 *
 * @author vivien
 */
public class Pitch {

    public static Element bm_octave(Element bOctaveMark) throws DirectException {
        String brlSymbol = bOctaveMark.getAttribute("symbol").getValue();

        Integer o = null;
        if (brlSymbol.equals(TSymbols.BR_OK_UNDER_C1)) {
            o = 0;
        } else if (brlSymbol.equals(TSymbols.BR_OK_C1)) {
            o = 1;
        } else if (brlSymbol.equals(TSymbols.BR_OK_C2)) {
            o = 2;
        } else if (brlSymbol.equals(TSymbols.BR_OK_C2)) {
            o = 3;
        } else if (brlSymbol.equals(TSymbols.BR_OK_C3)) {
            o = 4;
        } else if (brlSymbol.equals(TSymbols.BR_OK_C4)) {
            o = 5;
        } else if (brlSymbol.equals(TSymbols.BR_OK_C5)) {
            o = 6;
        } else if (brlSymbol.equals(TSymbols.BR_OK_C6)) {
            o = 7;
        } else if (brlSymbol.equals(TSymbols.BR_OK_C7)) {
            o = 8;
        } else if (brlSymbol.equals(TSymbols.BR_OK_OVER_C7)) {
            o = 9;
        }
        if (o == null) {
            throw new DirectException("unexpected symbol '" + brlSymbol + "' for octave mark");
        }
        Element octave = new Element("octave");
        octave.appendChild(o.toString());
        return octave;

    }

    public static Element bm_pitchStep(Element brlNote) throws DirectException {
        Element step = new Element("step");

        String bNoteSymbol = brlNote.getAttribute("symbol").getValue();

        step.appendChild(bm_pitchStep(bNoteSymbol));
        return step;
    }
    
    
    public static String bm_pitchStep(String brlSymbol) throws DirectException {

        if (brlSymbol.equals(TSymbols.C_HEIGHTH)) {
            return "C";
        } else if (brlSymbol.equals(TSymbols.D_HEIGHTH)) {
            return "D";
        } else if (brlSymbol.equals(TSymbols.E_HEIGHTH)) {
            return "E";
        } else if (brlSymbol.equals(TSymbols.F_HEIGHTH)) {
            return "F";
        } else if (brlSymbol.equals(TSymbols.G_HEIGHTH)) {
            return "G";
        } else if (brlSymbol.equals(TSymbols.A_HEIGHTH)) {
            return "A";
        } else if (brlSymbol.equals(TSymbols.B_HEIGHTH)) {
            return "B";
        } else if (brlSymbol.equals(TSymbols.C_QUARTER)) {
            return "C";
        } else if (brlSymbol.equals(TSymbols.D_QUARTER)) {
            return "D";
        } else if (brlSymbol.equals(TSymbols.E_QUARTER)) {
            return "E";
        } else if (brlSymbol.equals(TSymbols.F_QUARTER)) {
            return "F";
        } else if (brlSymbol.equals(TSymbols.G_QUARTER)) {
            return "G";
        } else if (brlSymbol.equals(TSymbols.A_QUARTER)) {
            return "A";
        } else if (brlSymbol.equals(TSymbols.B_QUARTER)) {
            return "B";
        } else if (brlSymbol.equals(TSymbols.C_HALF)) {
            return "C";
        } else if (brlSymbol.equals(TSymbols.D_HALF)) {
            return "D";
        } else if (brlSymbol.equals(TSymbols.E_HALF)) {
            return "E";
        } else if (brlSymbol.equals(TSymbols.F_HALF)) {
            return "F";
        } else if (brlSymbol.equals(TSymbols.G_HALF)) {
            return "G";
        } else if (brlSymbol.equals(TSymbols.A_HALF)) {
            return "A";
        } else if (brlSymbol.equals(TSymbols.B_HALF)) {
            return "B";
        } else if (brlSymbol.equals(TSymbols.C_WHOLE)) {
            return "C";
        } else if (brlSymbol.equals(TSymbols.D_WHOLE)) {
            return "D";
        } else if (brlSymbol.equals(TSymbols.E_WHOLE)) {
            return "E";
        } else if (brlSymbol.equals(TSymbols.F_WHOLE)) {
            return "F";
        } else if (brlSymbol.equals(TSymbols.G_WHOLE)) {
            return "G";
        } else if (brlSymbol.equals(TSymbols.A_WHOLE)) {
            return "A";
        } else if (brlSymbol.equals(TSymbols.B_WHOLE)) {
            return "B";
        }
        //
        else if (brlSymbol.equals(TSymbols.C_BREVE_A)) {
            return "C";
        } else if (brlSymbol.equals(TSymbols.D_BREVE_A)) {
            return "D";
        } else if (brlSymbol.equals(TSymbols.E_BREVE_A)) {
            return "E";
        } else if (brlSymbol.equals(TSymbols.F_BREVE_A)) {
            return "F";
        } else if (brlSymbol.equals(TSymbols.G_BREVE_A)) {
            return "G";
        } else if (brlSymbol.equals(TSymbols.A_BREVE_A)) {
            return "A";
        } else if (brlSymbol.equals(TSymbols.B_BREVE_A)) {
            return "B";
        }
        //
        else if (brlSymbol.equals(TSymbols.C_BREVE_B)) {
            return "C";
        } else if (brlSymbol.equals(TSymbols.D_BREVE_B)) {
            return "D";
        } else if (brlSymbol.equals(TSymbols.E_BREVE_B)) {
            return "E";
        } else if (brlSymbol.equals(TSymbols.F_BREVE_B)) {
            return "F";
        } else if (brlSymbol.equals(TSymbols.G_BREVE_B)) {
            return "G";
        } else if (brlSymbol.equals(TSymbols.A_BREVE_B)) {
            return "A";
        } else if (brlSymbol.equals(TSymbols.B_BREVE_B)) {
            return "B";
        }
        
        /*
         else if (brlSymbol.equals(TSymbols.REST_HEIGHTH)) {
         return null;
         } else if (brlSymbol.equals(TSymbols.REST_QUARTER)) {
         return null;
         }else if (brlSymbol.equals(TSymbols.REST_HALF)) {
         return null;
         } else if (brlSymbol.equals(TSymbols.REST_WHOLE)) {
         return null;
         }
         */
        throw new DirectException("unexpected symbol for note '" + brlSymbol + "'");
    }

    private static Integer stepDegres(String step) throws DirectException {
        Integer degres = null;
        switch (step) {
            case "C":
                degres = 0;
                break;
            case "D":
                degres = 1;
                break;
            case "E":
                degres = 2;
                break;
            case "F":
                degres = 3;
                break;
            case "G":
                degres = 4;
                break;
            case "A":
                degres = 5;
                break;
            case "B":
                degres = 6;
                break;
        }
        if (degres == null) {
            throw new DirectException("bad symbol for pitch step'" + step + "'");
        }
        return degres;
    }

    public static Integer intervalDegres(String step1, String octave1, String step2, String octave2) throws DirectException {

        int dStep1 = stepDegres(step1);
        int dStep2 = stepDegres(step2);

        int dStepDeg = dStep2 - dStep1;
        int dOctaveDeg = 7 * (Integer.parseInt(octave2) - Integer.parseInt(octave1));

        int dDeg = dStepDeg + dOctaveDeg;
        return dDeg;

    }

    public static boolean NeedsOctaveMark(String step1, String octave1, String step2, String octave2) throws DirectException {
        Integer degre = intervalDegres(step1, octave1, step2, octave2);
        Integer degreEtendue = Math.abs(degre);
        if (degreEtendue <= 2) {
            // IMBT 7 : never mark a second or third
            return false;
        }
        if (degreEtendue >= 5) {
            // IMBT 7 : always mark a sixth or more
            return true;
        }
        // IMBT 7 : fourth or fifth only if notes are in a different C octave
        return Integer.parseInt(octave1) != Integer.parseInt(octave2);
    }

    @Deprecated
    private static int stepSemitones(String step) throws DirectException {
        Integer semitones = null;
        switch (step) {
            case "C":
                semitones = 0;
                break;
            case "D":
                semitones = 2;
                break;
            case "E":
                semitones = 4;
                break;
            case "F":
                semitones = 5;
                break;
            case "G":
                semitones = 7;
                break;
            case "A":
                semitones = 9;
                break;
            case "B":
                semitones = 11;
                break;
        }
        if (semitones == null) {
            throw new DirectException("bad symbol for pitch step'" + step + "'");
        }
        return semitones;
    }

    @Deprecated
    private static int midiPitch(String step, String octave, String alter) throws DirectException {
        int octavePart = 12 * Integer.parseInt(octave);
        int alterPart = Integer.parseInt(alter);
        Integer stepPart = stepSemitones(step);
        return octavePart + alterPart + stepPart;
    }

}
