/*
 * NatBraille Music 
 * Copyright (C) 2014 Vivien Guillet, Bruno Mascret
 * Contact: vivien.guillet@univ-lyon1.fr
 * 
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */
package org.natbraille.music.mbbm;

import java.io.File;
import java.io.IOException;
import nu.xom.Attribute;
import nu.xom.Builder;
import nu.xom.Document;
import nu.xom.Element;
import nu.xom.Node;
import nu.xom.Nodes;
import nu.xom.ParsingException;
import org.natbraille.music.braille.TSymbols;
import org.natbraille.music.braillemusicxml.BElements;

/**
 *
 * @author vivien
 */
public class Root {

    public static Element mb_root(Node n) throws DirectException {

        Element el = new Element("void");
        el.appendChild(n.copy());
        return el;

    }

    public static Element mb_root(File f) throws DirectException {
        // TODO implement
        Element bRoot = new Element("void");
        try {
            Builder parser = new Builder();
            Document doc = parser.build(f);
            Element e = doc.getRootElement();
            e.detach();
            bRoot.appendChild(e);
            //MFuncts.getPartIds(doc);
        } catch (ParsingException ex) {
            System.err.println("Cafe con Leche is malformed today. How embarrassing!");
        } catch (IOException ex) {
            System.err.println("Could not connect to Cafe con Leche. The site may be down.");
        }
        return bRoot;

    }

    public static Node bm_root(Node bmxml) throws DirectException {

        BmRunParams bmRunParams = new BmRunParams();

        Element scorePartwise = new Element("score-partwise");
        scorePartwise.addAttribute(new Attribute("version", "3.0"));

        ///
        Element partList = new Element("part-list");
        Element scorePart = new Element("score-part");
        scorePart.addAttribute(new Attribute("id", "P1"));

        /*
         Element partName = new Element("part-name");
         partName.appendChild("MUSIQUE!");
         scorePart.appendChild(partName);
         */
        partList.appendChild(scorePart);
        scorePartwise.appendChild(partList);
        //

        Element direction = Direction.bm_direction(bmxml);
        bmRunParams.setDirection(direction);

        //  part
        Element part = new Element("part");
        part.addAttribute(new Attribute("id", "P1"));

        Nodes bMusicLines = bmxml.query("//" + BElements.MUSIC_SEGMENT + "/*");
        for (int i = 0; i < bMusicLines.size(); i++) {
            Element m = (Element) bMusicLines.get(i);

            if (m.getLocalName().equals(BElements.MARGINAL_MEASURE_NUMBER)) {

                //bmRunParams.setMeasureNumber();
            } else if (m.getLocalName().equals(BElements.MEASURE)) {

                if (m.getFirstChildElement(BElements.FULL_MEASURE_RESTS) != null) {
                    /*
                     <MEASURE id="1259">
                     <FULL_MEASURE_RESTS id="1260">
                     <DIRECT id="1262" symbol="⠍" />
                     </FULL_MEASURE_RESTS>
                     </MEASURE>
                     */
                    /*
                     <measure number="1">
                     
                     <attributes>
                     <measure-style>
                     <multiple-rest>32</multiple-rest>
                     </measure-style>
                     </attributes>
                     
                     <note>
                     <rest/>
                     <duration>4</duration>
                     <voice>1</voice>
                     </note>
                     </measure>
                    
                     <measure number="2">
                     <note>
                     <rest/>
                     <duration>4</duration>
                     <voice>1</voice>
                     </note>
                     </measure>
                     */
                    Element bFullMeasureRests = m.getFirstChildElement(BElements.FULL_MEASURE_RESTS);
                    Integer bRestMeasureNumber = null;

                    if (bFullMeasureRests.getFirstChildElement(BElements.DIRECT) != null) {
                        Element bDirect = bFullMeasureRests.getFirstChildElement(BElements.DIRECT);
                        String symbols = bDirect.getAttributeValue("symbol");
                        bRestMeasureNumber = symbols.length();
                    } else if (bFullMeasureRests.getFirstChildElement(BElements.PREFIXED_UPPER_NUMBER) != null) {
                        Element prefixedUpperNumber = bFullMeasureRests.getFirstChildElement(BElements.PREFIXED_UPPER_NUMBER);
                        Element upperNumber = prefixedUpperNumber.getFirstChildElement(BElements.UPPER_NUMBER);
                        String upperNumberSymbolsValue = Nums.bm_upperNumber(upperNumber);
                        bRestMeasureNumber = Integer.parseInt(upperNumberSymbolsValue);

                    }

                    Integer quarterNotePerMeasure = 4;
                    Integer measureDuration = Duration.TICKS_PER_QUARTER_NOTE * quarterNotePerMeasure;

                    for (int j = 1; j <= bRestMeasureNumber; j++) {
                        Element measure = new Element("measure");
                        
                        //TODO uncopycolle
                        Integer measureNumber = bmRunParams.getMeasureNumber();
                        measure.addAttribute(new Attribute("number", measureNumber.toString()));
                        if (measureNumber == 0) {
                            measure.addAttribute(new Attribute("implicit", "yes"));
                        }
                        //
                        if (j == 1) {
                            Element attributes = new Element("attributes");
                            Element measureStyle = new Element("measure-style");
                            Element multipleRest = new Element("multiple-rest");
                            multipleRest.appendChild(String.valueOf(measureDuration * bRestMeasureNumber));
                            measureStyle.appendChild(multipleRest);
                            attributes.appendChild(measureStyle);
                            measure.appendChild(attributes);

                        }
                        Element note = new Element("note");
                        note.appendChild(new Element("rest"));
                        Element duration = new Element("duration");
                        duration.appendChild(measureDuration.toString());
                        note.appendChild(duration);
                        Element voice = new Element("voice");
                        voice.appendChild("1");
                        note.appendChild(voice);
                        measure.appendChild(note);
                        //
                        part.appendChild(measure);
                        bmRunParams.setLastMeasure(measure);
                        bmRunParams.setMeasureNumber(bmRunParams.getMeasureNumber() + 1);
                    }

                } else {
                    Element measure = Measure.bm_measure(m, bmRunParams);
                    bmRunParams.setLastMeasure(measure);
                    //measure.addAttribute(new Attribute("bid", m.getAttributeValue("id")));
                    part.appendChild(measure);
                }
            } else if (m.getLocalName().equals(BElements.MEASURE_BAR)) {
                String bMeasureBarSymbol = m.getAttributeValue("symbol");

                if (TSymbols.MEASURE_BAR.equals(bMeasureBarSymbol)) {
                    // nothing
                } else {
                    /*                    
                     <barline location="left">
                     <bar-style>heavy-light</bar-style>
                     <repeat direction="forward"/>
                     </barline>
                     */
                    // get last braille measure;

                    Element xmlMeasure = bmRunParams.getLastMeasure();
                    if (xmlMeasure != null) {
                        Element measureBar = new Element("barline");
                        measureBar.addAttribute(new Attribute("location", "right"));
                        Element barStyle = new Element("bar-style");
                        if (bMeasureBarSymbol.equals(TSymbols.MEASURE_ENDING_DOUBLE_BAR)) {
                            barStyle.appendChild("light-heavy");
                        } else if (bMeasureBarSymbol.equals(TSymbols.MEASURE_SECTIONNAL_DOUBLE_BAR)) {
                            barStyle.appendChild("light-light");
                        }
                        measureBar.appendChild(barStyle);
                        xmlMeasure.appendChild(measureBar);
                    }

                }

            }

        }

        //
        scorePartwise.appendChild(part);
        //

        //
        return scorePartwise;
    }
}
