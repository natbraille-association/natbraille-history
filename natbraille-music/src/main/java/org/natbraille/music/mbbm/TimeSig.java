/*
 * NatBraille Music 
 * Copyright (C) 2014 Vivien Guillet, Bruno Mascret
 * Contact: vivien.guillet@univ-lyon1.fr
 * 
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */

package org.natbraille.music.mbbm;

import nu.xom.Element;
import nu.xom.Nodes;
import org.natbraille.music.braillemusicxml.BElements;

/**
 *
 * @author vivien
 */
public class TimeSig {

    public static Element bm_TimeSignature(Element bTimeSig) throws DirectException {
        Element time = null;
        if (bTimeSig != null) {
            Element bFractions = bTimeSig.getFirstChildElement(BElements.FRACTIONS);
            if (bFractions != null) {

                // TODO : named (aot fraction)                                
                Nodes nFrac = bFractions.query(BElements.FRACTION);
                for (int k = 0; k < nFrac.size(); k++) {
                    Element bFraction = (Element) nFrac.get(k);
                    Element bFracNum = bFraction.getFirstChildElement(BElements.UPPER_NUMBER);
                    Element bFracDen = bFraction.getFirstChildElement(BElements.LOWER_NUMBER);
                    String num = null;
                    String den = null;
                    if (bFracNum != null) {
                        num = Nums.bm_upperNumber(bFracNum);
                    }
                    if (bFracDen != null) {
                        //TODO !!!!!
                        den = Nums.bm_lowerNumber(bFracDen);
                    }
                    if ((num != null) && (den != null)) {
                        time = new Element("time");
                        Element beats = new Element("beats");
                        Element beatType = new Element("beat-type");
                        beats.appendChild(num);
                        beatType.appendChild(den);
                        time.appendChild(beats);
                        time.appendChild(beatType);
                    }

                }

            }
        }
        return time;

    }

}
