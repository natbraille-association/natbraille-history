/*
 * NatBraille Music 
 * Copyright (C) 2014 Vivien Guillet, Bruno Mascret
 * Contact: vivien.guillet@univ-lyon1.fr
 * 
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */

package org.natbraille.music.musicxml;

import java.util.ArrayList;
import java.util.List;
import nu.xom.Document;
import nu.xom.Element;
import nu.xom.Node;
import nu.xom.Nodes;
import org.natbraille.music.XmlFuncts;
import org.natbraille.music.mbbm.DirectException;
import org.natbraille.music.mbbm.KeySig;

/**
 *
 * @author vivien
 */
public class MFuncts {


    public static String partIds() {
        return "//part-list/score-part/@id";
    }

    public static String scorePart(String id) {
        return "//part-list/score-part[@id='" + id + "']";
    }
    /*
     <part-group number="1" type="start">
     <group-symbol default-x="-12">
     bracket
     </group-symbol>
     <group-barline>
     yes
     </group-barline>
     */

    public static void getPartIds(Document doc) throws DirectException {
        List<String> resu = new ArrayList<>();
        Nodes nodes3 = doc.query("//part-list/score-part");
        for (int i = 0; i < nodes3.size(); i++) {
            System.err.println("return " + i + " : " + nodes3.get(i).toXML());
        }

        Nodes partIds = doc.query(partIds());
        for (int i = 0; i < partIds.size(); i++) {
            String id = partIds.get(i).getValue();
            Node scorePart = XmlFuncts.queryFirst(doc, scorePart(id));
            String partName = XmlFuncts.queryFirstValue(scorePart, "part-name");
            String partAbbreviation = XmlFuncts.queryFirstValue(scorePart, "part-abbreviation");
            String partInstrumentName = XmlFuncts.queryFirstValue(scorePart, "score-instrument/instrument-name");

            System.err.println("return " + i + " : " + partName + " " + partAbbreviation + " " + partInstrumentName);

            //System.err.println("return partName "+partName);
        }

        String lookup = "//part/measure/attributes/key";
        Nodes xmlKss = doc.query(lookup);
        for (int i = 0; i < xmlKss.size(); i++) {
            Node xmlKs = xmlKss.get(i);
            Element e;            
            System.err.println("////xmlKs" + i + " : " + xmlKs.toXML());
            Node mb_KeySignature = KeySig.mb_KeySignature(xmlKs);
            System.err.println( " ///>>mb_KeySignature>>>> " + mb_KeySignature.toXML());
            
            Node back = KeySig.bm_KeySignature(mb_KeySignature);
            System.err.println( " ///>>>>back>>>> " + back.toXML());
            
               Node backback = KeySig.mb_KeySignature(back);
            System.err.println( " ///>>>backback>>>>> " + backback.toXML());
        }
    }

    public static String getPartAbbreviation() {
        return null;

    }

}
