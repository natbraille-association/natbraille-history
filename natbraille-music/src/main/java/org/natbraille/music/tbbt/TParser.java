/*
 * NatBraille Music 
 * Copyright (C) 2014 Vivien Guillet, Bruno Mascret
 * Contact: vivien.guillet@univ-lyon1.fr
 * 
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */
package org.natbraille.music.tbbt;

import javax.xml.parsers.ParserConfigurationException;
import nu.xom.Node;
import nu.xom.Element;
import org.natbraille.music.braille.TSymbols;
import org.natbraille.music.braillemusicxml.BElements;
import org.natbraille.music.mbbm.DirectException;
import org.natbraille.music.mbbm.Nums;
import org.parboiled.Action;
import org.parboiled.BaseParser;
import static org.parboiled.BaseParser.ACTION;
import org.parboiled.Context;
import org.parboiled.Rule;
import org.parboiled.support.Var;

/**
 * Issued format uses the concepts of the Braille musical code
 */
/**
 *
 * @author vivien
 */
//@BuildParseTree
public class TParser extends BaseParser<Node> {

    TParserActions a;

    public TParser() throws ParserConfigurationException {
        this.a = new TParserActions();
    }

    /*
     * measure bar
     */
    public Rule MeasureBar() {
        return Sequence(
                FirstOf(
                        TSymbols.MEASURE_SECTIONNAL_DOUBLE_BAR,
                        TSymbols.MEASURE_ENDING_DOUBLE_BAR,
                        TSymbols.MEASURE_BAR
                ),
                ACTION(a.domPushMatchElement(BElements.MEASURE_BAR))
        );
    }

    /*
     * tempo and mood
     */
    public Rule TempoAndMoodWord() {
        return Sequence(
                OneOrMore(
                        Sequence(
                                TestNot(
                                        FirstOf(Spaces(),
                                                NewLines(), BElements.TEMPO_MOOD_END)
                                ),
                                ANY
                        )
                ),
                ACTION(a.domPushMatchElement(BElements.TEMPO_AND_MOOD_WORD))
        );

    }

    // IBMT
    public Rule TempoAndMood() {
        return Sequence(
                ACTION(a.domPushElement(BElements.TEMPO_AND_MOOD)),
                TSymbols.TEMPO_MOOD_START,
                ACTION(a.domPushMatchElement(BElements.TEMPO_MOOD_START)),
                ACTION(a.domAppendChild()),
                OneOrMore(
                        FirstOf(
                                Sequence(
                                        TempoAndMoodWord(),
                                        ACTION(a.domAppendChild())
                                ),
                                Sequence(
                                        RightPadding(),
                                        ACTION(a.domAppendChild()),
                                        NewLine(),
                                        ACTION(a.domAppendChild()),
                                        LeftPadding(),
                                        ACTION(a.domAppendChild())
                                ),
                                Sequence(
                                        Spaces(),
                                        ACTION(a.domAppendChild())
                                )
                        )
                ),
                TSymbols.TEMPO_MOOD_END,
                ACTION(a.domPushMatchElement(BElements.TEMPO_MOOD_END)),
                ACTION(a.domAppendChild())
        );
    }

    /*
     * Metronome Marking
     */
    public Rule MetronomeTimeValue() {
        return Sequence(
                ACTION(a.domPushElement(BElements.TIME_VALUE)),
                FirstOf(
                        TSymbols.C_HALF,
                        TSymbols.C_HEIGHTH,
                        TSymbols.C_QUARTER,
                        TSymbols.C_WHOLE
                ),
                ACTION(a.domPushMatchElement(BElements.BASE_DURATION)),
                ACTION(a.domAppendChild()),
                Optional(
                        DurationDot(),
                        ACTION(a.domAppendChild())
                )
        );

    }

    public Rule MetronomeTimeValueCirca() {
        return Sequence(
                ACTION(a.domPushElement(BElements.METRONOME_MARKING_CIRCA)),
                AnyButSpaceAndNewLine(),
                ACTION(a.domAppendChild()),
                Space(),
                ACTION(a.domAppendChild())
        );

    }

    public Rule MetronomeMarking() {
        return Sequence(
                ACTION(a.domPushElement(BElements.METRONOME_MARKING)),
                Optional(
                        TestNot(MetronomeTimeValue()),
                        MetronomeTimeValueCirca(),
                        ACTION(a.domAppendChild())
                ),
                MetronomeTimeValue(),
                ACTION(a.domAppendChild()),
                //
                TSymbols.METRONOME_EQUALS,
                ACTION(a.domPushMatchElement(BElements.EQUALS)),
                ACTION(a.domAppendChild()),
                //
                FirstOf(
                        Sequence(
                                PrefixedUpperNumber(),
                                ACTION(a.domAppendChild())
                        ),
                        Sequence(
                                MetronomeTimeValue(),
                                ACTION(a.domAppendChild())
                        )
                )
        );
    }

    /*
     * Time Signature
     */
    public Rule TimeSigNamed() {
        return Sequence(
                FirstOf(
                        TSymbols.TIME_SIG_COMMON,
                        TSymbols.TIME_SIG_CUT
                ),
                ACTION(a.domPushMatchElement(BElements.NAMED))
        );

    }

    public Rule TimeSigFraction() {
        return Sequence(
                TSymbols.NUMBER_SIGN,
                ACTION(a.domPushMatchElement(BElements.FRACTION)),
                UpperNumber(),
                /*
                 ACTION(a.domPushElement(BElements.FRACTION)),
                 PrefixedUpperNumber(),
                 ACTION(a.domAppendChild()),
                 */
                ACTION(a.domAppendChild()),
                Optional(
                        LowerNumber(),
                        ACTION(a.domAppendChild())
                )
        );

    }

    public Rule TimeSigFractions() {
        return Sequence(
                ACTION(a.domPushElement(BElements.FRACTIONS)),
                OneOrMore(
                        TimeSigFraction(),
                        ACTION(a.domAppendChild())
                )
        );

    }

    public Rule TimeSig() {
        return Sequence(
                ACTION(a.domPushElement(BElements.TIME_SIG)),
                FirstOf(
                        TimeSigNamed(),
                        TimeSigFractions()
                ),
                ACTION(a.domAppendChild())
        );

    }

    /* 
     * KeySignature
     */
    public Rule KeySig() {
        return Sequence(
                ACTION(a.domPushElement(BElements.KEY_SIG)),
                FirstOf(
                        Sequence(
                                FirstOf(
                                        NTimes(3, TSymbols.ACC_FLAT),
                                        NTimes(2, TSymbols.ACC_FLAT),
                                        TSymbols.ACC_FLAT,
                                        NTimes(3, TSymbols.ACC_SHARP),
                                        NTimes(2, TSymbols.ACC_SHARP),
                                        TSymbols.ACC_SHARP,
                                        NTimes(3, TSymbols.ACC_NATURAL),
                                        NTimes(2, TSymbols.ACC_NATURAL),
                                        TSymbols.ACC_NATURAL
                                ),
                                ACTION(a.domPushMatchElement(BElements.DIRECT))
                        ),
                        Sequence(
                                TSymbols.NUMBER_SIGN,
                                ACTION(a.domPushMatchElement(BElements.NUMBERED)),
                                UpperNumber(),
                                ACTION(a.domAppendChild()),
                                FirstOf(
                                        TSymbols.ACC_FLAT,
                                        TSymbols.ACC_SHARP,
                                        TSymbols.ACC_NATURAL
                                ),
                                ACTION(a.domPushMatchElement(BElements.ALTERATION)),
                                ACTION(a.domAppendChild())
                        )
                ),
                ACTION(a.domAppendChild())
        );

    }

    /*
     * MetronomeMarkingAndSignaturesLayout
     */
    public Rule MetronomeMarkingAndSignatures() {
        return Sequence(
                ACTION(a.domPushElement(BElements.METRONOME_MARKING_AND_SIGNATURE)),
                MetronomeMarking(),
                ACTION(a.domAppendChild()),
                Spaces(),
                ACTION(a.domAppendChild()),
                Optional(Sequence(KeySig(), ACTION(a.domAppendChild()))),
                Optional(Sequence(TimeSig(), ACTION(a.domAppendChild())))
        );
    }


    /*
     * Marginal Measure Num
     */
    public Rule MarginalMeasureNum() {
        return Sequence(
                ACTION(a.domPushElement(BElements.MARGINAL_MEASURE_NUMBER)),
                PrefixedUpperNumber(),
                ACTION(a.domAppendChild())
        );
    }

    /*
     * Notes and rests
     */
    public Rule Rest() {
        return Sequence(
                FirstOf(
                        
                        TSymbols.REST_BREVE_B,
                        TSymbols.REST_BREVE_A,
                        TSymbols.REST_HALF,
                        TSymbols.REST_HEIGHTH,
                        TSymbols.REST_QUARTER,
                        TSymbols.REST_WHOLE
                        
                ),
                ACTION(a.domPushMatchElement(BElements.REST))
        );
    }

    // IBMT 6
    public Rule Accidental() {
        return Sequence(
                FirstOf(
                        TSymbols.ACC_DOUBLE_FLAT,
                        TSymbols.ACC_FLAT,
                        TSymbols.ACC_NATURAL,
                        TSymbols.ACC_SHARP,
                        TSymbols.ACC_DOUBLE_SHARP
                ),
                ACTION(a.domPushMatchElement(BElements.ACCIDENTAL))
        );
    }

    // IBMT 7
    public Rule OctaveMark() {
        return Sequence(
                FirstOf(
                        TSymbols.BR_OK_UNDER_C1,
                        TSymbols.BR_OK_C1,
                        TSymbols.BR_OK_C2,
                        TSymbols.BR_OK_C3,
                        TSymbols.BR_OK_C4,
                        TSymbols.BR_OK_C5,
                        TSymbols.BR_OK_C6,
                        TSymbols.BR_OK_C7,
                        TSymbols.BR_OK_OVER_C7
                ),
                ACTION(a.domPushMatchElement(BElements.OCTAVE_MARK))
        );
    }

    public Rule BrlNote() {
        return Sequence(
                FirstOf(
                        TSymbols.C_BREVE_B,
                        TSymbols.C_BREVE_A,
                        TSymbols.C_HALF,
                        TSymbols.C_HEIGHTH,
                        TSymbols.C_QUARTER,
                        TSymbols.C_WHOLE,
                        TSymbols.D_BREVE_B,
                        TSymbols.D_BREVE_A,
                        TSymbols.D_HALF,
                        TSymbols.D_HEIGHTH,
                        TSymbols.D_QUARTER,
                        TSymbols.D_WHOLE,
                        TSymbols.E_BREVE_B,
                        TSymbols.E_BREVE_A,
                        TSymbols.E_HALF,
                        TSymbols.E_HEIGHTH,
                        TSymbols.E_QUARTER,
                        TSymbols.E_WHOLE,
                        TSymbols.F_BREVE_B,
                        TSymbols.F_BREVE_A,
                        TSymbols.F_HALF,
                        TSymbols.F_HEIGHTH,
                        TSymbols.F_QUARTER,
                        TSymbols.F_WHOLE,
                        TSymbols.G_BREVE_B,
                        TSymbols.G_BREVE_A,
                        TSymbols.G_HALF,
                        TSymbols.G_HEIGHTH,
                        TSymbols.G_QUARTER,
                        TSymbols.G_WHOLE,
                        TSymbols.A_BREVE_B,
                        TSymbols.A_BREVE_A,
                        TSymbols.A_HALF,
                        TSymbols.A_HEIGHTH,
                        TSymbols.A_QUARTER,
                        TSymbols.A_WHOLE,
                        TSymbols.B_BREVE_B,
                        TSymbols.B_BREVE_A,
                        TSymbols.B_HALF,
                        TSymbols.B_HEIGHTH,
                        TSymbols.B_QUARTER,
                        TSymbols.B_WHOLE
                ),
                ACTION(a.domPushMatchElement(BElements.BRLNOTE))
        );
    }

    // IBMT 3
    public Rule DurationDot() {
        return Sequence(
                OneOrMore(TSymbols.DOT),
                ACTION(a.domPushMatchElement(BElements.DOTS))
        );

    }

    /*
     * fingering
     */
    // IBMT 9 9.1 9.2 9.3    
    public Rule Finger() {
        return Sequence(
                FirstOf(
                        TSymbols.FINGERING_FIRST,
                        TSymbols.FINGERING_SECOND,
                        TSymbols.FINGERING_THIRD,
                        TSymbols.FINGERING_FOURTH,
                        TSymbols.FINGERING_FIFTH
                ),
                ACTION(a.domPushMatchElement(BElements.FINGER))
        );

    }

    // IBMT 9.4a 9.4b
    public Rule FingerChange() {
        return Sequence(
                ACTION(a.domPushElement(BElements.FINGER_CHANGE)),
                Finger(),
                ACTION(a.domAppendChild()),
                TSymbols.FINGERING_CHANGE,
                ACTION(a.domPushMatchElement(BElements.FINGERING_CHANGE_SYMBOL)),
                ACTION(a.domAppendChild()),
                Finger(),
                ACTION(a.domAppendChild())
        );

    }

    // IMBT 9
    // IBMT 9.5a 9.5b // extension : can be change inside choice
    public Rule Fingering() {
        return Sequence(
                ACTION(a.domPushElement(BElements.FINGERING)),
                OneOrMore(
                        FirstOf(FingerChange(), Finger()),
                        ACTION(a.domAppendChild())
                )
        );

    }

    public Rule DomAppendChild(Rule r) {
        return Sequence(
                r,
                ACTION(a.domAppendChild())
        );

    }

    public Rule Note() {
        return Sequence(
                ACTION(a.domPushElement(BElements.NOTE)),
                Optional(
                        Accidental(),
                        ACTION(a.domAppendChild())
                ),
                Optional(
                        OctaveMark(),
                        ACTION(a.domAppendChild())
                ),
                BrlNote(),
                ACTION(a.domAppendChild()),
                Optional(
                        DurationDot(),
                        ACTION(a.domAppendChild())
                ),
                Optional(
                        Fingering(),
                        ACTION(a.domAppendChild())
                ),
                Optional(
                        Sequence(
                                TSymbols.TIE,
                                ACTION(a.domPushMatchElement(BElements.TIE)),
                                ACTION(a.domAppendChild())
                        )
                )
        );
    }

    /*
     * Numbers
     */
    public Rule PrefixedUpperNumber() {
        return Sequence(
                TSymbols.NUMBER_SIGN,
                ACTION(a.domPushMatchElement(BElements.PREFIXED_UPPER_NUMBER)),
                UpperNumber(),
                ACTION(a.domAppendChild())
        );
    }

    public Rule UpperNumber() {
        return Sequence(
                OneOrMore(
                        FirstOf(
                                TSymbols.NUMBER_UPPER_ZERO,
                                TSymbols.NUMBER_UPPER_ONE,
                                TSymbols.NUMBER_UPPER_TWO,
                                TSymbols.NUMBER_UPPER_THREE,
                                TSymbols.NUMBER_UPPER_FOUR,
                                TSymbols.NUMBER_UPPER_FIVE,
                                TSymbols.NUMBER_UPPER_SIX,
                                TSymbols.NUMBER_UPPER_SEVEN,
                                TSymbols.NUMBER_UPPER_EIGHT,
                                TSymbols.NUMBER_UPPER_NINE
                        )
                ),
                ACTION(a.domPushMatchElement(BElements.UPPER_NUMBER))
        );

    }

    public Rule LowerNumber() {
        return Sequence(
                OneOrMore(
                        FirstOf(
                                TSymbols.NUMBER_LOWER_ONE,
                                TSymbols.NUMBER_LOWER_TWO,
                                TSymbols.NUMBER_LOWER_THREE,
                                TSymbols.NUMBER_LOWER_FOUR,
                                TSymbols.NUMBER_LOWER_FIVE,
                                TSymbols.NUMBER_LOWER_SIX,
                                TSymbols.NUMBER_LOWER_SEVEN,
                                TSymbols.NUMBER_LOWER_EIGHT,
                                TSymbols.NUMBER_LOWER_NINE
                        )
                ),
                ACTION(a.domPushMatchElement(BElements.LOWER_NUMBER))
        );

    }

    /*
     * layout
     */
    /* 
     * layout 
     */
    public Rule AnyButSpaceAndNewLine() {
        return Sequence(
                OneOrMore(
                        Sequence(
                                TestNot(FirstOf(Spaces(), NewLines())),
                                ANY
                        )
                ),
                ACTION(a.domPushMatchElement(BElements.ANY_BUT_SPACE_AND_NEWLINE))
        );

    }

    public Rule TestLineStart() {
        return Test(ACTION(getContext().getPosition().column == 1));
    }

    public Rule TestLineEnd() {
        return Test(NewLine());
    }

    public Rule LeftPadding() {
        return Sequence(
                TestLineStart(),
                ACTION(a.domPushElement(BElements.LEFT_PADDING)),
                Optional(
                        Spaces(),
                        ACTION(a.domAppendChild())
                )
        );
    }

    public Rule RightPadding() {
        return Sequence(
                ACTION(a.domPushElement(BElements.RIGHT_PADDING)),
                Optional(
                        Spaces(),
                        ACTION(a.domAppendChild())
                ),
                TestLineEnd()
        );
    }
    /*
     * Spacing ang Line breaking
     */

    public Rule Spaces() {
        return Sequence(
                OneOrMore(
                        FirstOf(
                                TSymbols.SPACE, " "
                        )
                ),
                ACTION(a.domPushMatchElement(BElements.SPACES))
        );
    }

    public Rule Space() {
        return Sequence(
                FirstOf(
                        TSymbols.SPACE, " "
                ),
                ACTION(a.domPushMatchElement(BElements.SPACE))
        );
    }

    public Rule NewLine() {
        return Sequence(
                FirstOf('\n', '\r'),
                ACTION(a.domPushMatchElement(BElements.NEW_LINE))
        );

    }

    public Rule NewLines() {
        return Sequence(
                OneOrMore(
                        FirstOf('\n', '\r')
                ),
                ACTION(a.domPushMatchElement(BElements.NEW_LINES))
        );

    }
    /*
     *
     */

    public Rule BlocLayout(Rule r) {
        return Sequence(
                ACTION(a.domPushElement(BElements.BLOC_LAYOUT)),
                LeftPadding(),
                ACTION(a.domAppendChild()),
                r,
                ACTION(a.domAppendChild()),
                RightPadding(),
                ACTION(a.domAppendChild()),
                NewLine(),
                ACTION(a.domAppendChild())
        );
    }

    public Rule InlineLayout(Rule r) {
        return Sequence(
                ACTION(a.domPushElement(BElements.INLINE_LAYOUT)),
                r,
                ACTION(a.domAppendChild())
        );
    }
    /*
     * Measure
     */

    public Rule MeasureLineInterruption() {
        return Sequence(
                ACTION(a.domPushElement(BElements.MEASURE_LINE_INTERRUPTION)),
                Sequence(
                        TSymbols.MUSIC_HYPHEN,
                        ACTION(a.domPushMatchElement(BElements.MUSIC_HYPHEN)),
                        ACTION(a.domAppendChild()),
                        Optional(
                                Spaces(),
                                ACTION(a.domAppendChild())
                        ),
                        NewLine(),
                        ACTION(a.domAppendChild()),
                        Spaces(),
                        ACTION(a.domAppendChild())
                )
        );

    }

    public Rule MeasureSectionnalContinued() {
        return Sequence(
                ACTION(a.domPushElement(BElements.MEASURE_SECTIONNAL_CONTINUED)),
                Sequence(
                        TSymbols.MEASURE_SECTIONNAL_DOUBLE_BAR,
                        ACTION(a.domPushMatchElement(BElements.MEASURE_SECTIONNAL_DOUBLE_BAR)),
                        ACTION(a.domAppendChild()),
                        TSymbols.MUSIC_HYPHEN,
                        ACTION(a.domPushMatchElement(BElements.MUSIC_HYPHEN)),
                        ACTION(a.domAppendChild()),
                        Spaces(),
                        ACTION(a.domAppendChild())
                )
        );

    }

    public Rule SignatureChange() {
        return Sequence(
                ACTION(a.domPushElement(BElements.SIGNATURE_CHANGE)),
                Sequence(
                        KeySig(),
                        ACTION(a.domAppendChild()),
                        TimeSig(),
                        ACTION(a.domAppendChild()),
                        Optional(
                                Spaces(),
                                ACTION(a.domAppendChild())
                        )
                )
        );
    }
//////////////////////////////////////////////

    /**
     * peeks() first upper_number elementof current stack
     * and sets the integer variable value
     */
    public class InterpretPrefixedUpperNumberAction implements Action {

        final Var<Integer> numberValue;

        InterpretPrefixedUpperNumberAction(Var<Integer> numberValue) {
            this.numberValue = numberValue;
        }

        @Override
        public boolean run(Context context) {
            debug("run InterpretPrefixedUpperNumberAction");
            Element pun = (Element) context.getValueStack().peek();
            Element un = pun.getFirstChildElement(BElements.UPPER_NUMBER);
            System.out.println(pun.toXML());
            System.out.println(un.toXML());
            try {
                String num = Nums.bm_upperNumber(un);
                numberValue.set(Integer.parseInt(num));
            } catch (DirectException | NumberFormatException e) {
                e.printStackTrace();
                return false;
            }
            return true;
        }
    }

    public class AddFakeMeasureAction implements Action {

        final Var<Integer> numberOfFullMeasures = new Var<>();

        public boolean setNumber(Integer ni) {
            numberOfFullMeasures.set(ni);
            return true;
        }

        @Override
        public boolean run(Context context) {
            if (numberOfFullMeasures.get() != null) {
                for (int i = 1; i < numberOfFullMeasures.get(); i++) {
                    a.domPushElement(BElements.MEASURE);
                    a.domAppendChild();
                }
            }
            return true;
        }
    }

    public Rule FullMeasureRestsDirect(AddFakeMeasureAction addFakeMeasureAction) {
        return Sequence(
                FirstOf(
                        Sequence(
                                TSymbols.REST_WHOLE,
                                TSymbols.REST_WHOLE,
                                TSymbols.REST_WHOLE,
                                addFakeMeasureAction.setNumber(3)
                        ),
                        Sequence(
                                TSymbols.REST_WHOLE,
                                TSymbols.REST_WHOLE,
                                addFakeMeasureAction.setNumber(2)
                        ),
                        Sequence(
                                TSymbols.REST_WHOLE,
                                addFakeMeasureAction.setNumber(1)
                        )
                ),
                ACTION(a.domPushMatchElement(BElements.DIRECT))
        );
    }

    public Rule FullMeasureRestsNumbered(AddFakeMeasureAction addFakeMeasureAction) {
        Var<Integer> numberOfFullMeasures = new Var<>();
        Action ipuna = new InterpretPrefixedUpperNumberAction(numberOfFullMeasures);
        return Sequence(
                ACTION(a.domPushElement(BElements.NUMBERED)),
                PrefixedUpperNumber(),
                ipuna,
                addFakeMeasureAction.setNumber(numberOfFullMeasures.get()),
                ACTION(a.domAppendChild()),
                TSymbols.REST_WHOLE,
                ACTION(a.domPushMatchElement(BElements.REST))
        );
    }

    /**
     *
     * @return
     */
    public Rule FullMeasureRests(AddFakeMeasureAction addFakeMeasureAction) {
        return Sequence(
                addFakeMeasureAction.setNumber(0),
                ACTION(a.domPushElement(BElements.FULL_MEASURE_RESTS)),
                FirstOf(
                        FullMeasureRestsNumbered(addFakeMeasureAction),
                        FullMeasureRestsDirect(addFakeMeasureAction)
                ),
                ACTION(a.domAppendChild()),
                //       addFakeMeasureAction,
                Test(
                        FirstOf(
                                Sequence(
                                        FirstOf(
                                                Space(),
                                                NewLine(),
                                                MeasureBar()
                                        ),
                                        //pop()
                                        debug(peek().toXML())
                                ),
                                EOI)
                )
        );
    }
///////////////////////////////////////////////////

    public Rule Measure(AddFakeMeasureAction addFakeMeasureAction) {

        return Sequence(
                ACTION(a.domPushElement(BElements.MEASURE)),
                FirstOf(
                        Sequence(
                                FullMeasureRests(addFakeMeasureAction),
                                ACTION(a.domAppendChild())
                        ),
                        OneOrMore(
                                Sequence(
                                        FirstOf(
                                                Note(),
                                                Rest(),
                                                MeasureLineInterruption(),
                                                MeasureSectionnalContinued(),
                                                SignatureChange()
                                        ),
                                        ACTION(a.domAppendChild())
                                )
                        )
                )
        );

    }

    public Rule MelodyLineInterruption() {
        return Sequence(
                ACTION(a.domPushElement(BElements.MELODY_LINE_INTERRUPTION)),
                Sequence(
                        Optional(
                                Spaces(),
                                ACTION(a.domAppendChild())
                        ),
                        NewLine(),
                        ACTION(a.domAppendChild()),
                        Spaces(),
                        ACTION(a.domAppendChild())
                )
        );
    }

    /*
     * oneline version :
     * ------------------
     *           [6]online mood
     *        and tempo indication words[256]
     *      MetronomeMarking[ ]KeyAndTimeSignature
     * MUSIC
     *
     * one-liner "inline" version :
     * ------------------
     *    [6]mood and tempo ind.[256][ ]MetronomeMarking[ ]KeyAndTimeSignature
     * MUSIC
     *
     */
    public Rule Heading() {
        return Sequence(
                a.domPushElement(BElements.HEADING),
                FirstOf(
                        Sequence(
                                BlocLayout(TempoAndMood()),
                                ACTION(a.domAppendChild()),
                                BlocLayout(MetronomeMarkingAndSignatures()),
                                ACTION(a.domAppendChild())
                        ),
                        BlocLayout(
                                Sequence(
                                        TempoAndMood(),
                                        ACTION(a.domAppendChild()),
                                        Spaces(),
                                        ACTION(a.domAppendChild()),
                                        MetronomeMarkingAndSignatures(),
                                        ACTION(a.domAppendChild())
                                )
                        )
                )
        );
    }

    public Rule MusicSegment() {
        AddFakeMeasureAction addFakeMeasureAction = new AddFakeMeasureAction();

        return Sequence(
                ACTION(a.domPushElement(BElements.MUSIC_SEGMENT)),
                MarginalMeasureNum(),
                ACTION(a.domAppendChild()),
                Space(),
                ACTION(a.domAppendChild()),
                ZeroOrMore(
                        Sequence(
                                FirstOf(
                                        //                                        FullMeasureRests(addFakeMeasureAction),
                                        Measure(addFakeMeasureAction),
                                        MelodyLineInterruption(),
                                        MeasureBar()
                                //          SignatureChange()
                                ),
                                ACTION(a.domAppendChild())
                        // TODO don't happend extra empty measures anymore
                        // addFakeMeasureAction
                        )
                )
        );
    }

    public Rule Movement() {
        return Sequence(
                ACTION(a.domPushElement(BElements.MOVEMENT)),
                Heading(),
                ACTION(a.domAppendChild()),
                MusicSegment(),
                ACTION(a.domAppendChild())
        );

    }

    boolean debug(String s) {
        System.out.println(s); // set breakpoint here if required
        return true;
    }

    boolean debug(Var<Integer> i) {
        System.out.println(i.get()); // set breakpoint here if required
        return true;
    }

}
