/*
 * NatBraille Music 
 * Copyright (C) 2014 Vivien Guillet, Bruno Mascret
 * Contact: vivien.guillet@univ-lyon1.fr
 * 
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */

package org.natbraille.music.tbbt;

import nu.xom.Attribute;
import nu.xom.Element;
import nu.xom.Node;
import org.parboiled.Context;
import org.parboiled.ContextAware;


/**
 *
 * @author vivien
 */
public class TParserActions implements ContextAware<Node> {

    Context<Node> context;
    Long counter = 0L;

    private String newId() {
        counter++;
        return counter.toString();
    }


    @Override
    public void setContext(Context<Node> context) {
        this.context = context;
    }


    private static final String ID = "id";

    private void setElementId(Element el) {

        Attribute attr = new Attribute(ID,newId());
        attr.setType(Attribute.Type.ID);
        el.addAttribute(attr);


    }

    public boolean domPushElement(String elementName) {
        //Element el = document.createElement(elementName);
        Element el = new Element(elementName);
        setElementId(el);
        context.getValueStack().push(el);

        //http://stackoverflow.com/questions/3423430/java-xml-dom-how-are-id-attributes-special    
        //el.setIdAttribute(newId(), false);
        //el.setPrefix("elementName");
        return true;
    }

    public boolean domPushMatchElement(String elementName) {
        domPushElement(elementName);
        domPushAttribute("symbol", context.getMatch());
        domSetAttributeNode();
        return true;
    }

    public boolean domPushAttribute(String attributeName, String attributeValue) {
        Attribute attr = new Attribute(attributeName,attributeValue);
        context.getValueStack().push(attr);
        return true;
    }
/*
    public boolean domPushCDATA(String CDATA) {
        
        CDATASection cdata = document.createCDATASection(CDATA);
        context.getValueStack().push(cdata);
        return true;
    }

    public boolean domPushText(String text) {
        Text t = document.createTextNode(text);
        context.getValueStack().push(t);
        return true;
    }
*/
    public boolean domAppendChild() {
        Node n0 = (Node) context.getValueStack().pop();
        Element n1 = (Element) context.getValueStack().pop();
        
        n1.appendChild(n0);
        context.getValueStack().push(n1);
        return true;
    }

    public boolean domSetAttributeNode() {
        Attribute n0 = (Attribute) context.getValueStack().pop();
        Element n1 = (Element) context.getValueStack().pop();        
        n1.addAttribute(n0);
        context.getValueStack().push(n1);
        return true;
    }
}
