/*
 * NatBraille Music 
 * Copyright (C) 2014 Vivien Guillet, Bruno Mascret
 * Contact: vivien.guillet@univ-lyon1.fr
 * 
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */

package org.natbraille.music.tbbt;

import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import nu.xom.Element;
import nu.xom.Node;
import org.parboiled.Rule;
import org.parboiled.errors.ParseError;
import org.parboiled.parserunners.ParseRunner;
import org.parboiled.parserunners.ReportingParseRunner;
import org.parboiled.parserunners.TracingParseRunner;
import org.parboiled.support.ParseTreeUtils;
import org.parboiled.support.ParsingResult;
import org.parboiled.support.ValueStack;

/**
 * uses paraboiled
 * <p>
 * @author vivien
 */
public class TParserHelper {

    private final static Logger L = Logger.getLogger(TParserHelper.class.getName());

    private static final TParser parser = org.parboiled.Parboiled.createParser(TParser.class);

    public static TParser getParser() {        
        return parser;
    }

    private static ParseRunner newParseRunner(Rule startRule, boolean dbg) {
        ParseRunner parseRunner;
        if (dbg) {
            TracingParseRunner tpr = new TracingParseRunner(startRule);            
            parseRunner = tpr;
            
        } else {
            ReportingParseRunner rpr = new ReportingParseRunner(startRule);
            parseRunner = rpr;
        }
        
        return parseRunner;
    }

    public static Node parse(String docString, Rule startRule, Boolean debug) {

        Element resuRoot = new Element("root");

        // builderparser
     //   L.log(Level.INFO, "building parser:");
        ParseRunner parseRunner = newParseRunner(startRule, debug);

        // parsing
      //  L.log(Level.INFO, "parsing:");
        ParsingResult parsingResult = parseRunner.run(docString);

        // parsing errors
        if (!parsingResult.parseErrors.isEmpty()) {
            for (ParseError a : ((List<ParseError>) parsingResult.parseErrors)) {
                L.log(Level.SEVERE, "parsing error at position {0} ... {1} : {2}",
                        new Object[]{a.getStartIndex(), a.getEndIndex(), a.getErrorMessage()});

            }
        } else {
            L.log(Level.INFO, "parsing without error");
        }

        // treenode 
       // L.log(Level.INFO, "treenode: ");
        String printTreeNode = ParseTreeUtils.printNodeTree(parsingResult);
        if (!printTreeNode.isEmpty()) {
            L.log(Level.INFO, printTreeNode);
        } else {
         //   L.log(Level.SEVERE, "treenode is empty");
        }

        // look for tree in valuestack
        //System.err.println("valuestack items:");
        ValueStack<Node> vs = parsingResult.valueStack;
        if (vs.isEmpty()) {
            L.log(Level.SEVERE, "valueStack is empty");
        } else {
            for (int i = 0; i < parsingResult.valueStack.size(); i++) {
                Node n = vs.peek();
                L.log(Level.SEVERE, "* valueStack item # {0} : {1}", new Object[]{i, n});
                resuRoot.appendChild(n);
            }
        }
        return resuRoot;

    }

}
