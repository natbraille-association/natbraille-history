/*
 * NatBraille Music 
 * Copyright (C) 2014 Vivien Guillet, Bruno Mascret
 * Contact: vivien.guillet@univ-lyon1.fr
 * 
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */

package org.natbraille.music.tbbt;

import java.io.IOException;
import nu.xom.Node;
import nu.xom.Nodes;
import nu.xom.ParsingException;

/**
 *
 * @author vivien
 */
public class Tbbt {
  //------------------------------------
    public static Node tb_musicLines(String s, Boolean debug) throws ParsingException, IOException {
        TParser bmParser = TParserHelper.getParser();
        Node rootNode = TParserHelper.parse(s, bmParser.MusicSegment(),debug);
        return rootNode;
    }
    public static Node tb_movement(String s,Boolean debug) throws ParsingException, IOException {
        TParser bmParser = TParserHelper.getParser();
        Node rootNode = TParserHelper.parse(s, bmParser.Movement(),debug);
        return rootNode;
    }

    public static String bt_root(Node node) {
        Nodes nxps = node.query("//*/@symbol");
        StringBuilder sb = new StringBuilder();
        for (int nxpIdx = 0; nxpIdx < nxps.size(); nxpIdx++) {
            Node nxp = nxps.get(nxpIdx);
            sb.append(nxp.getValue());
        }
        return sb.toString();
    }   
}
