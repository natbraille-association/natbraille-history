/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.natbraille.music.ui;

import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.KeyListener;
import java.beans.PropertyChangeListener;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.AbstractAction;
import javax.swing.Action;
import javax.swing.ActionMap;
import javax.swing.InputMap;
import javax.swing.KeyStroke;
import javax.swing.text.BadLocationException;
import javax.swing.text.Style;
import javax.swing.text.StyleConstants;
import javax.swing.text.StyleContext;
import javax.swing.text.StyledDocument;
import nu.xom.Element;
import nu.xom.Node;
import nu.xom.Nodes;
import nu.xom.ParsingException;
import org.natbraille.music.braillemusicxml.BElements;
import org.natbraille.music.braillemusicxml.BTFuncts;
import org.natbraille.music.braillesamples.IBMTExamples;
import org.natbraille.music.tbbt.Tbbt;

/**
 *
 * @author vivien
 */
public class bmxEdit extends javax.swing.JFrame {

    private StyledDocument styledDoc;
    private Node rootNode;
    private Perkins perkins = new Perkins();
    /*
     {
     perkins.setPerkinsListener(new Perkins.PerkinsListener() {

     @Override
     public void PerkinsEvent(Boolean[] points) {
     for (int i = 0; i < points.length; i++) {
     System.err.print(points[i]);
     System.err.print(" ");
     }
     System.err.println("");
     }
     });
     }
     */

    /**
     * Creates new form bmxEdit
     */
    public static void bt_root2(Node node, StyledDocument styledDoc) throws BadLocationException {

        styledDoc.remove(0, styledDoc.getLength());
        Nodes nxps = node.query("//*/@symbol");
        StringBuilder sb = new StringBuilder();
        for (int nxpIdx = 0; nxpIdx < nxps.size(); nxpIdx++) {
            Node nxp = nxps.get(nxpIdx);

            Element parent = null;
            parent = (Element) nxp.getParent();

            Style s = styledDoc.getStyle(parent.getLocalName());
            if (s == null) {
                Element grandParent = null;
                if (parent.getParent() != null) {
                    grandParent = ((Element) parent.getParent());
                    s = styledDoc.getStyle(grandParent.getLocalName());
                }
                if (s == null) {
                    Element grandgrandParent = null;
                    if (grandParent.getParent() != null) {
                        grandgrandParent = ((Element) grandParent.getParent());
                        s = styledDoc.getStyle(grandgrandParent.getLocalName());
                    }

                }
                if (s == null) {
                    System.err.println("missin sty for " + parent.getLocalName());
                    s = styledDoc.getStyle("base");
                }
            }

            styledDoc.insertString(sb.length(), nxp.getValue(), s);

            sb.append(nxp.getValue());
        }

    }

    public bmxEdit() {

        initComponents();

        for (KeyListener kl : jTextPaneBmx.getKeyListeners()) {
            //  jTextPaneBmx.removeKeyListener(kl);
        }
        InputMap im = jTextPaneBmx.getInputMap();
        //im.clear();
        for (KeyStroke k : im.allKeys()) {
            System.err.println("ks:" + k);
        }

        ActionMap am = jTextPaneBmx.getActionMap();
        Action insertContentAction = new AbstractAction() {
            @Override
            public void actionPerformed(ActionEvent ae) {
                System.out.println("action performed " + ae);
            }
        };

        for (Object ami : am.allKeys()) {
            Action a = am.get(ami);
            //System.err.println("amap item :" + ami + "\t" + ami.getClass() + "\t" + a);
            am.put(ami, insertContentAction);
            //am.remove(ami);
        }

        // define styles
        StyledDocument styledDoc = jTextPaneBmx.getStyledDocument();

        Style def = StyleContext.getDefaultStyleContext().getStyle(StyleContext.DEFAULT_STYLE);

        Style baseStyle = styledDoc.addStyle("base", def);
        StyleConstants.setFontSize(baseStyle, 20);

        {
            Style style = styledDoc.addStyle(BElements.BRLNOTE, baseStyle);

            StyleConstants.setForeground(style, Color.BLACK);
            StyleConstants.setBackground(style, Color.PINK);
        }
        {
            Style style = styledDoc.addStyle(BElements.DOTS, baseStyle);

            StyleConstants.setForeground(style, Color.MAGENTA);
            StyleConstants.setBackground(style, Color.PINK);
        }
        {
            Style style = styledDoc.addStyle(BElements.REST, baseStyle);

            StyleConstants.setForeground(style, Color.GRAY);
            StyleConstants.setBackground(style, Color.PINK);
        }
        {
            Style style = styledDoc.addStyle(BElements.MELODY_LINE_INTERRUPTION, baseStyle);

            StyleConstants.setForeground(style, Color.GRAY);
            StyleConstants.setBackground(style, Color.YELLOW);
        }
        {
            Style style = styledDoc.addStyle(BElements.MARGINAL_MEASURE_NUMBER, baseStyle);

            StyleConstants.setForeground(style, Color.GREEN);
            StyleConstants.setBackground(style, Color.BLACK);
        }
        {
            Style style = styledDoc.addStyle(BElements.MEASURE_BAR, baseStyle);

            StyleConstants.setForeground(style, Color.green);
            StyleConstants.setBackground(style, Color.orange);
        }
        this.styledDoc = styledDoc;
        // write doc;
        jTextPaneBmx.setCaretPosition(0);

        //jTextPaneBmx.setCaretColor(Color.orange);
    }

    /**
     * This method is called from within the constructor to
     * initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is
     * always regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jScrollPane1 = new javax.swing.JScrollPane();
        jTextPaneBmx = new javax.swing.JTextPane();
        jTextFieldName = new javax.swing.JTextField();
        jButton1 = new javax.swing.JButton();
        jTextFieldXPath = new javax.swing.JTextField();
        jTextFieldXPath2 = new javax.swing.JTextField();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);

        jTextPaneBmx.addCaretListener(new javax.swing.event.CaretListener() {
            public void caretUpdate(javax.swing.event.CaretEvent evt) {
                jTextPaneBmxCaretUpdate(evt);
            }
        });
        jTextPaneBmx.addInputMethodListener(new java.awt.event.InputMethodListener() {
            public void inputMethodTextChanged(java.awt.event.InputMethodEvent evt) {
                jTextPaneBmxInputMethodTextChanged(evt);
            }
            public void caretPositionChanged(java.awt.event.InputMethodEvent evt) {
            }
        });
        jTextPaneBmx.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                jTextPaneBmxKeyPressed(evt);
            }
            public void keyReleased(java.awt.event.KeyEvent evt) {
                jTextPaneBmxKeyReleased(evt);
            }
        });
        jScrollPane1.setViewportView(jTextPaneBmx);

        jTextFieldName.setText("3-6.unicode");
        jTextFieldName.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jTextFieldNameActionPerformed(evt);
            }
        });
        jTextFieldName.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyTyped(java.awt.event.KeyEvent evt) {
                jTextFieldNameKeyTyped(evt);
            }
        });

        jButton1.setText("jButton1");
        jButton1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton1ActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(jTextFieldXPath2)
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(jTextFieldName, javax.swing.GroupLayout.DEFAULT_SIZE, 655, Short.MAX_VALUE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jButton1))
                    .addComponent(jTextFieldXPath)
                    .addComponent(jScrollPane1, javax.swing.GroupLayout.Alignment.LEADING))
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jTextFieldName, javax.swing.GroupLayout.DEFAULT_SIZE, 35, Short.MAX_VALUE)
                    .addComponent(jButton1))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 463, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jTextFieldXPath, javax.swing.GroupLayout.PREFERRED_SIZE, 31, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jTextFieldXPath2, javax.swing.GroupLayout.PREFERRED_SIZE, 29, javax.swing.GroupLayout.PREFERRED_SIZE))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void jTextFieldNameActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jTextFieldNameActionPerformed


    }//GEN-LAST:event_jTextFieldNameActionPerformed

    private void jTextFieldNameKeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_jTextFieldNameKeyTyped

        // read document

    }//GEN-LAST:event_jTextFieldNameKeyTyped

    private void jButton1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton1ActionPerformed
        // TODO add your handling code here:
        try {
            String name = jTextFieldName.getText();
            String s = IBMTExamples.get(name);//"3-6.unicode");
            Node rootNode = Tbbt.tb_musicLines(s, false);
            bt_root2(rootNode, styledDoc);
            this.rootNode = rootNode;

        } catch (ParsingException ex) {
            Logger.getLogger(bmxEdit.class
                    .getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(bmxEdit.class
                    .getName()).log(Level.SEVERE, null, ex);
        } catch (BadLocationException ex) {
            Logger.getLogger(bmxEdit.class
                    .getName()).log(Level.SEVERE, null, ex);
        } // TODO add your handling code here:
    }//GEN-LAST:event_jButton1ActionPerformed

    private void jTextPaneBmxCaretUpdate(javax.swing.event.CaretEvent evt) {//GEN-FIRST:event_jTextPaneBmxCaretUpdate
        // TODO add your handling code here:

        if (rootNode != null) {
            Element el = BTFuncts.getElementForOffset(rootNode, evt.getDot());
            if (el != null) {
                jTextFieldXPath.setText(BTFuncts.getNodePath(el));
            } else {
                jTextFieldXPath.setText("");
            }

            Element el2 = BTFuncts.getElementForOffset(rootNode, evt.getMark());
            if (el2 != null) {
                jTextFieldXPath2.setText(BTFuncts.getNodePath(el2));
            } else {
                jTextFieldXPath2.setText("");
            }
        }


    }//GEN-LAST:event_jTextPaneBmxCaretUpdate

    private void jTextPaneBmxInputMethodTextChanged(java.awt.event.InputMethodEvent evt) {//GEN-FIRST:event_jTextPaneBmxInputMethodTextChanged
        // TODO add your handling code here:

    }//GEN-LAST:event_jTextPaneBmxInputMethodTextChanged

    public static class Perkins {

        public static interface PerkinsListener {

            public void PerkinsEvent(Boolean points[]);
        }

        private Integer keyCodes[] = {65, 90, 69, 73, 79, 80, 18};
        private Boolean points[] = {false, false, false, false, false, false};
        private PerkinsListener perkinsListener = null;

        public void setPerkinsListener(PerkinsListener perkinsListener) {
            this.perkinsListener = perkinsListener;
        }

        public void pushed(Integer keyCode) {
            for (int i = 0; i < keyCodes.length; i++) {
                if (keyCode == keyCodes[i]) {
                    points[i] = true;
                }
            }
        }

        public void released(Integer keyCode) {
            for (int i = 0; i < keyCodes.length; i++) {
                if (keyCode == keyCodes[i]) {
                    if (perkinsListener != null) {
                        if (points[i] == true) {
                            perkinsListener.PerkinsEvent(points);
                        }
                    }
                    for (int j = 0; j < points.length; j++) {
                        points[j] = false;
                    }
                    return;
                }
            }

        }

    };

    private void jTextPaneBmxKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_jTextPaneBmxKeyPressed
        perkins.pushed(evt.getKeyCode());
    }//GEN-LAST:event_jTextPaneBmxKeyPressed

    private void jTextPaneBmxKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_jTextPaneBmxKeyReleased
        perkins.released(evt.getKeyCode());// TODO add your handling code here:
    }//GEN-LAST:event_jTextPaneBmxKeyReleased

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {


        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new bmxEdit().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton jButton1;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JTextField jTextFieldName;
    private javax.swing.JTextField jTextFieldXPath;
    private javax.swing.JTextField jTextFieldXPath2;
    private javax.swing.JTextPane jTextPaneBmx;
    // End of variables declaration//GEN-END:variables
}
