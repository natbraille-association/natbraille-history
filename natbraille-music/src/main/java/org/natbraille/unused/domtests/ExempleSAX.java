/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.natbraille.unused.domtests;

/**
 *
 * @author vivien
 */
import java.io.File;
import java.io.IOException;
import java.util.Stack;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;
import org.xml.sax.Attributes;
import org.xml.sax.SAXException;
import org.xml.sax.ext.Attributes2;
import org.xml.sax.ext.Attributes2Impl;
import org.xml.sax.helpers.DefaultHandler;

public class ExempleSAX {

    public class MonHandler extends DefaultHandler {

        public StringBuilder Oh = new StringBuilder();

        public Stack<String> qNameStack = new Stack<>();
        public Stack<Attributes2> attributesStack = new Stack<>();
        public Stack<String> charsStack = new Stack<>();

        @Override
        public void endDocument() throws SAXException {
            super.endDocument(); //To change body of generated methods, choose Tools | Templates.
        }

        @Override
        public void startDocument() throws SAXException {
            super.startDocument(); //To change body of generated methods, choose Tools | Templates.
        }

        @Override
        public void startElement(String uri, String localName, String qName, Attributes atrbts) throws SAXException {

            qNameStack.push(qName);
            Attributes2 atrb = new Attributes2Impl(atrbts);
            attributesStack.push(atrb);
            charsStack.push("");

            //  super.startElement(uri, localName, qName, attributesStack);
        }

        @Override
        public void endElement(String uri, String localName, String qName) throws SAXException {

            for (int i = 0; i < qNameStack.size(); i++) {
                Oh.append(qNameStack.get(i));
                Attributes2 a2 = attributesStack.get(i);

                for (int y = 0; y < a2.getLength(); y++) {
                    Oh.append("[");
                    Oh.append(a2.getQName((y)));
                    Oh.append(":");
                    Oh.append(a2.getValue(y));
                    Oh.append("]");
                }
                if (charsStack.get(i).isEmpty()){
                    
                } else {
                Oh.append("(");
                Oh.append(charsStack.get(i));
                Oh.append(")");
                }
                Oh.append("/");
            }
            Oh.append("\n");

            qNameStack.pop();
            attributesStack.pop();
            charsStack.pop();  
            //  super.endElement(uri, localName, qName);
        }

        @Override
        public void characters(char[] chars, int start, int length) throws SAXException {
            charsStack.pop();
            charsStack.push(new String(chars).substring(start,  start + length ));

          //  super.characters(chars, start, length);
        }

        public StringBuilder getOh() {
            return Oh;
        }

    }
    static final File BACH = new File("bachxml/000106B_.xml");
    static final File BACH2 = new File("bachxml/Fantasia_con_imitazione_BWV563.xml");
    
    public static void main(String[] args) {

        try {
            // création d'une fabrique de parseurs SAX
            SAXParserFactory fabrique = SAXParserFactory.newInstance();

            // création d'un parseur SAX
            SAXParser parseur = fabrique.newSAXParser();

            // lecture d'un fichier XML avec un DefaultHandler
            File fichier = BACH2;

            MonHandler gestionnaire;

            System.err.println("1");
            gestionnaire = new ExempleSAX().new MonHandler();
            parseur.parse(fichier, gestionnaire);

            System.err.println(gestionnaire.Oh);
        } catch (ParserConfigurationException pce) {
            System.out.println("Erreur de configuration du parseur");
            System.out.println("Lors de l'appel à newSAXParser()");
        } catch (SAXException se) {
            System.out.println("Erreur de parsing");
            System.out.println("Lors de l'appel à parse()");
        } catch (IOException ioe) {
            System.out.println("Erreur d'entrée/sortie");
            System.out.println("Lors de l'appel à parse()");
        }
    }
    /*
     {
     doc.braille(options).setchar(14,'c');
     doc.braille(options).getChar(pageNum,lineNum, charNum);
    
     docBrailleView = doc.getBrailleView(options);
     docBrailleView.setChar(14,'c');
    
     docBraillePrintView = doc.getBraillePrintView(options);
     docBraillePrintView.setChar(pageNum,lineNum,charNum);
    
    
    
    
    
     }
     */
}
