/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.natbraille.unused.domtests;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Callable;
import java.util.concurrent.CompletionService;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorCompletionService;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;

/**
 *
 * @author vivien
 */
public class Futurezz {

    public class Task implements Callable<Integer> {

        private final int sleepTime;

        public Task(int n) {
            sleepTime = n;
        }

        @Override
        public Integer call() throws Exception {
            Thread.sleep(1000 * sleepTime);
            return sleepTime;
        }
    }

    public static void main(String[] args) {
        List<Callable<Integer>> taches = new ArrayList<>();

        Futurezz proute = new Futurezz();
        
        Callable<Integer> tache1 = proute.new Task(1);
        Callable<Integer> tache2 = proute.new Task(5);
        Callable<Integer> tache3 = proute.new Task(10);
        Callable<Integer> tache4 = proute.new Task(2);

        taches.add(tache1);
        taches.add(tache2);
        taches.add(tache3);
        taches.add(tache4);

        ExecutorService executorServices = Executors.newFixedThreadPool(10);

        resoudre(executorServices, taches);
    }

    public static void resoudre(final ExecutorService executor, List<Callable<Integer>> taches) {
        //Le service de terminaison
        CompletionService<Integer> completionService = new ExecutorCompletionService<Integer>(executor);

        //une liste de Future pour récupérer les résultats
        List<Future<Integer>> futures = new ArrayList<Future<Integer>>();

        Integer res = null;
        try {
            //On soumet toutes les tâches à l'executor
            for (Callable<Integer> t : taches) {
                futures.add(completionService.submit(t));
            }

            for (int i = 0; i < taches.size(); ++i) {
                try {
                               //On récupère le premier résultat disponible
                    //sous la forme d'un Future avec take(). Puis l'appel
                    //à get() nous donne le résultat du Callable.
                    res = completionService.take().get();
                    if (res != null) {
                        //On affiche le resultat de la tâche
                        System.out.println(res);
                    }
                } catch (ExecutionException ignore) {
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            executor.shutdown();
        }
    }
}
