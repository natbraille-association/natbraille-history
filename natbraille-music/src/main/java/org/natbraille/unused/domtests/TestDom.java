/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.natbraille.unused.domtests;

import java.io.File;
import java.io.IOException;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.xpath.XPath;
import javax.xml.xpath.XPathConstants;
import javax.xml.xpath.XPathExpressionException;
import javax.xml.xpath.XPathFactory;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

/**
 *
 * @author vivien
 */
public class TestDom {

    static final File BACH = new File("bachxml/000106B_.xml");
    static final File BACH2 = new File("bachxml/Fantasia_con_imitazione_BWV563.xml");

    private static XPath xPath = XPathFactory.newInstance().newXPath();

    private static void xploremeasure(Element measure) {
        NodeList measureEls = measure.getChildNodes();

        for (int j = 0; j < measureEls.getLength(); j++) {
            Node o = measureEls.item(j);
            short nodetype = o.getNodeType();
            if (nodetype == Node.ELEMENT_NODE) {
                Element measureEl = (Element) measureEls.item(j);
                System.err.println(measureEl.getNodeName());
            } else {
                //System.err.println(o);
            }

        }

    }

    public static void main(String[] args) throws ParserConfigurationException, SAXException, IOException, XPathExpressionException {
        Document document = null;
        DocumentBuilderFactory factory = null;

        factory = DocumentBuilderFactory.newInstance();
        factory.setValidating(false);
        factory.setSchema(null);
        factory.setNamespaceAware(false);
        factory.setValidating(false);
        factory.setFeature("http://xml.org/sax/features/namespaces", false);
        factory.setFeature("http://xml.org/sax/features/validation", false);
        factory.setFeature("http://apache.org/xml/features/nonvalidating/load-dtd-grammar", false);
        factory.setFeature("http://apache.org/xml/features/nonvalidating/load-external-dtd", false);

        DocumentBuilder builder = factory.newDocumentBuilder();
        document = builder.parse(BACH2);

        NodeList scoreParts = (NodeList) xPath.evaluate("//score-part", document.getDocumentElement(), XPathConstants.NODESET);
        for (int i = 0; i < scoreParts.getLength(); ++i) {

            Element scorePart = (Element) scoreParts.item(i);
            String partId = scorePart.getAttribute("id");
            String partName = scorePart.getAttribute("name");
            scorePart.getElementsByTagName("part-name");

            System.err.println(scorePart.getNodeName() + " " + partId + " " + partName);

            //NodeList part = (NodeList) xPath.evaluate("//part[@id=" + partId + "]", document.getDocumentElement(), XPathConstants.NODESET);
            NodeList measures = (NodeList) xPath.evaluate("//part[@id=\"" + partId + "\"]/*", document.getDocumentElement(), XPathConstants.NODESET);
            for (int j = 0; j < measures.getLength(); j++) {
                Element measure = (Element) measures.item(j);
                String measureNumber = measure.getAttribute("number");
                System.err.println(measure.getNodeName() + " " + measureNumber);
                xploremeasure(measure);
            }

        }
    }

}