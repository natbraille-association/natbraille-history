/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.natbraille.unused.domtests;

import nu.xom.Attribute;
import nu.xom.Builder;
import nu.xom.Document;
import nu.xom.Element;
import nu.xom.Nodes;
/*
 * @author vivien
 */
public class XOMe {
    

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here
        Element root = new Element("root");
        Document doc = new Document(root);
        
        MyXomNodeFactory  nodeFac = new MyXomNodeFactory();
        Builder builder = new Builder(nodeFac);
        
        
        
        
        
        
        Element e1 = new Element("e1");
        Element e2 = new Element("e2");
        Element e3 = new Element("e3");
        
        root.appendChild(e1);
        Attribute attr = new Attribute("id", "monid");
        attr.setType(Attribute.Type.ID);
        e1.addAttribute(attr);
        e1.appendChild("text");
        e1.appendChild(e2);
        e1.appendChild(e3);
        
        
        
        //Nodes xpath  = root.query("//*[@id='monid']/@id");
        Nodes xpath  = root.query("//*[@id='monid']");
        for (int i=0;i<xpath.size();i++){
            System.out.println("result "+xpath.get(i));
        }
        
        Nodes xpath3  = root.query("//*/text()");
        for (int i=0;i<xpath3.size();i++){
            System.out.println("result "+xpath3.get(i));
        }
        System.err.println("-------");
        Nodes xpath4  = e1.query("*");
        for (int i=0;i<xpath4.size();i++){
            System.out.println("result "+xpath4.get(i));
            System.out.println("-->"+xpath4.get(i).getValue());
        }
        
        
        String result = doc.toXML();
        System.out.println(result);
    }

}
