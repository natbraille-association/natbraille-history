/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package org.natbraille.unused.machinery;

/**
 *
 * @author vivien
 */
public class BrailleMusDoc extends MusDoc {

        private final StringBuilder theString = new StringBuilder();

        private BrailleMusEditor editor = null;

        @Override
        public BrailleMusEditor getEditor() {
            if (editor == null) {
                editor = new BrailleMusEditor();
            }
            return editor;
        }

        public class BrailleMusEditor extends MusEditor {

            public String getString(){
                return theString.toString();
            }
            @Override
            public void doEdit(MusEditionEvent editionEvent) {
                if (editionEvent instanceof BrailleMusEditionEvent.Insert) {
                    BrailleMusEditionEvent.Insert e = (BrailleMusEditionEvent.Insert) editionEvent;
                    theString.insert(e.position, e.string);

                } else if (editionEvent instanceof BrailleMusEditionEvent.Append) {
                    BrailleMusEditionEvent.Append e = (BrailleMusEditionEvent.Append) editionEvent;
                    theString.append(e.string);

                } else if (editionEvent instanceof BrailleMusEditionEvent.Remove) {
                    BrailleMusEditionEvent.Remove e = (BrailleMusEditionEvent.Remove) editionEvent;
                    theString.delete(e.start, e.end);

                } else if (editionEvent instanceof BrailleMusEditionEvent.Replace) {
                    BrailleMusEditionEvent.Replace e = (BrailleMusEditionEvent.Replace) editionEvent;
                    theString.replace(e.start, e.end, e.string);
                }
            }

            @Override
            protected MusEditListenerList getListenerList() {
                return listeners;
            }

        }
    }
