/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.natbraille.unused.machinery;

/**
 *
 * @author vivien
 */
public class BrailleMusEditionEvent {

    public class Insert extends MusEditionEvent  {

        public final String string;
        public final Integer position;

        public Insert(BrailleMusDoc doc, Integer position, String string) {
            super(MusEditionEvent.Order.DO_THEN_TELL, doc);
            this.string = string;
            this.position = position;
        }
    }

    public class Append extends MusEditionEvent {

        public final String string;

        public Append(BrailleMusDoc doc, String string) {
            super(MusEditionEvent.Order.DO_THEN_TELL, doc);
            this.string = string;
        }
    }

    public class Remove extends MusEditionEvent {

        public final Integer start;
        public final Integer end;
        public final String removedString;

        public Remove(BrailleMusDoc doc, Integer start, Integer end) {
            super(MusEditionEvent.Order.DO_THEN_TELL, doc);
            this.removedString = doc.getEditor().getString().substring(start, end);
            this.start = start;
            this.end = end;
        }
    }

    public class Replace extends MusEditionEvent {

        public final Integer start;
        public final Integer end;
        public final String string;
        public final String replacedString;
        
        public Replace(BrailleMusDoc doc, Integer start, Integer end, String string) {
            super(MusEditionEvent.Order.DO_THEN_TELL, doc);
            this.replacedString = doc.getEditor().getString().substring(start, end);
            this.start = start;
            this.end = end;
            this.string = string;
        }

    }

}
