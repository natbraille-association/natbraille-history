/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.natbraille.unused.machinery;

import nu.xom.Element;

/**
 *
 * @author vivien
 */
public class BrailleXmlMusDoc extends MusDoc {

    private final Element rootNode = new Element("BrailleXmlRoot");
    private BrailleXmlMusEditor editor;

    public Element getRootNode() {
        return rootNode;
    }

    @Override
    public BrailleXmlMusEditor getEditor() {
        if (editor == null) {
            editor = new BrailleXmlMusDoc.BrailleXmlMusEditor();
        }
        return editor;
    }

    public class BrailleXmlMusEditor extends MusEditor {

        Element getRootNode() {
            return BrailleXmlMusDoc.this.rootNode;

        }

        @Override
        public void doEdit(MusEditionEvent editionEvent) {
            if (editionEvent instanceof BrailleXmlMusEditionEvent.Detach) {
                BrailleXmlMusEditionEvent.Detach e = (BrailleXmlMusEditionEvent.Detach) editionEvent;
                e.node.detach();
            } else if (editionEvent instanceof BrailleXmlMusEditionEvent.RemoveChildren) {
                BrailleXmlMusEditionEvent.RemoveChildren e = (BrailleXmlMusEditionEvent.RemoveChildren) editionEvent;
                e.node.removeChildren();
            } else if (editionEvent instanceof BrailleXmlMusEditionEvent.AppendChild) {
                BrailleXmlMusEditionEvent.AppendChild e = (BrailleXmlMusEditionEvent.AppendChild) editionEvent;
                e.node.appendChild(e.child);
            } else if (editionEvent instanceof BrailleXmlMusEditionEvent.InsertChild) {
                BrailleXmlMusEditionEvent.InsertChild e = (BrailleXmlMusEditionEvent.InsertChild) editionEvent;
                e.node.insertChild(e.child, e.position);
                // ======> e.node.insertBefore(e.child)
            } else if (editionEvent instanceof BrailleXmlMusEditionEvent.AppendTextChild) {
                BrailleXmlMusEditionEvent.AppendTextChild e = (BrailleXmlMusEditionEvent.AppendTextChild) editionEvent;
                e.node.appendChild(e.text);
            } else if (editionEvent instanceof BrailleXmlMusEditionEvent.InsertTextChild) {
                BrailleXmlMusEditionEvent.InsertTextChild e = (BrailleXmlMusEditionEvent.InsertTextChild) editionEvent;
                e.node.insertChild(e.text, e.position);
            } else if (editionEvent instanceof BrailleXmlMusEditionEvent.RemoveAttribute) {
                BrailleXmlMusEditionEvent.RemoveAttribute e = (BrailleXmlMusEditionEvent.RemoveAttribute) editionEvent;
                e.node.removeAttribute(e.attribute);
            } else if (editionEvent instanceof BrailleXmlMusEditionEvent.AddAttribute) {
                BrailleXmlMusEditionEvent.AddAttribute e = (BrailleXmlMusEditionEvent.AddAttribute) editionEvent;
                e.node.addAttribute(e.attribute);
            }

        }

        @Override
        protected MusEditListenerList getListenerList() {
            return listeners;
        }
    }
}
