/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package org.natbraille.unused.machinery;

import nu.xom.Attribute;
import nu.xom.Element;
import nu.xom.Node;

/**
 *
 * @author vivien
 */
public class BrailleXmlMusEditionEvent {
 
    ///
    ///
    public class Detach extends MusEditionEvent {

        public final Element node;

        public Detach(BrailleXmlMusDoc doc, Element node) {
            super(MusEditionEvent.Order.TELL_THEN_DO, doc);
            
            this.node = node;
        }

    }

    public class RemoveChildren extends MusEditionEvent {

        public final Element node;
        public final Element originalNode;

        public RemoveChildren(BrailleXmlMusDoc doc, Element node) {
            super(MusEditionEvent.Order.DO_THEN_TELL, doc);
            this.node = node;
            this.originalNode = (Element) node.copy();
        }
    }

    public class AppendChild extends MusEditionEvent {

        public final Element node;
        public final Node child;

        public AppendChild(MusDoc doc, Element parent, Node child) {
            super(MusEditionEvent.Order.DO_THEN_TELL, doc);
            this.node = parent;
            this.child = child;
        }
    }

    public class InsertChild extends MusEditionEvent {

        public final Element node;
        public final Integer position;
        public final Node child;

        public InsertChild(MusDoc doc, Element node, Integer position, Node child) {
            super(MusEditionEvent.Order.DO_THEN_TELL, doc);
            this.node = node;
            this.position = position;
            this.child = child;
        }

    }

    public class AppendTextChild extends MusEditionEvent {

        public final Element node;
        public final String text;

        public AppendTextChild(MusDoc doc, Element parent, String text) {
            super(MusEditionEvent.Order.DO_THEN_TELL, doc);
            this.node = parent;
            this.text = text;
        }
    }

    public class InsertTextChild extends MusEditionEvent {

        public final Element node;
        public final Integer position;
        public final String text;

        public InsertTextChild(MusDoc doc, Element node, Integer position, String text) {
            super(MusEditionEvent.Order.DO_THEN_TELL, doc);
            this.node = node;
            this.position = position;
            this.text = text;
        }

    }

    public class RemoveAttribute extends MusEditionEvent {

        public final Element node;
        public final Attribute attribute;

        public RemoveAttribute(MusDoc doc, Element node, Attribute attribute) {
            super(MusEditionEvent.Order.DO_THEN_TELL, doc);
            this.node = node;
            this.attribute = attribute;
        }

    }

    public class AddAttribute extends MusEditionEvent {

        public final Element node;
        public final Attribute attribute;

        public AddAttribute(MusDoc doc, Element node, Attribute attribute) {
            super(MusEditionEvent.Order.DO_THEN_TELL, doc);
            this.node = node;
            this.attribute = attribute;
        }

    }
   
}
