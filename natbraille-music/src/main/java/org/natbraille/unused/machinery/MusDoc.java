/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.natbraille.unused.machinery;

/**
 *
 * @author vivien
 */
public abstract class MusDoc {

    public final MusEditListenerList listeners = new MusEditListenerList();

    public abstract MusEditor getEditor();
}
