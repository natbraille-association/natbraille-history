/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package org.natbraille.unused.machinery;

import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author vivien
 */
public final class MusEditListenerList {

        List<MusListener> listeners = new ArrayList<>();

        public void addListener(MusListener listener) {
            listeners.add(listener);
        }

        public void removeListener(MusListener listener) {
            listeners.remove(listener);
        }

        public void broadcast(MusEditionEvent e) {
            for (MusListener listener : listeners) {
                listener.edited(e);
            }
        }
    }
