/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.natbraille.unused.machinery;

/**
 *
 * @author vivien
 */
public abstract class MusEditionEvent<X extends MusDoc> {

    // eidition event 
    public static enum Order {

        DO_THEN_TELL,
        TELL_THEN_DO
    };
    public final X musDoc;
    public final Order order;

    public MusEditionEvent(Order order, X musDoc) {
        this.order = order;
        this.musDoc = musDoc;
    }
}
