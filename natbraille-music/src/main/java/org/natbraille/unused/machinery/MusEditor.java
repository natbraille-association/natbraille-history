/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.natbraille.unused.machinery;

/**
 *
 * @author vivien
 */
public abstract class MusEditor {

    protected abstract MusEditListenerList getListenerList();

    // command
    public final synchronized void edit(MusEditionEvent editionEvent) {
        // put order in pile 

        // depile une 
        switch (editionEvent.order) {
            case DO_THEN_TELL: {
                doEdit(editionEvent);
                getListenerList().broadcast(editionEvent);
            }
            break;

            case TELL_THEN_DO: {
                getListenerList().broadcast(editionEvent);
                doEdit(editionEvent);
            }
            break;

        }

    }

    protected abstract void doEdit(MusEditionEvent editionEvent);

}
