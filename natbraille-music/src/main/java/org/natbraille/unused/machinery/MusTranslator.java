/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package org.natbraille.unused.machinery;

/**
 *
 * @author vivien
 */
public abstract class MusTranslator extends MusListener {
    
    private final MusDoc from;
    private final MusDoc to;

    public MusTranslator(MusDoc from, MusDoc to) {
        this.from = from;
        this.to = to;
        this.from.getEditor().getListenerList().addListener(this);
    }

    @Override
    public final void edited(MusEditionEvent sourceEvent) {
        MusEditor editor = to.getEditor();
        translateEvent(sourceEvent,editor);
    }

    public abstract void translateEvent(MusEditionEvent sourceEvent,MusEditor targetEditor);
   

}
