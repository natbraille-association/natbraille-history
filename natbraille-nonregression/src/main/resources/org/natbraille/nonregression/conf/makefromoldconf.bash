#!/bin/bash

for i in ../oldconf/* ; 
do (
	NAME=`basename $i` 
	echo "making $NAME from $i"
	rm -f $NAME 
	../oldtonew.pl < $i > $NAME 
) ; 
done

# (
    
#     echo "making nat2.default.converted.nat3.properties from nat2.default.properties"
#     ../oldtonew.pl < nat2.default.properties > nat2.default.converted.nat3.properties
    
#     echo "stripping from nat3 default"
#     cd ../..
#     ./nat-jar-launch \
# 	--filter nonregression/conf/nat2.default.converted.nat3.properties \
# 	--writefilter nonregression/conf/nat2.default.converted.nat3.nondefault.properties 
    
# )