#!/usr/bin/perl
#
# transforms the nat filter .properties
# files for NatBraille 2 to a NatBraille 3
#
# exemple use for transforming an old conf directory 
# for i in oldconf/* ; do ( ./oldtonew.pl < $i > $i.new ) ; done
#

use Encode;
use strict;
use warnings;

sub ep {
    return encode('ISO8859-1',(shift));
#    return encode('UTF-8',(shift));
}
sub dp {
    return decode('ISO8859-1',(shift));
}

my %conv = (
    "cv-preserve-hyphen"=>"MODE_DETRANS_KEEP_HYPHEN",
    "cv-preserve-page-break"=>"LAYOUT_PAGE_BREAKS_KEEP_ORIGINAL",
    "fi-braille-table" => "FORMAT_OUTPUT_BRAILLE_TABLE", # fi-emboss-table ???
#    "pr-emboss-table"=> "FORMAT_OUTPUT_BRAILLE_TABLE",
    "fi-hyphenation-dirty"=>"LAYOUT_DIRTY_HYPHENATION",
    "fi-hyphenation-lit" => "LAYOUT_LIT_HYPHENATION",
    "pf-index-name"=>"LAYOUT_PAGE_TOC_NAME",
    "fi-infos" => "CONFIG_DESCRIPTION",
    "fi-line-length" => "FORMAT_LINE_LENGTH",
    "fi-line-number" => "FORMAT_PAGE_LENGTH",
    "fi-litt-abbreg"=>"MODE_G2",
    "fi-litt-transcribe" => "MODE_LIT",
    "fi-math-force-prefix" => "MODE_MATH_ALWAYS_PREFIX",
    "fi-math-transcribe"=>"MODE_MATH",
    "fi-math-use-trigo-notation" => "MODE_MATH_SPECIFIC_NOTATION",
    "fi-music-transcribe"=>"MODE_MUSIC",
    "fi-name" => "CONFIG_NAME",
    "out-encoding"=>"FORMAT_OUTPUT_ENCODING",
    "pf-do-layout" => "LAYOUT",
    "pf-empty-line-mode" => "LAYOUT_PAGE_EMPTY_LINES_MODE",
    "pf-generate-page-break" => "LAYOUT_PAGE_BREAKS",
    "pf-insert-index"=>"LAYOUT_PAGE_TOC",
    "pf-keep-page-break" => "LAYOUT_PAGE_BREAKS_KEEP_ORIGINAL",
    "pf-linearise-table" => "MODE_ARRAY_LINEARIZE_ALWAYS",
    "pf-min-cell-linearise" => "MODE_ARRAY_2D_MIN_CELL_NUM",
    "pf-min-empty-line-1"=>"LAYOUT_PAGE_EMPTY_LINES_CUSTOM_1",
    "pf-min-empty-line-2"=>"LAYOUT_PAGE_EMPTY_LINES_CUSTOM_2",
    "pf-min-empty-line-3"=>"LAYOUT_PAGE_EMPTY_LINES_CUSTOM_3",
    "pf-min-page-break" => "LAYOUT_PAGE_BREAKS_MIN_LINES",
    "pf-numbering-start-page" => "LAYOUT_NUMBERING_START_PAGE",
    "pf-numbering-style" => "LAYOUT_NUMBERING_MODE",
    "pf-string-replace-in" => "LAYOUT_NOLAYOUT_TR_INPUT",
    "pf-string-replace-out" => "LAYOUT_NOLAYOUT_TR_OUTPUT",
    "pf-strings-addons" => "LAYOUT_NOLAYOUT_OUTPUT_TAGS",
    "pf-titles-levels"=>"LAYOUT_PAGE_HEADING_LEVEL",

    "tr-litt-use-part-emphasis-prefix" => "MODE_LIT_FR_EMPH_IN_PASSAGE",
    "tr-litt-show-in-word-emphasis" => "MODE_LIT_FR_EMPH_IN_WORD",
    "tr-litt-use-word-emphasis-prefix" => "MODE_LIT_EMPH",

    "tr-image-processing"=>"MODE_IMAGES",
    "tr-litt-use-double-upper-prefix" => "MODE_LIT_FR_MAJ_DOUBLE",
    "tr-litt-use-mixed-upper-lower-rules" => "MODE_LIT_FR_MAJ_MELANGE",
    "tr-litt-use-part-upper-prefix" => "MODE_LIT_FR_MAJ_PASSAGE",
    "tr-min-title-contracted" => "MODE_G2_HEADING_LEVEL",


    # never used...
    
    "fi-abr-integral" => "MODE_G2_G1_WORDS",
    "fi-abr-integral-amb" => "MODE_G2_G1_AMB_WORDS",
    "fi-abr-ivb" => "MODE_G2_IVB_WORDS",
    "fi-abr-ivb-amb" => "MODE_G2_IVB_AMB_WORDS",
    


    );


my %discard = map {$_,0} (
    "ad-log-file-size",
    "ad-nb-log-files",
    "ad-oo-wait",
    "conf-version",
    "pr-emboss-table",
    "fi-hyphenation",
    "fi-is-sys-braille-table",
    "fi-is-sys-config",
    "fi-is-sys-emboss-table",
    "fi-optimize",
    "ge-change-sys-files",
    "ge-check-update",
    "ge-language",
    "ge-log-level",
    "ge-reader-choice",
    "ge-translaiton-mode",
    "pf-add-form-feed",
    "pf-number-first-page",
    "pf-par-indent",
    "pf-strict-titles",
    "pf-strings-addons-count",
    "pr-emboss-auto",
    "pr-emboss-command",
    "pr-emboss-print-service",
    "pr-use-emboss-command",
    "tr-litt-show-in-word_emphasis", # feinteux
    "system-config",
    "tr-use-saxon-processor",
    "ui-editor-braille-font",
    "ui-editor-braille-font-size",
    "ui-editor-default",
    "ui-editor-external",
    "ui-editor-font",
    "ui-editor-font-size",
    "ui-editor-nat",
    "ui-help-key",
    "ui-open-with-browser",
    "ui-read-descr",
    "ui-read-ttt",
    "ui-trad-key",
    "ui-use-internet",
    "ui-use-ttt",
    );

print ep("#\n# Natbraille\n# converted configuration\n#\n");
print ep("\nCONFIG_VERSION=5\n");

# ignored but ...
my @ignored;

# kown to be safe to ignore
my @discarded;

my @converted;

foreach (<STDIN>){
    chomp;
    my $l = dp($_);
    $l =~ s/\n//g;
    if ($l =~ /(.+?)=(.+)/){
	my $oldname = $1;
	if (defined $conv{$1}){
	    my $oldarg = $2;
	    my $name = $conv{$1};
	    my $arg = $oldarg;

	    # table emboss
	    if ($arg =~ /(.*)\.ent/){
		$arg = $1;
	    }
	    my $comment = "# was $l",
	    my $newline = join('=',$name,$arg);
    
#	    print join("",map {ep($_)}($name),'=',$arg)."\n";

	    push @converted, [$name,join("\n",$comment,$newline)];
	} elsif (defined $discard{$1}){
	    push @discarded, $l;
	} else {
	    push @ignored, $l;
	}
    }
}

print ep("\n#\n# converted options\n#\n\n");
print ep(join("\n\n",map { $_->[1] } sort { $a->[0] cmp $b->[0] } @converted)."\n");

print ep("\n#\n# discarded options \n# (safe to ignore)\n#\n");
print ep(join("\n",map { '# '.$_ } sort @discarded)."\n");

print ep("\n#\n# ignored options \n#\n");
print ep(join("\n",map { '# '.$_ } sort @ignored)."\n");
