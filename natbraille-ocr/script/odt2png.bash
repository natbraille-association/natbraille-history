#!/bin/bash


ODTFILE=$1;
DIR=`dirname $ODTFILE`;
PDFFILE=`echo $FILE | sed 's/\.odt$/.pdf/'`;
PNGFILE=`echo $FILE | sed 's/\.odt$/.png/'`;

lowriter --nodefault \
    --nolockcheck \
    --nologo \
    --norestore \
    --nosplash \
    --headless \
    --outdir "$DIR" \
    --convert-to pdf \
    -- "$ODTFILE";

convert "$PDFFILE" "$PNGFILE"



