/*
 * NatBraille - Ocr - An universal Translator
 * Copyright (C) 2014 Vivien Guillet, Bruno Mascret
 * Contact: bmascret@free.fr
 * 
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */
package org.natbraille.ocr;

import java.awt.image.BufferedImage;
import java.io.IOException;
import java.io.InputStream;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.imageio.ImageIO;
import org.natbraille.ocr.calculators.PageBestRotationCalculator;
import org.natbraille.ocr.calculators.PageBestRotationCalculator.RotationListener;
import org.natbraille.ocr.calculators.PageDispositionCalculator;
import org.natbraille.ocr.calculators.PageRotationCalculator;
import org.natbraille.ocr.calculators.PageTextCalculator;
import org.natbraille.ocr.structures.PageDisposition;
import org.natbraille.ocr.utils.G2D;

/**
 *
 * <p>
 * @author vivien
 */
public class Ocr {

    private RotationListener rotationListener;
    private boolean angleIsAuto = true;
    private double userAngle = 0;
    private boolean pageDispositionIsAuto = true;
    private PageDisposition userPageDisposition;
    private PageBestRotationCalculator.Config pageBestRotationCalculatorConfig = PageBestRotationCalculator.defaultConfig();

    public void setPageBestRotationCalculatorConfig(PageBestRotationCalculator.Config pageBestRotationCalculatorConfig) {
        this.pageBestRotationCalculatorConfig = pageBestRotationCalculatorConfig;
    }

    public PageBestRotationCalculator.Config getPageBestRotationCalculatorConfig() {
        return pageBestRotationCalculatorConfig;
    }
    
    
    public void setUserAngle(double angle) {
        this.userAngle = angle;
        this.angleIsAuto = false;
    }

    public void setAngleIsAuto(boolean angleAuto) {
        this.angleIsAuto = angleAuto;
    }

    public boolean getAngleIsAuto() {
        return angleIsAuto;
    }

    public void setUserPageDisposition(PageDisposition pageDisposition) {
        this.userPageDisposition = pageDisposition;
        this.pageDispositionIsAuto = false;
    }

    public void setPageDispositionIsAuto(boolean pageDispositionAuto) {
        this.pageDispositionIsAuto = pageDispositionAuto;
    }

    public boolean getPageDispositionIsAuto() {
        return pageDispositionIsAuto;
    }

    public void setRotationListener(RotationListener rotationListener) {
        this.rotationListener = rotationListener;
    }

    public class OcrResult {

        public final BufferedImage greyscaleImage;
        public final PageBestRotationCalculator pbrc;
        public final PageRotationCalculator prc;
        public final PageDispositionCalculator pdc;
        public final PageDisposition pd;
        public final PageTextCalculator ptc;

        public OcrResult(BufferedImage greyscaleImage, PageBestRotationCalculator pbrc, PageRotationCalculator prc, PageDispositionCalculator pdc, PageDisposition pd, PageTextCalculator ptc) {
            this.greyscaleImage = greyscaleImage;
            this.pbrc = pbrc;
            this.prc = prc;
            this.pdc = pdc;
            this.pd = pd;
            this.ptc = ptc;
        }

        public String getText(double whiteFloor) {
            return this.ptc.getText(whiteFloor);
        }

        public double getAngle() {
            return this.prc.angle;
        }

        public PageDisposition getPageDisposition() {
            return this.pd;
        }
    }

    public OcrResult analyze(InputStream is) throws IOException {
        BufferedImage sourceImage = ImageIO.read(is);
        BufferedImage greyScaleImage = G2D.ByteGreyScale(sourceImage);
        return analyze(new OcrResult(greyScaleImage, null, null, null, null, null));
    }
    public OcrResult analyze(OcrResult ocrResult) throws IOException {

        PageBestRotationCalculator pbrc;
        PageRotationCalculator prc;

        if (getAngleIsAuto()) {
            if ((ocrResult.pbrc == null)||(ocrResult.pbrc.config != pageBestRotationCalculatorConfig)) {
                pbrc = new PageBestRotationCalculator(ocrResult.greyscaleImage, rotationListener,pageBestRotationCalculatorConfig);
                prc = pbrc.prc;
            } else {
                pbrc = ocrResult.pbrc;
                prc = ocrResult.pbrc.prc;
            }
        } else {
            pbrc = ocrResult.pbrc;
            if ((ocrResult.prc != null) && (ocrResult.prc.angle == userAngle)) {
                prc = ocrResult.prc;
            } else {
                prc = new PageRotationCalculator(ocrResult.greyscaleImage, userAngle);
            }
        }

        PageDisposition pd;
        PageDispositionCalculator pdc;
        if (getPageDispositionIsAuto()) {
            if (prc == ocrResult.prc) {
                pdc = ocrResult.pdc;
                pd = ocrResult.pdc.pageDisposition;
            } else {
                pdc = new PageDispositionCalculator(prc);
                pd = pdc.pageDisposition;
            }
        } else {            
            pdc = ocrResult.pdc;
            pd = userPageDisposition;
        }
        PageTextCalculator ptc;
        if ((ocrResult.ptc != null) && (ocrResult.pd == pd)) {
            ptc = ocrResult.ptc;
        } else {
            ptc = new PageTextCalculator(prc.image, pd);
        }
        return new OcrResult(ocrResult.greyscaleImage, pbrc, prc, pdc, pd, ptc);

    }

    public interface OcrResultListener {

        public void newOcrResult(OcrResult oldOcrResult, OcrResult newOcrResult);
    }

    public void analyzeThread(final InputStream is, final OcrResultListener ocrResultListener) {
        new Thread(
                new Runnable() {
                    @Override
                    public void run() {
                        try {
                            OcrResult newOcrResult = analyze(is);
                            ocrResultListener.newOcrResult(newOcrResult, newOcrResult);
                        } catch (IOException ex) {
                            Logger.getLogger(Ocr.class.getName()).log(Level.SEVERE, null, ex);
                        }

                    }
                }
        ).start();

    }

    public void analyzeThread(final OcrResult ocrResult, final OcrResultListener ocrResultListener) {
        new Thread(
                new Runnable() {
                    @Override
                    public void run() {
                        try {
                            OcrResult newOcrResult = analyze(ocrResult);
                            ocrResultListener.newOcrResult(newOcrResult, newOcrResult);
                        } catch (IOException ex) {
                            Logger.getLogger(Ocr.class.getName()).log(Level.SEVERE, null, ex);
                        }

                    }
                }
        ).start();
    }

}
