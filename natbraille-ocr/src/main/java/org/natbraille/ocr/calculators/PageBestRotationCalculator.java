/*
 * NatBraille - Ocr - An universal Translator
 * Copyright (C) 2014 Vivien Guillet, Bruno Mascret
 * Contact: bmascret@free.fr
 * 
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */
package org.natbraille.ocr.calculators;

import java.awt.image.BufferedImage;
import org.natbraille.ocr.utils.Trigo;
import org.natbraille.ocr.utils.VE;

/**
 *
 * @author vivien
 */
public class PageBestRotationCalculator {

    public final PageRotationCalculator prc;
    public final Config config;

    public interface RotationListener {

        public void rotationEvent(PageRotationCalculator irc, int rotNum, int maxRot);
    }

    public static class Config {

        public final int nTest1a;
        public final double searchAngle1a;
        public final int nTest1b;
        public final double searchAngle1b;
        public final int nTest1c;
        public final double searchAngle1c;
        public final int nTest2;
        public final double searchAngle2;

        public Config(int nTest1a, double searchAngle1a, int nTest1b, double searchAngle1b, int nTest1c, double searchAngle1c, int nTest2, double searchAngle2) {
            this.nTest1a = nTest1a;
            this.searchAngle1a = searchAngle1a;
            this.nTest1b = nTest1b;
            this.searchAngle1b = searchAngle1b;
            this.nTest1c = nTest1c;
            this.searchAngle1c = searchAngle1c;
            this.nTest2 = nTest2;
            this.searchAngle2 = searchAngle2;
        }

        @Override
        public String toString() {
            StringBuilder sb = new StringBuilder();
            sb.append(nTest1a).append("/");
            sb.append(searchAngle1a).append("/");
            sb.append(nTest1b).append("/");
            sb.append(searchAngle1b).append("/");
            sb.append(nTest1c).append("/");
            sb.append(searchAngle1c).append("/");
            sb.append(nTest2).append("/");
            sb.append(searchAngle2);
            return sb.toString();

        }

    }


    public static Config defaultConfig() {
        return new Config(
                30, Math.PI / 2.0,
                30, Math.PI / (2.0 * 8.0),
                30, Math.PI / (2.0 * 8.0 * 8.0),
                60, 0.05//Math.PI / 80.0
        );
    }

    public PageBestRotationCalculator(BufferedImage greyScaleImage, RotationListener rl, Config config) {
        PageRotationCalculator lPr;
        {
            double searchAngle1a = config.searchAngle1a;
            double minAngle1a = -searchAngle1a / 2.0;
            double maxAngle1a = searchAngle1a / 2.0;
            double angles1a[] = Trigo.getSteps(minAngle1a, maxAngle1a, (int) config.nTest1a);
            PageRotationCalculator pr1a = findRotationZone(greyScaleImage, angles1a, rl);

            double searchAngle1b = config.searchAngle1b;
            double minAngle1b = pr1a.angle- searchAngle1b / 2.0;
            double maxAngle1b = pr1a.angle+ searchAngle1b / 2.0;
            double angles1b[] = Trigo.getSteps(minAngle1b, maxAngle1b, (int) config.nTest1b);
            PageRotationCalculator pr1b = findRotationZone(greyScaleImage, angles1b, rl);

            double searchAngle1c = config.searchAngle1c;
            double minAngle1c = pr1b.angle- searchAngle1c / 2.0;
            double maxAngle1c = pr1b.angle + searchAngle1c / 2.0;
            double angles1c[] = Trigo.getSteps(minAngle1c, maxAngle1c, (int) config.nTest1c);
            PageRotationCalculator pr1c = findRotationZone(greyScaleImage, angles1c, rl);

            lPr = pr1c;
        }
        {
            double searchAngle2 = config.searchAngle2;
            double nTest2 = config.nTest2;

            PageRotationCalculator pr2 = findPreciseRotation(greyScaleImage, searchAngle2, lPr.angle, nTest2, rl);

            this.prc = pr2;
        }
        this.config = config;
    }

    private static PageRotationCalculator findPreciseRotation(BufferedImage gsi, double searchAngle2, double center, double nTest2, RotationListener rl) {
        // explore 40 angles 6 times smaller around best result
        //double searchAngle2 // searchAngle1 / 6.0;
        double minAngle2 = center - searchAngle2 / 2.0;
        double maxAngle2 = center + searchAngle2 / 2.0;

        double newAngles2[] = Trigo.getSteps(minAngle2, maxAngle2, (int) nTest2);
        double scores[] = new double[newAngles2.length];
        for (int z = 0; z < newAngles2.length; z++) {
            PageRotationCalculator pr2 = new PageRotationCalculator(gsi, newAngles2[z]);
            scores[z] = pr2.score;
            if (rl != null) {
                rl.rotationEvent(pr2, z, newAngles2.length);
            }
        }
        // find max and min of derivates  dresult / dAngle.
        double dScores[] = VE.derivate(scores);
        int idxMax = VE.minIdx(dScores);
        int idxMin = VE.maxIdx(dScores);

        double angleMax = newAngles2[idxMin];
        double angleMin = newAngles2[idxMax];

        double pftAngle = (angleMin + angleMax) / 2;
//        System.out.println(idxMax + " < " + idxMin + "  =>  angles : "
//                + newAngles2[idxMax] + "<  " + newAngles2[idxMin] + " pf " + " " + pftAngle);

        PageRotationCalculator pr = new PageRotationCalculator(gsi, pftAngle);
        if (rl != null) {
            rl.rotationEvent(pr, 0, 0);
        }
        return pr;

    }

    private static PageRotationCalculator findRotationZone(BufferedImage gsi, double angles[], RotationListener rl) {

        // angles= new double[]{0.0};
        PageRotationCalculator bestIrc = null;

        for (int i = 0; i < angles.length; i++) {
            PageRotationCalculator pr = new PageRotationCalculator(gsi, angles[i]);
            if ((bestIrc == null) || (pr.score > bestIrc.score)) {
                bestIrc = pr;
            }
            // listener call
            if (rl != null) {
                rl.rotationEvent(pr, i, angles.length);
            }

        }
        return bestIrc;
    }
}
