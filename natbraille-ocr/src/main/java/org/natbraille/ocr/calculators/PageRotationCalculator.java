/*
 * NatBraille - Ocr - An universal Translator
 * Copyright (C) 2014 Vivien Guillet, Bruno Mascret
 * Contact: bmascret@free.fr
 * 
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */

package org.natbraille.ocr.calculators;

import java.awt.Rectangle;
import java.awt.image.BufferedImage;
import java.awt.image.DataBuffer;
import org.natbraille.ocr.utils.G2D;
import org.natbraille.ocr.utils.VE;

/**
 *
 * @author vivien
 */
public class PageRotationCalculator {

    public final double angle;
    public final BufferedImage image;
    public final double[] mX;
    public final double[] mY;
    public final double[] dmX;
    public final double[] dmY;
    public final double score;
    public final double scoreX;
    public final double scoreY;

    public PageRotationCalculator(BufferedImage originalImage, double angle) {

        // rotate image;
        BufferedImage img = G2D.applyCentredRotation(originalImage, angle);

        // pixels IS a 1 byte grayscale
        DataBuffer pixels = img.getRaster().getDataBuffer();
        int npixels = pixels.getSize();
        Rectangle b = img.getRaster().getBounds();

        //
        // Image is projected along v and h axis
        // for this angle value. 
        //
        // when projection vector is the sharpest
        // (i.e. ~sum of abs of derivates of x + y axis moy is AROUND max)
        // braille points are aligned on each axis 
        //
        // horizontal and vertical moy
        double sX[] = new double[b.width];
        double sY[] = new double[b.height];

        for (int pos = 0; pos < npixels; pos++) {
            int x = (pos) % (b.width);
            int y = (pos) / (b.width);
            int lum = pixels.getElem(pos);
            sX[x] += ((double) lum) / ((double) b.height) / (double) 255;
            sY[y] += ((double) lum) / ((double) b.width) / (double) 255;
        }

        double dsX[] = VE.derivate(sX);
        double dsY[] = VE.derivate(sY);

        double adsX[] = VE.abs(dsX);
        double adsY[] = VE.abs(dsY);

        double xRotationScore = VE.sum(adsX);
        double yRotationScore = VE.sum(adsY);

        double totalScore = Math.sqrt(
                Math.pow(xRotationScore, 2) + Math.pow(yRotationScore, 2));

        this.mX = sX;
        this.mY = sY;
        this.dmX = dsX;
        this.dmY = dsY;
        this.angle = angle;
        this.image = img;
        this.scoreX = xRotationScore;
        this.scoreY = yRotationScore;
        this.score = totalScore;

    }

}
