/*
 * NatBraille - Ocr - An universal Translator
 * Copyright (C) 2014 Vivien Guillet, Bruno Mascret
 * Contact: bmascret@free.fr
 * 
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */
package org.natbraille.ocr.structures;

/**
 *
 * @author vivien
 */
public class CellDisposition {

    public final double radius;
    public final double interPoint;
    public final double interCell;
    public final double offset;

    public CellDisposition(double radius, double interPoint, double interCell, double offset) {
        this.radius = radius;
        this.interPoint = interPoint;
        this.interCell = interCell;
        this.offset = offset;
    }

    public double cellNum(int pos) {
        double rPos = (double) pos + this.offset;
        return Math.floor(rPos / this.interCell);

    }

    public int pointInCell(int pos, boolean isVerticalAxis) {
        double rPos = (double) pos + this.offset;
        double posInCell = rPos % this.interCell;

        int resu = 0;
        if (posInCell < (2.0 * this.radius)) {
            resu = 1;
        } else if (posInCell < (this.interPoint)) {
            resu = 0;
        } else if (posInCell < (this.interPoint + (2.0 * this.radius))) {
            resu = 2;
        } else if (posInCell < ((2.0 * this.interPoint))) {
            resu = 0;
        } else if (posInCell < ((2.0 * this.interPoint) + (2.0 * this.radius))) {
            if (isVerticalAxis) {
                resu = 3;
            }
        }
        return resu;
    }

    public double[] grid(int size, boolean isVerticalAxis) {
        double resu[] = new double[size];
        for (int i = 0; i < resu.length; i++) {
            resu[i] = pointInCell(i, isVerticalAxis);
        }
        return resu;
    }

    @Override
    public String toString() {
        return "" + radius + "/" + interPoint + "/" + interCell + "/" + offset;

    }

}
