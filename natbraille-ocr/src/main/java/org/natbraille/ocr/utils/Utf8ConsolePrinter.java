/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.natbraille.ocr.utils;

import java.io.IOException;
import java.io.OutputStreamWriter;

/**
 *
 * @author vivien
 */
public class Utf8ConsolePrinter {

    
    public void out(String text) {
        try {
            OutputStreamWriter ofw = new OutputStreamWriter(System.out, "utf-8");
            ofw.write(text);
            ofw.write("\n");
            ofw.flush();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
    
    public void err(String text) {
        try {
            OutputStreamWriter ofw = new OutputStreamWriter(System.out, "utf-8");
            ofw.write(text);
            ofw.write("\n");
            ofw.flush();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

}
