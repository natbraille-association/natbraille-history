/*
 * NatBraille - An universal Translator - soffice interface 
 * Copyright (C) 2014 Vivien Guillet, Bruno Mascret, Frédéric Schwebel
 * Contact: bmascret@free.fr
 * 
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */
package org.natbraille.soffice;

import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * soffice configuration, as needed by {@link SofficeDisposableConverter}.
 * <p>
 * Can be configured using setters or by using a configurator such as
 * {@link AutoConfigurator}, {@link configurators.UnixConfigurator} or
 * {@link configurators.WindowsRegConfigurator}
 * <p>
 * @author vivien
 */
public class SofficeConfig implements Comparable {

    private String exec = "soffice";
    private Long timeout = 60 * 1000L;
    private String path = null;
    private String version = null;
    private String windowsRegistryDir = null;
    private String brand = null;

    /**
     * get the "brand" of soffice installation. (probably "OpenOffice.org" or
     * "LibreOffice").
     * <p>
     * @return the brand
     */
    public String getBrand() {
        return brand;
    }

    /**
     * set the "brand" of soffice installation.
     * <p>
     * @param brand
     */
    public void setBrand(String brand) {
        this.brand = brand;
    }

    /**
     * gets the windows registry dir.
     * <p>
     * @return the registry key holding the soffice exe Path and soffice version
     */
    public String getWindowsRegistryDir() {
        return windowsRegistryDir;
    }

    /**
     * set the windows registry dir
     * <p>
     * @param windowsRegistryDir
     */
    public void setWindowsRegistryDir(String windowsRegistryDir) {
        this.windowsRegistryDir = windowsRegistryDir;
    }

    /**
     * get soffice version
     * <p>
     * @return the soffice version (ex 3.5.1) string
     */
    public String getVersion() {
        return version;
    }

    /**
     * set soffice version
     * <p>
     * @param version
     */
    public void setVersion(String version) {
        this.version = version;
    }

    /**
     * get executable name without path (probably soffice or soffice.exe)
     * <p>
     * @return the exec name
     */
    public String getExec() {
        return exec;
    }

    /**
     * set executable name without path (probably soffice or soffice.exe)
     * <p>
     * @param exec
     */
    public void setExec(String exec) {
        this.exec = exec;
    }

    /**
     * gets the soffice bin path.
     * <p>
     * Used for windows because soffice dirs not in PATH.
     * <p>
     * Also used for starting non system soffice install on linux.
     * <p>
     * @return the soffice path
     */
    public String getPath() {
        return path;
    }

    /**
     * sets the soffice bin path.
     * <p>
     * Used for windows because soffice dirs not in PATH.
     * <p>
     * Also used for starting non system soffice install on linux. if set,
     * soffice exec will be called with a full path (path+exec) and use this
     * path as working dir.
     * <p>
     * @param path the soffice installation path
     */
    public void setPath(String path) {
        this.path = path;
    }

    /**
     * get the timeout for executable run. After that time, soffice process will
     * be killed.
     * <p>
     * @return the timeout
     */
    public Long getTimeout() {
        return timeout;
    }

    /**
     * set the timeout for executable run.
     * <p>
     * @param timeout
     */
    public void setTimeout(Long timeout) {
        this.timeout = timeout;
    }

    /**
     * Full exec path is the conctatenation of the path and the exec parameters.
     * Exec starting with a leading "./" are not prefixed.
     * <p>
     * @return the full exec path as a String
     */
    public String getFullExecPath() {
        String fullExec;
        if (getPath() == null) {
            fullExec = getExec();
        } else {
            if (getExec().startsWith("./")){
                fullExec = getExec();
            } else {
                fullExec = Paths.get(getPath(), getExec()).toString();
            }
        }
        return fullExec;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(brand)
                .append(" version ").append(version).append("\n")
                .append(" - path:").append(path).append("\n")
                .append(" - exec:").append(exec).append("\n")
                .append(" - reg :").append(windowsRegistryDir).append("\n")
                .append(" - timeout:").append(timeout);
        return sb.toString();

    }

    final int BEFORE = -1;
    final int EQUAL = 0;
    final int AFTER = 1;

    /**
     * compare two SofficeConfig. Order : from libreoffice max version to
     * openoffice min version then no version then no brand.
     *
     * @param o
     * @return
     */
    @Override
    public int compareTo(Object o) {

        if (!(o instanceof SofficeConfig)) {
            throw new UnsupportedOperationException("Not supported.");
        }

        SofficeConfig other = (SofficeConfig) o;

        if ((other.brand == null) || (brand == null)) {
            throw new UnsupportedOperationException("Cannot sort unkown brand");
        }

        // libreoffice first
        boolean isLibreOffice = isLibreOffice();
        boolean otherIsLibreOffice = other.isLibreOffice();

        if (otherIsLibreOffice && !isLibreOffice) {
            return 1;
        }
        if (isLibreOffice && !otherIsLibreOffice) {
            return -1;
        }

        // having a version number first
        if ((version != null) && (other.version == null)) {
            return -1;
        }
        if ((version == null) && (other.version != null)) {
            return 1;
        }
        if ((version == null) && (other.version == null)) {
            return 0;
        }

        // max version number first
        String[] thisVersion = version.split("\\.");
        String[] otherVersion = other.version.split("\\.");
        for (int i = 0; i < Math.max(thisVersion.length, otherVersion.length); i++) {
            Integer thisSubVersion = (i < thisVersion.length) ? (Integer.parseInt(thisVersion[i])) : (0);
            Integer otherSubVersion = (i < otherVersion.length) ? (Integer.parseInt(otherVersion[i])) : (0);
            if (!thisSubVersion.equals(otherSubVersion)) {
                return otherSubVersion.compareTo(thisSubVersion);
            }
        }

        return 0;

    }

   public boolean isLibreOffice() {
        boolean isLibreOffice = (brand != null) && brand.toLowerCase().startsWith("libre");
        return isLibreOffice;

    }

    public boolean isOpenOffice() {
        boolean isOpenOffice = (brand != null) && brand.toLowerCase().startsWith("open");
        return isOpenOffice;
    }
    
     /**
     * For testing purposes. Remove after that.
     *
     * @param args
     */
    public static void main(String[] args) {
        SofficeConfig c1 = new SofficeConfig();
        SofficeConfig c2 = new SofficeConfig();
        SofficeConfig c3 = new SofficeConfig();
        SofficeConfig c4 = new SofficeConfig();
        SofficeConfig c5 = new SofficeConfig();
        SofficeConfig c6 = new SofficeConfig();

        c1.brand = "LibreOffice";
        c1.version = "2.5555.332";
        c2.brand = "LibreOffice";
        c2.version = "3.5556.3232";
        c3.brand = "OpenOffice";
        c3.version = null;
        c4.brand = "OpenOffice";
        c4.version = "13.6556.3232";
        c5.brand = "LibreOffice";
        c5.version = null;
        c6.brand = "OpenOffice";
        c6.version = "13.6556.3234";
        List<SofficeConfig> list = new ArrayList<>();
        list.add(c1);
        list.add(c2);
        list.add(c3);
        list.add(c4);
        list.add(c5);
        Collections.sort(list);
        for (SofficeConfig c : list) {
            System.err.println("==");
            System.err.println(c);
        }

    }
}
