/*
 * NatBraille - An universal Translator - soffice interface 
 * Copyright (C) 2014 Vivien Guillet, Bruno Mascret, Frédéric Schwebel
 * Contact: bmascret@free.fr
 * 
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */
package org.natbraille.soffice;

// TODO : remove println

import java.io.File;
import java.io.IOException;
import org.apache.commons.exec.ExecuteException;
import org.natbraille.soffice.executor.TimeoutException;

/**
 * Reusable soffice converter using a soffice configuration.
 * @author vivien
 */
public class SofficeConverter {

    private final SofficeConfig sofficeConfig;

    public SofficeConfig getSofficeConfig() {
        return sofficeConfig;
    }


    /**
     * creates a new reusable soffice converter using a soffice configuration.
     * <p>
     * @param sofficeConfig
     * @throws IOException
     */
    public SofficeConverter(SofficeConfig sofficeConfig) throws IOException {
        this.sofficeConfig = sofficeConfig;

    }

    /**
     * 
     * @param inputFile
     * @param destFile
     * @throws IOException when exec not found, etc.
     * @throws ExecuteException on bad exit value
     * @throws TimeoutException on end of user timeout
     */
    public void convert(File inputFile, File destFile) throws IOException, ExecuteException, TimeoutException {        
        SofficeDisposableConverter loDc = new SofficeDisposableConverter(sofficeConfig, inputFile, destFile);
        loDc.execute();
    }
    
}
