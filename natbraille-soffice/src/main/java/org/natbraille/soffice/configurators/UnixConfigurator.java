/*
 * NatBraille - An universal Translator - soffice interface 
 * Copyright (C) 2014 Vivien Guillet, Bruno Mascret, Frédéric Schwebel
 * Contact: bmascret@free.fr
 * 
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */
package org.natbraille.soffice.configurators;

import java.io.IOException;
import java.util.List;
import org.apache.commons.exec.CommandLine;
import org.natbraille.soffice.SofficeConfig;
import org.natbraille.soffice.executor.NatExecutor;
import org.natbraille.soffice.executor.SofficeCliTools;

/**
 * retrieves a {@link SofficeConfig} according to "soffice --version" .
 *
 * @author vivien
 */
public class UnixConfigurator {

    public final Long timeout = 60 * 1000L;
    private final SofficeCliTools sofficeCliTools;

    public UnixConfigurator() throws IOException {
        sofficeCliTools = new SofficeCliTools();
        sofficeCliTools.createUserInstallationDirectory();
    }

    /**
     * get office configuration according to the "soffice" binary in PATH by
     * running soffice --version
     * <p>
     * @return the generated configuration
     */
    public SofficeConfig getSofficeConfig() {
        // build initial config for unix
        SofficeConfig validConf = null;
        SofficeConfig testConf = new SofficeConfig();
        testConf.setExec("soffice");

        // build the command line
        CommandLine cmdLine = sofficeCliTools.getGenericCommandLine(testConf);
        cmdLine.addArgument("--version");

        // getSofficeConfig
        NatExecutor executor = new NatExecutor(timeout);

        try {
            executor.execute(cmdLine);
            List<String> log = executor.getStdOut();

            if (log.size() > 0) {
                String versionString = log.get(0);
                // LibreOffice 4.3.0.4 430m0(Build:4)
                String vss[] = versionString.split(" ");
                testConf.setBrand(vss[0]);
                testConf.setVersion(vss[1]);
                validConf = testConf;
            }
        } catch (IOException e) {
            //safely ignore : if there is an error, this office should not be listed anyway
        }
        sofficeCliTools.cleanUpUserInstallationDirectory();

        return validConf;

    }

}
