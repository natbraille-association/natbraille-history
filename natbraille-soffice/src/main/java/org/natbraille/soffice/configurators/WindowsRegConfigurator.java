/*
 * NatBraille - An universal Translator - soffice interface 
 * Copyright (C) 2014 Vivien Guillet, Bruno Mascret, Frédéric Schwebel
 * Contact: bmascret@free.fr
 * 
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */
package org.natbraille.soffice.configurators;

import java.util.ArrayList;
import java.util.List;
import org.natbraille.soffice.SofficeConfig;
import org.natbraille.tools.WinRegDumper;

/**
 * retrieve all system {@link SofficeConfig} according to to the windows
 * registry
 * <p>
 * @author vivien
 * @author raphael.mina@gmail.Com (Natbraille 2.0 "Path" class)
 */
public class WindowsRegConfigurator {

    /**
     * windows registry where libreoffice keys can be found. order matters
     * (libreoffice>openoffice)
     */
    private final static String OfficeVariantsRoots[] = {
        "HKEY_LOCAL_MACHINE\\Software\\LibreOffice\\LibreOffice",
        "HKEY_LOCAL_MACHINE\\Software\\Wow6432Node\\LibreOffice\\LibreOffice",
        "HKEY_LOCAL_MACHINE\\Software\\OpenOffice\\OpenOffice",
        "HKEY_LOCAL_MACHINE\\Software\\Wow6432Node\\OpenOffice\\OpenOffice",
        "HKEY_LOCAL_MACHINE\\Software\\OpenOffice.org\\OpenOffice.org",
        "HKEY_LOCAL_MACHINE\\Software\\Wow6432Node\\OpenOffice.org\\OpenOffice.org"
    };

    /**
     * returns value of the "Path" value for given office path registry
     * <p>
     * @param officePathRegistry
     * @return the Path registry key with soffice executable full path
     */
    private static String officeBinaryPath(String officePathRegistry) {
        List<String> regOutput2 = WinRegDumper.dump(officePathRegistry, "Path");
        for (String s2 : regOutput2) {
            if (s2.contains("Path")) {
                //in this string, we start from "c:" until the end, this is the full name of executable
                return s2.substring(s2.lastIndexOf(":") - 1);
            }
        }
        return null;
    }

    /**
     * returns the list of office version keys matching given office registry
     * root
     * <p>
     * @param officeRegistryRoot
     * @return the list of office version keys
     */
    private static List<String> officeVersions(String officeRegistryRoot) {
        List<String> confs = new ArrayList<>();
        List<String> regOutput = WinRegDumper.dump(officeRegistryRoot);
        for (String s : regOutput) {
            if (s.startsWith(officeRegistryRoot) && !s.equals(officeRegistryRoot)) {
                String version = s.substring(officeRegistryRoot.length()).substring(1);
                if (!version.contains("\\")) {
                    confs.add(s);
                }
            }
        }
        return confs;
    }

    /**
     * get the list of office configurations according to the windows registry
     * <p>
     * @return the configuration list
     */
    public List<SofficeConfig> getSofficeConfig() {
        List<SofficeConfig> confs = new ArrayList<>();
        for (String variantRegRoot : OfficeVariantsRoots) {
            List<String> officeVersions = officeVersions(variantRegRoot);
            for (String s : officeVersions) {
                String version = s.substring(variantRegRoot.length()).substring(1);
                String binaryPath = officeBinaryPath(s);
                if (binaryPath != null) {
                    SofficeConfig tLoConf = new SofficeConfig();
                    tLoConf.setWindowsRegistryDir(s);
                    tLoConf.setVersion(version);
                    tLoConf.setExec("soffice.exe");
                    tLoConf.setPath(binaryPath.replaceAll("soffice.exe", ""));
                    tLoConf.setBrand(variantRegRoot.substring(variantRegRoot.lastIndexOf("\\") + 1));
                    confs.add(tLoConf);
                }
            }
        }
        return confs;
    }

}
