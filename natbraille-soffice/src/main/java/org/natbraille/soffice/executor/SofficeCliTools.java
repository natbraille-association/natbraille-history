/*
 * NatBraille - An universal Translator - soffice interface 
 * Copyright (C) 2014 Vivien Guillet, Bruno Mascret, Frédéric Schwebel
 * Contact: bmascret@free.fr
 * 
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */
package org.natbraille.soffice.executor;

import java.io.File;
import java.io.IOException;
import java.net.URI;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.HashMap;
import org.apache.commons.exec.CommandLine;
import org.natbraille.soffice.SofficeConfig;
import org.natbraille.tools.DirectoryCleaner;

/**
 * Tools for use with the soffice command line.
 * <p>
 * @author vivien
 */
public class SofficeCliTools {

    private String userInstallationDir;
    private String outputDir;

    /**
     * Creates soffice "UserInstallation" directory passed as
     * "-env:UserInstallation=" argument in soffice cli.
     * <p>
     * @throws IOException
     */
    public void createUserInstallationDirectory() throws IOException {
        this.userInstallationDir = Files.createTempDirectory("natbraille_soffice_home").toFile().getAbsolutePath();
    }

    /**
     * Transforms "UserInstallation" directory representation
     * to the soffice uri/l cli format
     * <p>
     * @return the soffice compliant url of "UserInstallation"
     */
    public String getUserInstallationDirectoryAsSofficeURL() {
        URI javaURI = new File(userInstallationDir).toURI();
        String sofficeCliURI = javaURI.toString().replaceFirst("file:/", "/");
        return sofficeCliURI;
    }

    /**
     * Unlinks "UserInstallation" directory.
     */
    public void cleanUpUserInstallationDirectory() {
        DirectoryCleaner.cleanUp(Paths.get(userInstallationDir));
    }

    /**
     * Creates an output temp directory for soffice.
     * can be used in command line as --outdir temp_dir
     * <p>
     * @throws IOException
     */
    public void createOutputDirectory() throws IOException {
        this.outputDir = Files.createTempDirectory("natbraille_soffice_output").toFile().getAbsolutePath();
    }

    /**
     * Unlinks output temp directory.
     */
    public void cleanUpOutputDirectory() {
        DirectoryCleaner.cleanUp(Paths.get(outputDir));
    }

    /**
     * Creates an output temp directory for soffice.
     * for use in --outdir parameter
     * <p>
     * @return the temp directory file as String
     */
    public String getOutputDirectory() {
        return outputDir;
    }

    /**
     * Builds a generic line usable for any soffice cli task.
     * <p>
     * further command line arguments should be used for specific task
     * (--accept, --convert-to, -version)
     * <p>OpenOffice recognizes : "-option" for option
     * <p>LibreOffice recognizes : "--option" for option
     * <p>For both, the env param is "-env".
     * @param sOfficeConfig the config used to build the command line
     * @return the generic soffice command line
     */
    public CommandLine getGenericCommandLine(SofficeConfig sOfficeConfig) {

        HashMap map = new HashMap();

        CommandLine cl = new CommandLine(sOfficeConfig.getFullExecPath());

        cl.addArgument("-env:UserInstallation=file://${userid}");
        map.put("userid", getUserInstallationDirectoryAsSofficeURL());
        
        String optionMarker = (sOfficeConfig.isOpenOffice()?"-":"--");
        cl.addArgument(optionMarker+"nologo");
        cl.addArgument(optionMarker+"nofirststartwizard");
        cl.addArgument(optionMarker+"nodefault");
        cl.addArgument(optionMarker+"nocrashreport");
        cl.addArgument(optionMarker+"norestore");
        cl.addArgument(optionMarker+"nolockcheck");
        cl.addArgument(optionMarker+"headless");
        // for openoffice ?
        //-quickstart=no     
        cl.setSubstitutionMap(map);

        return cl;

    }

}
