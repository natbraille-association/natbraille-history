/*
 * NatBraille - An universal Translator - soffice interface 
 * Copyright (C) 2014 Vivien Guillet, Bruno Mascret, Frédéric Schwebel
 * Contact: bmascret@free.fr
 * 
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */
/**
 * natbraille-soffice uses soffice command line for doc, docx, rtf -> odt
 * conversion.
 * <ul>
 * <li> multiple soffice concurrent invocations (-env:UserInstallation
 * parameter)</li>
 * <li> converts with --convert-to (! works only with libreoffice !)</li>
 * <li> does not use jodconverter, uno library nor soffice server ("--accept"
 * parameter).</li>
 * <li> office path and version detection (using --version command line or
 * windows registry)</li>
 * </ul>
 * <p>
 * see {@link org.natbraille.soffice.test.App} and
 * {@link org.natbraille.soffice.test.Czif} source files for usage
 * <p>
 */
package org.natbraille.soffice;
