/*
 * NatBraille - An universal Translator - soffice interface 
 * Copyright (C) 2014 Vivien Guillet, Bruno Mascret, Frédéric Schwebel
 * Contact: bmascret@free.fr
 * 
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */
package org.natbraille.soffice.test;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import org.apache.commons.exec.ExecuteException;
import org.natbraille.soffice.SofficeConfig;
import org.natbraille.soffice.SofficeDisposableConverter;
import org.natbraille.soffice.executor.TimeoutException;

/**
 * when run, czif will convert the same document a specified number of times
 * and output usefull debug messages
 * <p>
 * @author vivien
 */
public class Czif implements Runnable {

    SofficeConfig sofficeConfig;
    File inputFile;
    String name;
    int count;

    public Czif(SofficeConfig sofficeConfig, File inputFile, String name, int count) {
        this.sofficeConfig = sofficeConfig;
        this.inputFile = inputFile;
        this.name = name;
        this.count = count;
    }

    public void dbg(String msg) {
        System.out.println("#" + name + " " + msg);
    }

    @Override
    public void run() {
        try {
            while (count > 0) {
                File destFile = new File(inputFile + "_" + name + "_" + count + ".odt");
                // create a converter
                SofficeDisposableConverter loDc = new SofficeDisposableConverter(sofficeConfig, inputFile, destFile);
                try {
                    // execute the converter
                    loDc.execute();
                    dbg("[ok] successfully converted '" + inputFile + "' to '" + destFile + "'");
                } catch (TimeoutException e) {
                    dbg("[user timeout] " + e.getLocalizedMessage());
                } catch (FileNotFoundException e) {
                    dbg("[missing or empty output file?] "+e.getLocalizedMessage());
                } catch (ExecuteException e) {
                    dbg("[error] while running process" + e.getLocalizedMessage());
                } catch (IOException e) {
                    dbg("[io error]" + e.getLocalizedMessage());
                }
                count--;
            }
        } catch (IOException ex) {
            dbg("[error] cannot create converter : " + ex.getLocalizedMessage());
        }

    }

}
