/*
 * NatBraille - An universal Translator - soffice interface 
 * Copyright (C) 2014 Vivien Guillet, Bruno Mascret, Frédéric Schwebel
 * Contact: bmascret@free.fr
 * 
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */
package org.natbraille.tools;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import org.apache.commons.exec.CommandLine;
import org.natbraille.soffice.executor.NatExecutor;

/**
 * Window registry dumper using "reg" executable.
 * <p>
 * Only minimal (windows XP compatible) switches are used.
 * <p>
 * see http://technet.microsoft.com/en-us/library/cc742028.aspx for more about
 * windows reg
 * <p>
 * @author vivien
 */
public class WinRegDumper {

    /**
     * <p>
     * queries windows registry keys by calling reg query
     * <p>
     * calls : query registryKey /s
     * <p>
     * @param registryKey (ex "HKLM\\Software")
     * @return the reg program output lines
     */
    public static List<String> dump(String registryKey) {
        return dump(registryKey, null);
    }

    /**
     * <p>
     * queries windows registry keys by calling reg query
     * <p>
     * calls : query registryKey /s if no registryName is provided
     * <p>
     * calls : query registryKey /v registryName if registryName is not null
     * <p>
     * @param registryKey (ex "HKLM\\Software")
     * @param registryName (ex "Path")
     * @return the reg program output line, and empty list if none
     */
    public static List<String> dump(String registryKey, String registryName) {
        CommandLine cmdLine = new CommandLine("reg");
        HashMap map = new HashMap();
        cmdLine.addArgument("query");
        cmdLine.addArgument("${registryKey}");
        map.put("registryKey", registryKey);
        if (registryName == null) {
            cmdLine.addArgument("/s");
        } else {
            cmdLine.addArgument("/v");
            cmdLine.addArgument(registryName);
        }
        cmdLine.setSubstitutionMap(map);
        NatExecutor natExecutor = new NatExecutor(10L * 1000);
        try {
            natExecutor.execute(cmdLine);
            return natExecutor.getStdOut();
        } catch (IOException e) {
            return new ArrayList<>();
        }
    }

}
