package org.natbraille.unused;

/**
 *
 * @author vivien
 */
import java.io.IOException;
import java.io.InputStream;
import java.io.StringWriter;

/**
 * NOT USED.
 * Trouve le chemin d'accès à OpenOffice.org sur une machine Windows qui
 * utilise NAT, et prévient l'utilisateur si cette application n'est pas
 * installée
 * <p>
 * @author Raphaël Mina
 * raphael.mina@gmail.Com
 */
public class WindowsLoOo {

    /**
     * L'utilitaire Windows pour accéder à la base de registre
     */
    private static final String REGQUERY_UTIL = "reg query ";
    /**
     * Attribut de la clé de la base de registre
     */
    private static final String REGSTR_TOKEN = "REG_SZ";
    /**
     * La racine d'OpenOffice.org dans la base de registre sous Windows
     */
    private static final String OpenOfficeRoot = "HKLM\\Software\\OpenOffice.org\\OpenOffice.org\\";

    /**
     * La version d'OpenOffice installé sur la machine
     */
    private static final String OpenOfficeRootXp64 = "HKLM\\Software\\Wow6432Node\\OpenOffice.org\\OpenOffice.org\\";
    /**
     * La version d'OpenOffice installé sur la machine
     */
    private static String OpenOfficeVersion = "";
    /**
     * Flux d'entrée
     */
    private InputStream is;
    /**
     * Flux d'écriture de chaîne de caractères
     */
    private StringWriter sw;

    /**
     * lit le flux d'entrée et l'écrit dans {@link #sw}
     */
    public void lire() {
        try {
            int c;
            while ((c = is.read()) != -1) {
                sw.write(c);
            }
        } catch (IOException e) {
            System.out.println("\nErreur d'entrée sortie lors de la lecture du flux dans Path");
        }
    }

    /**
     * @return {@link #sw} convertit en String
     */
    public String getResult() {
        return sw.toString();
    }

    /**
     * Construit les flux
     * <p>
     * @param inputS le flux d'entrée pour {@link #is}
     */
    public void setParameters(InputStream inputS) {
        is = inputS;
        sw = new StringWriter();
    }

    /**
     * Détecte et renvoie le nom de version d'open office
     * <p>
     * @return la version d'open office
     */
    public String getOOVersion() {
        try {
            /*Numéro de la version, stocké sous forme de double afin de
             * permettre des comparaisons facilement
             */
            double version;
            Process process = Runtime.getRuntime().exec(REGQUERY_UTIL + OpenOfficeRoot);
            this.setParameters(process.getInputStream());

            this.lire();
            process.waitFor();
            String result = this.getResult();
            if (result.isEmpty()) {
                process = Runtime.getRuntime().exec(REGQUERY_UTIL + OpenOfficeRootXp64);
                this.setParameters(process.getInputStream());

                this.lire();
                process.waitFor();
                result = this.getResult();
            }
            if (result.isEmpty()) {
                return "";
            }

            /*Curseur permettant de se déplacer dans la chaîne. On le position
             * d'abord au premier "OpenOffice.org" trouvé, car c'est cette
             * ligne qui est la première intéressante
             */
            int p = result.indexOf("\n", result.indexOf("OpenOffice.org"));

            // On extrait ce qui suit le dernier "\" de la ligne
            String versionString = result.substring(result.lastIndexOf("\\", p) + 1, p);

            /*Si le caractère extrait est un saut de ligne, on l'ignore,
             * c'est à dire que l'on déclare son numéro de version à 0
             */
            if (versionString.charAt(0) == 13) {//13 = saut de ligne
                version = 0;
            } else {
                version = Double.parseDouble(versionString);
            }

            //On se place au début de la prochaine ligne
            p = result.indexOf("HKEY", p);

            /*On récupère la dernière version d'OpenOffice installée
             * en parcourant tout la chaîne de caractères
             */
            while (p != -1) {

                /*Pour chaque ligne contenant la chaîne "HKEY", on extrait
                 * la partie de la chaîne contenue entre le dernier "\" et le
                 * caractère LF
                 */
                versionString = result.substring(result.lastIndexOf("\\", result.indexOf("\n", p)) + 1, result.indexOf("\n", p) - 1);

                // De nouveau, si on a extrait une chaîne vide, on l'ignore
                if (versionString.charAt(0) == 13) {
                    versionString = "0";
                }

                /*Si la version trouvée à ce stade est plus récente que la
                 * dernière trouvée, on actualise le numéro de version
                 */
                version = (Double.parseDouble(versionString) > version ? Double.parseDouble(versionString) : version);

                /*Avancement du curseur au prochain début de ligne commençant
                 * par HKEY. S'il n'y en a pas, p prend la valeur -1 et on sort
                 * de la boucle
                 */
                p = result.indexOf("HKEY", p + 1);
            }

            return String.valueOf(version);

        } catch (Exception e) {
            System.out.println("Erreur lors de la récupération du numéro de version d'open office");
            return "";
        }
    }

    /**
     * @return le chemin de l'exécutable d'open office
     */
    public String getOOPath() {
        try {
            //Récupération du numéro de version
            OpenOfficeVersion = getOOVersion();

            // Si OpenOffice n'est pas installé
            if (OpenOfficeVersion.isEmpty()) {
                return OpenOfficeVersion;
            }
            System.out.println("\n***OpenOffice.org v." + OpenOfficeVersion
                    + " détecté");

            try {
                Process process = Runtime.getRuntime().exec(REGQUERY_UTIL + OpenOfficeRoot + OpenOfficeVersion + "\\ /s");
                //On donne à l'objet WindowsLoOo le flux sortant du process
                this.setParameters(process.getInputStream());
                this.lire();
                process.waitFor();
                String result = this.getResult();
                if (result.isEmpty()) {
                    process = Runtime.getRuntime().exec(REGQUERY_UTIL + OpenOfficeRootXp64 + OpenOfficeVersion + "\\ /s");
                    this.setParameters(process.getInputStream());

                    this.lire();
                    process.waitFor();
                }
            } catch (Exception e) {
                System.out.println("Erreur lors de la recherche du chemin "
                        + "d'accès à OpenOffice.org. Essayez de convertir "
                        + "manuellement votre document d'entrée");
            }

            String result = this.getResult();
            //Récupération de la ligne contenant le path :
            result = result.substring(result.indexOf("Path"), result.indexOf("\n", result.indexOf("Path")));
            //Parsage de la ligne : on ne conserve que le chemin d'accès
            result = result.substring(result.indexOf(REGSTR_TOKEN) + REGSTR_TOKEN.length() + (" ").length());

            /*Remplacement des \ par des \\ pour le traitement java correct et
             * suppression des sauts de ligne et retours chariots.
             */
            result = result.replace("\\", "\\\\");
            result = result.replace("\n", "");
            result = result.replace("\r", "");
            return result;

        } catch (Exception e) {
            return "";
        }
    }
}
