package org.natbraille.unused;

import com.sun.star.bridge.XUnoUrlResolver;
import com.sun.star.connection.ConnectionSetupException;
import com.sun.star.connection.NoConnectException;
import com.sun.star.lang.IllegalArgumentException;
import com.sun.star.lang.XMultiServiceFactory;
import com.sun.star.uno.UnoRuntime;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.natbraille.soffice.AutoConfigurator;
import org.natbraille.soffice.SofficeConfig;
import org.natbraille.soffice.server.SofficeDisposableServer;

/**
 * NOT USED.
 * <p>
 * @author vivien
 */
public class myUno {

    public static void main(String argv[]) {

        SofficeConfig loConf = new AutoConfigurator().getSofficeConfig();
        System.out.println(loConf);

        SofficeDisposableServer loServer = null;
        try {
            loServer = new SofficeDisposableServer(loConf);
        } catch (IOException ex) {
            Logger.getLogger(myUno.class.getName()).log(Level.SEVERE, null, ex);
        }
        loServer.start();
        System.out.println(loServer.toString());

        System.out.println("posA");
        XMultiServiceFactory xmultiservicefactory = null;
        try {
            xmultiservicefactory = com.sun.star.comp.helper.Bootstrap.createSimpleServiceManager();
        } catch (Exception ex) {
            Logger.getLogger(myUno.class.getName()).log(Level.SEVERE, null, ex);
        }

        System.out.println("posB");
        Object objectUrlResolver = null;
        try {
            objectUrlResolver = xmultiservicefactory.createInstance(
                    "com.sun.star.bridge.UnoUrlResolver");
        } catch (com.sun.star.uno.Exception ex) {
            Logger.getLogger(myUno.class.getName()).log(Level.SEVERE, null, ex);
        }

        XUnoUrlResolver xurlresolver = (XUnoUrlResolver) UnoRuntime.queryInterface(XUnoUrlResolver.class, objectUrlResolver);

        System.out.println("posC");
        Object objectInitial = null;
        try {
            loServer.getConnectionString();
            System.err.println(loServer.getConnectionString());

            objectInitial = xurlresolver.resolve(
                    "uno:socket,host=" + loServer.getHost() + ",port=" + loServer.getPort() + ";urp;StarOffice.ServiceManager");
            //"uno:socket,host=localhost,port="+loServer.getPort()+";urp;StarOffice.ServiceManager");
        } catch (NoConnectException ex) {
            Logger.getLogger(myUno.class.getName()).log(Level.SEVERE, null, ex);
        } catch (ConnectionSetupException ex) {
            Logger.getLogger(myUno.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IllegalArgumentException ex) {
            Logger.getLogger(myUno.class.getName()).log(Level.SEVERE, null, ex);
        }

        System.out.println("posD");
        xmultiservicefactory = (XMultiServiceFactory) UnoRuntime.queryInterface(XMultiServiceFactory.class, objectInitial);
        System.err.println("yes." + objectInitial);
        System.err.println("yes.");
    }
}
