sudo reset
dbegin=$(date)
echo "stop server ; remove webapp ; reset logs"
sudo service tomcat7 stop
sudo rm -f /var/log/tomcat7/catalina.out 
sudo rm /var/lib/tomcat7/webapps/natbraille-http* -rf 

echo "rebuild"
mvn clean install 
    

echo "restart server"
sudo cp natbraille-http/target/natbraille-http.war /var/lib/tomcat7/webapps/
sudo service tomcat7 start 
echo sudo watch -n1 tail -50 /var/log/tomcat7/catalina.out  

echo "start : $dbegin"
echo "end : $(date)"

